# Supplementary Materials - Journal Version under Review

This directory contains additional data and source code for the extended paper currently in revisions for Journal of Artificial Intelligence Research.

- `instances/`: all instance files used for benchmarking
- `seesaw/`: the implementation of the Seesaw algorithm used for comparison
- `experiment-results/`: runtime experiment results
