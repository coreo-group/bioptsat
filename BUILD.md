# Building the Project

To build the project, do the following steps:

Build the project in subdirectory:

- `mkdir build && cd build`
- `../configure && make`

This will automatically pull necessary submodules and build libraries (e.g., solver backends) the build depends on.

These steps will build the project as one binary in `build/src/bioptsat`

For debugging purposes it might be useful to configure the project with the command `CXXFLAGS="-g -O0 -UNDEBUG"` to have all symbols in the binary and all assertions working.

For development and if the build system is changed, it is useful to set the `--enable-maintainer-mode` so that the build system is automatically reconfigured at build, if required.

When using BiOptSat as a dependency of another project, the generated static library `/build/src/libbioptsat.a` as well as the required libraries of the included solvers should be linked to the project.

## Feature Flags

### Applications

`--disable-dimacs`: Do not include the application for solving files in modified DIMACS format in the build.

`--disable-opb`: Do not include the application for solving files in OPB format in the build.

`--disable-interpdecrules`: Do not include the application for learning interpretable decision rules with the MLIC.

`--disable-interpdectrees`: Do not include the application for learning interpretable decision trees.

`--disable-nonlindecrules`: Do not include the application for learning decision rules with false positives and negatives as the two objectives.

`--disable-nonlindectrees`: Do not include the application for learning decision trees with false positives and negatives as the two objectives.

`--disable-biobjsetcover`: Do not include the application for solving biobjective setcover problems in the build.

`--disable-relaxedsetcover`: Do not include the application for solving relaxed setcover problems in the build.

`--enable-syntheticadvantage`: Include solving synthetically created instances where we guessed BiOptSat would have an advantage over P-minimal enumeration.
The instances are parametrized by parameters `N` and `K`.
The downwards moving objective contains `N` variables, the upwards moving one `K` disjoined ones.
The only constraint is that `sum_{b \in U \cup D} b >= K`.

### Enumerators

`--disable-bioptsat`: Do not include the BiOptSat enumerator in the build.

`--disable-pminimal`: Do not include the P-minimal enumerator in the build.

`--enable-hybrid`: Do not include the hybrid between BiOptSat and P-minimal in the build.

### Backends

`--without-cadical`: Do not include the CaDiCaL solver as a backend.

`--without-maxpre`: Do not include preprocessing with MaxPre 2.1.

### Encodings

`--disable-totalizer`: Disable totalizer cardinality encoding.

`--disable-gte`: Disable generalized totalizer PB encoding.

`--disable-cardpbsim`: Disable PB encoding simulated with cardinality encoding.