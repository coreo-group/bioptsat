#!/usr/bin/python

# Compare two pareto front solution files
# compare-solutions.py <solution1.sol> <solution2.sol>

import sys


def parse_solution_file(path):
    pf = []
    up_name = ''
    down_name = ''

    with open(path, 'r') as log:
        while True:
            line = log.readline()
            if line.startswith('c Pareto point:'):
                toks = [t.strip() for t in line.split(':')[1].split(';')]
                assert(len(toks) == 3)
                up_name = toks[0].split(' = ')[0]
                down_name = toks[1].split(' = ')[0]
                up = int(toks[0].split(' = ')[1])
                down = int(toks[1].split(' = ')[1])
                n = int(toks[2].split(' = ')[1])
                pf.append({'up': up, 'down': down,
                          'n_sols': n, 'models': set()})
            elif line.startswith('s '):
                sol = []
                while True:
                    sol.append(line[2:])
                    line = log.readline()
                    if not line.startswith('s '):
                        break
                pf[-1]['models'].add(' '.join(sol))
            elif not line:
                break

    return pf, up_name, down_name


def compare_solution_files(file1, file2, shape_only=False):
    pf1, up_name1, down_name1 = parse_solution_file(file1)
    pf2, up_name2, down_name2 = parse_solution_file(file2)
    if up_name1 == up_name2:
        assert(down_name1 == down_name2)
    else:
        assert(up_name1 == down_name2)
        assert(down_name1 == up_name2)
        pf2 = [{'up': pp['down'], 'down': pp['up'],
                'n_sols': pp['n_sols'], 'models': pp['models']} for pp in pf2]

    assert(len(pf1) == len(pf2))

    pf1 = sorted(pf1, key=lambda pp: pp['up'])
    pf2 = sorted(pf2, key=lambda pp: pp['up'])
        
    for i in range(len(pf1)):
        assert(pf1[i]['up'] == pf2[i]['up'])
        assert(pf1[i]['down'] == pf2[i]['down'])
        if not shape_only:
            assert(pf1[i]['n_sols'] == pf2[i]['n_sols'])
            assert(pf1[i]['models'] == pf2[i]['models'])

if __name__ == '__main__':
    assert(len(sys.argv) == 3 or len(sys.argv) == 4)
    if len(sys.argv) > 3:
        compare_solution_files(sys.argv[1], sys.argv[2], sys.argv[3] == 'shape_only')
    else:
        compare_solution_files(sys.argv[1], sys.argv[2])
