c Pareto point: False positives = 0; False negatives = 1; #solutions = 2
c ----------
s ( 0 | 1 ) &
s ( 2 )
c ..........
s ( 0 | 1 | 5 ) &
s ( 2 )
c ..........
c Pareto point: False positives = 1; False negatives = 0; #solutions = 6
c ----------
s ( 0 | 1 | 5 ) &
s ( 1 | 2 )
c ..........
s ( 0 | 1 | 5 ) &
s ( 1 | 2 | 3 )
c ..........
s ( 0 | 1 | 5 ) &
s ( 2 | 3 )
c ..........
s ( 0 | 1 ) &
s ( 2 | 3 )
c ..........
s ( 0 | 1 ) &
s ( 1 | 2 | 3 )
c ..........
s ( 0 | 1 ) &
s ( 1 | 2 )
c ..........
