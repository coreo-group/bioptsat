c Pareto point: Element weight 1 = 110; Element weight 2 = 398; #solutions = 1
c ----------
s 5 9 39 49 54 73 78 82
c ..........
c Pareto point: Element weight 1 = 112; Element weight 2 = 384; #solutions = 1
c ----------
s 5 9 10 49 54 73 78 82
c ..........
c Pareto point: Element weight 1 = 117; Element weight 2 = 320; #solutions = 1
c ----------
s 9 22 39 54 73 78 82
c ..........
c Pareto point: Element weight 1 = 119; Element weight 2 = 306; #solutions = 1
c ----------
s 9 10 22 54 73 78 82
c ..........
c Pareto point: Element weight 1 = 151; Element weight 2 = 280; #solutions = 1
c ----------
s 9 22 39 54 69 73 82
c ..........
c Pareto point: Element weight 1 = 182; Element weight 2 = 268; #solutions = 1
c ----------
s 22 51 54 73 78 82
c ..........
c Pareto point: Element weight 1 = 186; Element weight 2 = 252; #solutions = 1
c ----------
s 9 22 39 41 54 82
c ..........
c Pareto point: Element weight 1 = 188; Element weight 2 = 238; #solutions = 1
c ----------
s 9 10 22 41 54 82
c ..........
c Pareto point: Element weight 1 = 216; Element weight 2 = 237; #solutions = 1
c ----------
s 6 10 22 39 73 89 96
c ..........
c Pareto point: Element weight 1 = 218; Element weight 2 = 231; #solutions = 1
c ----------
s 5 10 14 73 78 82 96 98
c ..........
c Pareto point: Element weight 1 = 219; Element weight 2 = 192; #solutions = 1
c ----------
s 9 10 22 41 58 82
c ..........
c Pareto point: Element weight 1 = 235; Element weight 2 = 166; #solutions = 1
c ----------
s 10 19 22 73 78 82 96
c ..........
c Pareto point: Element weight 1 = 250; Element weight 2 = 135; #solutions = 1
c ----------
s 9 10 22 41 96
c ..........
c Pareto point: Element weight 1 = 267; Element weight 2 = 93; #solutions = 1
c ----------
s 10 22 41 82 96
c ..........
