#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/biobj-set-cover/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/biobj-set-cover/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/biobj-set-cover ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Biobjective set cover tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/biobj-set-cover
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

bioptsat_test () {
  app=biobj-set-cover
  enumerator=bioptsat
  mininc_variant=$1
  solver=$2
  msg "running $app $mininc_variant solver $solver test ${HILITE}'$3'${NORMAL}"
  status=0
  instance=$tests/$3.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$3.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-test-$3
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --mininc-variant=$mininc_variant --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

pminimal () {
  app=biobj-set-cover
  enumerator=pminimal
  solver=$1
  msg "running $app pminimal solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

hybrid () {
  app=biobj-set-cover
  enumerator=hybrid
  solver=$1
  msg "running $app hybrid solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

sat_unsat () {
  bioptsat_test sat-unsat $*
}

unsat_sat () {
  bioptsat_test unsat-sat $*
}

msu3 () {
  bioptsat_test msu3 $*
}

msu3_su_hybrid () {
  bioptsat_test msu3-su-hybrid $*
}

oll_su_hybrid () {
  bioptsat_test oll-su-hybrid $*
}

oll () {
  bioptsat_test oll $*
}

run () {
  sat_unsat $*
  unsat_sat $*
  msu3 $*
  msu3_su_hybrid $*
  oll_su_hybrid $*
  oll $*
  pminimal $*
  hybrid $*
}

#--------------------------------------------------------------------------#

run cadical bp-100-20-2-10-910
run cadical bp-100-20-2-10-11430

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat biobjective set-cover testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
