#include "../../src/cadicalsolver.hpp"
#include "../../src/pbencoding.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  // Positive in lits
  BiOptSat::SatSolver *solver = new BiOptSat::CadicalSolver;

  solver->addClause(1, 2);
  solver->addClause(2);
  solver->addClause(2, 3);
  solver->addClause(3, 4);
  solver->addClause(4, 5);
  solver->addClause(5);
  solver->addClause(6);
  solver->addClause(7, 8);
  solver->addClause(8);
  solver->addClause(8, 9);
  solver->addClause(9, 10);
  solver->addClause(10, 11);
  solver->addClause(11);

  BiOptSat::PBEncoding *pbEnc =
      pbEncodingFactory(solver, BiOptSat::BOUND_BOTH, "card-pb-sim");
  BiOptSat::wlits_t wLits{};
  wLits[1] = 1;
  wLits[2] = 2;
  wLits[3] = 1;
  wLits[4] = 3;
  wLits[5] = 2;
  pbEnc->addLits(wLits);

  std::vector<BiOptSat::lit_t> assumps{};
  pbEnc->enforceLB(3, assumps);
  BiOptSat::SolverState res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  assumps.clear();
  pbEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(4, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(5, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  wLits.clear();
  wLits[6] = 4;
  pbEnc->addLits(wLits);
  assumps.clear();
  pbEnc->enforceUB(5, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(9, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  wLits.clear();
  wLits[7] = 1;
  wLits[8] = 2;
  wLits[9] = 1;
  wLits[10] = 3;
  wLits[11] = 2;
  pbEnc->addLits(wLits);
  assumps.clear();
  pbEnc->enforceUB(9, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(14, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  delete pbEnc;
  delete solver;

  // Negative in lits
  solver = new BiOptSat::CadicalSolver;

  solver->addClause(-1, -2);
  solver->addClause(-2);
  solver->addClause(-2, -3);
  solver->addClause(-3, -4);
  solver->addClause(-4, -5);
  solver->addClause(-5);
  solver->addClause(-6);

  pbEnc = pbEncodingFactory(solver, BiOptSat::BOUND_BOTH, "card-pb-sim");
  wLits.clear();
  wLits[-1] = 1;
  wLits[-2] = 2;
  wLits[-3] = 1;
  wLits[-4] = 3;
  wLits[-5] = 2;
  pbEnc->addLits(wLits);

  assumps.clear();
  pbEnc->enforceLB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  assumps.clear();
  pbEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(4, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(5, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  wLits.clear();
  wLits[-6] = 4;
  pbEnc->addLits(wLits);
  assumps.clear();
  pbEnc->enforceUB(5, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  pbEnc->enforceUB(9, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  delete pbEnc;
  delete solver;

  return 0;
}