#include "../../src/encodings.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>
#include <cstdlib>
#include <ctime>

int main() {
  std::srand(std::time(nullptr));

  std::vector<BiOptSat::lit_t> lits = {1, 2, 3, 4, 5, 6};
  BiOptSat::lit_t cond = 7;
  uint32_t nVars = 7;
  std::vector<BiOptSat::clause_t> clauses{};

  BiOptSat::condExactlyOne(nVars, clauses, cond, lits);

  for (uint32_t i = 0; i < 5; i++) {
    BiOptSat::SatSolver *solver = BiOptSat::solverFactory();

    for (BiOptSat::clause_t cl : clauses)
      solver->addClause(cl);

    assert(solver->getNVars() == nVars);

    BiOptSat::SolverState ret = solver->solve();
    assert(ret == BiOptSat::SAT);

    BiOptSat::lit_t a1 = (std::rand() % 6) + 1;
    BiOptSat::lit_t a2 = (((std::rand() % 5) + a1) % 6) + 1;
    assert(a1 != a2);

    std::vector<BiOptSat::lit_t> assumps = {a1};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);

    assumps = {a2};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);

    assumps = {a1, a2};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);

    assumps = {cond, a1};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);

    assumps = {cond, a2};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);

    assumps = {cond, a1, a2};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::UNSAT);

    delete solver;
  }

  return 0;
}