#include "../../src/cadicalsolver.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  BiOptSat::SatSolver *solver = new BiOptSat::CadicalSolver;

  // Encode Problem and check without assumptions.

  enum { TIE = 1, SHIRT = 2 };

  solver->addClause(-TIE, SHIRT);
  solver->addClause(TIE, SHIRT);
  solver->addClause(-TIE, -SHIRT);

  BiOptSat::SolverState res = solver->solve(); // Solve instance
  assert(res == BiOptSat::SAT);                // Check it is satisfiable

  bool tie = solver->litModelValue(TIE); // Obtain assignment of TIE
  assert(!tie);                       // Check TIE assigned to false

  bool shirt = solver->litModelValue(SHIRT); // Obtain assignment of SHIRT
  assert(shirt);                          // Check SHIRT assigned to true

  // Incrementally solve again under one assumption

  BiOptSat::clause_t conf{};
  std::vector<BiOptSat::lit_t> assumps{TIE};
  res = solver->solve(assumps); // Now force TIE to true
  solver->getConflict(conf);
  assert(res == BiOptSat::UNSAT);  // Check it is unsatisfiable

  assert(conf.size() == 1); // Check that conflict is of size 1
  assert(conf[0] == -TIE);  // Check that conflict is {-TIE}

  // Incrementally solve once more under another assumption

  assumps = {-SHIRT};
  res = solver->solve(assumps); // Now force SHIRT to false
  solver->getConflict(conf);
  assert(res == BiOptSat::UNSAT);     // Check it is unsatisfiable

  assert(conf.size() == 1); // Check that conflict is of size 1
  assert(conf[0] == SHIRT); // Check that conflict is {SHIRT}

  delete solver;

  return 0;
}