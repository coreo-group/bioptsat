#include "../../src/totmerger.hpp"
#include "../../src/cadicalsolver.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/totalizer.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  // Merging totalizers without offset
  BiOptSat::SatSolver *solver = new BiOptSat::CadicalSolver;

  solver->addClause(1, 4);
  solver->addClause(2, 4);
  solver->addClause(3, 4);
  solver->addClause(4, 5);
  solver->addClause(5, 6);
  solver->addClause(7, 8);

  BiOptSat::Totalizer *tot1 =
      new BiOptSat::Totalizer(solver, BiOptSat::BOUND_UB);
  tot1->addLits({1, 2, 3});

  BiOptSat::Totalizer *tot2 =
      new BiOptSat::Totalizer(solver, BiOptSat::BOUND_UB);
  tot2->addLits({4, 5, 6});

  BiOptSat::TotMerger *merger = new BiOptSat::TotMerger(solver);
  merger->addTotalizer(tot1, 3);
  merger->addTotalizer(tot2, 5);
  BiOptSat::wlits_t wLits{};
  wLits[7] = 3;
  wLits[8] = 2;
  merger->addLits(wLits);

  std::vector<BiOptSat::lit_t> assumps{};
  merger->enforceUB(10, assumps);
  BiOptSat::SolverState res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  merger->enforceUB(12, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  assert(merger->nextHigherPossible(3) == 5);
  assert(merger->nextLowerPossible(5) == 3);
  assert(merger->nextHigherPossible(6) == 7);
  assert(merger->nextLowerPossible(6) == 5);

  delete merger;
  delete solver;

  return 0;
}