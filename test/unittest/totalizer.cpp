#include "../../src/cadicalsolver.hpp"
#include "../../src/cardencoding.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  // Positive in lits
  BiOptSat::SatSolver *solver = new BiOptSat::CadicalSolver;

  solver->addClause(1, 2);
  solver->addClause(2);
  solver->addClause(2, 3);
  solver->addClause(3, 4);
  solver->addClause(4, 5);
  solver->addClause(5);
  solver->addClause(6);
  solver->addClause(7, 8);
  solver->addClause(8);
  solver->addClause(8, 9);
  solver->addClause(9, 10);
  solver->addClause(10, 11);
  solver->addClause(11);

  BiOptSat::CardEncoding *cardEnc =
      cardEncodingFactory(solver, BiOptSat::BOUND_BOTH, "totalizer");
  std::vector<BiOptSat::lit_t> lits{1, 2, 3, 4, 5};
  cardEnc->addLits(lits);

  std::vector<BiOptSat::lit_t> assumps{};
  cardEnc->enforceLB(1, assumps);
  BiOptSat::SolverState res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  assumps.clear();
  cardEnc->enforceUB(1, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(2, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  lits = {6};
  cardEnc->addLits(lits);
  assumps.clear();
  cardEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(4, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  lits = {7, 8, 9, 10, 11};
  cardEnc->addLits(lits);
  assumps.clear();
  cardEnc->enforceUB(4, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(7, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  delete cardEnc;
  delete solver;

  // Negative in lits
  solver = new BiOptSat::CadicalSolver;

  solver->addClause(-1, -2);
  solver->addClause(-2);
  solver->addClause(-2, -3);
  solver->addClause(-3, -4);
  solver->addClause(-4, -5);
  solver->addClause(-5);
  solver->addClause(-6);

  cardEnc = cardEncodingFactory(solver, BiOptSat::BOUND_BOTH, "totalizer");
  lits = {-1, -2, -3, -4, -5};
  cardEnc->addLits(lits);

  assumps.clear();
  cardEnc->enforceLB(1, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  assumps.clear();
  cardEnc->enforceUB(1, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(2, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  lits = {-6};
  cardEnc->addLits(lits);
  assumps.clear();
  cardEnc->enforceUB(3, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::UNSAT);

  assumps.clear();
  cardEnc->enforceUB(4, assumps);
  res = solver->solve(assumps);
  assert(res == BiOptSat::SAT);

  delete cardEnc;
  delete solver;

  return 0;
}