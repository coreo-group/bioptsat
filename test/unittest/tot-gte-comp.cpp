#include "../../src/cadicalsolver.hpp"
#include "../../src/pbencoding.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  // Positive in lits
  BiOptSat::SatSolver *totSolver = new BiOptSat::CadicalSolver;
  BiOptSat::SatSolver *gteSolver = new BiOptSat::CadicalSolver;

  totSolver->addClause(1, 2);
  totSolver->addClause(2);
  totSolver->addClause(2, 3);
  totSolver->addClause(3, 4);
  totSolver->addClause(4, 5);
  totSolver->addClause(5);
  totSolver->addClause(6);
  totSolver->addClause(7, 8);
  totSolver->addClause(8);
  totSolver->addClause(8, 9);
  totSolver->addClause(9, 10);
  totSolver->addClause(10, 11);
  totSolver->addClause(11);

  gteSolver->addClause(1, 2);
  gteSolver->addClause(2);
  gteSolver->addClause(2, 3);
  gteSolver->addClause(3, 4);
  gteSolver->addClause(4, 5);
  gteSolver->addClause(5);
  gteSolver->addClause(6);
  gteSolver->addClause(7, 8);
  gteSolver->addClause(8);
  gteSolver->addClause(8, 9);
  gteSolver->addClause(9, 10);
  gteSolver->addClause(10, 11);
  gteSolver->addClause(11);

  BiOptSat::PBEncoding *tot =
      pbEncodingFactory(totSolver, BiOptSat::BOUND_UB, "card-pb-sim");
  BiOptSat::PBEncoding *gte =
      pbEncodingFactory(gteSolver, BiOptSat::BOUND_UB, "gte");

  BiOptSat::wlits_t wLits{};
  wLits[1] = 1;
  wLits[2] = 2;
  wLits[3] = 1;
  wLits[4] = 3;
  wLits[5] = 2;

  tot->addLits(wLits);
  gte->addLits(wLits);

  std::vector<BiOptSat::lit_t> totAssumps{};
  tot->enforceUB(5, totAssumps);
  BiOptSat::SolverState totRes = totSolver->solve(totAssumps);
  assert(totRes == BiOptSat::SAT);
  uint64_t totVars = tot->getNVars();
  uint64_t totCls = tot->getNClauses();

  std::vector<BiOptSat::lit_t> gteAssumps{};
  gte->enforceUB(5, gteAssumps);
  BiOptSat::SolverState gteRes = gteSolver->solve(gteAssumps);
  assert(gteRes == BiOptSat::SAT);
  uint64_t gteVars = gte->getNVars();
  uint64_t gteCls = gte->getNClauses();

  assert(totVars >= gteVars);
  assert(totCls >= gteCls);

  delete tot;
  delete gte;
  delete totSolver;
  delete gteSolver;

  return 0;
}