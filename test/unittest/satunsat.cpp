#include "../../src/bioptsat.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>
#include <vector>

int main() {
  std::vector<BiOptSat::clause_t> clauses{};
  clauses.push_back({1, 2});
  clauses.push_back({2, 3});
  clauses.push_back({3, 4});
  BiOptSat::Objective inc({2, 4});
  BiOptSat::Objective dec({1, 3});

  BiOptSat::BiOptSatEnumerator *enumerator =
      new BiOptSat::BiOptSatEnumerator(clauses, inc, dec);
  enumerator->setMinIncVariant(BiOptSat::SAT_UNSAT);

  enumerator->enumerate();

  BiOptSat::ParetoFront<BiOptSat::model_t> pf = enumerator->getParetoFront();

  assert(pf.size() == 3);
  assert(pf[0].models.size() == 1);
  assert(pf[1].models.size() == 1);
  assert(pf[2].models.size() == 1);

  delete enumerator;

  return 0;
}