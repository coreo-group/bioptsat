#include "../../src/encodings.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  uint32_t nVars = 2;
  std::vector<BiOptSat::clause_t> clauses{};

  BiOptSat::lit_t out = BiOptSat::tseitinAnd(nVars, clauses, 1, 2);

  BiOptSat::SatSolver *solver = BiOptSat::solverFactory();

  for (BiOptSat::clause_t cl : clauses)
    solver->addClause(cl);

  assert(solver->getNVars() == nVars);

  BiOptSat::SolverState ret = solver->solve();
  assert(ret == BiOptSat::SAT);

  for (uint32_t i = 0; i < 4; i++) {
    BiOptSat::lit_t a1 = i % 2 ? 1 : -1;
    BiOptSat::lit_t a2 = i >> 2 ? 2 : -2;
    std::vector<BiOptSat::lit_t> assumps = {a1, a2};
    ret = solver->solve(assumps);
    assert(ret == BiOptSat::SAT);
    assert(solver->litModelValue(out) == ((i % 2) && (i >> 2)));
  }

  delete solver;

  return 0;
}