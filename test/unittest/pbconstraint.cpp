#include "../../src/pbconstraint.hpp"
#include "../../src/cadicalsolver.hpp"
#include "../../src/satsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  // Positive in lits

  // Case 1: Always SAT (rhs < 0)
  BiOptSat::SatSolver *solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc1("2 x1 +1 x2 >= -2;");
  assert(pbc1.addToSolver(solver) == BiOptSat::PBCE_NONE);
  std::vector<BiOptSat::lit_t> assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  delete solver;

  // Case 2: Always UNSAT (rhs < 0)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc2("2 x1 +1 x2 <= -2;");
  assert(pbc2.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 3: Always UNSAT (rhs < 0)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc3("2 x1 +1 x2 = -2;");
  assert(pbc3.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 4: Always UNSAT (rhs > sum)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc4("2 x1 +1 x2 >= 4;");
  assert(pbc4.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 5: Always SAT (rhs > sum)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc5("2 x1 +1 x2 <= 4;");
  assert(pbc5.addToSolver(solver) == BiOptSat::PBCE_NONE);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  delete solver;

  // Case 6: Clause (rhs < min)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc6("2 x1 +3 x2 >= 1;");
  assert(pbc6.addToSolver(solver) == BiOptSat::PBCE_CLAUSE);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 7: Units (rhs < min)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc7("2 x1 +3 x2 <= 1;");
  assert(pbc7.addToSolver(solver) == BiOptSat::PBCE_UNITS);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  delete solver;

  // Case 8: Always UNSAT (rhs < min)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc8("2 x1 +3 x2 = 1;");
  assert(pbc8.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 9: Units (rhs = sum)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc9("2 x1 +3 x2 >= 5;");
  assert(pbc9.addToSolver(solver) == BiOptSat::PBCE_UNITS);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 10: Clause (rhs = min)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc10("2 x1 +3 x2 >= 2;");
  assert(pbc10.addToSolver(solver) == BiOptSat::PBCE_CLAUSE);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 11: (Custom) card enc (rhs = min)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc11("2 x1 +3 x2 <= 2;");
  assert(pbc11.addToSolver(solver) == BiOptSat::PBCE_CARD);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  delete solver;

  // Case 12: Card enc (min = max)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc12("2 x1 +2 x2 = 2;");
  assert(pbc12.addToSolver(solver) == BiOptSat::PBCE_CARD);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 13: PB enc
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc13("2 x1 +1 x2 >= 2;");
  assert(pbc13.addToSolver(solver) == BiOptSat::PBCE_PBC);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 14: Always UNSAT (unreachable sum)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc14("2 x1 +2 x2 = 3;");
  assert(pbc14.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 15: Always UNSAT (unreachable sum)
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc15("6 x1 +4 x2 = 5;");
  assert(pbc15.addToSolver(solver) == BiOptSat::PBCE_UNSAT);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  // Case 16: Equality constraint
  solver = new BiOptSat::CadicalSolver;
  for (BiOptSat::var_t v = solver->getNVars(); v < 2; v = solver->getNewVar())
    ;
  BiOptSat::PBConstraint pbc16("6 x1 +4 x2 = 6;");
  assert(pbc16.addToSolver(solver) == BiOptSat::PBCE_PBC);
  assumps = {1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {1, -2};
  assert(solver->solve(assumps) == BiOptSat::SAT);
  assumps = {-1, 2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  assumps = {-1, -2};
  assert(solver->solve(assumps) == BiOptSat::UNSAT);
  delete solver;

  return 0;
}