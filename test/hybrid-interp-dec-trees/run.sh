#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/hybrid-interp-dec-trees/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/hybrid-interp-dec-trees/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/hybrid-interp-dec-trees ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Interpretable decision trees tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/hybrid-interp-dec-trees
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  app=interp-dec-trees
  enumerator=hybrid
  increasing=$1
  solver=cadical
  msg "running $app ($increasing) solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$increasing-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --interp-clf-increasing=$increasing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

run_no_pmin_bound () {
  app=interp-dec-trees
  enumerator=hybrid
  increasing=$1
  solver=cadical
  msg "running $app ($increasing) (no-pmin-bound) solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$increasing-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --no-pmin-bound --no-preprocessing --application=$app --interp-clf-increasing=$increasing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

run size example
run error example
run size iris
run error iris
run size parkinsons
run error parkinsons
run_no_pmin_bound size example
run_no_pmin_bound error example
run_no_pmin_bound size iris
run_no_pmin_bound error iris
run_no_pmin_bound size parkinsons
run_no_pmin_bound error parkinsons

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}Hybrid interp-dec-trees testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
