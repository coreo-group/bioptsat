#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/interp-dec-trees/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/interp-dec-trees/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/interp-dec-trees ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Interpretable decision trees tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/interp-dec-trees
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

exec_test () {
  app=interp-dec-trees
  enumerator=bioptsat
  mininc_variant=$1
  increasing=$2
  solver=$3
  msg "running $app $mininc_variant ($increasing) solver $solver test ${HILITE}'$4'${NORMAL}"
  status=0
  instance=$tests/$4.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$4.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-$increasing-test-$4
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --mininc-variant=$mininc_variant --interp-clf-increasing=$increasing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

sat_unsat_size () {
  exec_test sat-unsat size $*
}

sat_unsat_error () {
  exec_test sat-unsat error $*
}

unsat_sat_size () {
  exec_test unsat-sat size $*
}

unsat_sat_error () {
  exec_test unsat-sat error $*
}

msu3_size () {
  exec_test msu3 size $*
}

msu3_error () {
  exec_test msu3 error $*
}

oll_size () {
  exec_test oll size $*
}

oll_error () {
  exec_test oll error $*
}

msh_size () {
  exec_test msu3-su-hybrid size $*
}

msh_error () {
  exec_test msu3-su-hybrid error $*
}

run () {
  sat_unsat_size $*
  sat_unsat_error $*
  unsat_sat_size $*
  unsat_sat_error $*
  msu3_size $*
  msu3_error $*
  oll_size $*
  oll_error $*
  msh_size $*
  msh_error $*
}

#--------------------------------------------------------------------------#

run cadical example
run cadical ilpd
run cadical iris
run cadical parkinsons

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat interp-dec-trees testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
