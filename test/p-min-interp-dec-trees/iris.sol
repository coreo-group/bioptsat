c Pareto point: Tree size = 3; Classification error = 6; #solutions = 1
c ----------
s 0:(I,9,1,2)
s 1:(L,0); 2:(L,1)
c ..........
c Pareto point: Tree size = 5; Classification error = 5; #solutions = 2
c ----------
s 0:(I,9,1,2)
s 1:(L,0); 2:(I,2,3,4)
s 3:(L,1); 4:(L,0)
c ..........
s 0:(I,2,1,2)
s 1:(I,9,3,4); 2:(L,0)
s 3:(L,0); 4:(L,1)
c ..........
