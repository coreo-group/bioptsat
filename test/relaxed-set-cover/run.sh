#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/relaxed-set-cover/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/relaxed-set-cover/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/relaxed-set-cover ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Relaxed set cover tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/relaxed-set-cover
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

bioptsat_test () {
  app=relaxed-set-cover
  enumerator=bioptsat
  mininc_variant=$1
  inc_obj=$2
  solver=$3
  msg "running $app ($inc_obj) $mininc_variant solver $solver test ${HILITE}'$4'${NORMAL}"
  status=0
  instance=$tests/$4.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$4.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-$inc_obj-test-$4
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --mininc-variant=$mininc_variant --relaxed-set-cover-increasing=$inc_obj --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

pminimal () {
  app=relaxed-set-cover
  enumerator=pminimal
  solver=$1
  msg "running $app pminimal solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

hybrid () {
  app=relaxed-set-cover
  enumerator=hybrid
  solver=$1
  msg "running $app hybrid solver $solver test ${HILITE}'$2'${NORMAL}"
  status=0
  instance=$tests/$2.dat
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$2.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-test-$2
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

sat_unsat () {
  bioptsat_test sat-unsat elements $*
  bioptsat_test sat-unsat relaxation $*
}

unsat_sat () {
  bioptsat_test unsat-sat elements $*
  bioptsat_test unsat-sat relaxation $*
}

msu3 () {
  bioptsat_test msu3 elements $*
  bioptsat_test msu3 relaxation $*
}

msu3_su_hybrid () {
  bioptsat_test msu3-su-hybrid elements $*
  bioptsat_test msu3-su-hybrid relaxation $*
}

oll () {
  bioptsat_test oll elements $*
  bioptsat_test oll relaxation $*
}

run () {
  sat_unsat $*
  unsat_sat $*
  msu3 $*
  msu3_su_hybrid $*
  oll $*
  pminimal $*
  hybrid $*
}

#--------------------------------------------------------------------------#

run cadical bp-100-20-2-10-910
run cadical bp-100-20-2-10-11430

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat relaxed set-cover testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
