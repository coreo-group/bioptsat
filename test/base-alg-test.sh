#!/bin/sh

TEST_NAME=$ENUMERATOR-$VARIANT

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/${TEST_NAME}.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/${TEST_NAME}.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/test-insts ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "${TEST_NAME} tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/test-insts
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  enumerator=$ENUMERATOR
  mininc_variant=$VARIANT
  app=$1
  enum=$2
  prepro=$3
  instance=$4
  remaining=${@:5}
  msg "running $app $mininc_variant (${enum}, ${prepro}) test ${HILITE}'$instance'${NORMAL}"
  status=0
  if ! [ -f $tests/$instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$instance-$app.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$enumerator-$mininc_variant-$instance-$app-$enum-$prepro-test
  if [ $prepro == 'prepro' ]
  then
    prepro='--preprocessing'
  else
    prepro='--no-preprocessing'
  fi
  if [ $enum == 'enum' ]
  then
    enum='--max-sols-per-pp=-1'
    shape_only='no_shape_only'
  else
    enum='--max-sols-per-pp=1'
    shape_only='shape_only'
  fi
  if [ $mininc_variant ]
  then
    mininc_variant="--mininc-variant=$mininc_variant"
  fi
  cmd $tool --enumerator=$enumerator --application=$app $prepro $enum $mininc_variant $remaining --output-path=$name --dump-pareto-front $tests/$instance
  cmd python compare-solutions.py $solution $name.sol $shape_only
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

run_prepro_and_not() {
  app=$1
  instance=$2
  if ! [ $SHORT ]
  then
    run $app enum noprepro $instance ${@:3}
  fi
  run $app noenum prepro $instance ${@:3}
}

#--------------------------------------------------------------------------#

run_prepro_and_not interp-dec-rules example.csv
run_prepro_and_not interp-dec-rules ilpd.csv
run_prepro_and_not interp-dec-rules iris.csv
run_prepro_and_not interp-dec-trees example.csv
run_prepro_and_not interp-dec-trees iris.csv
run_prepro_and_not non-lin-dec-rules example.csv
run_prepro_and_not non-lin-dec-rules iris.csv
run_prepro_and_not non-lin-dec-trees example.csv
run_prepro_and_not non-lin-dec-trees iris.csv
run_prepro_and_not biobj-set-cover fixed-element-prob-100-20-0.1-1.dat
run_prepro_and_not relaxed-set-cover fixed-element-prob-100-20-0.1-1.dat
run opb noenum prepro f1-DataDisplay_0_order4.seq-A-2-1-abcdeir.opb
if ! [ $SHORT ]
then
  run_prepro_and_not interp-dec-rules parkinsons.csv
  run_prepro_and_not interp-dec-trees ilpd.csv
  run_prepro_and_not interp-dec-trees parkinsons.csv
  run_prepro_and_not non-lin-dec-rules ilpd.csv
  run_prepro_and_not non-lin-dec-trees parkinsons.csv
  run_prepro_and_not biobj-set-cover fixed-set-card-100-20-5-1.dat
  run_prepro_and_not relaxed-set-cover fixed-set-card-100-20-5-1.dat
fi

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat ${TEST_NAME} testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
