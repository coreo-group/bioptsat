#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/dimacs/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/dimacs/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/dimacs ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Dimacs tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

makefile=$PSBUILD/Makefile

CXX=`grep '^CXX =' "$makefile"|sed -e 's,CXX = ,,'`
CXXFLAGS=`grep '^CXXFLAGS =' "$makefile"|sed -e 's,CXXFLAGS = ,,'`

msg "using CXX=$CXX"
msg "using CXXFLAGS=$CXXFLAGS"

tests=../test/dimacs

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  msg "running dimacs test ${HILITE}'$1'${NORMAL}"
  name=$PSBUILD/dimacs-test-$1
  status=0
  instance=$tests/$1.mcnf
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  src=$tests/main.cpp
  rm -f $name.log $name.o
  cmd $CXX $CXXFLAGS -o $name.o -c $src -I../lib/cadical/src -I../lib/maxpre/src -I../lib/packup
  cmd $CXX $CXXFLAGS -o $name $name.o -L$PSBUILD/src -lbioptsat -L../lib/cadical/build -lcadical -L../lib/maxpre/src/lib -lmaxpre -L../lib/packup -lpackup
  cmd $name $instance $2
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

run small 3
run medium 6

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat dimacs testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
