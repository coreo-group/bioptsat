#include "../../src/application.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>
#include <vector>

int main(int argc, char *argv[]) {
  BiOptSat::Application *app = BiOptSat::appFactory("dimacs");

  assert(argc == 3);

  // Get expected number of pps from cli
  char *end;
  uint32_t pfLen = strtol(argv[2], &end, 10);

  app->loadInstance(argv[1]);
  app->solve();
  BiOptSat::ParetoFront<BiOptSat::model_t> pf = app->getParetoFront();

  assert(pf.size() == pfLen);

  return 0;
}