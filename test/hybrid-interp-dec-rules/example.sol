c Pareto point: Rule size = 0; Classification error = 3; #solutions = 1
c ----------
s ( ) &
s ( )
c ..........
c Pareto point: Rule size = 2; Classification error = 2; #solutions = 5
c ----------
s ( 2 ) &
s ( 2 )
c ..........
s ( 1 ) &
s ( 1 )
c ..........
s ( 1 ) &
s ( 3 )
c ..........
s ( 1 ) &
s ( 2 )
c ..........
s ( 0 ) &
s ( 2 )
c ..........
c Pareto point: Rule size = 3; Classification error = 1; #solutions = 1
c ----------
s ( 0 | 1 ) &
s ( 2 )
c ..........
