#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/non-lin-dec-trees/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/non-lin-dec-trees/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/non-lin-dec-trees ] || \
die "needs to be called from a top-level sub-directory of BiOptSat"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libbioptsat.a" ] || \
  die "can not find '$PSBUILD/src/libbioptsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Non-linear decision trees tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/non-lin-dec-trees
tool=$PSBUILD/src/bioptsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

exec_test () {
  app=non-lin-dec-trees
  enumerator=bioptsat
  mininc_variant=$1
  increasing=$2
  solver=$3
  msg "running $app $mininc_variant ($increasing) solver $solver test ${HILITE}'$4'${NORMAL}"
  status=0
  instance=$tests/$4.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$4.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$mininc_variant-$increasing-test-$4
  cmd $tool --backend=$solver --enumerator=$enumerator --application=$app --no-preprocessing --mininc-variant=$mininc_variant --non-lin-clf-increasing=$increasing --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

sat_unsat_fp () {
  exec_test sat-unsat false-pos $*
}

sat_unsat_fn () {
  exec_test sat-unsat false-neg $*
}

unsat_sat_fp () {
  exec_test unsat-sat false-pos $*
}

unsat_sat_fn () {
  exec_test unsat-sat false-neg $*
}

msu3_fp () {
  exec_test msu3 false-pos $*
}

msu3_fn () {
  exec_test msu3 false-neg $*
}

oll_fp () {
  exec_test oll false-pos $*
}

oll_fn () {
  exec_test oll false-neg $*
}

msh_fp () {
  exec_test msu3-su-hybrid false-pos $*
}

msh_fn () {
  exec_test msu3-su-hybrid false-neg $*
}

run () {
  sat_unsat_fp $*
  sat_unsat_fn $*
  unsat_sat_fp $*
  unsat_sat_fn $*
  msu3_fp $*
  msu3_fn $*
  oll_fp $*
  oll_fn $*
  msh_fp $*
  msh_fn $*
}

#--------------------------------------------------------------------------#

run cadical example
run cadical iris
run cadical parkinsons

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}BiOptSat non-lin-dec-trees testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
