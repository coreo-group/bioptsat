# Read a bicnf formatted bi-objective optimization problem and convert it to a wcnf format
# The single objective problem is created with the weighted sum method to find a lexicographically optimal solution


import sys
import os.path
localpath = os.path.abspath(os.path.dirname(__file__))
if localpath not in sys.path:
    sys.path.insert(0, localpath)
from bicnf import parse_bicnf  # nopep8


def compute_weighted_sum(objectives):
    """convert a multi-objective instance to a weighted sum single-objective instance

    the objectives are in order of decreasing priority

    objectives: tuple of multiple objectives as list of tuples containing weight and literal
    returns: single objective as list of tuples containing weight and literal"""
    multiplier = 1
    objective = []
    for i in range(len(objectives)-1, -1, -1):
        objective.extend((multiplier * w, l) for w, l in objectives[i])
        multiplier = sum(w for w, _ in objective) + 1
    return objective


def write_wcnf(outfile, objective, clauses, comments, wcnf_version='2022'):
    """write an instance to a WCNF file

    outfile: the path to write to as a string or a python file object
    objective: objective as list of tuples containing weight and literal
    clauses: list of clauses as lists of literals
    comments: list of comments to put at the top of file
    wcnf_version: either '2022' or 'pre-2022' (formats differ in p line and if they have a top)"""
    lines = ['c This file is a weighted sum instance converted from a bicnf with the bicnf2weightedsum script\n']

    for c in comments:
        lines.append('c {}\n'.format(c))

    top = 'h'
    if wcnf_version != '2022':
        top = sum(w for w, _ in objective) + 1
        nvars = max((max(abs(l) for _, l in objective), max(abs(l)
                    for c in clauses for l in c)))
        lines.append('p wcnf {} {} {}\n'.format(
            nvars, len(clauses) + len(objective), top))

    for w, l in objective:
        lines.append('{} {} 0\n'.format(w, -l))

    for c in clauses:
        lines.append('{} {} 0\n'.format(top, ' '.join(str(l) for l in c)))

    if isinstance(outfile, str):
        with open(outfile, 'w') as inst:
            inst.writelines(lines)
    else:
        outfile.writelines(lines)


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description='Convert a bicnf to a wcnf for lexicographic optimization '
                    'with the weighted sum method')
    parser.add_argument('infile', type=argparse.FileType('r'),
                        help='the bicnf file to read')
    parser.add_argument('outfile', type=argparse.FileType('w'),
                        help='the wcnf file to write to')
    parser.add_argument('--primary', type=int, default=1, choices=(1, 2),
                        help='which objective to treat as the primary one')
    parser.add_argument('--wcnf-version', default='2022', choices=('2022', 'pre-2022'),
                        help='the format to use for the output file')

    args = parser.parse_args()

    inst = parse_bicnf(args.infile)

    # Swap objectives if second is primary
    if args.primary == 2:
        inst = (inst[0][::-1], *inst[1:])

    weighted_sum_obj = compute_weighted_sum(inst[0])

    comments = ['converted from {} with primary objective {}'.format(
        args.infile.name, args.primary)] + inst[2]

    write_wcnf(args.outfile, weighted_sum_obj, inst[1], comments,
               wcnf_version=args.wcnf_version)


if __name__ == '__main__':
    main()
