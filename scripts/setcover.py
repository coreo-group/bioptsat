# Parse a set-cover instance
# Author: Christoph Jabs - christoph.jabs@helsinki.fi

# Usage: either as a python script, which will print the
# parsed instance or the function parse_set_cover


def parse_set_cover(instance_path):
    """parse a set-cover instance

    instance_path: the path to a instance file as a string"""
    instance = {'elements': [], 'sets': [], 'objective weights': []}

    with open(instance_path, 'r') as f:
        line = f.readline().strip()
        toks = [int(t) for t in line.split(' ')]
        assert(len(toks) == 2)
        n_elem = toks[0]
        n_sets = toks[1]
        instance['elements'] = list(range(1, n_elem+1))

        line = f.readline().strip()
        n_obj = int(line)

        for _ in range(n_obj):
            line = f.readline().strip()
            weights = [int(t) for t in line.split(' ')]
            assert(len(weights) == n_elem)
            instance['objective weights'].append(weights)

        while True:
            line1 = f.readline().strip()
            line2 = f.readline().strip()
            if not line1:
                break
            set_len = int(line1)
            set_elem = [int(t) for t in line2.split(' ')]
            assert(len(set_elem) == set_len)
            instance['sets'].append(set_elem)

        assert(len(instance['sets']) == n_sets)

    return instance


def export_set_cover(instance_path, instance):
    """export a set-cover instance

    instance_path: the path to export to
    instance: the instance as a dict"""
    n_elem = len(instance['elements'])
    n_sets = len(instance['sets'])
    n_obj = len(instance['objective weights'])

    lines = ['{} {}\n'.format(n_elem, n_sets), '{}\n'.format(n_obj)]
    lines.extend([' '.join([str(w) for w in weights])+'\n'
                 for weights in instance['objective weights']])

    for s in instance['sets']:
        lines.append('{}\n'.format(len(s)))
        lines.append(' '.join([str(e) for e in s])+'\n')

    with open(instance_path, 'w') as f:
        f.writelines(lines)


def generate_set_cover_fixed_card(instance_path, n_elem, n_sets, set_card, n_obj=2,
                                  max_weight=100, seed=42):
    """generate a set-cover instance

    instance_path: the path to export to
    n_elem: the number of elements to create
    n_sets: the number of sets to create
    set_card: the cardinality of the sets to create
    n_obj: the number of objectives to create
    max_weight: the maximum weight in the objective weights"""
    instance = {'elements': [], 'sets': [], 'objective weights': []}
    instance['elements'] = list(range(1, n_elem+1))

    from numpy.random import default_rng
    rng = default_rng(seed=seed)

    for _ in range(n_obj):
        instance['objective weights'].append(
            list(rng.integers(low=1, high=max_weight, size=n_elem, endpoint=True)))

    for _ in range(n_sets):
        instance['sets'].append(list(rng.permutation(n_elem)[:set_card]+1))

    export_set_cover(instance_path, instance)


def generate_set_cover_fixed_prob(instance_path, n_elem, n_sets, elem_prob, n_obj=2,
                                  max_weight=100, seed=42):
    """generate a set-cover instance

    instance_path: the path to export to
    n_elem: the number of elements to create
    n_sets: the number of sets to create
    elem_prob: the probability of an element being in a set
    n_obj: the number of objectives to create
    max_weight: the maximum weight in the objective weights"""
    instance = {'elements': [], 'sets': [], 'objective weights': []}
    instance['elements'] = list(range(1, n_elem+1))

    from numpy.random import default_rng
    rng = default_rng(seed=seed)

    for _ in range(n_obj):
        instance['objective weights'].append(
            list(rng.integers(low=1, high=max_weight, size=n_elem, endpoint=True)))

    for _ in range(n_sets):
        s = []
        for i in range(n_elem):
            if rng.random() < elem_prob:
                s.append(i+1)
        instance['sets'].append(s)

    export_set_cover(instance_path, instance)


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('command', choices=['parse', 'generate'])
    parser.add_argument('path',
                        help='the path to the instance to parse or generate')
    parser.add_argument('--elements', type=int, default=100,
                        help='the number of elements in the instance to generate')
    parser.add_argument('--sets', type=int, default=20,
                        help='the number of sets in the instance to generate')
    parser.add_argument('--instance-type', default='fixed-set-card',
                        choices=['fixed-set-card', 'fixed-element-prob'],
                        help='wether to generate instances with a fixed set cardinality '
                        'or with a fixed probability of a element being in a set')
    parser.add_argument('--set-card', type=int, default=5,
                        help='the cardinality of the sets to generate')
    parser.add_argument('--element-prob', type=float, default=.1,
                        help='the probability of a element being in a set')
    parser.add_argument('--objectives', type=int, default=2,
                        help='the number of objectives in the instance')
    parser.add_argument('--max-weight', type=int, default=100,
                        help='the maximum weight for an element')
    parser.add_argument('--seed', type=int, default=42,
                        help='the random seed for instance generation')
    args = parser.parse_args()

    from pprint import pprint

    if args.command == 'parse':
        set_cover = parse_set_cover(args.path)

        pprint(set_cover)

    elif args.command == 'generate':
        if args.instance_type == 'fixed-set-card':
            generate_set_cover_fixed_card(
                args.path, n_elem=args.elements, n_sets=args.sets,
                set_card=args.set_card, n_obj=args.objectives,
                max_weight=args.max_weight, seed=args.seed)
        else:
            generate_set_cover_fixed_prob(
                args.path, n_elem=args.elements, n_sets=args.sets,
                elem_prob=args.element_prob, n_obj=args.objectives,
                max_weight=args.max_weight, seed=args.seed)


if __name__ == '__main__':
    main()
