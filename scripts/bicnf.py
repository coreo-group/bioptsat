# Helper functions for dealing with bicnf files in python


def parse_bicnf(infile):
    """parse a instance in bicnf format

    infile: the path to an input file in bicnf format as a string or a python file object
    returns: tuple of two objectives as list of tuples containing weight and literal
        list of clauses as lists of literals
        list of comments"""
    objectives = ([], [])
    clauses = []
    comments = []
    if isinstance(infile, str):
        infile = open(infile, 'r')

    while True:
        line = infile.readline()
        line = line.strip()

        if line.startswith('c'):
            comments.append(line[2:])
            continue
        elif not line:
            break

        toks = line.split()
        assert (toks[-1] == '0')

        if line.startswith('1'):
            objectives[0].append((int(toks[1]), int(toks[2])))
        elif line.startswith('2'):
            objectives[1].append((int(toks[1]), int(toks[2])))
        elif line.startswith('h'):
            clauses.append([int(l) for l in toks[1:-1]])

    infile.close()

    return objectives, clauses, comments


def remove_independent_hards(objectives, clauses, print_progress=True):
    """remove hard clauses that are independent of the objectives

    objectives: the first return parameter of parse_bicnf
    clauses: the second return parameter of parse_bicnf
    print_progress: whether to print the progress to stdout
    returns: a reduced list of clauses"""
    svars = set(abs(l) for (_, l) in objectives[0])
    svars.update(abs(l) for (_, l) in objectives[1])
    reduced_clauses = []
    its = 0
    while True:
        its += 1
        print('Iteration #{:04d}, #vars={:04d}, #necessary clauses={:05d}, #remaining clauses={:05d}'.format(
            its, len(svars), len(reduced_clauses), len(clauses)))
        selected_idx = []
        for (idx, cl) in enumerate(clauses):
            for l in cl:
                if abs(l) in svars:
                    selected_idx.append(idx)
                    reduced_clauses.append(cl)
                    svars.update(abs(lit) for lit in cl)
                    break
        if not selected_idx:
            break
        for idx in selected_idx[::-1]:
            clauses.pop(idx)
    return reduced_clauses


def reindex_instance(objectives, clauses):
    """reindex the instance to have no gaps in indexing after simplification

    objectives: the first return parameter of parse_bicnf
    clauses: the second return parameter of parse_bicnf
    returns: reindexed objectives and clauses"""
    o2n = dict()
    next_var = 1
    new_objectives = ([], [])
    new_clauses = []
    for (w, l) in objectives[0]:
        if abs(l) not in o2n:
            o2n[abs(l)] = next_var
            next_var += 1
        new_objectives[0].append((w, o2n[abs(l)] if l > 0 else -o2n[abs(l)]))
    for (w, l) in objectives[1]:
        if abs(l) not in o2n:
            o2n[abs(l)] = next_var
            next_var += 1
        new_objectives[1].append((w, o2n[abs(l)] if l > 0 else -o2n[abs(l)]))
    for cl in clauses:
        new_clause = []
        for l in cl:
            if abs(l) not in o2n:
                o2n[abs(l)] = next_var
                next_var += 1
            new_clause.append(o2n[abs(l)] if l > 0 else -o2n[abs(l)])
        new_clauses.append(new_clause)
    return new_objectives, new_clauses


def save_bicnf(outfile, instance):
    """saves an instance to a file

    outfile: the path to an input file in bicnf format as a string or a python file object
    instance: an instances as returned by parse_bicnf"""
    lines = ['c {}\n'.format(c) for c in instance[2]]
    lines.extend(['h {}\n'.format(s)
                 for s in ['{} 0'.format(' '.join([str(l) for l in cl])) for cl in instance[1]]])
    lines.extend(['1 {} {} 0\n'.format(w, l) for (w, l) in instance[0][0]])
    lines.extend(['2 {} {} 0\n'.format(w, l) for (w, l) in instance[0][1]])

    if isinstance(outfile, str):
        outfile = open(outfile, 'w')

    outfile.writelines(lines)
    outfile.close()


def main():
    from sys import argv
    from os.path import isfile

    script_usage_string = \
        """bicnf.py [stats/simplify] [input bicnf file]

    input bicnf file: a input file in bicnf format to print stats for"""

    if (len(argv) != 3) or not isfile(argv[2]):
        print(script_usage_string)
        return

    inst = parse_bicnf(argv[2])

    if argv[1] == 'stats':
        for c in inst[2]:
            print(c)
        print('Objective 1 length: {}'.format(len(inst[0][0])))
        print('Objective 1 weight sum: {}'.format(
            sum(w for w, _ in inst[0][0])))
        print('Objective 2 length: {}'.format(len(inst[0][1])))
        print('Objective 2 weight sum: {}'.format(
            sum(w for w, _ in inst[0][1])))
        print('# Clauses: {}'.format(len(inst[1])))
        print('Avg. Clause length: {}'.format(sum(len(c)
              for c in inst[1]) / len(inst[1])))
    elif argv[1] == 'simplify':
        (objectives, clauses, comments) = inst
        clauses = remove_independent_hards(objectives, clauses)
        (objectives, clauses) = reindex_instance(objectives, clauses)
        save_bicnf(argv[2] + '.simpl', (objectives, clauses, comments))


if __name__ == '__main__':
    main()
