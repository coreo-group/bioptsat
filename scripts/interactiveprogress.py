# Parse an experiment log and generate an interactive progress plot
# Author: Christoph Jabs - christoph.jabs@helsinki.fi

# Usage: as a python script

from parselog import parse_experiment_log

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


script_usage_string = \
    """interactiveprogress.py [log file]

log file: a log file produced by BiOptSat with encoding state and search trace logging"""


def parse_log(log_path, log_id_string='c BiOptSat Program', trace_scaler=(lambda x: x, lambda x: x)):
    """parse a log file of BiOptSat

    log_path: the path to a log file as a string
    log_id_string: the first line of the log to check if the log is valid"""
    exp_log = parse_experiment_log(
        log_path, skip_models=True, log_id_string=log_id_string)
    pf = [(pp['inc'], pp['dec']) for pp in exp_log['paretofront']]

    search_trace = pd.DataFrame()
    solver_traces = dict()
    pb_enc_traces = dict()
    card_enc_traces = dict()

    with open(log_path, 'r') as log:
        line = log.readline()
        line = line.strip()

        if line != log_id_string:
            raise ValueError('Log file not of expected format')

        search_trace_start = 'c <SearchTrace>;'
        pb_enc_start = 'c <PBEncoding>'
        card_enc_start = 'c <CardEncoding>'
        solver_start = 'c <SatSolver>'

        while True:
            line = log.readline()
            line = line.strip()

            if line.startswith(search_trace_start):
                search_trace = pd.concat(
                    [search_trace, pd.DataFrame.from_records(
                        [parse_search_trace(line, trace_scaler)],
                        index=[search_trace.shape[0]+1])])
            elif line.startswith(pb_enc_start):
                id, info = parse_pb_enc(line)
                if id not in pb_enc_traces:
                    pb_enc_traces[id] = pd.DataFrame()
                pb_enc_traces[id] = pd.concat(
                    [pb_enc_traces[id],
                     pd.DataFrame.from_records([info], index=[pb_enc_traces[id].shape[0]+1])])
            elif line.startswith(card_enc_start):
                id, info = parse_card_enc(line)
                if id not in card_enc_traces:
                    card_enc_traces[id] = pd.DataFrame()
                card_enc_traces[id] = pd.concat(
                    [card_enc_traces[id],
                     pd.DataFrame.from_records([info], index=[card_enc_traces[id].shape[0]+1])])
            elif line.startswith(solver_start):
                id, info = parse_solver(line)
                if id not in solver_traces:
                    solver_traces[id] = pd.DataFrame()
                solver_traces[id] = pd.concat(
                    [solver_traces[id],
                     pd.DataFrame.from_records([info], index=[solver_traces[id].shape[0]+1])])
            elif not line:
                break

        return search_trace, solver_traces, pb_enc_traces, card_enc_traces, pf


def parse_search_trace(line, trace_scaler):
    """parse a search trace line"""
    elms = line.split('; ')
    info = parse_sep_vals('; '.join(elms[2:]))
    info['sat'] = elms[1] == '[SAT]'
    info['inc'] = trace_scaler[0](info['inc'])
    info['dec'] = trace_scaler[1](info['dec'])
    return info


def parse_pb_enc(line):
    """parse a PB encoding state line"""
    elms = line.split('; ')
    id = elms[0][15:-1]
    info = parse_sep_vals('; '.join(elms[1:]))
    return id, info


def parse_card_enc(line):
    """parse a card encoding state line"""
    elms = line.split('; ')
    id = elms[0][17:-1]
    info = parse_sep_vals('; '.join(elms[1:]))
    return id, info


def parse_solver(line):
    """parse a solver state line"""
    elms = line.split('; ')
    id = elms[0][14:-1]
    info = parse_sep_vals('; '.join(elms[1:]))
    return id, info


def parse_sep_vals(line):
    """parse a string of key value pairs separated by '; '"""
    elms = line.split('; ')
    info = dict()
    for e in elms:
        toks = [tok.strip() for tok in e.split('=')]
        info[toks[0]] = convert(toks[1])
    return info


def convert(tok):
    """convert a string to int, float or string"""
    if tok.endswith('s'):
        return convert(tok[:-1])
    try:
        return int(tok)
    except ValueError:
        try:
            return float(tok)
        except ValueError:
            try:
                # Values with unit
                return float(tok[:-1])
            except ValueError:
                return tok


def generate_interactive_progress(search_trace, solver_traces, pb_enc_traces, card_enc_traces, pareto_front, title=None):
    """generate a interactive progress plot in the browser"""
    fig = go.Figure()
    # Search trace vlines
    for _, r in search_trace.iterrows():
        if (r['inc'], r['dec']) in pareto_front:
            fig.add_vline(x=r['absolute cpu time'], line_width=3,
                          line_dash='dash', line_color='green')
        elif r['sat']:
            fig.add_vline(x=r['absolute cpu time'], line_width=1,
                          line_dash='dot', line_color='orange')
        else:
            fig.add_vline(x=r['absolute cpu time'], line_width=1,
                          line_dash='dot', line_color='yellow')
    # Solver traces
    for id, trace in solver_traces.items():
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#solves'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] #solves'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#satSolves'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] #satSolves'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#unsatSolves'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] #unsatSolves'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#clauses'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] #clauses'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#vars'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] #vars'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['cpu solve time'], mode='lines', line=dict(color='blue'),
                                 name='SatSolver[{}] cpu solve time'.format(id)))
    # PB enc traces
    for id, trace in pb_enc_traces.items():
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#clauses'], mode='lines', line=dict(color='purple'),
                                 name='PBEncoding[{}] #clauses'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#vars'], mode='lines', line=dict(color='purple'),
                                 name='PBEncoding[{}] #vars'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#lits'], mode='lines', line=dict(color='purple'),
                                 name='PBEncoding[{}] #lits'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['weight sum'], mode='lines', line=dict(color='purple'),
                                 name='PBEncoding[{}] weight sum'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['upper'], mode='lines', line=dict(color='purple'),
                                 name='PBEncoding[{}] upper'.format(id)))
    # Card enc traces
    for id, trace in card_enc_traces.items():
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#clauses'], mode='lines', line=dict(color='teal'),
                                 name='CardEncoding[{}] #clauses'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#vars'], mode='lines', line=dict(color='teal'),
                                 name='CardEncoding[{}] #vars'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['#lits'], mode='lines', line=dict(color='teal'),
                                 name='CardEncoding[{}] #lits'.format(id)))
        fig.add_trace(go.Scatter(x=trace['absolute cpu time'],
                                 y=trace['upper'], mode='lines', line=dict(color='teal'),
                                 name='CardEncoding[{}] upper'.format(id)))
    if title:
        fig.update_layout(title_text=title)

    return fig


if __name__ == '__main__':
    from sys import argv
    from os.path import isfile
    from pathlib import Path

    if not (len(argv) >= 2 and len(argv) <= 3) or (not isfile(argv[1])):
        print(script_usage_string)

    filename = Path(argv[1]).stem

    if len(argv) == 2:
        res = parse_log(argv[1])
    else:
        res = parse_log(argv[1], log_id_string=argv[2])

    generate_interactive_progress(*res, title=filename).show()
