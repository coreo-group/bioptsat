import numpy as np
import pandas as pd


class DecisionTree:
    """A decision tree class for testing"""

    def __init__(self, specifier):
        """Read in a string in BiOptSat DT format and create a DT"""
        lines = specifier.split('\n')
        lines = [l[2:] if l.startswith('s ') else l for l in lines]
        nodes = [n.strip() for n in sum([l.split(';') for l in lines], [])]
        self.nodes = []
        for i, n in enumerate(nodes):
            toks1 = n.split(':')
            assert(i == int(toks1[0]))
            toks2 = toks1[1].strip()[1:-1].split(',')
            if toks2[0] == 'L':
                self.nodes.append({'label node': True, 'label': int(toks2[1])})
            elif toks2[0] == 'I':
                self.nodes.append({'label node': False, 'feature': int(
                    toks2[1]), 'left': int(toks2[2]), 'right': int(toks2[3])})
            else:
                raise ValueError

    def predict(self, sample):
        """Predict a single sample and return prediction"""
        node = 0
        while not self.nodes[node]['label node']:
            feature = sample[self.nodes[node]['feature']]
            if feature:
                node = self.nodes[node]['right']
            else:
                node = self.nodes[node]['left']
        return self.nodes[node]['label']

    def predict_dataset(self, ds):
        """Predict a full dataset and return predictions"""
        y = np.zeros(shape=ds.shape[0], dtype=int)
        for i in range(ds.shape[0]):
            y[i] = self.predict(ds[i,:])
        return y

    def evaluate_dataset(self, ds_path, sep=';'):
        """Read a dataset from a path, evaluate it and print summary statistics"""
        ds = pd.read_csv(ds_path, sep=sep)
        x = ds.values[:,:-1]
        y = ds.values[:,-1]

        y_pred  = self.predict_dataset(x)

        print('Number of samples: {}'.format(x.shape[0]))
        print('Number of features: {}'.format(x.shape[1]))
        print('Prediction accuracy: {}'.format((y == y_pred).mean()))
        print('Classification errors: {}'.format((y != y_pred).sum()))
        print('False positives: {}'.format((y[y == 0] != y_pred[y == 0]).sum()))
        print('False negatives: {}'.format((y[y == 1] != y_pred[y == 1]).sum()))
