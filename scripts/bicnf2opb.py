# Read a bicnf formatted bi-objective optimization problem and convert it to a modified OPB format
# This script is used to convert instances encoded by BiOptSat to instances readable by the ParetoMCS implementation
# https://gitlab.ow2.org/sat4j/moco


import sys
import os.path
localpath = os.path.abspath(os.path.dirname(__file__))
if localpath not in sys.path:
    sys.path.insert(0, localpath)
from bicnf import parse_bicnf  # nopep8


def write_opb(output_path, objectives, clauses, comments):
    """write an instance to a OPB file

    output_path: the path to write to as a string
    objectives: tuple of two objectives as list of tuples containing weight and literal
    clauses: list of clauses as lists of literals
    comments: list of comments to put at the top of file"""
    lines = ['* This file was converted from bicnf with the bicnf2opb script\n']

    for c in comments:
        lines.append('* {}\n'.format(c))

    for o in objectives:
        lines.append('min: {};\n'.format(
            ' '.join(['{} {}'.format(w, opb_format_lit(l)) for w, l in o])))

    for c in clauses:
        lines.append('{} >= 1;\n'.format(
            ' '.join(['1 {}'.format(opb_format_lit(l)) for l in c])))

    with open(output_path, 'w') as inst:
        inst.writelines(lines)


def opb_format_lit(lit):
    if lit > 0:
        return 'x{}'.format(lit)
    else:
        return '~x{}'.format(-lit)


def main():
    from sys import argv
    from os.path import isfile

    script_usage_string = \
        """bicnf2opb.py [input bicnf file] [output path]

    input bicnf file: a input file in bicnf format to convert
    output path: path to write the OPB instance to"""

    if (len(sys.argv) != 3) or not os.path.isfile(sys.argv[1]):
        print(script_usage_string)
        return

    inst = parse_bicnf(argv[1])

    write_opb(argv[2], *inst)


if __name__ == '__main__':
    main()
