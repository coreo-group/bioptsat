#!/usr/bin/bash
# This script assumes the working directory to be the cadical source dir
if [ -f "makefile" ]; then
  make
else
  ./configure && make -j
fi
