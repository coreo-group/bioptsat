#!/usr/bin/bash

OUT_PATH=$1
N_ELEMENTS=(100 150 200)
N_SETS=(20 40 60 80)
SEED=(1 2 3 4 5)
INST_TYPE=('fixed-set-card' 'fixed-element-prob')
SET_CARD=(5 10)
ELEMENT_PROB=(0.1 0.2)

for elem in ${N_ELEMENTS[@]}
do
  for sets in ${N_SETS[@]}
  do
    for type in ${INST_TYPE[@]}
    do
      for seed in ${SEED[@]}
      do
        if [ $type = 'fixed-set-card' ]
        then
          for card in ${SET_CARD[@]}
          do
            file=$OUT_PATH/$type-$elem-$sets-$card-$seed.dat
            python setcover.py generate $file --elements $elem --sets $sets --instance-type $type --set-card $card --seed $seed
            echo $file >> $OUT_PATH/$type.inst
          done
        else
          for prob in ${ELEMENT_PROB[@]}
          do
            file=$OUT_PATH/$type-$elem-$sets-$prob-$seed.dat
            python setcover.py generate $file --elements $elem --sets $sets --instance-type $type --element-prob $prob --seed $seed
            echo $file >> $OUT_PATH/$type.inst
          done
        fi
      done
    done
  done
done