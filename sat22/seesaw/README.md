# Seesaw Algorithm

An implementation of the Seesaw algorithm as a comparison to BiOptSat.
This implementation is _not_ general and only applicable to certain applications.
Furthermore, it is intertwined with the BiOptSat implementation and not standing alone.

Seesaw paper:

Mikolas Janota and Antonio Morgado and Jose Fragoso Santos and Vasco M. Manquinho (2021): _The Seesaw Algorithm: Function Optimization Using Implicit Hitting Sets_, Schloss Dagstuhl - Leibniz-Zentrum für Informatik.
