#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/interp-dec-rules/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/interp-dec-rules/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/interp-dec-rules ] || \
die "needs to be called from a top-level sub-directory of SeeSaw"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libseesawsat.a" ] || \
  die "can not find '$PSBUILD/src/libseesawsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Interpretable decision rules tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/interp-dec-rules
tool=$PSBUILD/src/seesawsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  app=interp-dec-rules
  solver=$1
  core_ext=$2
  msg "running $app solver $solver test ${HILITE}'$3'${NORMAL}"
  status=0
  instance=$tests/$3.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$3.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$minup_variant-$upwards-test-$3
  cmd $tool --backend=$solver --application=$app --core-extraction=$core_ext --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol 1
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

run cadical seesaw-improved example
run cadical-noninc seesaw-improved example
run cadical seesaw-improved iris
run cadical-noninc seesaw-improved iris
run cadical sat-cores example
run cadical sat-cores iris

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}Interp-dec-rules testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
