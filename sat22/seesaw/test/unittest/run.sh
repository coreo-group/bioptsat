#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/unittest/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/unittest/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/unittest ] || \
die "needs to be called from a top-level sub-directory of SeeSaw"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libseesawsat.a" ] || \
  die "can not find '$PSBUILD/src/libseesawsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Unittests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

makefile=$PSBUILD/Makefile

CXX=`grep '^CXX =' "$makefile"|sed -e 's,CXX = ,,'`
CXXFLAGS=`grep '^CXXFLAGS =' "$makefile"|sed -e 's,CXXFLAGS = ,,'`

msg "using CXX=$CXX"
msg "using CXXFLAGS=$CXXFLAGS"

tests=../test/unittest

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  msg "running unittest ${HILITE}'$1'${NORMAL}"
  if [ -f $tests/$1.cpp ]
  then
    src=$tests/$1.cpp
    COMPILE="$CXX $CXXFLAGS"
  else
    die "cannot find '$tests.cpp'"
  fi
  name=$PSBUILD/unittest-$1
  rm -f $name.log $name.o $name
  status=0
  cmd $COMPILE -o $name.o -c $src -I$CPLEXINCDIR
  cmd $COMPILE -o $name $name.o $PSBUILD/src/libseesawsat.a $CPLEXLIBDIR/libcplex.a -lpthread -ldl
  cmd $name
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

run cplexhs

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}Unittest results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
