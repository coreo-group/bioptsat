#include "../../src/cplex.hpp"
#include "../../src/hittingsetsolver.hpp"
#include "../../src/types.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

int main() {
  SeeSaw::HittingSetSolver *solver = new SeeSaw::CplexHittingSetSolver;

  // Define problem
  BiOptSat::clause_t c1 = {0, 1, 2, 3};
  BiOptSat::clause_t c2 = {1, 2};
  BiOptSat::clause_t c3 = {3, 4};
  BiOptSat::clause_t c4 = {4, 5};
  BiOptSat::clause_t c5 = {};

  solver->init();

  solver->addCore(c1);
  solver->addCore(c2);

  std::vector<BiOptSat::lit_t> solution{};
  double val = solver->solve(solution);

  assert(val == 1);
  assert(solution.size() == val);
  assert(solution[0] == 1 || solution[0] == 2);

  solver->addCore(c3);
  solver->addCore(c4);

  val = solver->solve(solution);

  assert(val == 2);
  assert(solution.size() == val);
  assert(solution[0] == 1 || solution[0] == 2);
  assert(solution[1] == 4);

  solver->addCore(c5);

  val = solver->solve(solution);

  assert(val == -1);

  delete solver;

  return 0;
}