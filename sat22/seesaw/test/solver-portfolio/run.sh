#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  cecho "${HIDE}test/solver-portfolio/run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  cecho "${HIDE}test/solver-portfolio/run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../test -a -d ../test/solver-portfolio ] || \
die "needs to be called from a top-level sub-directory of SeeSaw"

[ x"$PSBUILD" = x ] && PSBUILD="../build"

[ -f "$PSBUILD/Makefile" ] || \
  die "can not find '$PSBUILD/Makefile' (run 'configure' first)"

[ -f "$PSBUILD/src/libseesawsat.a" ] || \
  die "can not find '$PSBUILD/src/libseesawsat.a' (run 'make' first)"

cecho -n "$HILITE"
cecho "---------------------------------------------------------"
cecho "Solver portfolio tests in '$PSBUILD'"
cecho "---------------------------------------------------------"
cecho -n "$NORMAL"

make -C $PSBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

tests=../test/solver-portfolio
tool=$PSBUILD/src/seesawsat

export PSBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  cecho $*
  $* >> $name.log
  status=$?
}

run () {
  app=solver-portfolio
  msg "running $app test ${HILITE}'$1'${NORMAL}"
  status=0
  instance=$tests/$1.csv
  if ! [ -f $instance ]
  then
    die "cannot find $instance"
  fi
  solution=$tests/$1.sol
  if ! [ -f $solution ]
  then
    die "cannot find $solution"
  fi
  name=$PSBUILD/$app-$minup_variant-$upwards-test-$1
  cmd $tool --application=$app --output-path=$name --dump-pareto-front $instance
  cmd python compare-solutions.py $solution $name.sol
  if test $status = 0
  then
    cecho "# 0 ... ${GOOD}ok${NORMAL} (zero exit code)"
    ok=`expr $ok + 1`
  else
    cecho "# 0 ... ${BAD}failed${NORMAL} (non-zero exit code)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

run small
run rule-portfolio
run tree-portfolio

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}Interp-dec-rules testing results:${NORMAL} ${OK}$ok ok${NORMAL}, ${FAILED}$failed failed${NORMAL}"

exit $failed
