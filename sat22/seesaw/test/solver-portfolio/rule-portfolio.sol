c Pareto point: Portfolio size = 0; PAR-2 score = 4294966924; #solutions = 1
c ----------
s {  }
c ..........
c Pareto point: Portfolio size = 1; PAR-2 score = 1738091; #solutions = 1
c ----------
s { idr-msu3-size }
c ..........
c Pareto point: Portfolio size = 2; PAR-2 score = 1730118; #solutions = 1
c ----------
s { idr-msu3-size, idr-oll-error }
c ..........
c Pareto point: Portfolio size = 3; PAR-2 score = 1727060; #solutions = 1
c ----------
s { idr-msu3-error, idr-msu3-size, idr-oll-error }
c ..........
c Pareto point: Portfolio size = 4; PAR-2 score = 1725944; #solutions = 1
c ----------
s { idr-msu3-error, idr-msu3-size, idr-oll-error, idr-su-size }
c ..........
c Pareto point: Portfolio size = 5; PAR-2 score = 1725661; #solutions = 1
c ----------
s { idr-msu3-error, idr-msu3-size, idr-oll-error, idr-su-size, idr-us-error }
c ..........
c Pareto point: Portfolio size = 6; PAR-2 score = 1725566; #solutions = 1
c ----------
s { idr-msu3-error, idr-msu3-size, idr-oll-error, idr-su-size, idr-us-error, idr-us-size }
c ..........
c Pareto point: Portfolio size = 7; PAR-2 score = 1725542; #solutions = 1
c ----------
s { idr-msu3-error, idr-msu3-size, idr-oll-error, idr-oll-size, idr-su-size, idr-us-error, idr-us-size }
c ..........
