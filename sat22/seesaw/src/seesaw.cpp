/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "seesaw.hpp"

#include <cassert>

#include "globaloptions.hpp"
#include "logging.hpp"
#include "utils.hpp"

using namespace BiOptSat;

namespace SeeSaw {

// Options
static string cat = "2.2 - SeeSaw";
static IntOption opt_max_sols(
    cat, "max-sols",
    "Maximum number of solutions in total to enumerate (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption
    opt_max_pps(cat, "max-pps",
                "Maximum number of pareto points to find (-1 for no limit)", -1,
                IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_max_downwards(
    cat, "max-downwards",
    "Maximum value for the downwards objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_min_downwards(
    cat, "min-downwards",
    "Minimum value for the downwards objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));

void defaultCoreExtFunc(const Objective &universe,
                        const std::vector<lit_t> &hittingSet, uint32_t hsOracle,
                        OracleFunc oracle, std::vector<clause_t> &cores) {
  // The weakest possible core extraction implementation that always works
  // universe \setminus hittingSet
  clause_t core{};
  setdiff(universe, hittingSet, core);
  cores.clear();
  cores.push_back(core);
}

void antiMonCoreExtFunc(const Objective &universe,
                        const std::vector<lit_t> &hittingSet, uint32_t hsOracle,
                        OracleFunc oracle, std::vector<clause_t> &cores) {
  // A core extraction function only valid for anti-monotonic oracles
  std::vector<lit_t> sortedHS = hittingSet;
  std::sort(sortedHS.begin(), sortedHS.end());
  uint32_t i = 0;
  for (lit_t l : universe) {
    // Skip variables already in the HS
    if (i < hittingSet.size() && l == sortedHS[i]) {
      i++;
      continue;
    }
    // Check if oracle gets better
    std::vector<lit_t> testHS = sortedHS;
    testHS.push_back(l);
    if (oracle(universe, testHS, hsOracle) >= hsOracle)
      sortedHS.push_back(l);
  }
  clause_t core{};
  setdiff(universe, sortedHS, core);
  cores.clear();
  cores.push_back(core);
}

SeeSawEnumerator::SeeSawEnumerator(const Objective &universe, OracleFunc oracle,
                                   std::string backend)
    : universe(universe), oracle(oracle), verbose(opt_verbose),
      maxSols(opt_max_sols), maxPPs(opt_max_pps),
      maxDownwards(opt_max_downwards), minDownwards(opt_min_downwards) {
  hsSolver = hittingSetSolverFactory(backend);
  hsSolver->init();
}

SeeSawEnumerator::SeeSawEnumerator(const Objective &universe,
                                   std::unordered_map<lit_t, uint32_t> &weights,
                                   OracleFunc oracle, std::string backend)
    : universe(universe), oracle(oracle), verbose(opt_verbose),
      maxSols(opt_max_sols), maxPPs(opt_max_pps),
      maxDownwards(opt_max_downwards), minDownwards(opt_min_downwards) {
  hsSolver = hittingSetSolverFactory(backend, weights);
  hsSolver->init();
}

SeeSawEnumerator::~SeeSawEnumerator() {
  if (hsSolver)
    delete hsSolver;
}

// Timer functions
void SeeSawEnumerator::startTimer() {
  realStartTime = absolute_real_time();
  cpuStartTime = absolute_process_time();
}

void SeeSawEnumerator::stopTimer() {
  double realTime = absolute_real_time() - realStartTime;
  double cpuTime = absolute_process_time() - cpuStartTime;

  totalRealTime += realTime;
  totalCpuTime += cpuTime;

  switch (phase) {
  case PHASE_HS:
    hsRealTime += realTime;
    hsCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Hitting set phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PHASE_ORACLE:
    oracleRealTime += realTime;
    oracleCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Oracle phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PHASE_EXCORE:
    exCoreRealTime += realTime;
    exCoreCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Core extraction phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PHASE_NONE:
    break;
  }
  if (verbose >= 2) {
    INFO << "Total times: real time=" << totalRealTime
         << "; cpu time=" << totalCpuTime << "\n";
    INFO << "Hitting set phase times: real time=" << hsRealTime
         << "; cpu time=" << hsCpuTime << "\n";
    INFO << "Oracle phase times: real time=" << oracleRealTime
         << "; cpu time=" << oracleCpuTime << "\n";
    INFO << "Core extraction phase times: real time=" << exCoreRealTime
         << "; cpu time=" << exCoreCpuTime << "\n";
  }
}

void SeeSawEnumerator::enumerate() {
  std::vector<std::vector<lit_t>> bestHSs{};
  uint32_t bestG{std::numeric_limits<int32_t>::max()};
  uint32_t bestF{maxDownwards >= 0 ? maxDownwards + 1
                                   : std::numeric_limits<uint32_t>::max() - 1};
  std::vector<lit_t> hs{};
  int32_t g{};
  uint32_t f{};

  state = SOLVING;

  while (true) {
    // Solve hitting set problem
    phase = PHASE_HS;
    startTimer();
    g = hsSolver->solve(hs);
    stopTimer();
    if (g == -1) {
      if (maxDownwards != -1 && bestF <= static_cast<uint32_t>(maxDownwards)) {
        paretoFront.push_back((ParetoPoint<std::vector<lit_t>>){
            .models = bestHSs, .incVal = bestG, .decVal = bestF});
        nSols += bestHSs.size();
        if (verbose >= 1)
          INFO << "New Pareto Point found; up = " << bestG
               << "; down = " << bestF
               << "; absolute wall time = " << absolute_real_time()
               << "s; absolute cpu time = " << absolute_process_time() << "s\n";
      }
      if (verbose >= 1)
        INFO << "Terminating algorithm due to infeasible hitting set problem\n";
      state = SOLVED;
      break;
    }
    assert(g >= static_cast<int32_t>(bestG) ||
           bestG == std::numeric_limits<int32_t>::max());
    if (g > static_cast<int32_t>(bestG)) {
      if (paretoFront.size() == 0 || bestF < paretoFront.back().decVal) {
        if (maxDownwards == -1 ||
            bestF <= static_cast<uint32_t>(maxDownwards)) {
          paretoFront.push_back((ParetoPoint<std::vector<lit_t>>){
              .models = bestHSs, .incVal = bestG, .decVal = bestF});
          nSols += bestHSs.size();
          if (verbose >= 1)
            INFO << "New Pareto Point found; up = " << bestG
                 << "; down = " << bestF
                 << "; absolute wall time = " << absolute_real_time()
                 << "s; absolute cpu time = " << absolute_process_time()
                 << "s\n";
          if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
            if (verbose >= 1)
              INFO << "Terminating algorithm due to max solutions reached\n";
            state = BOUND_REACHED;
            return;
          }
          if (maxPPs != -1 &&
              paretoFront.size() >= static_cast<uint32_t>(maxPPs)) {
            if (verbose >= 1)
              INFO
                  << "Terminating algorithm due to max pareto points reached\n";
            state = BOUND_REACHED;
            return;
          }
          if (minDownwards != -1 && paretoFront.back().decVal <=
                                        static_cast<uint32_t>(minDownwards)) {
            if (verbose >= 1)
              INFO << "Terminating algorithm due to min downwards reached\n";
            state = BOUND_REACHED;
            return;
          }
          bestF = paretoFront.back().decVal;
        }
      }
    }
    // Check oracle
    phase = PHASE_ORACLE;
    startTimer();
    f = oracle(universe, hs, bestF);
    stopTimer();
    if (f < bestF) {
      bestHSs.clear();
      bestHSs.push_back(hs);
      bestF = f;
      assert(g >= 0);
      bestG = static_cast<uint32_t>(g);
    } else if (f == bestF) {
      bestHSs.push_back(hs);
    }
    nCandidates++;
    // Extract core
    std::vector<clause_t> cores{};
    phase = PHASE_EXCORE;
    startTimer();
    coreExt(universe, hs, f, oracle, cores);
    stopTimer();
    for (clause_t cr : cores) {
      avgCoreSize = (avgCoreSize * hsSolver->getNCores() + cr.size()) /
                    (hsSolver->getNCores() + 1);
      hsSolver->addCore(cr);
    }
  }
}

void SeeSawEnumerator::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "SeeSaw Enumerator Stats\n";
  INFO << LOG_H1;

  std::string st{};
  switch (state) {
  case INIT:
    st = "INIT";
    break;
  case SOLVING:
    st = "SOLVING";
    break;
  case SOLVED:
    st = "SOLVED";
    break;
  case BOUND_REACHED:
    st = "BOUND_REACHED";
    break;
  }

  INFO << "\n";
  INFO << "state = " << st << "\n";
  INFO << "#solutions = " << nSols << "\n";
  INFO << "#candidates = " << nCandidates << "\n";
  INFO << "universe size = " << universe.size() << "\n";
  INFO << "#cores = " << getNCores() << "\n";
  INFO << "avg core size = " << avgCoreSize << "\n";
  INFO << "#HS solves = " << getNHsSolves() << "\n";

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Total\n";
  INFO << LOG_H3;
  INFO << "Total wall clock time: " << totalRealTime << "s\n";
  INFO << "Total Cpu time: " << totalCpuTime << "s\n";
  INFO << "\n";
  INFO << "Hitting set solving\n";
  INFO << LOG_H3;
  INFO << "Hitting set solving wall clock time: " << hsRealTime << "s\n";
  INFO << "Hitting set solving Cpu time: " << hsCpuTime << "s\n";
  INFO << "\n";
  INFO << "Oracle\n";
  INFO << LOG_H3;
  INFO << "Oracle wall clock time: " << oracleRealTime << "s\n";
  INFO << "Oracle Cpu time: " << oracleCpuTime << "s\n";
  INFO << "\n";
  INFO << "Core extraction\n";
  INFO << LOG_H3;
  INFO << "Core extraction wall clock time: " << exCoreRealTime << "s\n";
  INFO << "Core extraction Cpu time: " << exCoreCpuTime << "s\n";

  hsSolver->printStats();
}

void SeeSawEnumerator::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "SeeSaw Enumerator Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
  INFO << "maxSols = " << maxSols << "\n";
  INFO << "maxPPs = " << maxPPs << "\n";
}
} // namespace SeeSaw