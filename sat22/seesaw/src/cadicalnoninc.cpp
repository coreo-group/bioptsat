/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "cadicalnoninc.hpp"

#include <cassert>

#include "globaloptions.hpp"
#include "logging.hpp"

namespace BiOptSat {
SolverState CadicalNonIncSolver::solve(const std::vector<lit_t> &assumps) {
  if (opt_verbose >= 2)
    INFO << "Solving with non-incremental CaDiCaL backend\n";
  if (opt_verbose >= 3)
    INFO << "Solver stats: #solves=" << nSolves + 1 << "; #clauses=" << nClauses
         << "#vars=" << nVars << "\n";

  if (backend) {
    delete backend;
    backend = nullptr;
  }
  backend = new CaDiCaL::Solver();

  for (clause_t cl : clauses) {
    for (lit_t l : cl)
      backend->add(l);
    backend->add(0);
  }

  for (lit_t l : phases)
    backend->phase(l);

  for (lit_t l : assumps)
    backend->assume(l);

  startTimer();
  int ret = backend->solve();
  stopTimer();
  nSolves++;

  switch (ret) {
  case 10:
    if (opt_verbose >= 3)
      INFO << "Query is satisfiable\n";
    nSatSolves++;
    return SAT;
  case 20:
    if (opt_verbose >= 3)
      INFO << "Query is unsatisfiable\n";
    conflict.clear();
    for (lit_t l : assumps)
      if (backend->failed(l))
        conflict.push_back(-l);
    nUnsatSolves++;
    return UNSAT;
  default:
    if (opt_verbose >= 3)
      INFO << "Query terminated\n";
    return UNDEF;
  }
}
} // namespace BiOptSat