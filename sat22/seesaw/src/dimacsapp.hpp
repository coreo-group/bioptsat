/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _dimacsapp_hpp_INCLUDED
#define _dimacsapp_hpp_INCLUDED

#include <cassert>
#include <limits>
#include <unordered_map>

#include "application.hpp"
#include "hash.hpp"
#include "satsolver.hpp"
#include "totalizer.hpp"
#include "types.hpp"

namespace SeeSaw {

// Application of SeeSaw to learning interpretable decision rules
class DimacsApp : public Application {
protected:
  BiOptSat::SatSolver *satSolver{};
  BiOptSat::Totalizer *tot{};

  uint32_t nVars{};
  uint32_t nClauses{};

  BiOptSat::Objective decreasing{};
  BiOptSat::var_t minIncreasing = std::numeric_limits<uint32_t>::max();
  BiOptSat::var_t maxIncreasing{};

  bool oldBicnf{};
  
  size_t incObjIdx{};
  size_t decObjIdx{};

  // Saving found models for hitting sets
  std::unordered_map<std::vector<BiOptSat::lit_t>, BiOptSat::model_t>
      hittingSetModelMap{};

  void formatSolution(const std::vector<BiOptSat::lit_t> &,
                      std::vector<std::string> &) const;

  uint32_t oracle(const BiOptSat::Objective &,
                  const std::vector<BiOptSat::lit_t> &, uint32_t);
  void coreExt(const BiOptSat::Objective &,
               const std::vector<BiOptSat::lit_t> &, uint32_t, OracleFunc,
               std::vector<BiOptSat::clause_t> &);

  uint32_t getCurrentIncreasing();
  uint32_t getCurrentDecreasing();

public:
  DimacsApp();
  void loadInstance(std::string);
  virtual void printStats() const;
  virtual void printOptions() const;

  // Stats getter
  uint32_t getNClauses() const { return nClauses; }
  uint32_t getNVars() const { return nVars; }

  // Option getter and setter (don't change options after encoding)
  ConfMinVariant getConfMinVariant() const { return coreMinVariant; }
  void setConfMinVariant(ConfMinVariant cmv) { coreMinVariant = cmv; }
};
} // namespace SeeSaw

#endif