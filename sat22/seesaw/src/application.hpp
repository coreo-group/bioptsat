/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _application_hpp_INCLUDED
#define _application_hpp_INCLUDED

#include <unordered_map>

#include "satsolver.hpp"
#include "seesaw.hpp"
#include "types.hpp"

namespace SeeSaw {

enum ConfMinVariant { CM_NONE, CM_EXACT, CM_HEURISTIC };

// Abstract base class for applications to SeeSaw
class Application {
protected:
  SeeSawEnumerator *enumerator{}; // The enumerator algorithm used
  BiOptSat::Objective universe{}; // The universe of the problem. Note: this is
                                  // assumed to be sorted
  std::string upName =
      "Increasing objective"; // The name of the upwards objective. Can be
                              // initialized differently by inheriting class.
  std::string downName =
      "Decreasing objective"; // The name of the upwards objective. Can be
                              // initialized differently by inheriting class.

  bool loaded{}; // Wether a instance is loaded and encoded
  bool solved{}; // Wether the instance is solved
  std::string instance =
      ""; // A descriptor of the instance, typically a file path

  std::unordered_map<BiOptSat::lit_t, uint32_t>
      weights{}; // Weights for the universe elements

  void finalizeLoading(); // Finalize loading procedure. Amongst
                          // other stuff, generates enumerator
  virtual void formatSolution(const std::vector<BiOptSat::lit_t> &,
                              std::vector<std::string> &)
      const; // Format a given solution to lines as strings. Used for printing
             // to stdout and files.

  void minimizeCore(BiOptSat::SatSolver *,
                    BiOptSat::clause_t &); // Minimize a core

  // Options
  uint32_t verbose{};       // The verbosity level of output
  bool doPrintStats{};      // Wether or not to print statistics
  std::string outputPath{}; // The output path to dump to if applicable
  bool doDumpParetoFront{}; // Wether or not to dump the found pareto front to a
                            // file
  ConfMinVariant coreMinVariant{}; // Variant for minimizing conflicts

  // SeeSaw subroutines
  virtual uint32_t oracle(const BiOptSat::Objective &,
                          const std::vector<BiOptSat::lit_t> &,
                          uint32_t) = 0; // The oracle function for SeeSaw
  virtual void coreExt(const BiOptSat::Objective &uni,
                       const std::vector<BiOptSat::lit_t> &hs, uint32_t f,
                       OracleFunc o,
                       std::vector<BiOptSat::clause_t>
                           &crs) { // The core extraction subroutine for SeeSaw
    return defaultCoreExtFunc(uni, hs, f, o, crs);
  }

public:
  Application();
  virtual ~Application() {
    if (enumerator)
      delete enumerator;
  }
  virtual void loadInstance(
      std::string) = 0; // To be implemented by concrete application.
                        // Initialize an enumerator, load an instance from a
                        // file and encode it and add clauses to enumerator.
  void solve();         // Solve the instance
  void
  printSolution(const std::vector<BiOptSat::lit_t> &) const; // Print a solution
  virtual void printParetoFront() const; // Print the found pareto front
  virtual uint32_t scaleUp(uint32_t up)
      const { // Function applied to the upwards objective before printing
    return up;
  }
  virtual uint32_t scaleDown(uint32_t down)
      const { // Function applied to the upwards objective before printing
    return down;
  }
  virtual void
  printStats() const; // Print stats of the application and the enumerator
  virtual void
  printOptions() const; // Print options of the application and the enumerator
  inline void interrupt() {
    if (enumerator)
      enumerator->interrupt();
  }
  virtual BiOptSat::ParetoFront<std::vector<BiOptSat::lit_t>>
  getParetoFront() const {
    return enumerator->getParetoFront();
  }
  void dumpParetoFront() const; // Dump the found pareto front to .sol a file

  // Options getter and setter
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
  bool hasPrintStats() const { return doPrintStats; }
  void setPrintStats(bool ps) { doPrintStats = ps; }
  std::string getOutputPath() const { return outputPath; }
  void setOutputPath(std::string s) { outputPath = s; }
  bool hasDumpParetoFront() const { return doDumpParetoFront; }
  void setDumpParetoFront(bool dpf) { doDumpParetoFront = dpf; }
};

// Application factory to generate fitting application class from string
Application *appFactory(std::string id = ""); // Empty string for default
} // namespace SeeSaw

#endif
