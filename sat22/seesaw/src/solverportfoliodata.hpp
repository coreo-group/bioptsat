/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _solverportfoliodata_hpp_INCLUDED
#define _solverportfoliodata_hpp_INCLUDED

#include <cassert>
#include <cstdint>
#include <iterator>
#include <vector>

namespace SeeSaw {
class SolverPortfolioData {
protected:
  std::vector<std::vector<uint32_t>> data{};
  std::vector<std::string> solverNames{};

public:
  SolverPortfolioData(std::string filename);
  void readFile(std::string filename, char separator = '\0');
  size_t getNSolvers() const { return solverNames.size(); }
  size_t getNInstances() const { return data.size(); }
  uint32_t getRuntime(size_t i, size_t s) const {
    assert(i < data.size());
    assert(s < solverNames.size());
    return data[i][s];
  }
  std::string getSolverName(size_t i) const {
    assert(i < solverNames.size());
    return solverNames[i];
  }

  using iterator = std::vector<std::vector<uint32_t>>::iterator;
  using const_iterator = std::vector<std::vector<uint32_t>>::const_iterator;

  inline iterator begin() { return data.begin(); }
  inline const_iterator begin() const { return data.cbegin(); }
  inline iterator end() { return data.end(); }
  inline const_iterator end() const { return data.cend(); }
};
} // namespace SeeSaw

#endif
