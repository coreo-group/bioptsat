/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "cplex.hpp"

#include <cassert>
#include <cmath>

#include "globaloptions.hpp"
#include "options.hpp"

using namespace BiOptSat;

namespace SeeSaw {
// Options
static std::string cat = "2.3.1 - CplexHittingSetSolver";
static BoolOption opt_cplex_output(cat, "cplex-output",
                                   "Wether to have CPLEX print stuff", false);
static IntOption opt_cplex_threads(cat, "cplex-threads",
                                   "The number of threads for CPLEX to use", 1,
                                   IntRange(1, 32));
static IntOption
    opt_cplex_pop_nsoln(cat, "cplex-pop-nsoln",
                        "The size of the CPLEX population pool", 256,
                        IntRange(0, std::numeric_limits<int>::max()));
static BoolOption opt_cplex_data_chk(cat, "cplex-data-chk",
                                     "Run CPLEX data checker on its input",
                                     true);

CplexHittingSetSolver::CplexHittingSetSolver()
    : printOutput(opt_cplex_output), nThreads(opt_cplex_threads),
      popNSoln(opt_cplex_pop_nsoln), dataCheck(opt_cplex_data_chk) {}

CplexHittingSetSolver::CplexHittingSetSolver(
    std::unordered_map<lit_t, uint32_t> &weights)
    : HittingSetSolver(weights), printOutput(opt_cplex_output),
      nThreads(opt_cplex_threads), popNSoln(opt_cplex_pop_nsoln),
      dataCheck(opt_cplex_data_chk) {}

void CplexHittingSetSolver::init() {
  int status;
  env = CPXXopenCPLEX(&status);
  if (env == nullptr)
    processError(status, true, "Could not open CPLEX environment");

  if (printOutput && (status = CPXXsetintparam(env, CPX_PARAM_SCRIND, CPX_ON)))
    WARN << "failure to turn on CPLEX screen indicator, error " << status
         << "\n";

  INFO << "Using IBM CPLEX version " << CPXXversion(env)
       << " under IBM's Academic Initiative licensing program\n";

  if (!(mip = CPXXcreateprob(env, &status, "cplex_prob")))
    processError(status, true, "Could not create CPLEX problem");
  if ((status = CPXXchgprobtype(env, mip, CPXPROB_MILP)))
    processError(status, false, "Could not change CPLEX problem to MIP");
  if ((status = CPXXsetdblparam(env, CPX_PARAM_EPAGAP, absGap)))
    processError(status, false, "Could not set CPLEX absolute gap");
  if ((status = CPXXsetdblparam(env, CPX_PARAM_EPGAP, 0.0)))
    processError(status, false, "Could not set CPLEX relative gap");
  if ((status = CPXXsetintparam(env, CPX_PARAM_CLOCKTYPE, 1)))
    processError(status, false, "Could not set CPLEX CLOCKTYPE");
  if ((status = CPXXsetintparam(env, CPX_PARAM_THREADS, nThreads)))
    processError(status, false, "Could not set CPLEX global threads");
  if ((status = CPXXsetintparam(env, CPX_PARAM_DATACHECK, dataCheck)))
    processError(status, false, "Could not set CPLEX Data Check");
  if ((status = CPXXsetintparam(env, CPX_PARAM_MIPEMPHASIS,
                                CPX_MIPEMPHASIS_OPTIMALITY)))
    processError(status, false, "Could not set CPLEX Optimality Emphasis");
  if ((status = CPXXsetintparam(env, CPX_PARAM_POPULATELIM, popNSoln)))
    processError(status, false, "Could not set CPLEX Population limit");
  if ((status = CPXXsetintparam(env, CPX_PARAM_SOLNPOOLCAPACITY, popNSoln)))
    processError(status, false, "Could not set CPLEX Solution Pool limit");
  if ((status = CPXXsetintparam(env, CPX_PARAM_SOLNPOOLINTENSITY, 2)))
    processError(status, false, "Could not set CPLEX Solution Pool limit");
}

CplexHittingSetSolver::~CplexHittingSetSolver() {
  int status;
  if (mip && (status = CPXXfreeprob(env, &mip)))
    processError(status, false, "Could not free the CPLEX model");
  if (env && (status = CPXXcloseCPLEX(&env)))
    processError(status, false, "Could not close the CPLEX environment");
}

void CplexHittingSetSolver::processError(int status, bool terminal,
                                         const char *msg) {
  char errmsg[CPXMESSAGEBUFSIZE];
  auto errstr = CPXXgeterrorstring(env, status, errmsg);
  WARN << msg << "\n";
  if (errstr)
    WARN << errmsg << "\n";
  else
    WARN << "error code = " << status << "\n";
  if (terminal)
    solverValid = false;
}

void CplexHittingSetSolver::ensureMapping(var_t ex, bool pos) {
  // Create new CPLEX bool variable if one does not already exist
  if (ex >= ex2inMap.size()) {
    ex2inMap.resize(ex + 1, UNDEF);
    exPositive.resize(ex + 1);
  }
  if (ex2inMap[ex] == UNDEF) {
    var_t newCplexVar = CPXXgetnumcols(env, mip);
    ex2inMap[ex] = newCplexVar;
    exPositive[ex] = pos;
    addNewVar(ex);
  }
  assert(pos == exPositive[ex]);
}

void CplexHittingSetSolver::addNewVar(var_t ex) {
  // Add external variable to CPLEX as a new column
  double lb{0};
  double ub{1};
  char type{'B'};

  double weight{1};
  std::unordered_map<lit_t, uint32_t>::const_iterator search{};
  if (exPositive[ex])
    search = weights.find(ex);
  else
    search = weights.find(-ex);
  if (search != weights.end())
    weight = search->second;

  if (int status =
          CPXXnewcols(env, mip, 1, &weight, &lb, &ub, &type, nullptr)) {
    processError(status, false, "Could not create new CPLEX variable");
    WARN << "var = " << ex << ", wt = " << weight << "\n";
  }
  nVars++;
}

double CplexHittingSetSolver::getSolution(std::vector<lit_t> &solution) {
  double objval{};
  int status;
  if ((status = CPXXgetobjval(env, mip, &objval)))
    processError(status, false, "Problem getting MIP objective value");

  int nVars = getNVars();
  std::vector<double> vals(nVars, 0.0);

  if (nVars > 0) {
    status = CPXXgetx(env, mip, vals.data(), 0, nVars - 1);
    if (status)
      processError(status, false, "Problem getting MIP variable assignment");
  }

  solution.clear();
  for (size_t i = 0; i < ex2inMap.size(); i++) {
    var_t in = ex2inMap[i];
    if (in != UNDEF) {
      double val = vals[in];
      if (val > 0.99)
        solution.push_back(exPositive[i] ? i : -i);
      else if (val >= 0.01) {
        // Found unset value
        solution.clear();
        return -1;
      }
    }
  }
  return objval;
}

int32_t CplexHittingSetSolver::solve_(std::vector<lit_t> &solution) {
  // Return a optimal solution over all variables in the solver
  // Return -1 as lower bound if error

  int status;

  if ((status = CPXXsetdblparam(env, CPX_PARAM_TILIM, 1e+75)))
    processError(status, false, "Could not set CPLEX time limit");

  status = CPXXmipopt(env, mip);

  if (status)
    processError(status, false, "CPLEX Failed to optimize MIP");

  status = CPXXgetstat(env, mip);
  if (verbose > 2) {
    INFO << "Solution pool size " << CPXXgetsolnpoolnumsolns(env, mip) << "\n";
    INFO << "Solution pool replaced " << CPXXgetsolnpoolnumreplaced(env, mip)
         << "\n";
    for (int i = 0; i < CPXXgetsolnpoolnumsolns(env, mip); i++) {
      double objval{0.0};
      CPXXgetsolnpoolobjval(env, mip, i, &objval);
      INFO << "Solution " << i << ": objval = " << objval << "\n";
    }
  }

  if (status == CPXMIP_OPTIMAL || status == CPXMIP_OPTIMAL_TOL) {
    return round(getSolution(solution));
  } else {
    if (status == CPXMIP_TIME_LIM_FEAS || CPXMIP_TIME_LIM_INFEAS)
      return -1;
    char buf[CPXMESSAGEBUFSIZE];
    char *p = CPXXgetstatstring(env, status, buf);
    if (p)
      WARN << "CPLEX status = " << status << " " << buf << "\n";
    else
      WARN << "CPLEX status = " << status << "\n";
    return -1;
  }
}

void CplexHittingSetSolver::addCore(const clause_t &core) {
  if (!solverValid)
    return;

  std::vector<int> cplexVars{};
  std::vector<double> cplexCoeff{};
  char sense{'G'};
  double rhs{1};
  CPXNNZ beg{0};

  for (lit_t l : core) {
    ensureMapping(var(l), (l >= 0));
    cplexVars.push_back(ex2inMap[var(l)]);
    cplexCoeff.push_back(1.0);
  }

  if (int status =
          CPXXaddrows(env, mip, 0, 1, cplexVars.size(), &rhs, &sense, &beg,
                      cplexVars.data(), cplexCoeff.data(), nullptr, nullptr))
    processError(status, false, "Could not add core to CPLEX");

  nCores++;
}

void CplexHittingSetSolver::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Cplex Hitting Set Solver Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "print output = " << printOutput << "\n";
  INFO << "#threads = " << nThreads << "\n";
  INFO << "population pool size = " << popNSoln << "\n";
  INFO << "data checker = " << dataCheck << "\n";
  HittingSetSolver::printOptions();
}
} // namespace SeeSaw