/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "application.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>

#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if ENABLE_SOLVERPORTFOLIO
#include "solverportfolioapp.hpp"
#endif

#if ENABLE_INTERPDECRULES
#include "interpdecruleapp.hpp"
#endif

#if ENABLE_DIMACS
#include "dimacsapp.hpp"
#endif

using namespace BiOptSat;

namespace SeeSaw {
// Options
static string cat = "3 - Application";
static StringOption opt_application(cat, "application",
                                    "Select the application for the algorithm",
                                    "solver-portfolio", StringSet({
#if ENABLE_SOLVERPORTFOLIO
                                      "solver-portfolio",
#endif
#if ENABLE_INTERPDECRULES
                                          "interp-dec-rules",
#endif
#if ENABLE_DIMACS
                                          "dimacs",
#endif
                                    }));
static BoolOption opt_print_stats(cat, "app-print-stats",
                                  "Wether or not to print application stats",
                                  true);
static BoolOption opt_dump_pf(cat, "dump-pareto-front",
                              "Dump found pareto front to a .sol file", false);
static StringOption
    opt_core_minimization(cat, "core-minimization",
                          "The conflict minimization approach for SAT cores",
                          "exact", StringSet({"none", "exact", "heuristic"}));

Application::Application()
    : verbose(opt_verbose), doPrintStats(opt_print_stats),
      outputPath(opt_output_path), doDumpParetoFront(opt_dump_pf) {
  if (opt_core_minimization == "none")
    coreMinVariant = CM_NONE;
  else if (opt_core_minimization == "exact")
    coreMinVariant = CM_EXACT;
  else
    coreMinVariant = CM_HEURISTIC;
}

void Application::solve() {
  if (!loaded) {
    WARN << "Calling solve on application without having an instance loaded\n";
    return;
  }
  if (verbose >= 0)
    INFO << "Solving instance " << instance << "\n";

  enumerator->enumerate();

  solved = true;

  dumpParetoFront();
  printParetoFront();
}

void Application::finalizeLoading() {
  if (enumerator)
    delete enumerator;

  // Create enumerator
  // Add clauses one by one to save memory
  enumerator = new SeeSawEnumerator(
      universe, weights,
      [&](const Objective &uni, const std::vector<lit_t> &hs, uint32_t ub) {
        return this->oracle(uni, hs, ub);
      });
  enumerator->setCoreExtFunc(
      [&](const Objective &uni, const std::vector<lit_t> &hs, uint32_t f,
          OracleFunc o,
          std::vector<clause_t> &crs) { this->coreExt(uni, hs, f, o, crs); });

  loaded = true;
}

void Application::formatSolution(const std::vector<lit_t> &solution,
                                 std::vector<string> &outLines) const {
  std::vector<lit_t> sortedSol = solution;
  std::sort(sortedSol.begin(), sortedSol.end());
  string buf = "";
  uint32_t i = 0;
  for (lit_t l : universe) {
    if (i < solution.size() && l == sortedSol[i]) {
      buf += std::to_string(l) + " ";
      i++;
    } else
      buf += std::to_string(-l) + " ";
  }
  buf.pop_back();
  outLines.push_back(buf);
}

void Application::printSolution(const std::vector<lit_t> &model) const {
  std::vector<string> lines{};
  formatSolution(model, lines);
  for (string s : lines)
    SOL << s << "\n";
}

void Application::printParetoFront() const {
  if (!solved)
    WARN << "Calling printParetoFront but instance is not solved\n";

  assert(enumerator);
  ParetoFront<std::vector<lit_t>> pf = getParetoFront();

  INFO << "\n";
  INFO << "\n";
  INFO << "Found Pareto Front\n";
  INFO << LOG_H1;
  for (ParetoPoint<std::vector<lit_t>> pp : pf) {
    INFO << "Pareto point: " << upName << " = " << scaleUp(pp.incVal) << "; "
         << downName << " = " << scaleDown(pp.decVal)
         << "; #solutions = " << pp.models.size() << "\n";
    INFO << LOG_H2;
    for (std::vector<lit_t> mdl : pp.models) {
      printSolution(mdl);
      INFO << LOG_H3;
    }
  }
}

void Application::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "Application Stats\n";
  INFO << LOG_H1;

  INFO << "Instance: " << instance << "\n";

  if (enumerator)
    enumerator->printStats();
}

void Application::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Application Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
  INFO << "output path = " << outputPath << "\n";

  if (enumerator)
    enumerator->printOptions();
}

void Application::dumpParetoFront() const {
  if (!doDumpParetoFront)
    return;

  if (!solved)
    WARN << "Calling dumpParetoFront but instance is not solved\n";

  std::ofstream file{outputPath + ".sol"};

  assert(enumerator);
  ParetoFront<std::vector<lit_t>> pf = getParetoFront();

  for (ParetoPoint<std::vector<lit_t>> pp : pf) {
    file << "c Pareto point: " << upName << " = " << scaleUp(pp.incVal) << "; "
         << downName << " = " << scaleDown(pp.decVal)
         << "; #solutions = " << pp.models.size() << "\n";
    file << "c " << LOG_H2;
    for (std::vector<lit_t> mdl : pp.models) {
      std::vector<string> lines{};
      formatSolution(mdl, lines);
      for (string s : lines)
        file << "s " << s << "\n";
      file << "c " << LOG_H3;
    }
  }
}

void Application::minimizeCore(BiOptSat::SatSolver *satSolver, clause_t &core) {
  if (coreMinVariant == CM_NONE)
    return;

  clause_t origCore = core;
  std::vector<lit_t> assumps{};

  if (coreMinVariant == CM_EXACT) {
    // Exact minimization
    core.clear();
    for (uint32_t i = 0; i < origCore.size(); i++) {
      assumps.clear();
      for (uint32_t j = i + 1; j < origCore.size(); j++)
        assumps.push_back(-origCore[j]);
      for (uint32_t j = 0; j < core.size(); j++)
        assumps.push_back(-core[j]);
      SolverState ret = satSolver->solve(assumps);
      if (ret == UNSAT) // Literal not necessary
        continue;
      core.push_back(origCore[i]);
    }
  } else if (coreMinVariant == CM_HEURISTIC) {
    // Heuristic minimization
    for (lit_t l : origCore)
      assumps.push_back(-l);
#ifndef NDEBUG
    SolverState ret = satSolver->solve(assumps);
#else
    satSolver->solve(assumps);
#endif
    assert(ret == UNSAT);
    core.clear();
    satSolver->getConflict(core);
    assert(core.size() <= origCore.size());
    if (core.size() < origCore.size())
      minimizeCore(satSolver, core);
  }

  if (verbose >= 3)
    INFO << "Core minimization reduced core from " << origCore.size() << " to "
         << core.size() << " literals\n";
}

Application *appFactory(string id) {
  if (id == "")
    id = opt_application;
#if ENABLE_SOLVERPORTFOLIO
  if (id == "solver-portfolio")
    return new SolverPortfolioApp();
#endif
#if ENABLE_INTERPDECRULES
  if (id == "interp-dec-rules")
    return new InterpDecRuleApp();
#endif
#if ENABLE_DIMACS
  if (id == "dimacs")
    return new DimacsApp();
#endif
  ERROR << "Unknown application " << id << "\n";
  exit(1);
}
} // namespace SeeSaw