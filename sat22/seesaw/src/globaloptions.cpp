/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "globaloptions.hpp"

#include <string>

namespace BiOptSat {

static std::string cat = "0 - Global";

IntOption opt_verbose(cat, "verbose", "The verbosity of output", 1,
                      IntRange(0, 4));
StringOption opt_output_path(
    cat, "output-path",
    "The output path for different files. Should be without a file extension.",
    "bioptsat-out");
IntOption
    opt_inc_obj(cat, "inc-obj-idx",
                "The index of the objective to use as the increasing objective "
                "when parsing instances that might have more objectives than "
                "2. Indices start at 1",
                1, IntRange(1, std::numeric_limits<int32_t>::max()));
IntOption
    opt_dec_obj(cat, "dec-obj-idx",
                "The index of the objective to use as the decreasing objective "
                "when parsing instances that might have more objective than 2. "
                "Indices start at 1",
                2, IntRange(1, std::numeric_limits<int32_t>::max()));

} // namespace BiOptSat