/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "solverportfoliodata.hpp"

#include <fstream>
#include <sstream>

#include "logging.hpp"
#include "options.hpp"

using namespace BiOptSat;

namespace SeeSaw {
// Options
static std::string cat = "3.1 - Solver portfolio data";
static StringOption opt_separator(
    cat, "portfolio-separator",
    "The column separator for the data files. Needs to be a single character",
    ";");

SolverPortfolioData::SolverPortfolioData(std::string filename) {
  readFile(filename);
}

void SolverPortfolioData::readFile(std::string filename, char separator) {
  if (separator == '\0') {
    std::string sepString = opt_separator;
    separator = sepString[0];
  }

  std::ifstream input{filename};
  if (!input.is_open()) {
    ERROR << "File could not be opened.\n";
    exit(1);
  }
  std::string line;
  getline(input, line);
  std::stringstream header(line);
  while (header.good()) {
    std::string fname;
    getline(header, fname, separator);
    solverNames.push_back(fname);
  }
  while (getline(input, line)) {
    std::stringstream sample(line);
    std::vector<uint32_t> rts{};
    while (sample.good()) {
      std::string val{};
      getline(sample, val, separator);
      rts.push_back(std::stoi(val));
    }
    data.push_back(rts);
  }
}
} // namespace SeeSaw