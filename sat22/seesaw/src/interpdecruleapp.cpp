/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "interpdecruleapp.hpp"

#include <algorithm>

#include "dataset.hpp"
#include "logging.hpp"
#include "options.hpp"
#include "utils.hpp"

using namespace BiOptSat;

namespace SeeSaw {
// Options
static std::string cat = "3.2 - Decision Rules";
static StringOption opt_rule_type(cat, "rule-type", "The type of rule to learn",
                                  "cnf", StringSet({"cnf", "dnf"}));
static BoolOption
    opt_symmetry_breaking(cat, "dec-rule-symmetry-breaking",
                          "Wether to add symmetry breaking clauses or not",
                          false);
static IntOption
    opt_n_clauses(cat, "dec-rule-clauses",
                  "The number of clauses in the learned decision rule", 2,
                  IntRange(1, 20));
static StringOption opt_core_ext(cat, "core-extraction",
                                 "The core extraction method to use",
                                 "sat-cores",
                                 StringSet({"seesaw-improved", "sat-cores"}));

InterpDecRuleApp::InterpDecRuleApp(uint32_t _nRuleClauses, RuleType type)
    : nRuleClauses(_nRuleClauses), symmetryBreaking(opt_symmetry_breaking) {
  upName = "Classification error";
  downName = "Rule size";
  setRuleType(type);
  if (nRuleClauses == 0)
    nRuleClauses = opt_n_clauses;

  if (opt_core_ext == "sat-cores")
    coreExtMethod = SAT_CORES;
  else
    coreExtMethod = SEESAW_IMPROVED;
}

void InterpDecRuleApp::formatSolution(const std::vector<lit_t> &noiseSet,
                                      std::vector<string> &outLines) const {
  string litSep = "|";
  string clSep = "&";
  if (dnf) {
    litSep = "&";
    clSep = "|";
  }

  if (noiseSet.size() == 1 && noiseSet[0] == DUMMY) {
    // Special case: empty rule (not found by SAT solver)
    outLines.clear();
    for (uint32_t i = 0; i < nRuleClauses; i++) {
      std::string buf = "( )";
      if (i != nRuleClauses - 1)
        buf += " " + clSep;
      outLines.push_back(buf);
    }
    return;
  }

  auto search = noiseSetRuleMap.find(noiseSet);
  if (search == noiseSetRuleMap.end()) {
    // No rule found for noise set
    // Should only be the case if perfect rule doesn't exist
    outLines.clear();
    outLines.push_back("n/a");
    return;
  }

  std::vector<std::vector<uint32_t>> rule = search->second;

  for (uint32_t i = 0; i < rule.size(); i++) {
    std::vector<uint32_t> cl = rule[i];
    std::string buf = "(";
    for (uint32_t lt : cl)
      buf += " " +
             ((hasNegated && lt > nFeatures / 2)
                  ? ("-" + std::to_string(lt - nFeatures / 2))
                  : std::to_string(lt)) +
             " " + litSep;
    if (buf.length() > 1)
      buf.pop_back();
    else
      buf += " ";
    buf += ")";
    if (i < nRuleClauses - 1)
      buf += " " + clSep;
    outLines.push_back(buf);
  }
}

void InterpDecRuleApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "InterpDecRuleApp Stats\n";
  INFO << LOG_H1;

  INFO << "Learning " << (dnf ? "DNF" : "CNF") << "-rule\n";
  INFO << "#rule clauses = " << nRuleClauses << "\n";
  INFO << "#samples = " << nSamples << "\n";
  INFO << "#features = " << nFeatures << "\n";

  satSolver->printStats();

  Application::printStats();
}

void InterpDecRuleApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "InterpDecRuleApp Options\n";
  INFO << LOG_H1;

  std::string coreExt{};
  switch (coreExtMethod) {
  case SEESAW_IMPROVED:
    coreExt = "seesaw-improved";
    break;
  case SAT_CORES:
    coreExt = "sat-cores";
    break;
  }

  std::string minCore{};
  switch (coreMinVariant) {
  case CM_NONE:
    minCore = "NONE";
    break;
  case CM_EXACT:
    minCore = "EXACT";
    break;
  case CM_HEURISTIC:
    minCore = "HEURISTIC";
    break;
  }

  INFO << "\n";
  INFO << "#rule clauses = " << nRuleClauses << "\n";
  INFO << "symmetryBreaking = " << (symmetryBreaking ? "on" : "off") << "\n";
  INFO << "rule type = " << (dnf ? "DNF" : "CNF") << "\n";
  INFO << "coreExtMethod = " << coreExt << "\n";
  INFO << "coreMinVariant = " << minCore << "\n";
  Application::printOptions();
}

void InterpDecRuleApp::setRuleType(RuleType type) {
  assert(!encoded);
  switch (type) {
  case DEFAULT_RULE:
    if (opt_rule_type == "cnf")
      dnf = false;
    else
      dnf = true;
    break;
  case CNF_RULE:
    dnf = false;
    break;
  case DNF_RULE:
    dnf = true;
    break;
  }
}

void InterpDecRuleApp::loadInstance(std::string path) {
  instance = path;

  if (verbose >= 0)
    INFO << "Encoding instance " << instance << " for " << (dnf ? "DNF" : "CNF")
         << "-learning with " << nRuleClauses << " clauses\n";

  solved = false;
  if (enumerator) {
    delete enumerator;
    enumerator = nullptr;
  }
  if (satSolver)
    delete satSolver;
  satSolver = solverFactory();

  // Load data
  DataSet data{instance};
  nSamples = data.nSamples();
  nFeatures = data.nFeatures();
  hasNegated = data.hasNegatedColumns();
  nPosSamples = data.getNPositive();

  // Create variables
  var_t nVars = 0;

  clauseContainsFeature.resize(nRuleClauses * nFeatures);
  for (uint32_t i = 0; i < nRuleClauses; i++)
    for (uint32_t j = 0; j < nFeatures; j++)
      clauseContainsFeature[clauseFeature(i, j)] = ++nVars;

  sampleNotMatchClause.resize(nSamples * nRuleClauses);
  for (uint32_t i = 0; i < nSamples; i++)
    for (uint32_t j = 0; j < nRuleClauses; j++)
      sampleNotMatchClause[sampleClause(i, j)] = ++nVars;

  sampleNoisy.resize(nSamples);
  for (uint32_t i = 0; i < nSamples; i++)
    sampleNoisy[i] = ++nVars;

  if (symmetryBreaking) {
    clausesEqualTill.resize((nRuleClauses - 1) * nFeatures);
    for (uint32_t i = 0; i < nRuleClauses - 1; i++)
      for (uint32_t j = 0; j < nFeatures; j++)
        clausesEqualTill[clauseFeature(i, j)] = ++nVars;
  }

  // Encoding
  // MLIC
  for (uint32_t i = 0; i < nSamples; i++) {
    // Encode constraint for every sample
    DataSample sample = data.getSample(i);
    if ((sample.getClass() && !dnf) || (!sample.getClass() && dnf))
      // Positive sample and CNF or negative sample and DNF
      // -eta -> (X v B)
      for (uint32_t l = 0; l < nRuleClauses; l++) {
        clause_t clause = {lit(sampleNoisy[i])};
        for (uint32_t j = 0; j < nFeatures; j++)
          if (sample.getFeature(j))
            clause.push_back(clauseContainsFeature[clauseFeature(l, j)]);
        satSolver->addClause(clause);
      }
    else {
      // Negative sample and CNF or positive sample and DNF
      // -eta -> V z
      // z -> -(X v B)
      clause_t clause = {lit(sampleNoisy[i])};
      for (uint32_t l = 0; l < nRuleClauses; l++) {
        clause.push_back(sampleNotMatchClause[sampleClause(i, l)]);
        for (uint32_t j = 0; j < nFeatures; j++)
          if (sample.getFeature(j))
            satSolver->addClause(
                {-lit(sampleNotMatchClause[sampleClause(i, l)]),
                 -lit(clauseContainsFeature[clauseFeature(l, j)])});
      }
      satSolver->addClause(clause);
    }
  }
  if (symmetryBreaking) {
    // Symmetry breaking
    for (uint i = 0; i < nRuleClauses - 1; i++)
      for (uint32_t j = 0; j < nFeatures; j++) {
        lit_t b_i1 = clauseContainsFeature[clauseFeature(i, j)];
        lit_t b_i2 = clauseContainsFeature[clauseFeature(i + 1, j)];
        lit_t e_j1 = clausesEqualTill[clauseFeature(i, j)];

        if (j == 0) {
          // (b_i^j <-> b_i-1^j) <-> e_i^j
          satSolver->addClause({b_i1, -b_i2, -e_j1});
          satSolver->addClause({-b_i1, b_i2, -e_j1});
          satSolver->addClause({-b_i1, -b_i2, e_j1});
          satSolver->addClause({b_i1, b_i2, e_j1});
          // ~e_i^j -> (b_i^j & ~b_i+1^j)
          // Order enforcing clauses
          satSolver->addClause({e_j1, b_i1});
          satSolver->addClause({e_j1, -b_i2});
          continue;
        }

        lit_t e_j0 = clausesEqualTill[clauseFeature(i, j - 1)];

        // e_i^j-1 & (b_i^j <-> b_i-1^j) <-> e_i^j
        satSolver->addClause({-e_j1, e_j0});
        satSolver->addClause({b_i1, -b_i2, -e_j1});
        satSolver->addClause({-b_i1, b_i2, -e_j1});
        satSolver->addClause({-b_i1, -b_i2, -e_j0, e_j1});
        satSolver->addClause({b_i1, b_i2, -e_j0, e_j1});
        // (e_i^j-1 & ~e_i^j) -> b_i^j & ~b_i+1^j
        // Order enforcing clauses
        satSolver->addClause({-e_j0, e_j1, b_i1});
        satSolver->addClause({-e_j0, e_j1, -b_i2});
      }
  }

  // Add Totalizer
  if (tot)
    delete tot;
  tot = new Totalizer(satSolver, CARD_UB);
  Objective size(clauseContainsFeature.size(), 0);
  for (uint32_t i = 0; i < clauseContainsFeature.size(); i++)
    size[i] = clauseContainsFeature[i];
  tot->build(size, 1);

  // Define universe
  universe.resize(sampleNoisy.size());
  for (uint32_t i = 0; i < sampleNoisy.size(); i++)
    universe[i] = sampleNoisy[i];

  encoded = true;

  finalizeLoading();

  enumerator->setMinDownwards(nRuleClauses);
  enumerator->setMaxDownwards(nRuleClauses * nFeatures);
}

uint32_t InterpDecRuleApp::oracle(const Objective &universe,
                                  const std::vector<lit_t> &noiseSet,
                                  uint32_t upperBound) {
  if (upperBound > nRuleClauses * nFeatures)
    upperBound = nRuleClauses * nFeatures;
  std::vector<lit_t> sortedNoiseSet = noiseSet;
  std::sort(sortedNoiseSet.begin(), sortedNoiseSet.end());
  std::vector<lit_t> assumps{};
  std::vector<lit_t> baseAssumps{};
  uint32_t j = 0;
  for (uint32_t i = 0; i < sampleNoisy.size(); i++) {
    if (j < sortedNoiseSet.size() &&
        sampleNoisy[i] == static_cast<var_t>(sortedNoiseSet[j]))
      j++;
    else
      baseAssumps.push_back(-sampleNoisy[i]);
  }
  assumps = baseAssumps;
  if (upperBound < nRuleClauses * nFeatures) {
    tot->updateUpper(upperBound);
    assumps.push_back(-tot->getOutLit(upperBound));
  }

  SolverState ret = satSolver->solve(assumps);
  assert(ret != UNDEF);
  if (ret == UNSAT) {
    // No rule smaller than upper bound found
    return upperBound + 1;
  }

  assert(getCurrentError() <= noiseSet.size());

  // Save rule
  std::vector<std::vector<uint32_t>> rule{};
  for (uint32_t i = 0; i < nRuleClauses; i++) {
    std::vector<uint32_t> clause;
    for (uint32_t j = 0; j < nFeatures; j++)
      if (satSolver->varModelValue(clauseContainsFeature[clauseFeature(i, j)]))
        clause.push_back(j);
    rule.push_back(clause);
  }
  uint32_t bound = getCurrentRuleSize();

  tot->updateUpper(bound);

  // Sat-Unsat minimal search
  while (bound > 0) {
    if (verbose >= 3)
      INFO << "oracle minimization iteration, bound=" << bound << "\n";
    assumps = baseAssumps;
    assumps.push_back(-tot->getOutLit(bound - 1));
    ret = satSolver->solve(assumps);
    if (ret == UNSAT)
      break;
    assert(ret == SAT);
    // Save rule
    rule.clear();
    for (uint32_t i = 0; i < nRuleClauses; i++) {
      std::vector<uint32_t> clause;
      for (uint32_t j = 0; j < nFeatures; j++)
        if (satSolver->varModelValue(
                clauseContainsFeature[clauseFeature(i, j)]))
          clause.push_back(j);
      rule.push_back(clause);
    }
    bound = getCurrentRuleSize();
  }

  // Save current model for printing
  noiseSetRuleMap[noiseSet] = rule;

  return bound;
};

void InterpDecRuleApp::coreExt(const Objective &uni,
                               const std::vector<lit_t> &noiseSet, uint32_t f,
                               OracleFunc o, std::vector<clause_t> &cores) {
  switch (coreExtMethod) {
  case SEESAW_IMPROVED:
    return seesawImprovedCoreExt(noiseSet, f, cores);

  case SAT_CORES:
    return satCoreExt(noiseSet, f, cores);
  }
}

void InterpDecRuleApp::seesawImprovedCoreExt(const std::vector<lit_t> &noiseSet,
                                             uint32_t f,
                                             std::vector<clause_t> &cores) {
  if (f == 0) {
    // If f == 0, terminate
    cores.clear();
    cores.push_back({});
    return;
  }

  std::vector<lit_t> sortedNoiseSet = noiseSet;
  std::sort(sortedNoiseSet.begin(), sortedNoiseSet.end());
  std::vector<lit_t> assumps{};
  uint32_t j = 0;
  for (uint32_t i = 0; i < sampleNoisy.size(); i++) {
    if (j < sortedNoiseSet.size() &&
        sampleNoisy[i] == static_cast<var_t>(sortedNoiseSet[j]))
      j++;
    else
      assumps.push_back(-sampleNoisy[i]);
  }
  tot->updateUpper(f - 1);
  if (f - 1 < nRuleClauses * nFeatures)
    assumps.push_back(-tot->getOutLit(f - 1));

  assert(satSolver->solve(assumps) == UNSAT);

  uint32_t keep = 0;
  while (keep < assumps.size() - ((f - 1 < nRuleClauses * nFeatures) ? 1 : 0)) {
    std::vector<lit_t> tmpAssumps = assumps;
    tmpAssumps.erase(tmpAssumps.begin() + keep);

    SolverState ret = satSolver->solve(tmpAssumps);
    assert(ret != UNDEF);

    if (ret == SAT) {
      keep++;
    } else {
      sortedNoiseSet.push_back(-assumps[keep]);
      assumps.erase(assumps.begin() + keep);
    }
  }

  clause_t core{};
  setdiff(universe, sortedNoiseSet, core);

  if (verbose >= 2)
    INFO << "Core extraction: noise set size = " << noiseSet.size()
         << "; maximal noise set size = " << sortedNoiseSet.size()
         << "; core size = " << core.size() << "\n";

  cores.clear();
  cores.push_back(core);
}

void InterpDecRuleApp::satCoreExt(const std::vector<lit_t> &noiseSet,
                                  uint32_t f, std::vector<clause_t> &cores) {
  if (f == 0) {
    // If f == 0, terminate
    cores.clear();
    cores.push_back({});
    return;
  }

  clause_t core{};
  satSolver->getConflict(core);
  minimizeCore(satSolver, core);
  clause_t cleanedCore{};
  for (lit_t l : core) {
    if (var(l) >= sampleNoisy.front() && var(l) <= sampleNoisy.back()) {
      assert(l > 0);
      cleanedCore.push_back(l);
    }
  }

  cores.clear();
  cores.push_back(cleanedCore);
}

ParetoFront<std::vector<lit_t>> InterpDecRuleApp::getParetoFront() const {
  // Add trivial constant negative classifier, if not already there
  std::vector<lit_t> emptyMdl{DUMMY};
  ParetoFront<std::vector<lit_t>> pf = enumerator->getParetoFront();
  if (pf.size() == 0 || pf.back().decVal != 0) {
    ParetoPoint<std::vector<lit_t>> pp{
        .models = {emptyMdl}, .incVal = nPosSamples, .decVal = 0};
    pf.push_back(pp);
  }
  return pf;
}

// Helper functions
uint32_t InterpDecRuleApp::getCurrentRuleSize() {
  uint32_t value{};
  for (var_t v : clauseContainsFeature)
    if (satSolver->varModelValue(v))
      value++;
  return value;
}

uint32_t InterpDecRuleApp::getCurrentError() {
  uint32_t value{};
  for (var_t v : sampleNoisy)
    if (satSolver->varModelValue(v))
      value++;
  return value;
}
} // namespace SeeSaw
