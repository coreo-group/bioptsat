/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _options_hpp_INCLUDED
#define _options_hpp_INCLUDED

#include <cstdint>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

using std::string;

// Options implementation inspired by Minisat
// (https://github.com/niklasso/minisat/blob/master/minisat/utils/Options.h)

namespace BiOptSat {

void parseOptions(int &, char **, bool = false);
void printUsageAndExit(int, char **, bool = false);
void setUsageHelp(const char *);
void setHelpPrefixStr(const char *);

// String matching: in case of a match, the input iterator will be advanced the
// corresponding number of characters
template <class B> static bool match(B &in, string str) {
  int i;
  for (i = 0; str[i] != '\0'; i++)
    if (in[i] != str[i])
      return false;
  in += i;
  return true;
}

// Abstract base class for command line options
class Option {
protected:
  string name;
  string description;
  string category;
  string type_name;

  static std::vector<Option *> &getOptionList() {
    static std::vector<Option *> options;
    return options;
  }
  static const char *&getUsageString() {
    static const char *usage_str;
    return usage_str;
  }
  static const char *&getHelpPrefixString() {
    static const char *help_prefix_str = "";
    return help_prefix_str;
  }

  struct OptionLt {
    bool operator()(const Option *x, const Option *y) {
      int test = x->category.compare(y->category);
      return (test < 0) ||
             (test == 0 && x->type_name.compare(y->type_name) < 0);
    }
  };

  Option(string name, string desc, string cat, string type)
      : name(name), description(desc), category(cat), type_name(type) {
    getOptionList().push_back(this);
  }

public:
  virtual ~Option() {}

  virtual bool parse(const char *str) = 0;
  virtual void help(bool verbose = false) = 0;

  friend void parseOptions(int &, char **, bool);
  friend void printUsageAndExit(int, char **, bool);
  friend void setUsageHelp(const char *);
  friend void setHelpPrefixStr(const char *);
};

struct IntRange {
  int begin;
  int end;
  IntRange(int b, int e) : begin(b), end(e) {}
  IntRange()
      : begin(std::numeric_limits<int>::min()),
        end(std::numeric_limits<int>::max()) {}
};

struct DoubleRange {
  double begin;
  double end;
  bool begin_inclusive;
  bool end_inclusive;
  DoubleRange(double b, bool binc, double e, bool einc)
      : begin(b), end(e), begin_inclusive(binc), end_inclusive(einc) {}
  DoubleRange()
      : begin(std::numeric_limits<double>::min()),
        end(std::numeric_limits<double>::max()), begin_inclusive(true),
        end_inclusive(true) {}
};

struct StringSet {
  std::vector<string> strs;
  bool any;
  StringSet(std::vector<string> strs) : strs(strs), any(false) {}
  StringSet() : any(true) {}
};

class DoubleOption : public Option {
protected:
  DoubleRange range;
  double value;

public:
  DoubleOption(string cat, string name, string desc, double def = double(),
               DoubleRange r = DoubleRange())
      : Option(name, desc, cat, "<double>"), range(r), value(def) {}

  operator double() const { return value; }
  operator double &() { return value; }
  DoubleOption &operator=(double x) {
    value = x;
    return *this;
  }

  virtual bool parse(const char *str) {
    const char *span = str;

    if (!match(span, "--") || !match(span, name) || !match(span, "="))
      return false;

    char *end;
    double tmp = strtod(span, &end);

    if (end == nullptr)
      return false;
    else if (tmp >= range.end && (!range.end_inclusive || tmp != range.end)) {
      std::cerr << "ERROR! value " << span << " is too large for option \""
                << name << "\"\n";
      exit(1);
    } else if (tmp <= range.begin &&
               (!range.begin_inclusive || tmp != range.begin)) {
      std::cerr << "ERROR! value " << span << " is too small for option \""
                << name << "\"\n";
      exit(1);
    }

    value = tmp;

    return true;
  }

  virtual void help(bool verbose = false) {
    std::cout << std::left << "  --" << std::setw(12) << name << " = "
              << std::setw(8) << type_name << " "
              << (range.begin_inclusive ? '[' : '(') << std::setprecision(6)
              << range.begin << " .. " << std::setprecision(6) << range.end
              << (range.end_inclusive ? ']' : ')')
              << " (default: " << std::setprecision(6) << value << ")\n";
    if (verbose)
      std::cout << "\n    " << description << "\n\n";
  }
};

class IntOption : public Option {
protected:
  IntRange range;
  int32_t value;

public:
  IntOption(string cat, string name, string desc, int32_t def = int32_t(),
            IntRange r = IntRange())
      : Option(name, desc, cat, "<int32>"), range(r), value(def) {}

  operator int32_t() const { return value; }
  operator int32_t &() { return value; }
  IntOption &operator=(int32_t x) {
    value = x;
    return *this;
  }

  virtual bool parse(const char *str) {
    const char *span = str;

    if (!match(span, "--") || !match(span, name) || !match(span, "="))
      return false;

    char *end;
    int32_t tmp = strtol(span, &end, 10);

    if (end == nullptr)
      return false;
    else if (tmp > range.end) {
      std::cerr << "ERROR! value " << span << " is too large for option \""
                << name << "\"\n";
      exit(1);
    } else if (tmp < range.begin) {
      std::cerr << "ERROR! value " << span << " is too small for option \""
                << name << "\"\n";
      exit(1);
    }

    value = tmp;

    return true;
  }

  virtual void help(bool verbose = false) {
    std::cout << std::left << "  --" << std::setw(12) << name << " = "
              << std::setw(8) << type_name << " [" << range.begin << " .. "
              << range.end << "] (default: " << value << ")\n";
    if (verbose)
      std::cout << "\n    " << description << "\n\n";
  }
};

class StringOption : public Option {
protected:
  StringSet set;
  string value;

public:
  StringOption(string cat, string name, string desc, string def = nullptr,
               StringSet set = StringSet())
      : Option(name, desc, cat, "<string>"), set(set), value(def) {}

  operator string() const { return value; }
  operator string &() { return value; }
  StringOption &operator=(string x) {
    value = x;
    return *this;
  }
  bool operator==(const string str) { return value == str; }

  virtual bool parse(const char *str) {
    const char *span = str;

    if (!match(span, "--") || !match(span, name) || !match(span, "="))
      return false;

    if (set.any) {
      value = span;
      return true;
    }

    string spanString{span};
    for (string s : set.strs)
      if (spanString == s) {
        value = spanString;
        return true;
      }

    std::cerr << "ERROR! value " << span << " is not valid for option \""
              << name << "\"\n";
    exit(1);
  }

  virtual void help(bool verbose = false) {
    std::cout << std::left << "  --" << std::setw(12) << name << " = "
              << std::setw(8) << type_name;
    if (!set.any) {
      std::cout << " [";
      for (uint32_t i = 0; i < set.strs.size(); i++) {
        std::cout << set.strs[i];
        if (i < set.strs.size() - 1)
          std::cout << ", ";
      }
      std::cout << "]";
    }
    std::cout << " (default: \"" << value << "\")\n";
    if (verbose)
      std::cout << "\n    " << description << "\n\n";
  }
};

class BoolOption : public Option {
protected:
  bool value;

public:
  BoolOption(string cat, string name, string desc, bool val)
      : Option(name, desc, cat, "<bool>"), value(val) {}

  operator bool() const { return value; }
  operator bool &() { return value; }
  BoolOption &operator=(bool b) {
    value = b;
    return *this;
  }

  virtual bool parse(const char *str) {
    const char *span = str;

    if (match(span, "--")) {
      bool b = !match(span, "no-");

      if (name == span) {
        value = b;
        return true;
      }
    }

    return false;
  }

  virtual void help(bool verbose = false) {
    std::cout << "  --" << name << ", --no-" << name;
    uint ub = (name.length() < 16) ? 32 - name.length() * 2 : 5;
    for (uint32_t i = 0; i < ub; i++)
      std::cout << " ";
    std::cout << " (default: " << (value ? "on" : "off") << ")\n";
    if (verbose)
      std::cout << "\n    " << description << "\n\n";
  }
};

} // namespace BiOptSat

#endif
