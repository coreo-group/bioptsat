/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "satsolver.hpp"

#include "globaloptions.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if WITH_CADICAL
#include "cadicalnoninc.hpp"
#include "cadicalsolver.hpp"
#endif

namespace BiOptSat {
// Options
static std::string cat = "2.1 - SatSolver";
static StringOption opt_backend(cat, "backend", "The Sat backend to use",
                                "cadical", StringSet({
#if WITH_CADICAL
                                  "cadical", "cadical-noninc",
#endif
                                }));

SatSolver *solverFactory(std::string id) {
  if (id == "")
    id = opt_backend;
#if WITH_CADICAL
  if (id == "cadical")
    return new CadicalSolver();
  if (id == "cadical-noninc")
    return new CadicalNonIncSolver();
#endif
  ERROR << "Unknown solver " << id << "\n";
  exit(1);
}

SatSolver::SatSolver() : verbose(opt_verbose) {}
} // namespace BiOptSat