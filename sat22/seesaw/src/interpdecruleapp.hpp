/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _interpdecruleapp_hpp_INCLUDED
#define _interpdecruleapp_hpp_INCLUDED

#include <cassert>
#include <unordered_map>

#include "application.hpp"
#include "hash.hpp"
#include "satsolver.hpp"
#include "totalizer.hpp"
#include "types.hpp"

namespace SeeSaw {
enum RuleType { // Rule type selection
  CNF_RULE,
  DNF_RULE,
  DEFAULT_RULE
};

enum CoreExtMethod { // Core extraction method
  SEESAW_IMPROVED,
  SAT_CORES
};

// Application of SeeSaw to learning interpretable decision rules
class InterpDecRuleApp : public Application {
protected:
  BiOptSat::SatSolver *satSolver{};
  BiOptSat::Totalizer *tot{};

  bool encoded{};          // Wether or not the instance is already encoded
  bool dnf{};              // Wether to learn DNFs or CNFs
  uint32_t nRuleClauses{}; // Number of clauses in the learned rule
  uint32_t nSamples{};     // Number of samples in the data
  uint32_t nFeatures{};    // Number of features in the data
  bool symmetryBreaking{}; // Wether to add symmetry breaking or not
  bool hasNegated{};       // Wether negated features are used

  CoreExtMethod coreExtMethod{};   // The core extraction method to use

  // Indexing functions for the variables sets with two indices
  // The individual indices start at 0
  inline uint32_t clauseFeature(uint32_t c, uint32_t f) const {
    return c * nFeatures + f;
  }
  inline uint32_t sampleClause(uint32_t s, uint32_t c) const {
    return s * nRuleClauses + c;
  }

  // Variable sets
  std::vector<BiOptSat::var_t>
      clauseContainsFeature{}; // b variables of the MLIC paper
  std::vector<BiOptSat::var_t>
      sampleNotMatchClause{}; // Tseitin variables (z) from the MLIC paper
  std::vector<BiOptSat::var_t>
      sampleNoisy{}; // The eta variables from the MLIC paper
  std::vector<BiOptSat::var_t>
      clausesEqualTill{}; // The e variables from our paper for
                          // symmetry breaking
  // Helper for separating into positive and negative samples
  uint32_t nPosSamples{};

  // Saving found rules for noise sets
  std::unordered_map<std::vector<BiOptSat::lit_t>,
                     std::vector<std::vector<uint32_t>>>
      noiseSetRuleMap{};
  const BiOptSat::lit_t DUMMY =
      0; // Dummy value to mark noise set for empty rule

  void formatSolution(const std::vector<BiOptSat::lit_t> &,
                      std::vector<std::string> &) const;

  uint32_t oracle(const BiOptSat::Objective &,
                  const std::vector<BiOptSat::lit_t> &, uint32_t);
  void coreExt(const BiOptSat::Objective &,
               const std::vector<BiOptSat::lit_t> &, uint32_t, OracleFunc,
               std::vector<BiOptSat::clause_t> &);

  void seesawImprovedCoreExt(const std::vector<BiOptSat::lit_t> &, uint32_t,
                             std::vector<BiOptSat::clause_t> &);
  void satCoreExt(const std::vector<BiOptSat::lit_t> &, uint32_t,
                  std::vector<BiOptSat::clause_t> &);

  uint32_t getCurrentRuleSize();
  uint32_t getCurrentError();

public:
  InterpDecRuleApp(uint32_t nRuleClauses = 0, RuleType type = DEFAULT_RULE);
  void loadInstance(std::string);
  virtual void printStats() const;
  virtual void printOptions() const;
  virtual BiOptSat::ParetoFront<std::vector<BiOptSat::lit_t>>
  getParetoFront() const;

  // Stats getter
  uint32_t getNClauses() const {
    if (satSolver)
      return satSolver->getNClauses();
    return 0;
  }
  uint32_t getNVars() const {
    if (satSolver)
      return satSolver->getNVars();
    return 0;
  }

  // Option getter and setter (don't change options after encoding)
  uint32_t getNRuleClauses() const { return nRuleClauses; }
  void setNRuleClauses(uint32_t nrc) {
    assert(!encoded);
    nRuleClauses = nrc;
  }
  bool hasSymmetryBreaking() const { return symmetryBreaking; }
  void setSymmetryBreaking(bool sb) { symmetryBreaking = sb; }
  bool encodesDnf() const { return dnf; }
  void setRuleType(RuleType);
  bool usesNegated() const { return hasNegated; }
  CoreExtMethod getCoreExtMethod() const { return coreExtMethod; }
  void setCoreExtMethod(CoreExtMethod cem) { coreExtMethod = cem; }
  ConfMinVariant getConfMinVariant() const { return coreMinVariant; }
  void setConfMinVariant(ConfMinVariant cmv) { coreMinVariant = cmv; }
};
} // namespace SeeSaw

#endif