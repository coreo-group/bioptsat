/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _solverportfolioapp_hpp_INCLUDED
#define _solverportfolioapp_hpp_INCLUDED

#include "application.hpp"
#include "solverportfoliodata.hpp"
#include "types.hpp"

namespace SeeSaw {
// Application of SeeSaw to the solver portfolio problem
class SolverPortfolioApp : public Application {
protected:
  SolverPortfolioData *data{}; // The data about the solvers

  void formatSolution(const std::vector<BiOptSat::lit_t> &,
                      std::vector<std::string> &) const;

  uint32_t oracle(const BiOptSat::Objective &,
                  const std::vector<BiOptSat::lit_t> &,
                  uint32_t); // The PAR-2 score of the portfolio
  void coreExt(const BiOptSat::Objective &uni,
               const std::vector<BiOptSat::lit_t> &portfolio, uint32_t f,
               OracleFunc o, std::vector<BiOptSat::clause_t> &crs) {
    antiMonCoreExtFunc(uni, portfolio, f, o, crs);
  }

public:
  SolverPortfolioApp() {
    upName = "Portfolio size";
    downName = "PAR-2 score";
  }
  void loadInstance(std::string);
  virtual void printStats() const;

  // Stats getter
  uint32_t getNSolvers() const {
    if (data)
      return data->getNSolvers();
    return 0;
  }
  uint32_t getNInstances() const {
    if (data)
      return data->getNInstances();
    return 0;
  }
};
} // namespace SeeSaw

#endif