/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "cadicalsolver.hpp"

#include <cassert>

#include "globaloptions.hpp"
#include "logging.hpp"

namespace BiOptSat {
void CadicalSolver::addClause(const clause_t &cl) {
  for (lit_t l : cl) {
    assert(l != 0);
    backend.add(l);
    var_t v = var(l);
    if (v > nVars)
      nVars = v;
  }
  backend.add(0);
  avgClauseLen = ((avgClauseLen * nClauses) + cl.size()) / (nClauses + 1);
  nClauses++;
}

SolverState CadicalSolver::solve(const std::vector<lit_t> &assumps) {
  if (opt_verbose >= 3)
    INFO << "Solving with CaDiCaL backend\n";
  if (opt_verbose >= 4)
    INFO << "Solver stats: #solves=" << nSolves + 1 << "; #clauses=" << nClauses
         << "; #vars=" << nVars << "\n";

  for (lit_t l : assumps)
    backend.assume(l);
  startTimer();
  int ret = backend.solve();
  stopTimer();
  nSolves++;

  switch (ret) {
  case 10:
    if (opt_verbose >= 3)
      INFO << "Query is satisfiable\n";
    nSatSolves++;
    return SAT;
  case 20:
    if (opt_verbose >= 3)
      INFO << "Query is unsatisfiable\n";
    nUnsatSolves++;
    conflict.clear();
    for (lit_t l : assumps)
      if (backend.failed(l))
        conflict.push_back(-l);
    return UNSAT;
  default:
    if (opt_verbose >= 3)
      INFO << "Query terminated\n";
    return UNDEF;
  }
}
} // namespace BiOptSat