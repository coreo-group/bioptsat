/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "solverportfolioapp.hpp"

#include <algorithm>
#include <limits>

using namespace BiOptSat;

namespace SeeSaw {
void SolverPortfolioApp::loadInstance(std::string path) {
  instance = path;

  // Load data
  data = new SolverPortfolioData(instance);

  // Create universe
  universe.resize(getNSolvers());
  for (uint32_t i = 0; i < getNSolvers(); i++)
    universe[i] = i;

  finalizeLoading();
}

void SolverPortfolioApp::formatSolution(const std::vector<lit_t> &portfolio,
                                        std::vector<std::string> &lines) const {
  lines.clear();
  std::string buf = "{ ";
  for (lit_t l : portfolio)
    buf += data->getSolverName(l) + ", ";
  if (portfolio.size()) {
    buf.pop_back();
    buf.pop_back();
  }
  buf += " }";
  lines.push_back(buf);
}

uint32_t SolverPortfolioApp::oracle(const Objective &uni,
                                    const std::vector<lit_t> &portfolio,
                                    uint32_t upperBound) {
  // Calculate the PAR-2 score of a portfolio
  uint32_t totalScore = 0;
  for (std::vector<uint32_t> dat : *data) {
    uint32_t min = std::numeric_limits<uint32_t>::max();
    for (lit_t s : portfolio)
      if (dat[s] < min)
        min = dat[s];
    totalScore += min;
  }
  return totalScore;
}

void SolverPortfolioApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "SolverPortfolioApp Stats\n";
  INFO << LOG_H1;

  INFO << "#solvers " << getNSolvers() << "\n";
  INFO << "#instances = " << getNInstances() << "\n";

  Application::printStats();
}
} // namespace SeeSaw