/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _cplex_hpp_INCLUDED
#define _cplex_hpp_INCLUDED

#include <ilcplex/cplexx.h>
#include <limits>

#include "hittingsetsolver.hpp"
#include "logging.hpp"
#include "resources.hpp"
#include "types.hpp"

namespace SeeSaw {

// CPLEX interface for minimal hitting set solving
class CplexHittingSetSolver : public HittingSetSolver {
protected:
  // API handles
  CPXENVptr env{};
  CPXLPptr mip{};

  const double absGap = 0.75;

  // Variable mappings
  const BiOptSat::var_t UNDEF = std::numeric_limits<uint32_t>::max();
  std::vector<BiOptSat::var_t> ex2inMap;
  std::vector<bool>
      exPositive; // Wether the external variable is positive or negative

  void ensureMapping(BiOptSat::var_t ex, bool pos = true);
  void addNewVar(BiOptSat::var_t ex);
  double getSolution(std::vector<BiOptSat::lit_t> &solution);
  int32_t solve_(std::vector<BiOptSat::lit_t> &solution);

  // Stats
  bool solverValid = true;

  // Options
  bool printOutput{};  // Wether to print CPLEX output
  uint32_t nThreads{}; // The number of threads for CPLEX to use
  uint32_t popNSoln{}; // The size of the CPLEX population pool
  bool dataCheck{};    // Wether to run CPLEX data checker on its input

  // Error processing
  void processError(int, bool, const char *);

public:
  CplexHittingSetSolver();
  CplexHittingSetSolver(std::unordered_map<BiOptSat::lit_t, uint32_t> &);
  ~CplexHittingSetSolver();
  void init();

  // Adding constraints
  void addCore(const BiOptSat::clause_t &core);

  // Stats getter
  inline bool isValid() const { return solverValid; }
  virtual void printOptions() const;

  // Option getter and setter (Options need to be set before init method is
  // called)
  bool doesPrintOutput() const { return printOutput; }
  void setPrintOutput(bool po) { printOutput = po; }
  uint32_t getNThreads() const { return nThreads; }
  void setNThreads(uint32_t nt) { nThreads = nt; }
  uint32_t getPopNSoln() const { return popNSoln; }
  void setPopNSoln(uint32_t pns) { popNSoln = pns; }
  bool hasDataChecker() const { return dataCheck; }
  void setDataChecker(bool dc) { dataCheck = dc; }
};
} // namespace SeeSaw

#endif
