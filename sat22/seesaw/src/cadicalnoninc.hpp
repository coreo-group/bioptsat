/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _cadicalnoninc_hpp_INCLUDED
#define _cadicalnoninc_hpp_INCLUDED

#include "cadical.hpp"

#include <cstdint>
#include <vector>

#include "nonincsatsolver.hpp"
#include "types.hpp"

namespace BiOptSat {
// A CaDiCaL based backend rebuilding the solver for every solve
// Used for comparing to non-incremental solving
class CadicalNonIncSolver : public NonIncSatSolver {
private:
  CaDiCaL::Solver *backend{};

public:
  inline bool varModelValue(var_t var) { return backend->val(var) > 0; }
  SolverState solve(const std::vector<lit_t> &);
};
} // namespace BiOptSat

#endif
