/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _satsolver_hpp_INCLUDED
#define _satsolver_hpp_INCLUDED

#include <vector>

#include "logging.hpp"
#include "resources.hpp"
#include "types.hpp"

namespace BiOptSat {

enum SolverState {
  UNDEF = 1, // Neither SAT nor UNSAT found
  SAT = 2,
  UNSAT = 4,
};

// Abstract base class as interface to different SAT solvers
class SatSolver {
private:
  // Variables for timers
  double realStartTime{};
  double cpuStartTime{};

protected:
  // Private variables
  uint32_t nSolves{};      // Number of solver calls
  uint32_t nSatSolves{};   // Number of SAT solver calls
  uint32_t nUnsatSolves{}; // Number of UNSAT solver calls
  double realSolveTime{};  // Total wall clock time spend solving
  double cpuSolveTime{};   // Total cpu time spend solving
  uint32_t nClauses{};     // Number of clauses in the solver
  uint32_t nVars{};        // Number of variables in the solver
  clause_t conflict{}; // Conflict to be populated by inheriting class if solver
                       // call is UNSAT
  double avgClauseLen{}; // Average clause length
  // Timer functions
  inline void startTimer() {
    realStartTime = absolute_real_time();
    cpuStartTime = absolute_process_time();
  }
  inline void stopTimer() {
    double realTime = absolute_real_time() - realStartTime;
    double cpuTime = absolute_process_time() - cpuStartTime;
    realSolveTime += realTime;
    cpuSolveTime += cpuTime;
    if (verbose >= 3) {
      INFO << "Query time: real time=" << realTime << "; cpu time=" << cpuTime
           << "\n";
      INFO << "Total solver time: real time=" << realSolveTime
           << "; cpu time=" << cpuSolveTime << "\n";
    }
  }

  // Options
  uint32_t verbose{}; // The verbosity level of output

public:
  SatSolver();
  virtual ~SatSolver(){};
  // Get variable that was not previously used
  virtual inline var_t getNewVar() { return ++nVars; }
  // Adding clauses
  virtual void addClause(const clause_t &) = 0;
  void addClause(lit_t l1) {
    clause_t c{l1};
    addClause(c);
  }
  void addClause(lit_t l1, lit_t l2) {
    clause_t c{l1, l2};
    addClause(c);
  }
  void addClause(lit_t l1, lit_t l2, lit_t l3) {
    clause_t c{l1, l2, l3};
    addClause(c);
  }
  // Getting model values
  // In the model, the 0th index will always be false
  virtual bool varModelValue(
      var_t) = 0; // To be implemented by concrete solver interface class
  inline bool litModelValue(lit_t l) {
    return l > 0 ? varModelValue(var(l)) : !varModelValue(var(l));
  }
  inline void getModel(model_t &outModel) {
    outModel.resize(nVars + 1);
    outModel[0] = false;
    for (var_t i = 1; i <= nVars; i++)
      outModel[i] = varModelValue(i);
  }
  // Solving
  virtual SolverState
  solve(const std::vector<lit_t>
            &) = 0; // To be implemented by concrete solver instance class.
                    // Needs to populate conflict if call is UNSAT.
  inline SolverState solve() {
    std::vector<lit_t> assumps{};
    return solve(assumps);
  }
  void getConflict(clause_t &outConflict) { outConflict = conflict; }
  // Adjust phase (default value) of variable
  virtual void phase(lit_t) = 0;
  virtual void unphase(lit_t) = 0;

  void printStats() {
    INFO << "\n";
    INFO << "\n";
    INFO << "Sat Solver Stats\n";
    INFO << LOG_H1;

    INFO << "\n";
    INFO << "#solves = " << nSolves << "\n";
    INFO << "#SATsolves = " << nSatSolves << "\n";
    INFO << "#UNSATsolves = " << nUnsatSolves << "\n";
    INFO << "#clauses = " << nClauses << "\n";
    INFO << "#vars = " << nVars << "\n";
    INFO << "avg. clause length = " << avgClauseLen << "\n";

    INFO << "\n";
    INFO << "Times\n";
    INFO << LOG_H2;
    INFO << "Wall clock time: " << realSolveTime << "s\n";
    INFO << "Cpu time: " << cpuSolveTime << "s\n";
  }

  // Stats getter
  inline uint32_t getNSolves() const { return nSolves; }
  inline uint32_t getNSatSolves() const { return nSatSolves; }
  inline uint32_t getNUnsatSolves() const { return nUnsatSolves; }
  inline double getRealSolveTime() const { return realSolveTime; }
  inline double getCpuSolveTime() const { return cpuSolveTime; }
  inline uint32_t getNClauses() const { return nClauses; }
  inline uint32_t getNVars() const { return nVars; }
  inline double getAvgClauseLen() const { return avgClauseLen; }

  // Option getter and setter
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
};

// Satsolver factory to generate fitting satsolver class from string
SatSolver *solverFactory(std::string id = ""); // Empty string for default
} // namespace BiOptSat

#endif