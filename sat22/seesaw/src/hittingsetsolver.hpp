/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _hittingsetsolver_hpp_INCLUDED
#define _hittingsetsolver_hpp_INCLUDED

#include <cassert>
#include <unordered_map>
#include <vector>

#include "logging.hpp"
#include "resources.hpp"
#include "types.hpp"

namespace SeeSaw {

// Abstract base class as interface to different Hitting Set Solvers
class HittingSetSolver {
private:
  // Variables for timers
  double realStartTime{};
  double cpuStartTime{};

protected:
  std::unordered_map<BiOptSat::lit_t, uint32_t> weights{};

  // Stats
  uint32_t nSolves{};
  uint32_t nVars{};
  uint32_t nCores{};
  double realSolveTime{};
  double cpuSolveTime{};

  // Options
  uint32_t verbose{}; // The verbosity level of output

  // Timer functions
  inline void startTimer() {
    realStartTime = BiOptSat::absolute_real_time();
    cpuStartTime = BiOptSat::absolute_process_time();
  }
  inline void stopTimer() {
    double realTime = BiOptSat::absolute_real_time() - realStartTime;
    double cpuTime = BiOptSat::absolute_process_time() - cpuStartTime;
    realSolveTime += realTime;
    cpuSolveTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Query time: real time=" << realTime << "; cpu time=" << cpuTime
           << "\n";
      INFO << "Total solver time: real time=" << realSolveTime
           << "; cpu time=" << cpuSolveTime << "\n";
    }
  }

  // Main solving routine
  // The parameter is the returned hitting set
  virtual int32_t solve_(std::vector<BiOptSat::lit_t> &solution) = 0;

public:
  HittingSetSolver();
  HittingSetSolver(std::unordered_map<BiOptSat::lit_t, uint32_t> &);
  virtual ~HittingSetSolver() {}

  // Interface
  virtual void init() {} // Initializing the solver (needed for CPLEX)
  int32_t solve(std::vector<BiOptSat::lit_t> &solution) {
    startTimer();
    int32_t val = solve_(solution);
    stopTimer();
    nSolves++;
#ifndef UDEBUG
    uint32_t solWeight{};
    for (BiOptSat::lit_t l : solution) {
      std::unordered_map<BiOptSat::lit_t, uint32_t>::const_iterator search{};
      search = weights.find(l);
      if (search != weights.end())
        solWeight += search->second;
      else
        solWeight++;
    }
#endif
    assert(val == -1 || solWeight == static_cast<uint32_t>(val));
    return val;
  }
  virtual void addCore(const BiOptSat::clause_t
                           &core) = 0; // A core is a set that needs to be hit

  virtual void printStats() const;
  virtual void printOptions() const;

  // Stats getter
  inline uint32_t getNSolves() const { return nSolves; }
  inline double getRealSolveTime() const { return realSolveTime; }
  inline double getCpuSolveTime() const { return cpuSolveTime; }
  inline uint32_t getNVars() const { return nVars; }
  inline uint32_t getNCores() const { return nCores; }

  // Option getter and setter
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
};

HittingSetSolver *hittingSetSolverFactory(std::string id);
HittingSetSolver *
hittingSetSolverFactory(std::string id,
                        std::unordered_map<BiOptSat::lit_t, uint32_t> &weights);

} // namespace SeeSaw

#endif