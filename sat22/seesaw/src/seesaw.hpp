/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _seesaw_hpp_INCLUDED
#define _seesaw_hpp_INCLUDED

#include <functional>
#include <unordered_map>

#include "hittingsetsolver.hpp"
#include "types.hpp"

namespace SeeSaw {

enum AlgPhase { PHASE_NONE, PHASE_HS, PHASE_ORACLE, PHASE_EXCORE };
enum AlgState { INIT, SOLVING, SOLVED, BOUND_REACHED };

// Oracle function
// Parameters: <universe> <hitting set> <upper bound>
// If the true oracle value is higher than (upper bound), the oracle may return
// (upper bound + 1)
typedef std::function<uint32_t(const BiOptSat::Objective &,
                               const std::vector<BiOptSat::lit_t> &, uint32_t)>
    OracleFunc;
// Core extraction function
// Parameters: <universe> <hitting set> <oracle of hitting set> <oracle
// function> <ret: set of cores>
typedef std::function<void(const BiOptSat::Objective &,
                           const std::vector<BiOptSat::lit_t> &, uint32_t,
                           OracleFunc, std::vector<BiOptSat::clause_t> &)>
    CoreExtFunc;

void defaultCoreExtFunc(const BiOptSat::Objective &,
                        const std::vector<BiOptSat::lit_t> &, uint32_t,
                        OracleFunc, std::vector<BiOptSat::clause_t> &);
void antiMonCoreExtFunc(const BiOptSat::Objective &,
                        const std::vector<BiOptSat::lit_t> &, uint32_t,
                        OracleFunc, std::vector<BiOptSat::clause_t> &);

// Class implementing the SeeSaw algorithm
// For now, the only supported cost function is the cardinality of the hitting
// set
class SeeSawEnumerator {
private:
  double realStartTime{};
  double cpuStartTime{};

protected:
  HittingSetSolver *hsSolver{};         // The hitting set solver to use
  const BiOptSat::Objective universe{}; // The universe of the given problem

  AlgState state{INIT};       // The current state of the algorithm
  AlgPhase phase{PHASE_NONE}; // The current phase/subroutine of the algorithm

  uint32_t nSols{};       // The number of found solutions
  uint32_t nCandidates{}; // The number of candidates considers
  double avgCoreSize{};   // The average core size

  BiOptSat::ParetoFront<std::vector<BiOptSat::lit_t>>
      paretoFront; // The found pareto front

  OracleFunc oracle{}; // The oracle function of the problem
  CoreExtFunc coreExt =
      defaultCoreExtFunc; // The function to use for extracting cores

  // Time variables and functions
  double totalRealTime{}; // The total wall clock time spend in the algorithm
  double totalCpuTime{};  // The total cpu time spend in the algorithm
  double hsRealTime{}; // Wall clock time spend solving the hitting set problem
  double hsCpuTime{};  // Cpu time spend solving the hitting set problem
  double oracleRealTime{}; // Wall clock time spend in the oracle
  double oracleCpuTime{};  // Cpu time spend in the oracle
  double exCoreRealTime{}; // Wall clock time spend extracting cores
  double exCoreCpuTime{};  // Cpu time spend extracting cores

  void startTimer(); // Start the wall and cpu timers
  void stopTimer();  // Stop the timers. Make sure phase is set correctly first.

  // Options
  uint32_t verbose{};     // The verbosity level of output
  int32_t maxSols{};      // Maximum number of solutions
  int32_t maxPPs{};       // Maximum number of pareto points
  int32_t maxDownwards{}; // Maximum value for the downwards objective
  int32_t minDownwards{}; // Minimum value for the downwards objective

public:
  SeeSawEnumerator(const BiOptSat::Objective &universe, OracleFunc oracle,
                   std::string backend = "");
  SeeSawEnumerator(const BiOptSat::Objective &universe,
                   std::unordered_map<BiOptSat::lit_t, uint32_t> &weights,
                   OracleFunc oracle, std::string backend = "");
  ~SeeSawEnumerator();

  void enumerate();
  void setCoreExtFunc(CoreExtFunc cef) { coreExt = cef; }
  void printStats() const;
  void printOptions() const;
  inline void interrupt() { stopTimer(); }

  // Stats getter
  AlgState getAlgState() const { return state; }
  BiOptSat::ParetoFront<std::vector<BiOptSat::lit_t>> getParetoFront() const {
    return paretoFront;
  }
  uint32_t getNCores() const {
    if (hsSolver)
      return hsSolver->getNCores();
    return 0;
  }
  uint32_t getNHsSolves() const {
    if (hsSolver)
      return hsSolver->getNSolves();
    return 0;
  }
  uint32_t getNSols() const { return nSols; }
  double getAvgCoreSize() const { return avgCoreSize; }

  // Option getter and setter (don't change options after starting to enumerate)
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
  int32_t getMaxSols() const { return maxSols; }
  void setMaxSols(int32_t ms) { maxSols = ms; }
  int32_t getMaxPPs() const { return maxPPs; }
  void setMaxPPs(int32_t mp) { maxPPs = mp; }
  int32_t getMaxDownwards() const { return maxDownwards; }
  void setMaxDownwards(int32_t md) { maxDownwards = md; }
  int32_t getMinDownwards() const { return minDownwards; }
  void setMinDownwards(int32_t md) { minDownwards = md; }
};

} // namespace SeeSaw

#endif