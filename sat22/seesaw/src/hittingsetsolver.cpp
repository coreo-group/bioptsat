/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "hittingsetsolver.hpp"

#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if WITH_CPLEX
#include "cplex.hpp"
#endif

using namespace BiOptSat;

namespace SeeSaw {
// Options
static std::string cat = "2.3 - HittingSetSolver";
static StringOption opt_backend(cat, "hs-backend",
                                "The hitting set backend to use", "cplex",
                                StringSet({
#if WITH_CPLEX
                                  "cplex",
#endif
                                }));

HittingSetSolver *hittingSetSolverFactory(std::string id) {
  if (id == "")
    id = opt_backend;
#if WITH_CPLEX
  if (id == "cplex")
    return new CplexHittingSetSolver();
#endif
  ERROR << "Unknown solver " << id << "\n";
  exit(1);
}

HittingSetSolver *
hittingSetSolverFactory(std::string id,
                        std::unordered_map<lit_t, uint32_t> &weights) {
  if (id == "")
    id = opt_backend;
#if WITH_CPLEX
  if (id == "cplex")
    return new CplexHittingSetSolver(weights);
#endif
  ERROR << "Unknown solver " << id << "\n";
  exit(1);
}

HittingSetSolver::HittingSetSolver() : verbose(opt_verbose) {}

HittingSetSolver::HittingSetSolver(std::unordered_map<lit_t, uint32_t> &weights)
    : weights(weights), verbose(opt_verbose) {}

void HittingSetSolver::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Hitting Set Solver Stats\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "#solves = " << nSolves << "\n";
  INFO << "#vars = " << nVars << "\n";
  INFO << "#cores = " << nCores << "\n";
  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Total wall clock time: " << realSolveTime << "s\n";
  INFO << "Total Cpu time: " << cpuSolveTime << "s\n";
}

void HittingSetSolver::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Hitting Set Solver Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
}
} // namespace SeeSaw