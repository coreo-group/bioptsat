/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "dimacsapp.hpp"

#include <algorithm>
#include <fstream>

#include "dataset.hpp"
#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"
#include "utils.hpp"

using namespace BiOptSat;

namespace SeeSaw {
// Options
static std::string cat = "3.3 - DIMACS";
static BoolOption
    opt_bicnf(cat, "dimacs-old-fmt",
              "Parse the old bicnf format rather than the new mcnf", false);

DimacsApp::DimacsApp()
    : oldBicnf(opt_bicnf), incObjIdx(opt_inc_obj), decObjIdx(opt_dec_obj) {}

void DimacsApp::formatSolution(const std::vector<lit_t> &hittingSet,
                               std::vector<string> &outLines) const {
  outLines.clear();
  auto search = hittingSetModelMap.find(hittingSet);
  if (search == hittingSetModelMap.end()) {
    outLines.push_back("n/a");
    return;
  }
  model_t model = search->second;

  string buf = "";
  for (uint32_t i = 1; i < model.size(); i++)
    buf += (model[i] ? "1" : "0");
  outLines.push_back(buf);
}

void DimacsApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "DimacsApp Stats\n";
  INFO << LOG_H1;

  INFO << "#clauses = " << nClauses << "\n";
  INFO << "#vars = " << nVars << "\n";

  satSolver->printStats();

  Application::printStats();
}

void DimacsApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "DimacsApp Options\n";
  INFO << LOG_H1;

  std::string minCore{};
  switch (coreMinVariant) {
  case CM_NONE:
    minCore = "NONE";
    break;
  case CM_EXACT:
    minCore = "EXACT";
    break;
  case CM_HEURISTIC:
    minCore = "HEURISTIC";
    break;
  }

  INFO << "\n";
  INFO << "coreMinVariant = " << minCore << "\n";
  Application::printOptions();
}

void DimacsApp::loadInstance(std::string path) {
  instance = path;

  if (verbose >= 0)
    INFO << "Loading instance " << instance << "\n";

  solved = false;
  if (enumerator) {
    delete enumerator;
    enumerator = nullptr;
  }
  if (satSolver)
    delete satSolver;
  satSolver = solverFactory();

  std::pair<std::vector<std::pair<uint64_t, clause_t>>,
            std::vector<std::pair<uint64_t, clause_t>>>
      softClauses{};

  nVars = 0;

  // Parse file
  std::string line{};
  std::vector<std::string> toks{};
  std::fstream file;
  file.open(instance, std::fstream::in);
  std::string incObjTok = "o" + std::to_string(incObjIdx);
  std::string decObjTok = "o" + std::to_string(decObjIdx);

  while (getline(file, line)) {
    if (line[0] == 'c')
      continue;
    size_t pos = 0;
    toks.clear();
    while ((pos = line.find(" ")) != std::string::npos) {
      toks.push_back(line.substr(0, pos));
      line.erase(0, pos + 1);
    }
    toks.push_back(line);
    assert(toks.size() >= 1);
    if (toks[0] == "h") {
      // Parse clause
      assert(toks.back() == "0");
      clause_t cl{};
      lit_t l{};
      for (uint32_t i = 1; i < toks.size() - 1; i++) {
        l = std::stoi(toks[i]);
        if (var(l) > nVars)
          nVars = var(l);
        cl.push_back(l);
      }
      satSolver->addClause(cl);
      continue;
    }
    if (oldBicnf) {
      if (toks[0] == "1") {
        // Parse upwards objective
        assert(toks.size() == 4);
        uint64_t w = std::stoi(toks[1]);
        lit_t l = std::stoi(toks[2]);
        if (var(l) > nVars)
          nVars = var(l);
        universe.push_back(l);
        weights[l] = w;
        continue;
      }
      if (toks[0] == "2") {
        // Parse downwards objective
        assert(toks.size() == 4);
        uint64_t w = std::stoi(toks[1]);
        lit_t l = std::stoi(toks[2]);
        if (var(l) > nVars)
          nVars = var(l);
        for (uint64_t i = 0; i < w; i++)
          decreasing.push_back(l);
        continue;
      }
      ERROR << "Encountered unexpected line\n";
      exit(1);
    } else {
      if (toks[0] == incObjTok) {
        // Parse increasing objective soft _clause_
        assert(toks.size() >= 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        if (toks.size() == 4) {
          // Soft clause is unit, do not need to add blocking lit
          lit_t l = -std::stoi(toks[2]);
          if (var(l) > nVars)
            nVars = var(l);
          universe.push_back(l);
          weights[l] = w;
          continue;
        }
        // True soft _clause_
        clause_t cl{};
        for (size_t i = 2; i < toks.size() - 1; ++i) {
          lit_t l = std::stoi(toks[i]);
          cl.push_back(l);
          if (var(l) > nVars)
            nVars = var(l);
        }
        softClauses.first.emplace_back(std::make_pair(w, cl));
        continue;
      }
      if (toks[0] == decObjTok) {
        // Parse increasing objective soft _clause_
        assert(toks.size() >= 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        if (toks.size() == 4) {
          // Soft clause is unit, do not need to add blocking lit
          lit_t l = -std::stoi(toks[2]);
          if (var(l) > nVars)
            nVars = var(l);
          for (uint64_t i = 0; i < w; i++)
            decreasing.push_back(l);
          continue;
        }
        // True soft _clause_
        clause_t cl{};
        for (size_t i = 2; i < toks.size() - 1; ++i) {
          lit_t l = std::stoi(toks[i]);
          cl.push_back(l);
          if (var(l) > nVars)
            nVars = var(l);
        }
        softClauses.second.emplace_back(std::make_pair(w, cl));
        continue;
      }
    }
  }

  // Add blocking literals to soft clauses
  for (std::pair<uint64_t, clause_t> wcl : softClauses.first) {
    wcl.second.push_back(++nVars);
    satSolver->addClause(wcl.second);
    universe.push_back(nVars);
    weights[nVars] = wcl.first;
  }
  for (std::pair<uint64_t, clause_t> wcl : softClauses.second) {
    wcl.second.push_back(++nVars);
    satSolver->addClause(wcl.second);
    for (uint64_t i = 0; i < wcl.first; i++)
      decreasing.push_back(nVars);
  }

  nClauses = satSolver->getNClauses();

  // Track number of variables in solver in case variable is only in objective
  while (satSolver->getNVars() < nVars)
    satSolver->getNewVar();

  std::sort(universe.begin(), universe.end(),
            [](lit_t l1, lit_t l2) { return var(l1) < var(l2); });
  minIncreasing = var(universe.front());
  maxIncreasing = var(universe.back());

  // Add Totalizer
  if (tot)
    delete tot;
  tot = new Totalizer(satSolver, CARD_UB);
  tot->build(decreasing, 1);

  finalizeLoading();

  enumerator->setMaxDownwards(decreasing.size());
}

uint32_t DimacsApp::oracle(const Objective &universe,
                           const std::vector<lit_t> &hittingSet,
                           uint32_t upperBound) {
  if (upperBound > tot->getNLits())
    upperBound = tot->getNLits();
  std::vector<lit_t> sortedHittingSet = hittingSet;
  std::sort(sortedHittingSet.begin(), sortedHittingSet.end());
  std::vector<lit_t> assumps{};
  std::vector<lit_t> baseAssumps{};
  uint32_t j = 0;
  for (uint32_t i = 0; i < universe.size(); i++) {
    if (j < sortedHittingSet.size() && universe[i] == sortedHittingSet[j])
      j++;
    else
      baseAssumps.push_back(-universe[i]);
  }
  assumps = baseAssumps;
  if (upperBound < tot->getNLits()) {
    tot->updateUpper(upperBound);
    assumps.push_back(-tot->getOutLit(upperBound));
  }

  SolverState ret = satSolver->solve(assumps);
  assert(ret != UNDEF);
  if (ret == UNSAT) {
    // No solution smaller than upper bound found
    return upperBound + 1;
  }

  assert(getCurrentIncreasing() <= hittingSet.size());

  // Save model
  model_t model(nVars + 1, false);
  for (uint32_t i = 1; i <= nVars; i++)
    model[i] = satSolver->varModelValue(i);
  uint32_t bound = getCurrentDecreasing();

  tot->updateUpper(bound);

  // Sat-Unsat minimal search
  while (bound > 0) {
    if (verbose >= 3)
      INFO << "oracle minimization iteration, bound=" << bound << "\n";
    assumps = baseAssumps;
    assumps.push_back(-tot->getOutLit(bound - 1));
    ret = satSolver->solve(assumps);
    if (ret == UNSAT)
      break;
    assert(ret == SAT);
    // Save model
    for (uint32_t i = 1; i <= nVars; i++)
      model[i] = satSolver->varModelValue(i);
    bound = getCurrentDecreasing();
  }

  // Save current model for printing
  hittingSetModelMap[hittingSet] = model;

  return bound;
};

void DimacsApp::coreExt(const Objective &uni,
                        const std::vector<lit_t> &hittingSet, uint32_t f,
                        OracleFunc o, std::vector<clause_t> &cores) {
  if (f == 0) {
    // If f == 0, terminate
    cores.clear();
    cores.push_back({});
    return;
  }

  clause_t core{};
  satSolver->getConflict(core);
  minimizeCore(satSolver, core);
  clause_t cleanedCore{};
  for (lit_t l : core) {
    if (var(l) >= minIncreasing && var(l) <= maxIncreasing) {
      assert(l > 0);
      cleanedCore.push_back(l);
    }
  }

  cores.clear();
  cores.push_back(cleanedCore);
}

// Helper functions
uint32_t DimacsApp::getCurrentIncreasing() {
  uint32_t value{};
  for (var_t v : universe)
    if (satSolver->varModelValue(v))
      value++;
  return value;
}

uint32_t DimacsApp::getCurrentDecreasing() {
  uint32_t value{};
  for (var_t v : decreasing)
    if (satSolver->varModelValue(v))
      value++;
  return value;
}
} // namespace SeeSaw
