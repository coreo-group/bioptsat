/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _types_hpp_INCLUDED
#define _types_hpp_INCLUDED

#include <cstdint>
#include <vector>

namespace BiOptSat {
// Types used throughout the program
// SAT Types
typedef int32_t lit_t;
typedef uint32_t var_t;
typedef std::vector<lit_t> clause_t;
typedef std::vector<bool> model_t;
// Pareto Front Types
template <typename T>
struct ParetoPoint {
  std::vector<T> models;
  uint32_t incVal;
  uint32_t decVal;
};
template <typename T>
using ParetoFront = std::vector<ParetoPoint<T>>;
typedef std::vector<lit_t> Objective;
// Helper functions
inline var_t var(lit_t l) { return l > 0 ? l : -l; }
inline lit_t lit(var_t v) { return static_cast<lit_t>(v); }
} // namespace BiOptSat

#endif