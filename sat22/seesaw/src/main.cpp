/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <iostream>
#include <signal.h>

#include "application.hpp"
#include "logging.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

using namespace BiOptSat;
using namespace SeeSaw;

// Options
static std::string cat = "1 - Main";
static BoolOption opt_version(cat, "version", "Print version number and exit",
                              false);
static BoolOption opt_print_options(cat, "print-options",
                                    "Print the set options", true);
static BoolOption opt_solve(
    cat, "solve",
    "Option to turn off solving. Might be useful for just dumping encoding.",
    true);

static Application *theapp{};

static void SIGINT_exit(int signum) {
  if (theapp) {
    theapp->interrupt();
    theapp->dumpParetoFront();
    theapp->printParetoFront();
    theapp->printStats();
  }
  INFO << "Interrupt signal: " << signum << "\n";
  std::cout << std::flush;
  std::cerr << std::flush;
  _Exit(1);
}

int main(int argc, char **argv) {
  setUsageHelp("USAGE: %s [options] <input-file>\n  where input is in "
               "appropriate format for the selected application");
  parseOptions(argc, argv, true);

  if (opt_version) {
    std::cout << "SeeSaw Sat Program\n";
    std::cout << LOG_H1;
#ifdef HAVE_CONFIG_H
    std::cout << PACKAGE_STRING << "\n";
    std::cout << PACKAGE_BUGREPORT << "\n";
#endif
    return 0;
  }
  INFO << "SeeSaw Sat Program\n";
  INFO << LOG_H1;
#ifdef HAVE_CONFIG_H
  INFO << PACKAGE_STRING << "\n";
#endif

  signal(SIGINT, SIGINT_exit);
  signal(SIGXCPU, SIGINT_exit);
  signal(SIGSEGV, SIGINT_exit);
  signal(SIGTERM, SIGINT_exit);
  signal(SIGABRT, SIGINT_exit);

  if (argc < 2) {
    ERROR << "No input file specified\n";
    exit(0);
  }

  theapp = appFactory();

  theapp->loadInstance(argv[1]);
  if (opt_solve)
    theapp->solve();

  if (theapp) {
    theapp->printStats();
    delete theapp;
  }
}