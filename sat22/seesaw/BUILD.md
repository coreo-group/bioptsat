# Building the Project

To build the project, do the following steps:

- Pull submodules: `git submodule update --init --recursive`
- Build submodules as libraries
  - Cadical: `./configure && make` in `lib/cadical/`
- Build the project in subdirectory:
  - `mkdir build && cd build`
  - `CPLEXLIBDIR=<path to cplex lib> CPLEXINCDIR=<path to cplex headers> ../configure && make`

These steps will build the project as one binary in `build/src/seesawsat`

For debugging purposes it might be useful to configure the project with the command `CXXFLAGS="-g -O0 -UNDEBUG"` to have all symbols in the binary and all assertions working.

For development and if the build system is changed, it is useful to set the `--enable-maintainer-mode` so that the build system is automatically reconfigured at build, if required.

When using seesawsat as a dependency of another project, the generated static library `/build/src/libseesawsat.a` as well as the required libraries of the included solvers should be linked to the project.
