# MaxSAT-Based Bi-Objective Boolean Optimization - SAT'22

This directory contains additional data and source code for the following publication:

Christoph Jabs and Jeremias Berg and Andreas Niskanen and Matti Järvisalo (2022): _MaxSAT-Based Bi-Objective Boolean Optimization_, SAT'22

- `instances/`: all instance files used for benchmarking
- `seesaw/`: the implementation of the Seesaw algorithm used for comparison
- `experiment-results/`: runtime experiment results