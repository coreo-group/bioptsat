# BiOptSat - MaxSAT-based Bi-Objective Boolean Optimization

This is a tool for enumerating pareto-optimal solutions to the SAT problem where the two objectives are defined as two sets of literals.
The tool includes 3 different enumeration algorithms, some of them with additional variants.

## Related Publications

The [initial version](https://bitbucket.org/coreo-group/bioptsat/commits/tag/sat22) of BiOptSat was published for our SAT'22 paper "_MaxSAT-Based Bi-Objective Boolean Optimization_".
The `sat22/` directory contains additional data and the implementation of the Seesaw algorithm used in the evaluation of BiOptSat.

An extended journal version of the SAT'22 paper is currently in final revisions for Journal of Artificial Intelligence Research.
The newest version of BiOptSat used in the journal version is available here, and extends the earlier version presented at SAT'22 in the following ways:

- Objectives are encoded with the pseudo-Boolean _generalized_ totalizer encoding rather than a cardinality encoding
- A new hybrid algorithmic variant (`oll-su-hybrid`) combining OLL and SAT-UNSAT search

Additional data related to the journal manuscript can be found in `jair24/`.

BiOptSat was additionally used in the our CP'23 paper "_Preprocessing in SAT-Based Multi-Objective Combinatorial Optimization_".

## Citing BiOptSat

If you want to cite BiOptSat, please cite the paper introducing it at SAT'22:

```
@InProceedings{JabsEtAl2022MaxSATBasedBi,
  author    = {Christoph Jabs and Jeremias Berg and Andreas Niskanen and Matti J{\"{a}}rvisalo},
  booktitle = {25th International Conference on Theory and Applications of Satisfiability Testing (SAT 2022), Haifa, Israel, August 2--5, 2022},
  title     = {{MaxSAT}-Based Bi-Objective Boolean Optimization},
  year      = {2022},
  editor    = {Kuldeep S. Meel and Ofer Strichman},
  pages     = {12:1--12:23},
  publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
  series    = {LIPIcs},
  volume    = {236},
  doi       = {10.4230/LIPIcs.SAT.2022.12},
}
```

## Enumerators

The algorithms are the following:

**BiOptSat**: Our lexicographic enumeration algorithm, published in the paper "MaxSAT-Based Bi-Objective Boolean Optimization" at SAT'22.

This enumerator has five main variants that differ in the implementation of the minimization of the downwards moving objective:
SAT-UNSAT and UNSAT-SAT are model improving approaches, either under- or overestimating the objective value.
MSU3 and OLL are core-guided algorithms known form MaxSAT solving that are adapted to the given minimization process.
MSU3 SAT-UNSAT hybrid is a variant that starts with MSU3 and as soon as all literals of the objective are active, switches over to SAT-UNSAT.

**P-minimal model enumeration**: Enumerating pareto-optimal solutions by enumerating P-minimal models following \[Soh2017\] and \[Koshimura2009\].
Instead of using the order encoding, we are using the outputs of totalizers as our variables to minimize over.

**Hybrid between P-minimal and BiOptSat**: A hybrid where every candidate during the search of the SAT-UNSAT variant of BiOptSat gets minimized as in P-minimal enumeration.
In practice this is very close to P-minimal model enumeration.

## Applications

The tool offers different applications which specify in which format the data is provided and what problem is solved.

**DIMACS application**: With this application, the problem is specified in a format similar to the WDIMACS format for MaxSAT.
The file format is the following:

- Lines starting with `h` are considered clauses.
- Lines starting with `1` are considered to be part of the upwards objective  (e.g. `1 <weight> <lit> 0`).
- Lines starting with `2` are considered to be part of the downwards objective (e.g. `2 <weight> <lit> 0`).
- Note that these are _soft literals_ meaning the negation of soft unit clauses in MaxSAT (if `<lit>` is assigned to _true_ it incurs a cost of `<weight>`).

**Interpretable decision rules application**: This application learns interpretable decision rules with the MLIC (\[Malioutov2018\]) encoding, where the two objectives are the size of the decision rules as the number of included literals and the classification error.
The data expected is a CSV data file where every row is a data sample and the last column are the labels.
The data is expected to be binary in 0 and 1.

**Interpretable decision tree application**: This application learns interpretable decision trees with the number of nodes and the classification error as the objectives.
The encoding follows \[Narodytska2018\] and \[Hu2020\].
The data is expected to be in the same format as for the interpretable decision rules.

**Decision rules for non-linear metrics application**: This application learns decision rules with the MLIC encoding.
The two objectives are false negative and positive classifications as in \[Demirovic2021\].
The data is expected to be in the same format as for the interpretable decision rules.

**Decision trees for non-linear metrics application**: This application learns decision rules under the objectives false positives and negatives.
The data is expected to be in the same format as for the interpretable decision rules.

**Biobjective Set-cover application**: This application solves biobjective set-cover problems.
By this we mean that there are two different weights associated with it and minimizing those two weights are the two objective.
The data is expected to be in the following format:

- First line: `<n vars> <m sets>`.
- Second line: `<k objectives>`. This needs to be 2.
- `k` next lines: objectives as list of (integer) weights.
- `2m` next lines: sets.
- First set line: cardinality of set.
- Second set line: elements in set.

**Relaxed Set-cover application**: This application solves the weighted set-cover problem where the second objective is the number of sets that are _not_ covered by the solution.
The data is expected to be in the same format as for biobjective set-cover except that instances with one or two objectives work.
If the instance has two objectives, the second one will be ignored.

## Backends

At the moment, the tool only includes the CaDiCaL solver as a backend.

## Options

The full list of options can be found from the tool itself by running it with `--help` or `--help-verbose`.
Here are just some of the tweaks that require some more explanation.

`--enumerator`: Select the used enumerator from `bioptsat`, `pminimal` or `hybrid`.

`--application`: Select the used application from `dimacs` (DIMACS), `interp-dec-rules` (interpretable decision rules), `interp-dec-trees` (interpretable decision trees), `non-lin-dec-rules` (decision rules for non-linear metrics), `non-lin-dec-trees` (decision trees for non-linear metrics), `set-cover` (biobjective set-cover).

`--minup-variant`: Select the variant for minimizing the upwards moving objective in BiOptSat from `sat-unsat`, `unsat-sat`, `msu3`, `oll` and `msu3-su-hybrid`.

`--phase-adjustment`: For supported backends, select if the literals from the objectives should have a preferred polarity for the SAT solvers search.
This can be either `none` for no preferred polarity, `negative` for the polarity that minimizes the objective or `positive` for the polarity that maximizes the objective.

`--block-north-east`: For the SAT-UNSAT variant of the BiOptSat enumerator, if a candidate is found, block all solutions that are worse in both objectives.

`--hybrid-immediate-switch`: For the MSU3 SAT-UNSAT hybrid variant of BiOptSat, whether the switch to SAT-UNSAT happens as soon as all literals are active or only in the next iteration.

`--hybrid-switch-portion`: The portion of objective literals that needs to be active for the hybrid to switch from MSU3 to SAT-UNSAT.
If 1.0, all literals need to be active, at 0.5 half of them.

`--hybrid-switch-max-time`: The maximum CPU time after which to switch to SAT-UNSAT (in seconds).

`--core-guided-lazy-down-tot`: For the core-guided variants of BiOptSat, whether to build to lazily build the downwards totalizer.
To lazily build the totalizer, literals that are in both objective are only added when they become active for the upwards objective.

`--msu3-fast-relax`: For the MSU3 variant of BiOptSat, if a literal appears multiple times in the upwards moving objective, if it will be immediately fully relaxed when it appears in a core once or if it needs to appear as many times as it is in the objective.

`--msu3-gcd-increment`: Whether to estimate the possible increments for weighted MSU3 with a running GCD over the weights of the active variables.

`--conf-exhaustion`: For the OLL variant of BiOptSat, whether to do conflict exhaustion is OLL or not.

`--conf-minimization`: For the core-guided variants of BiOptSat, what type of conflict minimization to use.
Possible values are: `none`, `exact` and `heuristic`.

`--pmin-bound`: For the hybrid enumerator, wether to continue the 'outer' BiOptSat loop from the previous candidate from that loop or from the model found by P-minimization.

`--dec-rule-symmetry-breaking`: Add symmetry breaking clauses that weren't in the original MLIC encoding.
They work by enforcing a lexicographical ordering on the bitstrings representing the clauses in the learned decision rule.

## References

- \[Soh2017\]: Takehide Soh and Mutsunori Banbara and Naoyuki Tamura and Daniel Le Berre (2017): _Solving Multiobjective Discrete Optimization Problems with Propositional Minimal Model Generation_, Springer.
- \[Koshimura2009\]: Miyuki Koshimura and Hidetomo Nabeshima and Hiroshi Fujita and Ryuzo Hasegawa (2009): _Minimal Model Generation with Respect to an Atom Set_, CEUR-WS.org.
- \[Malioutov2018\]: Dmitry Malioutov and Kuldeep S. Meel (2018): _MLIC: A MaxSAT-Based Framework for Learning Interpretable Classification Rules_, Springer International Publishing.
- \[Narodytska2018\]: Nina Narodytska and Alexey Ignatiev and Filipe Pereira and Joao Marques-Silva (2018): _Learning Optimal Decision Trees with SAT_, International Joint Conferences on Artificial Intelligence Organization.
- \[Hu2020\]: Hao Hu and Mohamed Siala and Emmanuel Hebrard and Marie-Jos&eacute and Huguet (2020): _Learning Optimal Decision Trees with MaxSAT and its Integration in AdaBoost_, International Joint Conferences on Artificial Intelligence Organization.
- \[Demirovic2021\]: Emir Demirovic and Peter J. Stuckey (2021): _Optimal Decision Trees for Nonlinear Metrics_, AAAI Press.
