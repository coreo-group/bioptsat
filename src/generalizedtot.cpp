/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "generalizedtot.hpp"

#include <iterator>
#include <set>

#include "options.hpp"

namespace BiOptSat {
// Options
static std::string cat = "5 - Encodings";
static BoolOption opt_reserve(cat, "gte-reserve-vars",
                              "Reverse variables in the solver in advance "
                              "rather than when they are defined",
                              true);
static BoolOption opt_cone_of_influence(
    cat, "gte-cone-of-influence",
    "Use the cone of influence optimization in the GTE encoding", true);

GeneralizedTotalizer::GeneralizedTotalizer(SatSolver *const solver,
                                           const BoundType boundType)
    : PBEncoding(solver, boundType), reserveVars(opt_reserve),
      coi(opt_cone_of_influence) {
  // For now the GTE only works for upper bounds
  if (boundType == BOUND_BOTH) {
    ERROR << "The generalized totalizer encoding is not implemented for lower "
             "and upper bounding at the same time\n";
    exit(1);
  }
}

GeneralizedTotalizer::Node *
GeneralizedTotalizer::buildTree(wlits_t::const_iterator start, size_t n) {
  assert(n > 0);
  // Recursion anchor: build leaf
  if (n == 1) {
    if (boundType == BOUND_LB)
      return new Node(this->solver, start->second, -start->first);
    else
      return new Node(this->solver, start->second, start->first);
  }

  // Equally split inputs
  wlits_t::const_iterator split = std::next(start, n / 2);
  Node *left = buildTree(start, n / 2);
  Node *right = buildTree(split, n / 2 + (n % 2 ? 1 : 0));

  return new Node(this->solver, left, right);
}

void GeneralizedTotalizer::_enforceUB(uint64_t b, std::vector<lit_t> &assumps) {
  assert(boundType == BOUND_UB);
  wlits_t extension{};
  uint64_t maxWeight{}; // Maximum weight of a literal that can be set
  // Find weighted literals to add to the tree
  // Also assume literals with weight > b to false
  for (const auto &p : wLits) {
    if (p.second <= b) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        it->second += wAdd;
      }
      // Find the maximum active leaf weight
      if (wAdd > maxWeight)
        maxWeight = wAdd;
      if (p.second - wAdd > maxWeight)
        maxWeight = p.second - wAdd;

      if (wAdd)
        extension[p.first] = wAdd;
    } else
      assumps.push_back(-p.first);
  }

  if (maxWeight == 0) {
    assert(extension.empty());
    // No input literals can be set, do not need to encode anything
    return;
  }

  addExtensionToTree(extension);

  if (!root)
    return;

  // Encode up to b + maxWeight.
  // If encoded that far, it is guaranteed that one of the output lits with
  // weight >= b will be set.
  std::pair<uint32_t, uint32_t> added;
  if (coi) {
    added = root->encode(b + 1, b + maxWeight);
  } else {
    added = root->encode(1, b + maxWeight);
  }
  nVars += added.first;
  nClauses += added.second;

  // No output lit in the range b+1 -- b+maxWeight can be set
  for (auto it = root->weights.upper_bound(b);
       it != root->weights.end() && it->first <= b + maxWeight; ++it)
    assumps.push_back(-it->second);
}

void GeneralizedTotalizer::_enforceLB(uint64_t b, std::vector<lit_t> &assumps) {
  assert(boundType == BOUND_LB);
  // Calculate enforced bound as sum_of_all_weights - b - 1
  uint64_t enforcedBound{};
  for (const auto &p : wLits)
    enforcedBound += p.second;
  enforcedBound -= b + 1;

  wlits_t extension{};
  uint64_t maxWeight{}; // Maximum weight of a literal that can be set
  // Find weighted literals to add to the tree
  // Assume literals with weight > enforcedBound to true
  for (const auto &p : wLits) {
    if (p.second <= enforcedBound) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        encodedWLits[p.first] += wAdd;
      }
      // Find the maximum active leaf weight
      if (wAdd > maxWeight)
        maxWeight = wAdd;
      if (p.second - wAdd > maxWeight)
        maxWeight = p.second - wAdd;

      extension[p.first] = wAdd;
    } else
      assumps.push_back(p.first);
  }

  if (maxWeight == 0) {
    assert(extension.empty());
    // No input literals can be set, do not need to encode anything
    return;
  }

  addExtensionToTree(extension);

  if (!root)
    return;

  // Encode up to enforcedBound + maxWeight.
  // If encoded that far, it is guaranteed that one of the output lits with
  // weight >= enforcedBound will be set.
  std::pair<uint32_t, uint32_t> added;
  if (coi) {
    added = root->encode(enforcedBound + 1, enforcedBound + maxWeight);
  } else {
    added = root->encode(1, enforcedBound + maxWeight);
  }
  nVars += added.first;
  nClauses += added.second;

  // No output lit in the range enforcedBound+1 -- enforcedBound+maxWeight can
  // be set
  for (auto it = root->weights.upper_bound(enforcedBound);
       it != root->weights.end() && it->first <= enforcedBound + maxWeight;
       ++it)
    assumps.push_back(-it->second);
}

uint64_t compute_required_lower(uint64_t requested_lower,
                                uint64_t sibling_max_val) {
  if (requested_lower > sibling_max_val) {
    return requested_lower - sibling_max_val;
  } else {
    return 1;
  }
}

GeneralizedTotalizer::Node::~Node() {
  if (left)
    delete left;
  if (right)
    delete right;
}

uint32_t GeneralizedTotalizer::Node::reserveVars(bool recurse) {
  // Recursion anchor: leaf does not need reservation
  if (isLeaf())
    return 0;

  uint32_t nVars{};

  // Recurse
  if (recurse) {
    nVars += left->reserveVars();
    nVars += right->reserveVars();
  }

  // Determine possible weights for node
  // Done as separate step so that vars are in order
  std::set<uint64_t> ws{};
  for (const auto &p : left->weights)
    ws.insert(p.first);
  for (const auto &p : right->weights)
    ws.insert(p.first);
  for (const auto &l : left->weights)
    for (const auto &r : right->weights)
      ws.insert(l.first + r.first);

  // Reserve variables
  for (const auto &w : ws) {
    weights[w] = solver->getNewVar();
    nVars++;
  }

  return nVars;
}

std::pair<uint32_t, uint32_t>
GeneralizedTotalizer::Node::generate_encoding(uint64_t _lower,
                                              uint64_t _upper) {
  assert(_lower <= _upper);
  assert(_lower > 0);

  // Encode adder
  // Only value propagation from leafs to the root is implemented.
  // Lower bounds are enforced by encoding the negation of the constraint.
  // For this reason, upper and lower bounds cannot be encoded at the same time.
  uint32_t nNewVars{};
  uint32_t nNewClauses{};

  // Propagate left value
  for (auto li = left->weights.upper_bound(_lower - 1);
       li != left->weights.end(); ++li) {
    if (li->first > _upper)
      break;
    auto oi = weights.find(li->first);
    if (oi == weights.end()) {
      auto r = weights.insert({li->first, solver->getNewVar()});
      nNewVars++;
      assert(r.second);
      oi = r.first;
    }
    solver->addClause(-li->second, oi->second);
    nNewClauses++;
    nClauses++;
  }
  // Propagate right value
  for (auto ri = right->weights.upper_bound(_lower - 1);
       ri != right->weights.end(); ++ri) {
    if (ri->first > _upper)
      break;
    auto oi = weights.find(ri->first);
    if (oi == weights.end()) {
      auto r = weights.insert({ri->first, solver->getNewVar()});
      nNewVars++;
      assert(r.second);
      oi = r.first;
    }
    solver->addClause(-ri->second, oi->second);
    nNewClauses++;
    nClauses++;
  }
  // Propagate sum
  for (auto li = left->weights.begin(); li != left->weights.end(); ++li) {
    if (li->first >= _upper)
      break;
    for (auto ri = right->weights.lower_bound(
             _lower - 1 > li->first ? _lower - li->first : 0);
         ri != right->weights.end(); ++ri) {
      uint64_t wt = li->first + ri->first;
      if (wt > _upper)
        break;
      auto oi = weights.find(wt);
      if (oi == weights.end()) {
        auto r = weights.insert({wt, solver->getNewVar()});
        nNewVars++;
        assert(r.second);
        oi = r.first;
      }
      solver->addClause(-li->second, -ri->second, oi->second);
      nNewClauses++;
      nClauses++;
    }
  }

  return std::make_pair(nNewVars, nNewClauses);
}

std::pair<uint32_t, uint32_t>
GeneralizedTotalizer::Node::encode(uint64_t _lower, uint64_t _upper,
                                   bool recurse) {
  assert(_lower <= _upper);

  // Recursion anchor: leaf does not need encoding
  if (isLeaf())
    return std::make_pair(0, 0);

  if ((upper >= _upper && lower <= _lower) || _lower > max_val)
    // Already encoded or requested higher than max_val
    return std::make_pair(0, 0);

  uint32_t nNewVars{};
  uint32_t nNewClauses{};
  std::pair<uint32_t, uint32_t> added;

  if (_upper > max_val) {
    _upper = max_val;
  }

  if (recurse) {
    // Recurse
    uint64_t lower_left = compute_required_lower(_lower, right->max_val);
    uint64_t lower_right = compute_required_lower(_lower, left->max_val);
    added = left->encode(lower_left, _upper, true);
    nNewVars += added.first;
    nNewClauses += added.second;
    added = right->encode(lower_right, _upper, true);
    nNewVars += added.first;
    nNewClauses += added.second;
  }

  // Encode new segments for this node
  if (upper == 0) {
    // No encoding so far
    assert(lower == 0);
    added = generate_encoding(_lower, _upper);
    nNewVars += added.first;
    nNewClauses += added.second;
    lower = _lower;
    upper = _upper;
  }
  if (_upper > upper) {
    added = generate_encoding(upper + 1, _upper);
    nNewVars += added.first;
    nNewClauses += added.second;
    upper = _upper;
  }
  if (_lower < lower) {
    added = generate_encoding(_lower, lower - 1);
    nNewVars += added.first;
    nNewClauses += added.second;
    lower = _lower;
  }

  return std::make_pair(nNewVars, nNewClauses);
}

uint64_t GeneralizedTotalizer::nextHigherPossible(uint64_t b) {
  if (boundType == BOUND_LB) {
    // Calculate enforced bound as sum_of_all_weights - b - 1
    uint64_t sum{};
    for (const auto &p : wLits)
      sum += p.second;
    b = sum - b - 1;

    // Add all vars to the tree
    wlits_t extension{};
    for (const auto &p : wLits) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        it->second += wAdd;
      }

      if (wAdd)
        extension[p.first] = wAdd;
    }
    addExtensionToTree(extension);

    assert(root);
    auto it = root->weights.lower_bound(b + 1);
    if (it == root->weights.begin())
      return 0;
    return sum - (--it)->first;
  } else {
    // Add all vars with weight <=b that are not encoded yet but part of the GTE
    // to the tree
    wlits_t extension{};
    uint64_t minNotEncoded = std::numeric_limits<uint64_t>::max();
    for (const auto &p : wLits) {
      if (p.second <= b) {
        auto it = encodedWLits.find(p.first);
        uint64_t wAdd{};
        if (it == encodedWLits.end()) {
          wAdd = p.second;
          encodedWLits[p.first] = wAdd;
        } else {
          wAdd = p.second - it->second;
          it->second += wAdd;
        }

        if (wAdd)
          extension[p.first] = wAdd;
      } else {
        if (p.second < minNotEncoded)
          minNotEncoded = p.second;
      }
    }
    addExtensionToTree(extension);

    if (!root)
      return minNotEncoded;

    const auto it = root->weights.upper_bound(b);

    if (it == root->weights.end())
      return minNotEncoded;

    return minNotEncoded > it->first ? it->first : minNotEncoded;
  }
}

uint64_t GeneralizedTotalizer::nextLowerPossible(uint64_t b) {
  if (boundType == BOUND_LB) {
    // Calculate enforced bound as sum_of_all_weights - b - 1
    uint64_t sum{};
    for (const auto &p : wLits)
      sum += p.second;
    b = sum - b - 1;

    // Add all vars with weight <=b that are not encoded yet but part of the GTE
    // to the tree
    wlits_t extension{};
    uint64_t minNotEncoded = std::numeric_limits<uint64_t>::max();
    for (const auto &p : wLits) {
      if (p.second <= b || boundType == BOUND_LB) {
        auto it = encodedWLits.find(p.first);
        uint64_t wAdd{};
        if (it == encodedWLits.end()) {
          wAdd = p.second;
          encodedWLits[p.first] = wAdd;
        } else {
          wAdd = p.second - it->second;
          it->second += wAdd;
        }

        if (wAdd)
          extension[p.first] = wAdd;
      } else {
        if (p.second < minNotEncoded)
          minNotEncoded = p.second;
      }
    }
    addExtensionToTree(extension);

    if (!root)
      return sum - minNotEncoded;

    const auto it = root->weights.upper_bound(b + 1);

    if (it == root->weights.end())
      return sum - minNotEncoded;

    return sum - (minNotEncoded > it->first ? it->first : minNotEncoded);
  } else {
    // Add all vars to the tree
    wlits_t extension{};
    for (const auto &p : wLits) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        it->second += wAdd;
      }

      if (wAdd)
        extension[p.first] = wAdd;
    }
    addExtensionToTree(extension);

    assert(root);
    auto it = root->weights.lower_bound(b);
    if (it == root->weights.begin())
      return 0;
    return (--it)->first;
  }
}

void GeneralizedTotalizer::addExtensionToTree(wlits_t &extension) {
  if (!root) {
    if (extension.empty())
      return;
    // Build tree
    root = buildTree(extension.begin(), extension.size());
    if (reserveVars)
      nVars += root->reserveVars();
  } else if (!extension.empty()) {
    // Extend tree
    Node *secondRoot = buildTree(extension.begin(), extension.size());
    root = new Node(this->solver, root, secondRoot);
    if (reserveVars) {
      nVars += secondRoot->reserveVars();
      nVars += root->reserveVars(false);
    }
  }
}

void GeneralizedTotalizer::printStats(bool header) const {
  if (header) {
    INFO << "\n";
    INFO << "\n";
    INFO << "GeneralizedTotalizer Stats\n";
    INFO << LOG_H1;
    INFO << "\n";
  }
  INFO << "depth = " << getDepth() << "\n";
  // TODO: print more GTE specific stats
  PBEncoding::printStats(false);
}

} // namespace BiOptSat