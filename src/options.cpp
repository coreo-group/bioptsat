/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "options.hpp"

#include <algorithm>
#include <cstdio>

// Options implementation inspired by Minisat
// (https://github.com/niklasso/minisat/blob/master/minisat/utils/Options.cc)

namespace BiOptSat {

void parseOptions(int &argc, char **argv, bool strict) {
  int i, j;
  for (i = j = 1; i < argc; i++) {
    const char *str = argv[i];
    if (match(str, "--") && match(str, Option::getHelpPrefixString()) &&
        match(str, "help")) {
      if (*str == '\0')
        printUsageAndExit(argc, argv);
      else if (match(str, "-verb"))
        printUsageAndExit(argc, argv, true);
    } else {
      bool parsed_ok = false;

      for (uint k = 0; !parsed_ok && k < Option::getOptionList().size(); k++)
        parsed_ok = Option::getOptionList()[k]->parse(argv[i]);

      if (!parsed_ok) {
        if (strict && match(argv[i], "--")) {
          std::cerr << "ERROR! Unknown flag \"" << argv[i] << "\". Use '--"
                    << Option::getHelpPrefixString() << "help' for help.\n";
          exit(1);
        } else
          argv[j++] = argv[i];
      }
    }
  }

  argc -= (i - j);
}

void setUsageHelp(const char *str) { Option::getUsageString() = str; }
void setHelpPrefixStr(const char *str) { Option::getHelpPrefixString() = str; }

void printUsageAndExit(int, char **argv, bool verbose) {
  const char *usage = Option::getUsageString();
  if (usage != nullptr)
    printf(usage, argv[0]);

  sort(Option::getOptionList().begin(), Option::getOptionList().end(),
       Option::OptionLt());

  string prev_cat{};
  string prev_type{};

  for (uint i = 0; i < Option::getOptionList().size(); i++) {
    string cat = Option::getOptionList()[i]->category;
    string type = Option::getOptionList()[i]->type_name;

    if (cat != prev_cat)
      std::cout << "\n" << cat << " OPTIONS:\n\n";
    else if (type != prev_type)
      std::cout << "\n";

    Option::getOptionList()[i]->help(verbose);

    prev_cat = cat;
    prev_type = type;
  }

  std::cout << "\nHELP OPTIONS:\n\n";
  std::cout << "  --" << Option::getHelpPrefixString()
            << "help            Print help message.\n";
  std::cout << "  --" << Option::getHelpPrefixString()
            << "help-verbose    Print verbose help message.\n\n";
  exit(0);
}

} // namespace BiOptSat