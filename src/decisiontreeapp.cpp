/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "decisiontreeapp.hpp"

#include <algorithm>

#include "dataset.hpp"
#include "encodings.hpp"
#include "logging.hpp"

namespace BiOptSat {
// Options
static std::string cat = "3.3 - Decision Trees";
static IntOption
    opt_max_nodes(cat, "dec-tree-max-nodes",
                  "The maximum number of nodes in the decision tree", 7,
                  IntRange(1, 20));

DecisionTreeApp::DecisionTreeApp(uint32_t _maxNodes) : maxNodes(_maxNodes) {
  if (maxNodes == 0)
    maxNodes = opt_max_nodes;

  if (maxNodes % 2 == 0) {
    WARN << "Setting maximum number of nodes to a n odd value\n";
    maxNodes--;
  }
}

vector<uint32_t> DecisionTreeApp::LR(uint32_t i) const {
  vector<uint32_t> lr;
  for (uint32_t j = i + 1; j < std::min(2 * (i + 1), maxNodes - 1); j++)
    if (j % 2 == 1)
      lr.push_back(j);
  return lr;
}

vector<uint32_t> DecisionTreeApp::RR(uint32_t i) const {
  vector<uint32_t> rr;
  for (uint32_t j = i + 2; j < std::min(2 * (i + 1) + 1, maxNodes); j++)
    if (j % 2 == 0)
      rr.push_back(j);
  return rr;
}

var_t DecisionTreeApp::isRightChild(uint32_t p, uint32_t c) const {
  unordered_map<pair<uint32_t, uint32_t>, var_t>::const_iterator search{};
  search = isLeftChild.find(make_pair(p, c - 1));
  assert(search != isLeftChild.end());
  return search->second;
}

var_t DecisionTreeApp::isParent(uint32_t c, uint32_t p) const {
  unordered_map<pair<uint32_t, uint32_t>, var_t>::const_iterator search{};
  if (c % 2 == 1) { // Child is left
    assert(c > p);
    assert(c < std::min(2 * (p + 1), maxNodes - 1));
    search = isLeftChild.find(make_pair(p, c));
    assert(search != isLeftChild.end());
    return search->second;
  }
  // Child is right
  assert(c > p + 1);
  assert(c < std::min(2 * (p + 1) + 1, maxNodes));
  return isRightChild(p, c);
}

void DecisionTreeApp::formatSolution(const model_t &model,
                                     vector<string> &outLines) const {
  uint32_t nNodes{};
  for (uint32_t i = 0; i < maxNodes; i += 2)
    if (model[nodeUsed[evenNodes(i)]])
      nNodes = i + 1;

  unordered_map<pair<uint32_t, uint32_t>, var_t>::const_iterator search{};
  string buf = "";
  uint32_t nextLevel = nNodes;

  for (uint32_t i = 0; i < nNodes; i++) {
    if (i >= nextLevel) {
      outLines.push_back(buf);
      buf = "";
      nextLevel = nNodes;
    }
    buf += (buf == "" ? "" : "; ") + std::to_string(i) + ":";
    if (model[isLeaf[i]])
      buf += (model[nodeClassTrue[i]] ? "(L,1)" : "(L,0)");
    else {
      buf += "(I,";
      // Find feature
      for (uint32_t j = 0; j < nFeatures; j++) {
        search = featureAssignNode.find(make_pair(j, i));
        assert(search != featureAssignNode.end());
        if (model[search->second]) {
          buf += ((hasNegated && j > nFeatures / 2)
                      ? ("-" + std::to_string(j - nFeatures / 2))
                      : std::to_string(j)) +
                 ",";
          break;
        }
      }
      // Find left child
      for (uint32_t j : LR(i)) {
        search = isLeftChild.find(make_pair(i, j));
        assert(search != isLeftChild.end());
        if (model[search->second]) {
          if (j < nextLevel)
            nextLevel = j;
          buf += std::to_string(j) + "," + std::to_string(j + 1) + ")";
          break;
        }
      }
    }
  }
  outLines.push_back(buf);
}

void DecisionTreeApp::blockSolution(const model_t &model,
                                    clause_t &outClause) const {
  outClause.clear();
  // Block only variables for nodes in use to prevent permutations on unused
  // vars Only block structural variables (v, a, l)
  uint32_t nNodes{};
  for (uint32_t i = 0; i < maxNodes; i += 2)
    if (model[nodeUsed[evenNodes(i)]])
      nNodes = i + 1;

  unordered_map<pair<uint32_t, uint32_t>, var_t>::const_iterator search{};

  for (uint32_t i = 0; i < nNodes; i++) {
    outClause.push_back(model[isLeaf[i]] ? -isLeaf[i] : isLeaf[i]);
    if (model[isLeaf[i]]) {
      outClause.push_back(model[nodeClassTrue[i]] ? -nodeClassTrue[i]
                                                  : nodeClassTrue[i]);
    } else {
      for (uint32_t j = 0; j < nFeatures; j++) {
        search = featureAssignNode.find(make_pair(j, i));
        assert(search != featureAssignNode.end());
        outClause.push_back(model[search->second] ? -search->second
                                                  : search->second);
      }
      for (uint32_t j : LR(i)) {
        if (j >= nNodes)
          break;
        search = isLeftChild.find(make_pair(i, j));
        assert(search != isLeftChild.end());
        outClause.push_back(model[search->second] ? -search->second
                                                  : search->second);
      }
    }
  }
}

void DecisionTreeApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "DecisionTreeApp Stats\n";
  INFO << LOG_H1;

  INFO << "maxNodes = " << maxNodes << "\n";
  INFO << "#samples = " << nSamples << "\n";
  INFO << "#features = " << nFeatures << "\n";

  Application::printStats();
}

void DecisionTreeApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "DecisionTreeApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "maxNodes = " << maxNodes << "\n";
  Application::printOptions();
}

void DecisionTreeApp::baseEncoding(vector<clause_t> &outClauses) {
  if (verbose >= 0)
    INFO << "Encoding instance " << instance
         << " for decision tree learning with maximum " << maxNodes
         << " nodes\n";

  outClauses.clear();

  // Load data
  DataSet data{instance};
  nSamples = data.nSamples();
  nFeatures = data.nFeatures();
  hasNegated = data.hasNegatedColumns();

  // Create variables
  nVars = 0;

  isLeaf.resize(maxNodes);
  for (uint32_t i = 0; i < maxNodes; i++)
    isLeaf[i] = ++nVars;

  for (uint32_t i = 0; i < maxNodes; i++)
    for (uint32_t j : LR(i))
      isLeftChild[make_pair(i, j)] = ++nVars;

  for (uint32_t r = 0; r < nFeatures; r++)
    for (uint32_t j = 0; j < maxNodes; j++)
      featureAssignNode[make_pair(r, j)] = ++nVars;

  for (uint32_t r = 0; r < nFeatures; r++)
    for (uint32_t j = 0; j < maxNodes; j++)
      featureDiscNode[make_pair(r, j)] = ++nVars;

  for (uint32_t r = 0; r < nFeatures; r++)
    for (uint32_t j = 0; j < maxNodes; j++)
      featureDiscPos[make_pair(r, j)] = ++nVars;

  for (uint32_t r = 0; r < nFeatures; r++)
    for (uint32_t j = 0; j < maxNodes; j++)
      featureDiscNeg[make_pair(r, j)] = ++nVars;

  nodeClassTrue.resize(maxNodes);
  for (uint32_t i = 0; i < maxNodes; i++)
    nodeClassTrue[i] = ++nVars;

  sampleClassifiedCorrectly.resize(nSamples);
  for (uint32_t i = 0; i < nSamples; i++)
    sampleClassifiedCorrectly[i] = ++nVars;

  nodeUsed.resize(maxNodes / 2 + 1);
  for (uint32_t i = 0; i < maxNodes; i += 2)
    nodeUsed[evenNodes(i)] = ++nVars;

  posSample.resize(nSamples);

  // Encoding
  // Narodytska et al. IJCAI'18
  // Tree Structure
  // (1): root is not a leaf
  outClauses.push_back({-lit(isLeaf[0])});
  // (2): leaf node has no children
  for (uint32_t i = 0; i < maxNodes; i++)
    for (uint32_t j : LR(i))
      outClauses.push_back({-lit(nodeActive(j)), -lit(isLeaf[i]),
                            -lit(isLeftChild[make_pair(i, j)])});
  // (4): non-leaf node must have exactly one child (per side)
  for (uint32_t i = 0; i < maxNodes; i++) {
    std::vector<lit_t> lits{};
    for (uint32_t j : LR(i))
      // l and m
      lits.push_back(tseitinAnd(nVars, outClauses, isLeftChild[make_pair(i, j)],
                                nodeActive(j)));
    // Not v and m
    lit_t notVandM = tseitinAnd(nVars, outClauses, -isLeaf[i], nodeActive(i));
    condExactlyOne(nVars, outClauses, notVandM, lits);
  }
  // (6): all nodes but the root must have a parent
  // TODO: Check if redundant
  for (uint32_t j = 1; j < maxNodes; j++) {
    std::vector<lit_t> lits{};
    for (uint32_t i = (j - 1) / 2; i < (j % 2 ? j : j - 1); i++)
      lits.push_back(isParent(j, i));
    condExactlyOne(nVars, outClauses, nodeActive(j), lits);
  }
  // Tree classification
  // (7): discriminate feature for value 0 at node
  for (uint32_t r = 0; r < nFeatures; r++) {
    outClauses.push_back({-lit(featureDiscNeg[make_pair(r, 0)])});
    for (uint32_t j = 1; j < maxNodes; j++) {
      clause_t cl = {-lit(nodeActive(j)),
                     -lit(featureDiscNeg[make_pair(r, j)])};
      for (uint32_t i = (j - 1) / 2; i < (j % 2 ? j : j - 1); i++) {
        // Previous node on path discriminates feature negatively
        lit_t pAndD0 = tseitinAnd(nVars, outClauses, isParent(j, i),
                                  featureDiscNeg[make_pair(r, i)]);
        outClauses.push_back({-lit(nodeActive(j)),
                              lit(featureDiscNeg[make_pair(r, j)]), -pAndD0});
        cl.push_back(pAndD0);
        if (j % 2 != 0)
          continue;
        // This node discriminates feature negatively
        lit_t aAndR =
            tseitinAnd(nVars, outClauses, featureAssignNode[make_pair(r, i)],
                       isRightChild(i, j));
        outClauses.push_back({-lit(nodeActive(j)),
                              lit(featureDiscNeg[make_pair(r, j)]), -aAndR});
        cl.push_back(aAndR);
      }
      outClauses.push_back(cl);
    }
  }
  // (8): discriminate feature for value 1 at node
  for (uint32_t r = 0; r < nFeatures; r++) {
    outClauses.push_back({-lit(featureDiscPos[make_pair(r, 0)])});
    for (uint32_t j = 1; j < maxNodes; j++) {
      clause_t cl = {-lit(nodeActive(j)),
                     -lit(featureDiscPos[make_pair(r, j)])};
      for (uint32_t i = (j - 1) / 2; i < (j % 2 ? j : j - 1); i++) {
        // Previous node on path discriminates feature positively
        lit_t pAndD1 = tseitinAnd(nVars, outClauses, isParent(j, i),
                                  featureDiscPos[make_pair(r, i)]);
        outClauses.push_back({-lit(nodeActive(j)),
                              lit(featureDiscPos[make_pair(r, j)]), -pAndD1});
        cl.push_back(pAndD1);
        if (j % 2 != 1)
          continue;
        // This node discriminates feature positively
        lit_t aAndL =
            tseitinAnd(nVars, outClauses, featureAssignNode[make_pair(r, i)],
                       isLeftChild[make_pair(i, j)]);
        outClauses.push_back({-lit(nodeActive(j)),
                              lit(featureDiscPos[make_pair(r, j)]), -aAndL});
        cl.push_back(aAndL);
      }
      outClauses.push_back(cl);
    }
  }
  // (9): using feature at node
  for (uint32_t r = 0; r < nFeatures; r++) {
    for (uint32_t j = 0; j < maxNodes; j++) {
      clause_t cl = {-lit(nodeActive(j)),
                     -lit(featureDiscNode[make_pair(r, j)]),
                     lit(featureAssignNode[make_pair(r, j)])};
      outClauses.push_back({-lit(nodeActive(j)),
                            lit(featureDiscNode[make_pair(r, j)]),
                            -lit(featureAssignNode[make_pair(r, j)])});
      if (j != 0) {
        for (uint32_t i = (j - 1) / 2; i < (j % 2 ? j : j - 1); i++) {
          lit_t uAndP =
              tseitinAnd(nVars, outClauses, featureDiscNode[make_pair(r, i)],
                         isParent(j, i));
          outClauses.push_back({-lit(nodeActive(j)), -uAndP,
                                -lit(featureAssignNode[make_pair(r, j)])});
          outClauses.push_back({-lit(nodeActive(j)), -uAndP,
                                lit(featureDiscNode[make_pair(r, j)])});
          cl.push_back(uAndP);
        }
      }
      outClauses.push_back(cl);
    }
  }
  // (10): for non-leaf node exactly one feature is used
  for (uint32_t j = 0; j < maxNodes; j++) {
    vector<lit_t> lits;
    for (uint32_t r = 0; r < nFeatures; r++)
      lits.push_back(featureAssignNode[make_pair(r, j)]);
    lit_t notVAndM = tseitinAnd(nVars, outClauses, -isLeaf[j], nodeActive(j));
    condExactlyOne(nVars, outClauses, notVAndM, lits);
  }
  // (11): for leaf node no feature is used
  for (uint32_t j = 0; j < maxNodes; j++)
    for (uint32_t r = 0; r < nFeatures; r++)
      outClauses.push_back({-lit(nodeActive(j)), -lit(isLeaf[j]),
                            -lit(featureAssignNode[make_pair(r, j)])});
  // Classification
  for (uint32_t q = 0; q < nSamples; q++) {
    DataSample sample = data.getSample(q);
    posSample[q] = sample.getClass();
    if (sample.getClass()) {
      // (12): positive sample discriminated if leaf node associated with
      // negative class
      for (uint32_t j = 0; j < maxNodes; j++) {
        clause_t cl = {-lit(nodeActive(j)), -lit(sampleClassifiedCorrectly[q]),
                       -lit(isLeaf[j]), lit(nodeClassTrue[j])};
        for (uint32_t r = 0; r < nFeatures; r++)
          cl.push_back(sample.getFeature(r) ? featureDiscPos[make_pair(r, j)]
                                            : featureDiscNeg[make_pair(r, j)]);
        outClauses.push_back(cl);
      }
    } else {
      // (13): negative sample discriminated if leaf node associated with
      // positive class
      for (uint32_t j = 0; j < maxNodes; j++) {
        clause_t cl = {-lit(nodeActive(j)), -lit(sampleClassifiedCorrectly[q]),
                       -lit(isLeaf[j]), -lit(nodeClassTrue[j])};
        for (uint32_t r = 0; r < nFeatures; r++)
          cl.push_back(sample.getFeature(r) ? featureDiscPos[make_pair(r, j)]
                                            : featureDiscNeg[make_pair(r, j)]);
        outClauses.push_back(cl);
      }
    }
  }
  // Hu et al. IJCAI'20
  // At least 3 nodes
  outClauses.push_back({lit(nodeUsed[evenNodes(2)])});
  // (11): nodes used
  for (uint32_t i = 0; i < maxNodes - 2; i += 2)
    outClauses.push_back(
        {-lit(nodeUsed[evenNodes(i + 2)]), lit(nodeUsed[evenNodes(i)])});

  nClauses = outClauses.size();

  encoded = true;
}
} // namespace BiOptSat