/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _generalizedtot_hpp_INCLUDED
#define _generalizedtot_hpp_INCLUDED

#include <cassert>
#include <map>

#include "pbencoding.hpp"
#include "types.hpp"

using std::vector;

namespace BiOptSat {

class TotMerger; // Friend class defined in totmerger.hpp

// Generalized totalizer encoding extended for incremental use
// Ref: https://doi.org/10.1007/978-3-319-23219-5_15
class GeneralizedTotalizer : public PBEncoding {
  friend class Node;
  friend class TotMerger;

protected:
  // A node in the GTE tree
  class Node {
  public:
    SatSolver *const solver{};
    std::map<uint64_t, lit_t> weights{};
    uint64_t nClauses{};
    const uint64_t depth{}; // Reversed depth, leafs are 0, root is high
    uint64_t upper{};   // The highest encoded value, which is _not_ the highest
                        // enforceable value
    uint64_t lower{};   // The lowest encoded value
    uint64_t max_val{}; // The maximum value this node can take
    Node *const left{};
    Node *const right{};

    Node(SatSolver *const solver, uint64_t w,
         lit_t l)
        : solver(solver) { // Construct leaf
      weights[w] = l;
      upper = w;
      max_val = w;
    }

    Node(SatSolver *const solver, Node *left, Node *right)
        : solver(solver),
          depth((left->depth > right->depth ? left->depth : right->depth) + 1),
          max_val(left->max_val + right->max_val), left(left), right(right) {}

    virtual ~Node();

    inline bool isLeaf() const { return (left == nullptr); }
    virtual std::pair<uint32_t, uint32_t> generate_encoding(
        uint64_t,
        uint64_t); // Generate the encoding for a node between two bounds
    virtual std::pair<uint32_t, uint32_t>
    encode(uint64_t, uint64_t,
           bool = true); // Encode the adder from the children to
                         // this node between two given bounds.
                         // Recurses depth first if the second parameter is
                         // true. Returns the number of newly reserved variables
                         // and new clauses.
    virtual uint32_t
    reserveVars(bool = true); // Reserve all variables that the nodes
                              // potentially need. Recurses depth first
                              // if the parameter is true. Returns the
                              // number of newly reserved variables.
  };

  Node *root{}; // Root node

  Node *buildTree(wlits_t::const_iterator,
                  size_t); // Recursive function for initially
                           // building the tree structure

  void addExtensionToTree(wlits_t &);

  void _enforceUB(uint64_t, vector<lit_t> &);
  void _enforceLB(uint64_t, vector<lit_t> &);

  // Options
  bool reserveVars{};
  bool coi{}; // Cone of influence

public:
  GeneralizedTotalizer(SatSolver *const, const BoundType = BOUND_UB);
  ~GeneralizedTotalizer() {
    if (root)
      delete root;
  }

  uint64_t nextHigherPossible(uint64_t);
  uint64_t nextLowerPossible(uint64_t);

  virtual void printStats(bool header = false) const;

  // Stats getter
  uint64_t getDepth() const { return root ? root->depth : 0; }
  uint32_t getUpper() const { return root ? root->upper : 0; }

  // Option setters
  bool doesReserveVars() const { return reserveVars; }
  void setReserveVars(bool rv) { reserveVars = rv; }
};
} // namespace BiOptSat

#endif