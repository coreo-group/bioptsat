/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "hybrid.hpp"

#include <cassert>

#include "options.hpp"

namespace BiOptSat {
// Options
static string cat = "2.2.3 - Hybrid";
static BoolOption opt_pmin_lower_bound(
    cat, "pmin-bound",
    "Bound the next BiOptSat call based on the p-minimal model found", true);

// Constructor/Destructor
HybridEnumerator::HybridEnumerator(std::vector<clause_t> &clauses,
                                   Objective inc, Objective dec, string backend)
    : Enumerator(UNORDERED, clauses, inc, dec, backend),
      pminBound(opt_pmin_lower_bound) {
  oracleCallLog.resize(OCT_NEXT_FREE, 0);
  octNames.resize(OCT_NEXT_FREE);
  octNames[OCT_SAT] = "Satisfiable";
  octNames[OCT_PMIN] = "P-minimal";
  octNames[OCT_BOS] = "BiOptSat";
  octNames[OCT_ENUM] = "Enumeration";
}

HybridEnumerator::~HybridEnumerator() {
  if (incPB) {
    delete incPB;
    incPB = nullptr;
  }
  if (decPB) {
    delete decPB;
    decPB = nullptr;
  }
}

// Timer functions
void HybridEnumerator::stopTimer() {
  double realTime = absolute_real_time() - realStartTime;
  double cpuTime = absolute_process_time() - cpuStartTime;

  totalRealTime += realTime;
  totalCpuTime += cpuTime;

  switch (phase) {
  case HB_PHASE_BOS:
    psRealTime += realTime;
    psCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "BiOptSat phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case HB_PHASE_PM:
    pmRealTime += realTime;
    pmCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "P-minimal phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case HB_PHASE_ENUM:
    enumRealTime += realTime;
    enumCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Enum phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case HB_PHASE_NONE:
    break;
  }
  if (verbose >= 2) {
    INFO << "Total times: real time=" << totalRealTime
         << "; cpu time=" << totalCpuTime << "\n";
    INFO << "BiOptSat phase times: real time=" << psRealTime
         << "; cpu time=" << psCpuTime << "\n";
    INFO << "P-minimal phase times: real time=" << pmRealTime
         << "; cpu time=" << pmCpuTime << "\n";
    INFO << "Enum phase times: real time=" << enumRealTime
         << "; cpu time=" << enumCpuTime << "\n";
  }
}

// Main Algorithm
void HybridEnumerator::_enumerate() {
  // Settings warnings
  if (minIncreasing != -1)
    WARN << "minIncreasing is set but has no effect for hybrid enumerator\n";
  if (minDecreasing != -1)
    WARN << "minDecreasing is set but has no effect for hybrid enumerator\n";

  state = SOLVING;

  if (!incPB) {
    incPB = pbEncodingFactory(oracle, BOUND_UB);
    incPB->addLits(increasing);
    if (maxIncreasing != -1 &&
        static_cast<uint64_t>(maxIncreasing) < increasing.getWeightSum())
      incPB->enforceUB(maxIncreasing);
  }

  if (!decPB) {
    decPB = pbEncodingFactory(oracle, BOUND_UB);
    decPB->addLits(decreasing);
    if (maxDecreasing != -1 &&
        static_cast<uint64_t>(maxDecreasing) < decreasing.getWeightSum())
      decPB->enforceUB(maxDecreasing);
  }

  // Check that problem is SAT
  SolverState res = oracle->solve();
  ++oracleCallLog[OCT_SAT];
  if (res != SAT) {
    WARN << "Problem not satisfiable; terminating\n";
    return;
  }

  auto objVals = getObjValModel(true, true, model);
  bosInc = objVals.first;
  bosDec = objVals.second;

  while (true) {
    bool cont = pMinimization();
    if (!cont) {
      state = BOUND_REACHED;
      return;
    }

    phase = HB_PHASE_BOS;
    startTimer();
    // Check left
    if (pminBound)
      bosInc = pmInc - 1;
    else
      bosInc = bosInc - 1;
    if (bosInc + 1 > 0) {
      std::vector<lit_t> assumps{};
      incPB->enforceUB(bosInc, assumps);
      res = oracle->solve(assumps);
      ++oracleCallLog[OCT_BOS];
    } else {
      res = UNSAT;
    }
    if (res == UNSAT) {
      if (pmDec <= 0) {
        if (verbose >= 1)
          INFO << "Terminating enumeration due to no more models\n";
        stopTimer();
        state = SOLVED;
        return;
      }
      // Check lower
      bosDec = pmDec - 1;
      decPB->enforceUB(bosDec);
      res = oracle->solve();
      ++oracleCallLog[OCT_BOS];
      if (res == UNSAT) {
        if (verbose >= 1)
          INFO << "Terminating enumeration due to no more models\n";
        stopTimer();
        state = SOLVED;
        return;
      }
    }

    objVals = getObjValModel(true, true, model);
    bosInc = objVals.first;
    bosDec = objVals.second;

    logCandidate(bosInc, bosDec);

    stopTimer();
  }
}

// Algorithm subroutines
bool HybridEnumerator::pMinimization() {
  phase = HB_PHASE_PM;

  pmInc = bosInc;
  pmDec = bosDec;

  lit_t blockSwitch = 0;
  bool cont = true;

  startTimer();
  while (true) {
    // Build assumptions
    std::vector<lit_t> assumps{};
    incPB->enforceUB(pmInc, assumps);
    decPB->enforceUB(pmDec, assumps);
    // Block previous north east rectangle for good
    if (blockSwitch != 0)
      oracle->addClause({-blockSwitch});
    // Assume blocked north east rectangle
    blockSwitch = oracle->getNewVar();
    clause_t blCl{blockSwitch};
    lit_t out;
    if (pmInc > 0) {
      incPB->enforceUB(pmInc - 1, out);
      blCl.push_back(out);
    }
    if (pmDec > 0) {
      decPB->enforceUB(pmDec - 1, out);
      blCl.push_back(out);
    }
    oracle->addClause(blCl);
    assumps.push_back(-blockSwitch);
    // Log Candidate
    logCandidate(pmInc, pmDec);
    // Query if subset solution exists
    SolverState res = oracle->solve(assumps);
    ++oracleCallLog[OCT_PMIN];

    assert(res != UNDEF);
    if (res == UNSAT) {
      stopTimer();
      phase = HB_PHASE_ENUM;
      startTimer();
      cont = enumSols();
      stopTimer();
      if (verbose >= 1)
        INFO << "Hybrid iteration finished; inc = " << pmInc
             << "; dec = " << pmDec
             << "; proc start wall time = " << proc_start_real_time()
             << "s; absolute cpu time = " << absolute_process_time() << "s\n";
      if (cont)
        oracle->addClause({-blockSwitch});
      return cont;
    }

    auto objVals = getObjValModel(true, true, model);
    pmInc = objVals.first;
    pmDec = objVals.second;
  }
}

bool HybridEnumerator::enumSols() {
  if (verbose >= 2)
    INFO << "Starting solution enumeration\n";

  bool cont = true;
  bool loop = true;
  std::vector<lit_t> assumps{};
  clause_t blClause{};

  // Add already found model
  ParetoPoint<model_t> pp{.models = {model}, .incVal = pmInc, .decVal = pmDec};
  nSols++;

  // Check early termination conditions
  if (maxSolsPerPP != -1 &&
      pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
    if (verbose >= 2)
      INFO << "Terminating solution enumeration due to max solutions per "
              "pareto point reached\n";
    loop = false;
    cont = true;
  } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
    if (verbose >= 1)
      INFO << "Terminating algorithm due to max solutions reached\n";
    state = BOUND_REACHED;
    loop = false;
    cont = false;
  }
  if (loop) {
    // Block solution found previously
    blockingFunc(model, blClause);
    oracle->addClause(blClause);
    // Build assumptions
    if (pmInc < increasing.getWeightSum())
      incPB->enforceUB(pmInc, assumps);
    if (pmDec < decreasing.getWeightSum())
      decPB->enforceUB(pmDec, assumps);
  }

  while (loop) {
    // Solve and get model
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_ENUM];
    if (ret == UNSAT) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to no more models\n";
      cont = true;
      break;
    }
    assert(ret == SAT);
#ifndef NDEBUG
    auto objVals = getObjValModel(false, false, model);
    assert(objVals.first == pmDec);
    assert(objVals.second == pmInc);
#else
    getObjValModel(false, false, model);
#endif
    pp.models.push_back(model);
    nSols++;
    // Check early termination conditions
    if (maxSolsPerPP != -1 &&
        pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to max solutions per "
                "pareto point reached\n";
      cont = true;
      break;
    } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
      if (verbose >= 1)
        INFO << "Terminating algorithm due to max solutions reached\n";
      cont = false;
      break;
    }
    // Block solution
    blockingFunc(model, blClause);
    oracle->addClause(blClause);
  }

  // Add new pareto point
  uint32_t i;
  for (i = 0; i < paretoFront.size(); i++)
    if (paretoFront[i].incVal > pmInc)
      break;
  paretoFront.insert(paretoFront.begin() + i, pp);

  if (maxPPs != -1 && paretoFront.size() >= static_cast<uint32_t>(maxPPs)) {
    if (verbose >= 1)
      INFO << "Terminating due to maximum number of pareto points "
              "reached\n";
    cont = false;
  }

  return cont;
}

// Stats printing
void HybridEnumerator::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Hybrid Enumerator Stats\n";
  INFO << LOG_H1;

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "BiOptSat phase\n";
  INFO << LOG_H3;
  INFO << "BiOptSat wall clock time: " << psRealTime << "s\n";
  INFO << "BiOptSat Cpu time: " << psCpuTime << "s\n";
  INFO << "\n";
  INFO << "P-minimization phase\n";
  INFO << LOG_H3;
  INFO << "P-minimal wall clock time: " << pmRealTime << "s\n";
  INFO << "P-minimal Cpu time: " << pmCpuTime << "s\n";
  INFO << "\n";
  INFO << "Enumeration\n";
  INFO << LOG_H3;
  INFO << "Enumeration wall clock time: " << enumRealTime << "s\n";
  INFO << "Enumeration Cpu time: " << enumCpuTime << "s\n";

  Enumerator::printStats();
}

// Options printing
void HybridEnumerator::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Hybrid Enumerator Options\n";
  INFO << LOG_H1;

  INFO << "\n";
  INFO << "pminBound = " << pminBound << "\n";

  Enumerator::printOptions();
}
} // namespace BiOptSat