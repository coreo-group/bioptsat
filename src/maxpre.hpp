/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _maxpre_hpp_INCLUDED
#define _maxpre_hpp_INCLUDED

#include "preprocessor.hpp"

#include "preprocessorinterface.hpp"

namespace BiOptSat {

enum PreRun { PR_NONE = 0b00, PR_ALL = 0b01, PR_OBJ = 0b11 };
const uint8_t PR_DO = 0b01;

class MaxPre : public PreProcessor {
protected:
  maxPreprocessor::PreprocessorInterface *backend{};
  maxPreprocessor::PreprocessorInterface *prerunBackend{};
  vector<lit_t> fixedLits{}; // Literals determined as fixed by a
                             // prerun of MaxPre

  // Options
  std::string techniques{};       // The MaxPre techniques to use
  PreRun preRun{};                // The type of prerun to do
  std::string prerunTechniques{}; // Techniques for a variable fixing pre run
  int logLevel{};                 // The MaxPre log level
  bool maxpreReindexing{}; // Whether preprocessing should reindex variables

  void getPreProInstance(std::vector<clause_t> &, Objective &, uint64_t &,
                         Objective &, uint64_t &);
  void _preprocess();
  void _reconstruct(model_t &);
  void fixingPreRun(const vector<vector<int>> &,
                    const vector<std::pair<uint64_t, uint64_t>> &, uint64_t);

public:
  MaxPre();
  ~MaxPre();

  void printStats() const;   // Print MaxPre stats
  void printOptions() const; // Print MaxPre options

  // Option getter and setter
  std::string getTechniques() const { return techniques; }
  void setTechniques(std::string tech) { techniques = tech; }
  PreRun getPreRun() const { return preRun; }
  void setPreRun(PreRun pr) { preRun = pr; }
  std::string getPreRunTechniques() const { return prerunTechniques; }
  void setPreRunTechniques(std::string tech) { prerunTechniques = tech; }
  int getLogLevel() const { return logLevel; }
  void setLogLevel(int ll) { logLevel = ll; }
  bool doesMaxpreReindexing() const { return maxpreReindexing; }
  void doMaxpreReindexing(bool ri) { maxpreReindexing = ri; }
}; // namespace BiOptSat

} // namespace BiOptSat

#endif