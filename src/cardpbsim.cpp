/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "cardpbsim.hpp"

namespace BiOptSat {
void CardPBSim::_enforceUB(uint64_t b, std::vector<lit_t> &assumps) {
  std::vector<lit_t> extension{};
  // Build multiset of literals to add
  // Also assume literals with weight > b to false
  for (const auto &p : wLits) {
    if (p.second <= b) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        encodedWLits[p.first] += wAdd;
      }
      extension.reserve(extension.size() + wAdd);
      for (uint64_t i = 0; i < wAdd; i++)
        extension.push_back(p.first);
    } else
      assumps.push_back(-p.first);
  }
  // Extend and build totalizer if necessary
  if (extension.size()) {
    if (!cardEnc)
      cardEnc = cardEncodingFactory(solver, boundType);
    cardEnc->addLits(extension);
  }
  if (cardEnc)
    cardEnc->enforceUB(b, assumps);
  if (upper < b)
    upper = b;
}

void CardPBSim::_enforceLB(uint64_t b, std::vector<lit_t> &assumps) {
  std::vector<lit_t> extension{};
  // Build multiset of literals to add
  for (const auto &p : wLits) {
    auto it = encodedWLits.find(p.first);
    uint64_t wAdd = p.second - (it == encodedWLits.end() ? 0 : it->second);
    encodedWLits[p.first] += wAdd;
    extension.reserve(extension.size() + wAdd);
    for (uint64_t i = 0; i < wAdd; i++)
      extension.push_back(p.first);
  }
  // Extend and build totalizer if necessary
  if (extension.size()) {
    if (!cardEnc)
      cardEnc = cardEncodingFactory(solver, boundType);
    cardEnc->addLits(extension);
  }
  if (cardEnc)
    cardEnc->enforceLB(b, assumps);
  if (upper < b)
    upper = b;
}

void CardPBSim::printStats(bool header) const {
  if (header) {
    INFO << "\n";
    INFO << "\n";
    INFO << "CardPBSim Stats\n";
    INFO << LOG_H1;
    INFO << "\n";
  }
  PBEncoding::printStats(false);
  if (cardEnc) {
    INFO << "\n";
    INFO << "Underlying Card Encoding\n";
    INFO << LOG_H4;
    cardEnc->printStats(false);
  }
}
} // namespace BiOptSat