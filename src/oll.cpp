/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "logging.hpp"

#if ENABLE_OSHYBRID
#include "totmerger.hpp"
#endif

namespace BiOptSat {
bool BiOptSatEnumerator::ollIncObj() {
  uint64_t ubCost = increasing.getWeightSum() + 1;
  uint64_t lbCost = incBound;
  std::vector<lit_t> assumps{};
  std::vector<std::pair<clause_t, uint64_t>>
      delayedConfs{}; // Conflicts to later be reformulated and its weight

  while (true) {
    if (verbose >= 3) {
      INFO << "minIncObj (OLL) iteration, ubCost=" << ubCost
           << "; lbCost=" << lbCost << "; #conflicts=" << nConflicts
           << "; #card encodings=" << ollRefConfs.size()
           << "; inactiveWeightSum=" << inactiveWeightSum << "\n";
    }

    // Build assumptions
    assumps.clear();
    if (!decreasingHarden)
      upperBoundDec(assumps);
    ollBoundInc(assumps);

    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_OLL];
    assert(ret != UNDEF);

    if (ret == SAT) {
      // If SAT
      model_t mdl;
      auto objVals = getObjValModel(true, true, mdl);
      if (objVals.first < ubCost) {
        ubCost = objVals.first;
        // Cache model
        modelCache = mdl;
        decBound = objVals.second;
      }

      logCandidate(objVals.first, objVals.second);

      size_t nReformulated = delayedConfs.size();
      lbCost += ollReformulateConfs(delayedConfs);
      if (verbose >= 3)
        INFO << "Reformulated " << nReformulated << " conflicts\n";

      // Termination condition (optimum found)
      if (lbCost == ubCost)
        break;

#if ENABLE_OSHYBRID
      // Hybrid switch condition
      if ((minIncVariant & HYBRID) && hybridImmediateSwitch &&
          hybridSwitchCriterion()) {
        if (verbose >= 2)
          INFO << "Immediately switching over to sat-unsat\n";
        hybridSUCont = true;
        incBound = lbCost;
        return false;
      }
#endif
    } else {
      // If UNSAT
      logCandidate(lbCost, decBound, false);
      clause_t conf{};
      oracle->getConflict(conf);
      bool end = true;
      for (lit_t l : conf)
        if (!decPB || !decPB->isEnforcingAssump(-l)) {
          end = false;
          break;
        }
      if (end)
        // No more solutions; enumeration ends
        return false;
      nConflicts++;
      avgRawConflictLen =
          ((avgRawConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;
      minimizeConflict(conf);
      avgConflictLen =
          ((avgConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;

      // Compute core cost
      uint64_t minWeight = std::numeric_limits<uint64_t>::max();
      for (lit_t l : conf) {
        if (increasing.hasLit(l)) {
          assert(inactiveWeight.find(l) != inactiveWeight.end());
          if (inactiveWeight[l] < minWeight)
            minWeight = inactiveWeight[l];
        } else {
          assert(ollAssumpOutLits.find(-l) != ollAssumpOutLits.end());
          OutLitData cardData = ollAssumpOutLits[-l];
          if (cardData.weight < minWeight)
            minWeight = cardData.weight;
        }
      }
      if (verbose >= 3)
        INFO << "Found core of size " << conf.size() << " and weight "
             << minWeight << "\n";

      // Relax conflict and add to delayed reformulation list
      for (lit_t l : conf) {
        if (increasing.hasLit(l)) {
          // Literal is from objective
          inactiveWeight[l] -= minWeight;
          inactiveWeightSum -= minWeight;
          if (inactiveWeight[l] == 0) {
            // If fully relaxed, make active and relax for decreasing
            inactiveWeight.erase(l);
            addToDecPB(l);
          }
        } else {
          // Literal is a cardinality assumption
          assert(ollAssumpOutLits.find(-l) != ollAssumpOutLits.end());
          OutLitData cardData = ollAssumpOutLits[-l];
          assert(cardData.weight - minWeight >= 0);
          ollAssumpOutLits[-l] =
              OutLitData{.cardIdx = cardData.cardIdx,
                         .bound = cardData.bound,
                         .weight = cardData.weight - minWeight};
        }
      }
      lbCost += minWeight;
      delayedConfs.push_back(std::make_pair(conf, minWeight));

      // Termination condition (optimum proven)
      if (lbCost == ubCost) {
// Reformulate delayed conflicts for future calls
#ifndef NDEBUG
        uint64_t lbInc = ollReformulateConfs(delayedConfs);
        assert(!lbInc);
#else
        ollReformulateConfs(delayedConfs);
#endif
        break;
      }

#if ENABLE_OSHYBRID
      // Hybrid switch condition
      if ((minIncVariant & HYBRID) && hybridImmediateSwitch &&
          hybridSwitchCriterion()) {
        if (verbose >= 2)
          INFO << "Immediately switching over to sat-unsat\n";
        hybridSUCont = true;
        lbCost += ollReformulateConfs(delayedConfs);
        incBound = lbCost;
        return false;
      }
#endif
    }
  }
  hardenDec(decBound);
  incBound = ubCost;
  assert(delayedConfs.empty());
  return true;
}

size_t BiOptSatEnumerator::ollReformulateConfs(
    std::vector<std::pair<clause_t, uint64_t>> &confs) {
  // Reformulated delayed conflicts
  uint64_t lbInc{};
  while (confs.size()) {
    // Updated card encoding bounds, if bound assumption is in conflict
    clause_t conf = confs.back().first;
    uint64_t confWeight = confs.back().second;
    for (lit_t l : conf) {
      if (!increasing.hasLit(l) &&
          ollAssumpOutLits.find(-l) != ollAssumpOutLits.end()) {
        // Note: if second check in if is necessary in case a literal appears in
        // two "disjoint" cores
        OutLitData cardData = ollAssumpOutLits[-l];
        if (cardData.weight == 0 &&
            cardData.bound + 1 <
                ollRefConfs[cardData.cardIdx].first->getNLits()) {
          // Add next output literal to assumptions
          lit_t out;
          ollRefConfs[cardData.cardIdx].first->enforceUB(cardData.bound + 1,
                                                         out);
          ollAssumpOutLits[out] =
              OutLitData{.cardIdx = cardData.cardIdx,
                         .bound = cardData.bound + 1,
                         .weight = ollRefConfs[cardData.cardIdx].second};
          // Remove fully relaxed output literal from assumptions
#ifndef NDEBUG
          size_t nRem = ollAssumpOutLits.erase(-l);
#else
          ollAssumpOutLits.erase(-l);
#endif
          assert(nRem == 1);
        }
      }
    }

    // Build new card encoding from conflict
    if (conf.size() > 1) {
      ollRefConfs.push_back(std::make_pair(
          cardEncodingFactory(oracle, BOUND_UB,
                              minIncVariant & HYBRID ? "totalizer" : ""),
          confWeight)); // The OLL-SU-Hybrid requires the totalizer encoding for
                        // reformulating conflicts
      CardEncoding *newCard = ollRefConfs.back().first;
      newCard->addLits(conf);
      // Exhaust conflict
      uint64_t bound = 0;
      if (confExhaustion) {
        for (uint32_t i = 1; i < newCard->getNLits(); i++) {
          std::vector<lit_t> assumps{};
          newCard->enforceUB(i, assumps);
          SolverState ret = oracle->solve(assumps);
          ++oracleCallLog[OCT_CONF_EXH];
          if (ret == SAT) {
            bound = i;
            break;
          }
          assert(ret == UNSAT);
          lbInc += confWeight;
        }
        // If bound is not changed, no literal in conflict can be
        // satisfied
      } else {
        bound = 1;
      }
      // If bound <= 0 (core fully exhausted), harden core
      if (bound > 0) {
        lit_t out;
        newCard->enforceUB(bound, out);
        ollAssumpOutLits[out] = OutLitData{.cardIdx = ollRefConfs.size() - 1,
                                           .bound = bound,
                                           .weight = confWeight};
      } else {
        for (lit_t l : conf)
          oracle->addClause(l);
      }
    }

    confs.pop_back();
  }
  return lbInc;
}

#if ENABLE_OSHYBRID
void BiOptSatEnumerator::mergeOllTots() {
  // Merge the individual totalizers created by OLL and remaining inactive
  // literals so that Sat-Unsat search can be performed on the resulting
  // totalizer
  // All conflict reformulations are assumed to be totalizers
  TotMerger *merger = new TotMerger(oracle);
  // Merge all totalizers that are not yet fully relaxed
  for (const auto &p : ollAssumpOutLits) {
    assert(ollRefConfs[p.second.cardIdx].first);
    Totalizer *const tot = (Totalizer *)ollRefConfs[p.second.cardIdx].first;
    const uint64_t weightMult = ollRefConfs[p.second.cardIdx].second;
    const uint64_t weightOffset =
        (p.second.bound + 1) * weightMult - p.second.weight;
    merger->addTotalizer(tot, weightMult, weightOffset);
    // Mark totalizer as treated
    ollRefConfs[p.second.cardIdx].first = nullptr;
  }
  // Ownership of totalizers transferred to merger, delete fully relaxed ones
  for (auto &p : ollRefConfs)
    if (p.first)
      delete p.first;
  ollRefConfs.clear();
  ollAssumpOutLits.clear();
  // Add potentially remaining literals
  for (const auto &p : inactiveWeight)
    addToDecPB(p.first);
  merger->addLits(inactiveWeight);
  inactiveWeight.clear();
  inactiveWeightSum = 0;
  // Set removed weight from OLL as offset
  merger->setOffset(incBound);
  // Set merger as increasing PB encoding
  incPB = merger;
}
#endif

void BiOptSatEnumerator::ollBoundInc(std::vector<lit_t> &outAssumps) {
  if (!(minIncVariant & OLL))
    WARN << "Calling ollBoundInc but not in OLL mode\n";

  assumeInactive(outAssumps);
  for (const auto &p : ollAssumpOutLits)
    if (p.second.weight)
      outAssumps.push_back(p.first);
}
} // namespace BiOptSat