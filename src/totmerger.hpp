/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _totmerger_hpp_INCLUDED
#define _totmerger_hpp_INCLUDED

#include <cassert>
#include <map>

#include "generalizedtot.hpp"
#include "pbencoding.hpp"
#include "totalizer.hpp"
#include "types.hpp"

using std::vector;

namespace BiOptSat {

// PB encoding merging individual totalizers built by OLL into one big PB
// encoding with the help of a generalized Totalizer
class TotMerger : public PBEncoding {
protected:
  uint64_t boundOffset{}; // The hard lower bound OLL found

  typedef struct {
    Totalizer *const tot;
    const uint64_t weightMultiplier; // Weight of the reformulated conflict
    const uint64_t weightOffset; // Amount of weight that has been removed or
                                 // reformulated elsewhere
  } TotData;

  class Node : public GeneralizedTotalizer::Node {
  public:
    enum NodeType { INTERNAL, TOT, GTE };
    const NodeType nodeType{};

    // Only populated for nodeType == TOT
    Totalizer *const tot{};
    const uint64_t totWeightMultiplier{}; // Weight of the reformulated conflict
    const uint64_t totWeightOffset{}; // Amount of weight that has been removed
                                      // or reformulated elsewhere

    Node(SatSolver *const solver, Totalizer *const tot,
         const uint64_t weightMultiplier, const uint64_t weightOffset)
        : GeneralizedTotalizer::Node(solver, 0, 0), nodeType(TOT), tot(tot),
          totWeightMultiplier(weightMultiplier), totWeightOffset(weightOffset) {
      // Put totalizer outputs in weights map
      weights.clear();
      if (tot->outLits.empty())
        tot->build(tot->extension, 1);
      for (size_t i = 0; i < tot->outLits.size(); i++)
        if ((i + 1) * totWeightMultiplier >
            totWeightOffset) // Literal not removed or relaxed
          weights[(i + 1) * totWeightMultiplier - totWeightOffset] =
              tot->outLits[i];
      nClauses += tot->getNClauses();
      max_val = tot->outLits.size() * totWeightMultiplier - totWeightOffset;
    } // Construct totalizer node (the tot merger takes ownership of the
      // totalizer, meaning it will delete it)

    Node(SatSolver *const solver, Node *left, Node *right)
        : GeneralizedTotalizer::Node(solver, left, right),
          nodeType(left->nodeType == GTE && right->nodeType == GTE ? GTE
                                                                   : INTERNAL) {
    } // Construct internal node

    Node(SatSolver *const solver, uint64_t w, lit_t l)
        : GeneralizedTotalizer::Node(solver, w, l), nodeType(GTE) {
    } // Construct a GTE leaf

    virtual ~Node();

    // This calls GeneralizedTotalizer::Node::encode for all nodes except for
    // totalzier nodes where Totalizer::updateUpper is called
    virtual std::pair<uint32_t, uint32_t> encode(uint64_t, uint64_t,
                                                 bool = true);

    // reserveVars is completely inherited from GeneralizedTotalizer::Node since
    // totalizer are treated the same as actual leafs, where no reservation of
    // vars is necessary
  };

  Node *root{}; // Root node of the tree structure

  std::vector<TotData> totsToInclude{};

  // Stats
  uint32_t nTots{};
  uint64_t totWeightSum{};
  uint64_t maxTotMultiplier{}; // Maximum individual totalizer weight

  Node *mergeTotalizers(
      std::vector<TotData>::const_iterator,
      size_t); // Recursive function for building a tree over totalizers
  Node *buildGteTree(
      wlits_t::const_iterator,
      size_t); // Recursive function for building a tree over literals

  void addTotsToTree(std::vector<TotData> &);
  void addLitsToTree(wlits_t &);

  void _enforceUB(uint64_t, vector<lit_t> &);
  void _enforceLB(uint64_t, vector<lit_t> &);

  // Options
  bool reserveVars{};
  bool coi{}; // cone of influence

public:
  TotMerger(SatSolver *const, const BoundType = BOUND_UB);
  ~TotMerger() {
    if (root)
      delete root;
  }

  void addTotalizer(
      Totalizer *const, const uint64_t,
      const uint64_t = 0); // Add a totalizer with a multiplier and a weight
                           // offset of removed or reformulated weight

  uint64_t nextHigherPossible(uint64_t);
  uint64_t nextLowerPossible(uint64_t);

  virtual void printStats(bool header = false) const;

  // Stats getter
  uint64_t getDepth() const { return root ? root->depth : 0; }
  uint32_t getUpper() const { return root ? root->upper : 0; }
  uint32_t getNTots() const { return nTots; }
  virtual inline uint64_t getWeightSum() const {
    uint64_t sum = totWeightSum + PBEncoding::getWeightSum();
    for (const TotData &totData : totsToInclude)
      sum += totData.tot->getNLits() * totData.weightMultiplier -
             totData.weightOffset;
    return sum;
  } // Get sum of all weights, i.e., maximum sum

  // Option setters
  bool doesReserveVars() const { return reserveVars; }
  void setReserveVars(bool rv) { reserveVars = rv; }
};
} // namespace BiOptSat

#endif