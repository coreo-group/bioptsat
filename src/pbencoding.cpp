/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Based on (MSU3, OLL): Open-WBO (https://github.com/sat-group/open-wbo)
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "pbencoding.hpp"

#include <cassert>

#include "logging.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if ENABLE_GTE
#include "generalizedtot.hpp"
#endif

#if ENABLE_CARDPBSIM
#include "cardpbsim.hpp"
#endif

namespace BiOptSat {
// Options
static std::string cat = "5 - Encodings";
static StringOption opt_pbencoding(cat, "pb-encoding", "The PB encoding to use",
                                   "gte", StringSet({
#if ENABLE_GTE
                                     "gte",
#endif
#if ENABLE_CARDPBSIM
                                         "card-pb-sim",
#endif
                                   }));

PBEncoding::PBEncoding(SatSolver *const solver, const BoundType boundType)
    : boundType(boundType), solver(solver) {}

void PBEncoding::addLits(const wlits_t &wls) {
  for (const auto &p : wls)
    if (wLits.find(p.first) == wLits.end())
      wLits[p.first] = p.second;
    else
      wLits[p.first] += p.second;
  // UB becomes (potentially) invalid when adding lits
  hardUB = std::numeric_limits<uint64_t>::max();
}

void PBEncoding::enforceUB(uint64_t b, std::vector<lit_t> &assumps) {
  if (!(boundType & BOUND_UB)) {
    WARN << "Trying to upper bound PB encoding that does not encode upper "
            "bounds\n";
    return;
  }

  if (offset > 0 && static_cast<uint64_t>(offset) > b) {
    // Offset already larger than bound
    assumps.push_back(1);
    assumps.push_back(-1);
    return;
  }
  b -= offset;

  if (b >= hardUB)
    // Already enforced
    return;

  if (b >= getWeightSum())
    // Always true
    return;

  size_t preLen = assumps.size();
  _enforceUB(b, assumps);

  lastAssumps.clear();
  lastAssumps.insert(assumps.begin() + preLen, assumps.end());
  logState();
}

void PBEncoding::enforceUB(uint64_t b, lit_t &assump) {
  std::vector<lit_t> assumps{};
  enforceUB(b, assumps);

  if (assumps.empty()) {
    assump = solver->getNewVar();
    nVars++;
    lastAssumps.clear();
    lastAssumps.insert(assump);
    logState();
    return;
  }

  if (assumps.size() == 1) {
    assump = assumps.front();
    lastAssumps.clear();
    lastAssumps.insert(assump);
    logState();
    return;
  }
  assump = solver->getNewVar();
  nVars++;
  for (lit_t unit : assumps) {
    solver->addClause(-assump, unit);
    nClauses++;
  }

  lastAssumps.clear();
  lastAssumps.insert(assump);
  logState();
}

void PBEncoding::enforceUB(uint64_t b) {
  if (offset < 0 || static_cast<uint64_t>(offset) < b)
    b -= offset;
  else
    b = 0;

  if (b >= hardUB)
    // Already enforced
    return;

  std::vector<lit_t> assumps{};
  enforceUB(b, assumps);

  for (lit_t unit : assumps) {
    solver->addClause(unit);
    nClauses++;
  }
  hardUB = b;

  lastAssumps.clear();
  logState();
}

void PBEncoding::enforceLB(uint64_t b, std::vector<lit_t> &assumps) {
  if (!(boundType & BOUND_LB)) {
    WARN << "Trying to lower bound PB encoding that does not encode lower "
            "bounds\n";
    return;
  }

  if (offset > 0 && static_cast<uint64_t>(offset) > b)
    // Offset already larger than bound
    return;
  b -= offset;

  if (b <= hardLB)
    // Already enforced
    return;

  if (b >= getWeightSum()) {
    // Always false
    assumps.push_back(1);
    assumps.push_back(-1);
    return;
  }

  size_t preLen = assumps.size();
  _enforceLB(b, assumps);

  lastAssumps.clear();
  lastAssumps.insert(assumps.begin() + preLen, assumps.end());
  logState();
}

void PBEncoding::enforceLB(uint64_t b, lit_t &assump) {
  std::vector<lit_t> assumps{};
  enforceLB(b, assumps);

  if (assumps.empty()) {
    assump = solver->getNewVar();
    nVars++;
    lastAssumps.clear();
    lastAssumps.insert(assump);
    logState();
    return;
  }

  if (assumps.size() == 1) {
    assump = assumps.front();
    lastAssumps.clear();
    lastAssumps.insert(assump);
    logState();
    return;
  }
  assump = solver->getNewVar();
  nVars++;
  for (lit_t unit : assumps) {
    solver->addClause(-assump, unit);
    nClauses++;
  }

  lastAssumps.clear();
  lastAssumps.insert(assump);
  logState();
}

void PBEncoding::enforceLB(uint64_t b) {
  if (offset < 0 || static_cast<uint64_t>(offset) < b)
    b -= offset;
  else
    b = 0;

  if (b <= hardLB)
    // Already enforced
    return;

  std::vector<lit_t> assumps{};
  enforceLB(b, assumps);

  for (lit_t unit : assumps) {
    solver->addClause(unit);
    nClauses++;
  }
  hardLB = b;

  lastAssumps.clear();
  logState();
}

void PBEncoding::logState() const {
  if (stateLogging)
    INFO << "<PBEncoding>[" << this << "]; #vars = " << getNVars()
         << "; #clauses = " << getNClauses() << "; #lits = " << getNLits()
         << "; weight sum = " << getWeightSum() << "; upper = " << getUpper()
         << "; proc start wall time = " << proc_start_real_time()
         << "s; absolute cpu time = " << absolute_process_time() << "s\n";
}

void PBEncoding::printStats(bool header) const {
  if (header) {
    INFO << "\n";
    INFO << "\n";
    INFO << "PBEncoding Stats\n";
    INFO << LOG_H1;
    INFO << "\n";
  }
  INFO << "#input lits = " << getNLits() << "\n";
  INFO << "summed weight = " << getWeightSum() << "\n";
  INFO << "upper = " << getUpper() << "\n";
  INFO << "#clauses = " << getNClauses() << "\n";
  INFO << "#vars = " << getNVars() << "\n";
}

PBEncoding *pbEncodingFactory(SatSolver *solver, BoundType boundType,
                              std::string id) {
  if (id == "")
    id = opt_pbencoding;
#if ENABLE_CARDPBSIM
  if (id == "gte")
    return new GeneralizedTotalizer(solver, boundType);
#endif
#if ENABLE_CARDPBSIM
  if (id == "card-pb-sim")
    return new CardPBSim(solver, boundType);
#endif
  ERROR << "Unknown PB encoding " << id << "\n";
  exit(1);
}
} // namespace BiOptSat