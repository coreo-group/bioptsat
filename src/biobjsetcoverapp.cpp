/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "biobjsetcoverapp.hpp"

namespace BiOptSat {
// Options
static string cat = "3.7 - Biobjective Set Cover";
static StringOption
    opt_blocking(cat, "biobjective-set-cover-blocking",
                 "How to block solutions in biobjective setcover", "positive",
                 StringSet({"negative", "positive", "both"}));

BiobjSetCoverApp::BiobjSetCoverApp() {
  if (opt_blocking == "negative")
    blockingStrategy = BLOCK_NEG;
  else if (opt_blocking == "positive")
    blockingStrategy = BLOCK_POS;
  else
    blockingStrategy = BLOCK_BOTH;
}

void BiobjSetCoverApp::loadEncode(std::vector<clause_t> &clauses,
                                  Objective &inc, Objective &dec) {
  baseEncoding(clauses, inc, dec);
}

void BiobjSetCoverApp::setupEnumerator() {
  if (enumerator->getEnumType() != ORDERED)
    blockingStrategy = BLOCK_BOTH;
}
} // namespace BiOptSat