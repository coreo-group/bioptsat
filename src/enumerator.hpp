/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _enumerator_hpp_INCLUDED
#define _enumerator_hpp_INCLUDED

#include <cassert>
#include <functional>
#include <string>

#include "satsolver.hpp"
#include "types.hpp"

using std::string;

namespace BiOptSat {
enum AlgState { INIT, SOLVING, SOLVED, BOUND_REACHED };
enum EnumeratorType { ORDERED, UNORDERED };
enum PhaseAdjustment { PA_NONE, PA_NEG, PA_POS };
enum ModelTightening {
  MT_NONE = 0b00,
  MT_FLIP = 0b01,
  MT_LEARN = 0b10,
  MT_BOTH = MT_FLIP | MT_LEARN
};

typedef std::function<void(const model_t &, clause_t &)> BlockingFunc;

void defaultBlockingFunc(const model_t &, clause_t &);

// Abstract base class of all enumerators
class Enumerator {
protected:
  enum OracleCallType { OCT_NEXT_FREE };
  std::vector<string> octNames = {};

  const EnumeratorType type{}; // Some applications might allow model blocking
                               // with shorter clauses for enumerators that find
                               // the pareto front in an ordered fashion

  double realStartTime{};
  double cpuStartTime{};
  double enumerationStartTime{};

  SatSolver *oracle{}; // The satsolver used as NP-oracle
  std::vector<uint64_t> oracleCallLog{};

  const Objective increasing{}; // The increasing objective as a set of literals
  const Objective decreasing{}; // The decreasing objective as a set of literals
  ParetoFront<model_t> paretoFront; // The found pareto front

  std::vector<lit_t>
      objClauses{}; // Zero separated clauses with objective literals. This
                    // clause db representation is taken from Tuukka Korhonen's
                    // "oracle" in sharpsat-td, which might be included in the
                    // future, so then its clause db can be easily used instead.
  std::unordered_map<lit_t, std::vector<size_t>>
      objLitOcc{}; // Clauses that objective literals occur in. Clauses are
                   // represented by indices in `objClauses`.
  uint32_t nModelTightClsInc{};    // The number of clauses learned in model
                                   // tightening the increasing objective
  double avgModelTightClsIncLen{}; // The average length of model tightening
                                   // clauses for the increasing objective
  uint32_t nModelTightClsDec{};    // The number of clauses learned in model
                                   // tightening the decreasing objective
  double avgModelTightClsDecLen{}; // The average length of model tightening
                                   // clauses for the decreasing objective

  AlgState state{INIT}; // The current state of the algorithm

  // Time variables and functions
  double totalRealTime{}; // The total wall clock time spent in the algorithm
  double totalCpuTime{};  // The total cpu time spent in the algorithm

  virtual void startTimer(); // Start the wall and cpu timers
  virtual void stopTimer();  // Stop the timers

  BlockingFunc blockingFunc =
      defaultBlockingFunc; // The function used for generating blocking clauses

  uint32_t nOrigClauses{}; // The number of clauses of the original problem
  var_t maxOrigVar{};      // The highest original variable
  uint32_t nSols{};        // The number of found solutions

  // Main algorithm
  virtual void _enumerate() = 0;

  // Helper functions
  std::pair<uint64_t, uint64_t> getObjValModel(
      bool, bool,
      model_t &); // Returns the objective values, gets the model over
                  // only the original variables and returns it by
                  // reference in the `model_t` parameter. The first two
                  // parameters indicate whether to do model tightening
                  // for the increasing and the decreasing objective.
  bool
  canFlipObjLit(lit_t, const model_t &,
                clause_t &) const; // Checks whether an objective literal can be
                                   // flipped while preserving satisfiability.
                                   // If the literal can be flipped, a clause
                                   // forcing that flip is returned.
  static inline bool lval(lit_t l, const model_t &mdl) {
    assert(var(l) < mdl.size());
    return l > 0 ? mdl[var(l)] : !mdl[var(l)];
  } // Gets the literal value from a model
  void logCandidate(
      uint64_t inc, uint64_t dec,
      bool sat = true) const; // Log a candidate for precise search tracing
  virtual void
  printOracleCallLog() const; // Prints the types of oracle calls that were done

  void phaseModel();   // Phase the solution currently in the solver for
                       // solution-guided search
  void unphaseModel(); // Unphase all variables

  // Options
  uint32_t verbose{};      // The verbosity level of output
  int32_t maxSolsPerPP{};  // Maximum number of solutions per pareto point
  int32_t maxSols{};       // Maximum number of solutions
  int32_t maxPPs{};        // Maximum number of pareto points
  int64_t maxIncreasing{}; // Maximum value for the increasing objective
  int64_t minIncreasing{}; // Minimum value for the increasing objective
  int64_t maxDecreasing{}; // Maximum value for the decreasing objective
  int64_t minDecreasing{}; // Minimum value for the decreasing objective
  PhaseAdjustment phaseAdjustment{}; // Phase adjustment of the objective
                                     // variables in the solver
  ModelTightening modelTightening{}; // What process to tighten the model with
  bool logSearchTrace{};             // Whether to log the search trace
  bool solGuided;                    // Solution-guided search, where applicable

public:
  Enumerator(EnumeratorType type, std::vector<clause_t> &clauses, Objective inc,
             Objective dec,
             string backend = ""); // Note: consumes clauses
  virtual ~Enumerator();

  virtual void addClause(clause_t cl);
  void setBlockFunction(BlockingFunc fnc) { // Change the function used for
                                            // generating blocking clauses
    blockingFunc = fnc;
  }

  void enumerate();

  virtual void printStats() const;
  virtual void printOptions() const;

  inline void interrupt() { stopTimer(); }

  EnumeratorType getEnumType() const { return type; }
  AlgState getAlgState() const { return state; }
  ParetoFront<model_t> getParetoFront() const { return paretoFront; }
  uint32_t getNOrigClauses() const { return nOrigClauses; }
  uint32_t getNSols() const { return nSols; }
  std::pair<uint32_t, uint32_t> getNModelTighteningClauses() const {
    return std::make_pair(nModelTightClsInc, nModelTightClsDec);
  }

  // Option getter and setter (don't change options after starting to enumerate)
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
  int32_t getMaxSolsPerPP() const { return maxSolsPerPP; }
  void setMaxSolsPerPP(int32_t mspp) { maxSolsPerPP = mspp; }
  int32_t getMaxSols() const { return maxSols; }
  void setMaxSols(int32_t ms) { maxSols = ms; }
  int32_t getMaxPPs() const { return maxPPs; }
  void setMaxPPs(int32_t mp) { maxPPs = mp; }
  int64_t getMaxIncreasing() const { return maxIncreasing; }
  void setMaxIncreasing(int64_t mi) { maxIncreasing = mi; }
  int64_t getMinIncreasing() const { return minIncreasing; }
  void setMinIncreasing(int64_t mi) { minIncreasing = mi; }
  int64_t getMaxDecreasing() const { return maxDecreasing; }
  void setMaxDecreasing(int64_t md) { maxDecreasing = md; }
  int64_t getMinDecreasing() const { return minDecreasing; }
  void setMinDecreasing(int64_t md) { minDecreasing = md; }
  PhaseAdjustment getPhaseAdjustment() const { return phaseAdjustment; }
  void setPhaseAdjustment(PhaseAdjustment pa) { phaseAdjustment = pa; }
  bool doesModelTightening() const { return modelTightening; }
  void doModelTightening(ModelTightening mt) { modelTightening = mt; }
  bool doesLogSearchTrace() const { return logSearchTrace; }
  void doLogSearchTrace(bool lst) { logSearchTrace = lst; }
};

// Enumerator factory to generate fitting enumerator class from string
Enumerator *enumeratorFactory(std::vector<clause_t> &clauses, Objective inc,
                              Objective dec, string backend = "",
                              string id = ""); // Empty string for default
} // namespace BiOptSat

#endif