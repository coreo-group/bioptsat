/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "objective.hpp"

namespace BiOptSat {
Objective::Objective(const std::vector<lit_t> &lits) {
  for (lit_t l : lits) {
    if (var(l) > maxVar)
      maxVar = var(l);
    if (wLits.find(l) != wLits.end()) {
      wLits[l] += 1;
      weighted = true;
    } else
      wLits[l] = 1;
  }
}

void Objective::addLit(lit_t l, uint64_t w) {
  if (w == 0) {
    removeLit(l);
    return;
  }
  if (var(l) > maxVar)
    maxVar = var(l);
  wLits[l] = w;
  if (w > 1)
    weighted = true;
}

uint64_t Objective::removeLit(lit_t l) {
  if (wLits.find(l) == wLits.end())
    return 0;
  uint64_t w = wLits[l];
  wLits.erase(l);
  if (var(l) >= maxVar) {
    maxVar = 0;
    for (auto &p : wLits)
      if (var(p.first) > maxVar)
        maxVar = var(p.first);
  }
  return w;
}

uint64_t Objective::getWeight(lit_t l) const {
  const auto &it = wLits.find(l);
  if (it == wLits.end())
    return 0;
  return it->second;
}

bool Objective::hasLit(lit_t l) const {
  return wLits.find(l) != wLits.end();
}

uint64_t Objective::getWeightSum() const {
  uint64_t sum{};
  for (const auto &p : wLits)
    sum += p.second;
  return sum;
}
} // namespace BiOptSat