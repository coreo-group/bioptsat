/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _setcoverapp_hpp_INCLUDED
#define _setcoverapp_hpp_INCLUDED

#include <string>

#include "application.hpp"

using std::string;

namespace BiOptSat {

// An abstract base application for set-cover related problems
// The files are expected to be in the following format:
// First line: <n vars> <m sets>
// Second line: <k objectives>
// k next lines: objectives as list of (integer) weights
// 2m next lines: sets
// First set line: cardinality of set
// Second set line: elements in set
// Every set forms one clause in the CNF
// The weighted objectives are handled by adding the literals as often as the
// weight specifies
class SetCoverApp : public Application {
protected:
  uint32_t incGCD = 1;
  uint32_t decGCD = 1;

  uint32_t nElements{};
  bool biobjective{};

  BlockingStrategy blockingStrategy{};

  void formatSolution(const model_t &, std::vector<string> &) const;
  void baseEncoding(std::vector<clause_t> &, Objective &, Objective &);

public:
  SetCoverApp();

  void printStats() const;

  void blockSolution(const model_t &,
                     clause_t &) const; // Block a solution

  uint32_t scaleInc(uint32_t inc) const { return inc * incGCD; }
  uint32_t scaleDec(uint32_t dec) const { return dec * decGCD; }

  uint32_t getIncGCD() const { return incGCD; }
  uint32_t getDecGCD() const { return decGCD; }
};
} // namespace BiOptSat

#endif