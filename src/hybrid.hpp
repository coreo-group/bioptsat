/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _hybrid_hpp_INCLUDED
#define _hybrid_hpp_INCLUDED

#include <string>

#include "enumerator.hpp"
#include "pbencoding.hpp"

using std::string;

namespace BiOptSat {

enum HybridPhase { HB_PHASE_NONE, HB_PHASE_BOS, HB_PHASE_PM, HB_PHASE_ENUM };

// Class containing the hybrid algorithm between BiOptSat and p-minimal
class HybridEnumerator : public Enumerator {
protected:
  enum OracleCallType {
    OCT_SAT = Enumerator::OCT_NEXT_FREE,
    OCT_PMIN,
    OCT_BOS,
    OCT_ENUM,
    OCT_NEXT_FREE,
  };

  PBEncoding *incPB{}; // The PB encoding for the increasing objective
  PBEncoding *decPB{}; // The PB encoding for the decreasing objective

  uint64_t bosInc{}; // BiOptSat's current bound on the increasing objective
  uint64_t bosDec{}; // BiOptSat's current bound on the decreasing objective
  uint64_t pmInc{};  // P-minimal's current bound on the increasing objective
  uint64_t pmDec{};  // P-minimal's current bound on the decreasing objective
  model_t model{};   // The last model

  HybridPhase phase{HB_PHASE_NONE};

  // Time variables and functions
  double psRealTime{};   // Wall clock time spend in the BiOptSat phase
  double psCpuTime{};    // Cpu time spend on in the BiOptSat phase
  double pmRealTime{};   // Wall clock time spend in the p-minimal phase
  double pmCpuTime{};    // Cpu time spend on in the p-minimal phase
  double enumRealTime{}; // Wall clock time spend on enumerating solutions
  double enumCpuTime{};  // Cpu time spend on enumerating solutions

  void stopTimer(); // Stop the timers. Make sure phase is set correctly first.

  void _enumerate();

  // Algorithm subroutines
  bool pMinimization();
  bool enumSols();

  // Options
  bool pminBound{}; // Bound the next BiOptSat call based on the pmin model

public:
  HybridEnumerator(std::vector<clause_t> &clauses, Objective inc, Objective dec,
                   string backend = "");
  ~HybridEnumerator();

  void printStats() const;
  void printOptions() const;

  // Option getter and setter (don't change options after starting to enumerate)
  bool hasPminBound() const { return pminBound; }
  void doPminBound(bool pb) { pminBound = pb; }
};
} // namespace BiOptSat

#endif