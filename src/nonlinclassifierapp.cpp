/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "nonlinclassifierapp.hpp"

namespace BiOptSat {
// Options
static std::string cat = "3.5 - Non-linear Performance Metric Classifiers";
static StringOption opt_increasing_obj(
    cat, "non-lin-clf-increasing",
    "The increasing objective for learning classifiers optimal "
    "for non-linear performance metrics",
    "false-pos", StringSet({"false-pos", "false-neg"}));

NonLinClassifierApp::NonLinClassifierApp(string name) : clfName(name) {
  if (opt_increasing_obj == "false-pos")
    fpInc = true;
  else
    fpInc = false;
  setObjectiveNames();
}

void NonLinClassifierApp::setupObjectives(Objective &inc, Objective &dec) {
  if (fpInc) {
    getFalsePositiveObj(inc);
    getFalseNegativeObj(dec);
  } else {
    getFalsePositiveObj(dec);
    getFalseNegativeObj(inc);
  }
}

void NonLinClassifierApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "NonLinClassifierApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "increasing objective = " << (fpInc ? "false-pos" : "false-neg")
       << "\n";
}

void NonLinClassifierApp::setObjectiveNames() {
  incName = "False negatives";
  decName = "False positives";
  if (fpInc) {
    incName = "False positives";
    decName = "False negatives";
  }
}
} // namespace BiOptSat