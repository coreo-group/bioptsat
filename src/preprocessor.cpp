/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "preprocessor.hpp"

#include <limits>

#include "globaloptions.hpp"
#include "options.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if WITH_MAXPRE
#include "maxpre.hpp"
#endif

namespace BiOptSat {
// Options
static std::string cat = "4 - PreProcessing";
static BoolOption opt_prepro(cat, "preprocessing",
                             "Wether preprocessing is turned on", true);
static StringOption opt_preprocessor(cat, "preprocessor",
                                     "The MaxSAT preprocessor to use", "maxpre",
                                     StringSet({
#if WITH_MAXPRE
                                       "maxpre",
#endif
                                     }));
static DoubleOption opt_timelimit(
    cat, "prepro-limit", "Time limit for preprocessing (-1 for no limit)", -1,
    DoubleRange(-1, true, std::numeric_limits<double>::max(), true));
static BoolOption opt_print_stats(cat, "prepro-print-stats",
                                  "Wether or not to print preprocessing stats",
                                  true);
static BoolOption opt_reindexing(
    cat, "prepro-reindexing",
    "Whether to do reindexing of variables after preprocessing or not", false);

PreProcessor *preproFactory(std::string id) {
  if (id == "")
    id = opt_preprocessor;
#if WITH_MAXPRE
  if (id == "maxpre")
    return new MaxPre();
#endif
  ERROR << "Unknown solver " << id << "\n";
  exit(1);
}

PreProcessor::PreProcessor()
    : verbose(opt_verbose), doPrintStats(opt_print_stats), doPrepro(opt_prepro),
      timeLimit(opt_timelimit), reindexing(opt_reindexing) {}

void PreProcessor::setInstance(const std::vector<clause_t> &_clauses,
                               const Objective &_obj1, const Objective &_obj2) {
  clauses = &_clauses;
  obj1 = &_obj1;
  obj2 = &_obj2;
  for (clause_t cl : _clauses)
    for (lit_t l : cl)
      if (var(l) > nOrigVars)
        nOrigVars = var(l);
  if (_obj1.getMaxVar() > nOrigVars)
    nOrigVars = _obj1.getMaxVar();
  if (_obj2.getMaxVar() > nOrigVars)
    nOrigVars = _obj2.getMaxVar();
  nOrigClauses = _clauses.size();
  nPreproVars = nOrigVars;
  nPreproClauses = nOrigClauses;
  processed = false;
}

void PreProcessor::preprocess() {
  if (!doPrepro)
    return;

  INFO << "\n";
  INFO << "Starting preprocessing\n";
  INFO << LOG_H1;

  startTimer();
  _preprocess();
  stopTimer();
  processed = true;
}

void PreProcessor::reconstruct(model_t &model) {
  if (!doPrepro | !processed)
    return;

  if (verbose >= 2)
    INFO << "Reconstructing model\n";

  startTimer();

  if (reindexing) {
    model_t reindexedModel = model;
    // Map reindexed vars back
    for (uint32_t i = 1; i < reindexedModel.size(); i++) {
      var_t ov = reindexed2original(i);
      if (ov >= model.size())
        model.resize(ov + 1);
      model[ov] = reindexedModel[i];
    }
  }

  _reconstruct(model);

  stopTimer(true);
}

void PreProcessor::getInstance(std::vector<clause_t> &outClauses,
                               Objective &outObj1, uint64_t &weightOffset1,
                               Objective &outObj2, uint64_t &weightOffset2) {
  if (!processed) {
    // Return original instance
    outClauses = *clauses;
    outObj1 = *obj1;
    outObj2 = *obj2;
    weightOffset1 = 0;
    weightOffset2 = 0;
    return;
  }

  getPreProInstance(outClauses, outObj1, weightOffset1, outObj2, weightOffset2);

  if (reindexing) {
    const Objective origObj1 = outObj1;
    const Objective origObj2 = outObj2;
    const std::vector<clause_t> origClauses = outClauses;
    outObj1.clear();
    outObj2.clear();
    outClauses.clear();
    outClauses.reserve(origClauses.size());

    // Reindex variables
    // First indices for increasing objective, then decreasing objective, then
    // "hard" vars
    for (const auto p : origObj1) {
      lit_t rl = p.first < 0 ? -original2NewReindexed(var(p.first))
                             : original2NewReindexed(var(p.first));
      outObj1.addLit(rl, p.second);
    }
    for (const auto p : origObj2) {
      lit_t rl = p.first < 0 ? -original2NewReindexed(var(p.first))
                             : original2NewReindexed(var(p.first));
      outObj2.addLit(rl, p.second);
    }
    for (const auto &oCl : origClauses) {
      clause_t rCl{};
      rCl.reserve(oCl.size());
      for (const auto ol : oCl) {
        lit_t rl = ol < 0 ? -original2NewReindexed(var(ol))
                          : original2NewReindexed(var(ol));
        rCl.push_back(rl);
      }
      outClauses.push_back(rCl);
    }
    nPreproVars = r2o.size();
  } else {
    // Build identity map to keep track of which variables exist
    for (const auto p : outObj1)
      original2NewReindexed(var(p.first));
    for (const auto p : outObj2)
      original2NewReindexed(var(p.first));
    for (const auto &cl : outClauses)
      for (const auto l : cl)
        original2NewReindexed(var(l));
  }
}

void PreProcessor::reindexClause(clause_t &clause) const {
  if (!doPrepro | !processed)
    return;

  clause_t origClause = clause;
  clause.clear();
  clause.reserve(origClause.size());
  for (auto ol : origClause) {
    lit_t rl = (ol < 0 ? -1 : 1) * original2reindexed(var(ol));
    if (rl)
      clause.push_back(rl);
  }
}

void PreProcessor::printStats() const {
  if (!doPrintStats | !doPrepro)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "PreProcessor Stats\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "nOrigVars = " << nOrigVars << "\n";
  INFO << "nOrigClauses = " << nOrigClauses << "\n";
  INFO << "nPreproVars = " << nPreproVars << "\n";
  INFO << "nPreproClauses = " << nPreproClauses << "\n";
  INFO << "weightOffset1 = " << weightOffset1 << "\n";
  INFO << "weightOffset2 = " << weightOffset2 << "\n";

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Preprocessing Wall clock time: " << realPreproTime << "s\n";
  INFO << "Preprocessing Cpu time: " << cpuPreproTime << "s\n";
  INFO << "Reconstruction Wall clock time: " << realReconstTime << "s\n";
  INFO << "Reconstruction Cpu time: " << cpuReconstTime << "s\n";
}

void PreProcessor::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "PreProcessor Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
  INFO << "doPrepro = " << (doPrepro ? "on" : "off") << "\n";
  INFO << "timeLimit = " << timeLimit << "\n";
  INFO << "reindexing = " << (reindexing ? "on" : "off") << "\n";
}
} // namespace BiOptSat
