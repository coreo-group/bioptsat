/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (OLL Optimization): Jeremias Berg - jeremias.berg@helsinki.fi
 * Based on: Open-WBO (https://github.com/sat-group/open-wbo)
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Jeremias Berg, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "logging.hpp"
#include "utils.hpp"

namespace BiOptSat {
bool BiOptSatEnumerator::msu3IncObj() {
  std::vector<lit_t> assumps{};
  wlits_t pbAdd{};

  if (!msu3GcdIncrement || !increasing.isWeighted())
    msu3Inc = 1;

  uint64_t bound = msu3DisjointPhase();
  std::pair<uint64_t, uint64_t> objVals;

  while (true) {
    if (verbose >= 3) {
      uint32_t notRelaxed = 0;
      for (const auto &p : inactiveWeight)
        notRelaxed += p.second;
      INFO << "minIncObj (MSU3) iteration, bound=" << bound
           << "; #conflict=" << nConflicts
           << "; not relaxed weight=" << notRelaxed << "\n";
    }

    // Generate assumptions
    assumps.clear();
    if (!decreasingHarden)
      upperBoundDec(assumps);
    msu3BoundInc(bound, assumps);

    // Solve
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_MSU3];
    if (ret == SAT) {
      // Cache model
      objVals = getObjValModel(false, true, modelCache);
      assert(bound == objVals.first);
      break;
    }
    assert(ret == UNSAT);
    logCandidate(bound, decBound, false);
    clause_t conf{};
    oracle->getConflict(conf);
    bool end = true;
    for (lit_t l : conf)
      if (!decPB || !decPB->isEnforcingAssump(-l)) {
        end = false;
        break;
      }
    if (end)
      // No more solutions; enumeration ends
      return false;
    nConflicts++;
    avgRawConflictLen =
        ((avgRawConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;
    minimizeConflict(conf);
    avgConflictLen =
        ((avgConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;

    // Process conflict
    pbAdd.clear();
    for (lit_t l : conf) {
      if (incPB && incPB->isEnforcingAssump(-l))
        continue;
      assert(inactiveWeight.find(l) != inactiveWeight.end());
      uint64_t wt = increasing.getWeight(l);
      if (msu3GcdIncrement)
        msu3Inc = gcd(msu3Inc, wt);
      pbAdd[l] = wt;
      inactiveWeight.erase(l);
      inactiveWeightSum -= wt;
      addToDecPB(l);
    }
    bound += msu3Inc;

    if (verbose >= 3) {
      uint64_t weightSum{};
      for (const auto &p : pbAdd)
        weightSum += p.second;
      INFO << "Found core of size " << conf.size() << ", adding "
           << pbAdd.size() << " lits with total weight " << weightSum
           << " to PB encoding\n";
    }

    // Build/extend PB encoding
    if (!incPB)
      incPB =
          pbEncodingFactory(oracle, increasingHarden ? BOUND_BOTH : BOUND_UB);
    incPB->addLits(pbAdd);

    bound = incPB->nextHigherPossible(bound - 1);

    hardenInc(bound);

    if (incPB->getWeightSum() >= increasing.getWeightSum() &&
        pbAdd.size() > 0) {
      if (verbose >= 2)
        INFO << "Increasing PB encoding fully built\n";
    }
    if ((minIncVariant & HYBRID) && hybridImmediateSwitch &&
        hybridSwitchCriterion()) {
      incBound = bound;
      if (verbose >= 2)
        INFO << "Immediately switching over to sat-unsat\n";
      hybridSUCont = true;
      return false;
    }
  }
  if (bound < increasing.getWeightSum()) {
    decBound = objVals.second;
    hardenDec(decBound);
  }
  logCandidate(bound, decBound);
  incBound = bound;
  return true;
}

uint64_t BiOptSatEnumerator::msu3DisjointPhase() {
  if (msu3DisjointVariant == M3D_NONE ||
      (msu3DisjointVariant == M3D_ONCE && incBound != 0))
    return incBound;

  if (verbose >= 3)
    INFO << "Running disjoint phase\n";

  uint32_t origConflicts = nConflicts;
  uint64_t bound = incBound;
  std::vector<lit_t> assumps{};
  wlits_t pbAdd{};

  while (true) {
    if (verbose >= 4) {
      uint32_t notRelaxed = 0;
      for (const auto &p : inactiveWeight)
        notRelaxed += p.second;
      INFO << "disjointPhase (MSU3) iteration, bound=" << bound
           << "; #conflict=" << nConflicts
           << "; not relaxed weight=" << notRelaxed << "\n";
    }

    // Generate assumptions
    assumps.clear();
    if (!decreasingHarden)
      upperBoundDec(assumps);
    msu3BoundInc(incBound, assumps);

    // Solve
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_MSU3];
    if (ret == SAT)
      break;
    assert(ret == UNSAT);
    clause_t conf{};
    oracle->getConflict(conf);
    assert(conf.size() > 0);
    nConflicts++;
    avgRawConflictLen =
        ((avgRawConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;
    minimizeConflict(conf);
    avgConflictLen =
        ((avgConflictLen * (nConflicts - 1)) + conf.size()) / nConflicts;

    // Process conflict
    for (lit_t l : conf) {
      if (!increasing.hasLit(l))
        continue;
      assert(inactiveWeight.find(l) != inactiveWeight.end());
      uint64_t wt = increasing.getWeight(l);
      if (msu3GcdIncrement)
        msu3Inc = gcd(msu3Inc, wt);
      pbAdd[l] = wt;
      inactiveWeight.erase(l);
      addToDecPB(l);
    }
    bound += msu3Inc;

    if (pbAdd.size() <= 0)
      // Break condition for later iterations
      break;

    if (verbose >= 3)
      INFO << "Found core of size " << conf.size() << "\n";
  }

  // Build/extend PB encoding
  if (pbAdd.size() > 0) {
    if (!incPB) {
      if (verbose >= 3)
        INFO << "Building PB encoding over " << pbAdd.size() << " literals\n";
      incPB =
          pbEncodingFactory(oracle, increasingHarden ? BOUND_BOTH : BOUND_UB);
      incPB->addLits(pbAdd);
    } else {
      if (verbose >= 3)
        INFO << "Adding " << pbAdd.size() << " literals to PB encoding\n";
      incPB->addLits(pbAdd);
    }
    bound = incPB->nextHigherPossible(bound - 1);
  } else if (verbose >= 3)
    INFO << "Disjoint phase found no relaxable core\n";

  if (verbose >= 3)
    INFO << "Disjoint phase; found " << nConflicts - origConflicts
         << " cores\n";

  return bound;
}

void BiOptSatEnumerator::msu3BoundInc(uint64_t bound,
                                      std::vector<lit_t> &outAssumps) {
  if (!(minIncVariant & MSU3))
    WARN << "Calling msu3BoundInc but not in MSU3 mode\n";

  assumeInactive(outAssumps);
  if (incPB)
    incPB->enforceUB(bound, outAssumps);
}
} // namespace BiOptSat