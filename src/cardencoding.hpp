/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _cardencoding_hpp_INCLUDED
#define _cardencoding_hpp_INCLUDED

#include <limits>
#include <unordered_set>
#include <vector>

#include "encoding.hpp"
#include "satsolver.hpp"
#include "types.hpp"

namespace BiOptSat {
// Abstract base class for encoding of cardinality constraints
// All encodings allow incremental use
class CardEncoding : public Encoding {
protected:
  uint64_t upper{};               // Maximum encoded right hand side
  std::vector<lit_t> inLits{};    // Input literals
  std::vector<lit_t> extension{}; // Literals to add
  uint32_t nClauses{};            // The number of clauses in the encoding
  uint32_t nVars{};               // The number of variables in the encoding
  std::unordered_set<lit_t> lastAssumps{}; // Assumptions from the _last_ bound

  uint64_t hardUB =
      std::numeric_limits<uint64_t>::max(); // Value of the tightest hard upper
                                            // bound enforced
  uint64_t hardLB{}; // Value of the tightest hard lower bound enforced

  const BoundType boundType; // Whether the encoding encodes <= / > or both

  SatSolver *const solver; // The solver that the encoding is in

  virtual void
  _enforceUB(uint64_t,
             std::vector<lit_t> &) = 0; // Actual internal implementation called
                                        // by wrapper
  virtual void
  _enforceLB(uint64_t,
             std::vector<lit_t> &) = 0; // Actual internal implementation called
                                        // by wrapper

  void logState() const; // Log current encoding state

public:
  CardEncoding(SatSolver *const, const BoundType);
  virtual ~CardEncoding() {}
  void addLits(const std::vector<lit_t> &); // Add additional input literals
  void
  enforceUB(uint64_t,
            std::vector<lit_t> &); // Build necessary encoding and provide
                                   // assumptions to enforce upper bound (<= b)
  void enforceUB(uint64_t);        // Hard version adding units to solver
  void enforceUB(uint64_t,
                 lit_t &); // Version returning a single enforcing literal
  void
  enforceLB(uint64_t,
            std::vector<lit_t> &); // Build necessary encoding and provide
                                   // assumptions to enforce lower bound (> b)
  void enforceLB(uint64_t,
                 lit_t &);  // Version returning a single enforcing literal
  void enforceLB(uint64_t); // Hard version adding units to solver

  bool isEnforcingAssump(lit_t l) {
    return lastAssumps.find(l) != lastAssumps.end();
  } // Check whether a literal is an assumption from the _last_ bound

  virtual void printStats(bool header = true) const;

  // Stats getter
  inline size_t getNLits() const {
    return inLits.size() + extension.size();
  } // Get the number of distinct input literals
  inline uint32_t getUpper() const { return upper; }
  inline uint32_t getNClauses() const { return nClauses; }
  inline uint32_t getNVars() const { return nVars; }
};

// CardEncoding factory to generate fitting CardEncoding class from string
CardEncoding *
cardEncodingFactory(SatSolver *solver, BoundType boundType,
                    std::string id = ""); // Empty string for default
} // namespace BiOptSat

#endif