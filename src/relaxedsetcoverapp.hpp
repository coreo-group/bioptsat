/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _relaxedsetcoverapp_hpp_INCLUDED
#define _relaxedsetcoverapp_hpp_INCLUDED

#include <string>

#include "setcoverapp.hpp"

using std::string;

namespace BiOptSat {

// An application for solving set-cover with one objective and the second
// objective being how many sets are not covered
class RelaxedSetCoverApp : public SetCoverApp {
protected:
  bool relaxationInc{};

  void loadEncode(std::vector<clause_t> &, Objective &, Objective &);
  virtual void setupEnumerator();

public:
  RelaxedSetCoverApp();

  ParetoFront<model_t> getParetoFront() const;
};
} // namespace BiOptSat

#endif