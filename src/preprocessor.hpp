/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _preprocessor_hpp_INCLUDED
#define _preprocessor_hpp_INCLUDED

#include <cassert>
#include <string>
#include <vector>

#include "logging.hpp"
#include "resources.hpp"
#include "types.hpp"

namespace BiOptSat {

class PreProcessor {
private:
  // Variables for timers
  double realStartTime{};
  double cpuStartTime{};

protected:
  // Instance
  const std::vector<clause_t> *clauses{};
  const Objective *obj1{};
  const Objective *obj2{};
  // Reindexing maps
  std::vector<var_t> o2r{}; // Maps original vars to reindexed vars
  std::vector<var_t> r2o{}; // Maps reindexed vars to original vars
  // Timer vars
  double realPreproTime{};  // Total wall clock time spend preprocessing
  double cpuPreproTime{};   // Total cpu time spend preprocessing
  double realReconstTime{}; // Total wall clock time spend reconstructing
  double cpuReconstTime{};  // Total cpu time spend reconstructing
  // Stats
  uint32_t nOrigVars{};      // Number of original variables
  uint32_t nOrigClauses{};   // Number of original clauses
  uint32_t nPreproVars{};    // Number of preprocessed variables
  uint32_t nPreproClauses{}; // Number of preprocessed clauses
  uint64_t weightOffset1{};  // Weight offset after preprocessing
  uint64_t weightOffset2{};  // Weight offset after preprocessing

  bool processed{};

  // Internal preprocessing routine
  virtual void getPreProInstance(
      std::vector<clause_t> &, Objective &, uint64_t &, Objective &,
      uint64_t &) = 0; // Internal instance getter (assumes preprocessed)
  virtual void
  _preprocess() = 0; // Internal prepro routine (always does prepro)
  virtual void _reconstruct(
      model_t &) = 0; // Internal reconst routine (always does reconstruct)

  virtual inline var_t
  original2NewReindexed(var_t orig) { // Returns an existing mapping or creates
                                      // a new one if the variable in unknown
    if (orig > o2r.size())
      o2r.resize(orig, 0);
    if (o2r[orig - 1] == 0) {
      if (reindexing) {
        r2o.push_back(orig);
        o2r[orig - 1] = r2o.size();
      } else {
        if (orig > r2o.size())
          r2o.resize(orig, 0);
        r2o[orig - 1] = orig;
        o2r[orig - 1] = orig;
      }
    }
    assert(r2o[o2r[orig - 1] - 1] == orig);
    return o2r[orig - 1];
  }
  virtual inline var_t
  original2reindexed(var_t orig) const { // Returns 0 if variable is unknown
    if (orig > o2r.size())
      return 0;
    return o2r[orig - 1];
  }
  virtual inline var_t reindexed2original(
      var_t reindexed) const { // Returns 0 if variable is unknown
    if (reindexed > r2o.size())
      return 0;
    return r2o[reindexed - 1];
  }

  // Timer functions
  inline void startTimer() {
    realStartTime = absolute_real_time();
    cpuStartTime = absolute_process_time();
  }
  inline void stopTimer(bool reconst = false) {
    double realTime = absolute_real_time() - realStartTime;
    double cpuTime = absolute_process_time() - cpuStartTime;
    if (reconst) {
      realReconstTime += realTime;
      cpuReconstTime += cpuTime;
      if (verbose >= 3) {
        INFO << "Reconstruction time: real time=" << realTime
             << "; cpu time=" << cpuTime << "\n";
        INFO << "Total Reconstruction time: real time=" << realPreproTime
             << "; cpu time=" << cpuPreproTime << "\n";
      }
    } else {
      realPreproTime += realTime;
      cpuPreproTime += cpuTime;
      if (verbose >= 3) {
        INFO << "Preprocessing time: real time=" << realTime
             << "; cpu time=" << cpuTime << "\n";
        INFO << "Total preprocessing time: real time=" << realPreproTime
             << "; cpu time=" << cpuPreproTime << "\n";
      }
    }
  }

  // Options
  uint32_t verbose{};  // The verbosity level of output
  bool doPrintStats{}; // Wether or not to print statistics
  bool doPrepro{};     // Wether to do preprocessing or not
  double timeLimit{};  // Time limit for preprocessing
  bool reindexing{};   // Whether to reindex the instance after preprocessing

public:
  PreProcessor();
  virtual ~PreProcessor(){};
  // Load instance
  void setInstance(const std::vector<clause_t> &_clauses,
                   const Objective &_obj1, const Objective &_obj2);
  void getInstance(std::vector<clause_t> &, Objective &, uint64_t &,
                   Objective &, uint64_t &); // Get preprocessed instance
  void preprocess();                         // Preprocessing
  void reconstruct(model_t &);               // Reconstruction
  void reindexClause(
      clause_t &clause) const; // Reindex a clause and remove unknown variables

  virtual void printStats() const;   // Print stats of the preprocessor
  virtual void printOptions() const; // Print options of the preprocessor

  // Option getter and setter
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
  bool hasPrintStats() const { return doPrintStats; }
  void setPrintStats(bool ps) { doPrintStats = ps; }
  bool doesPreprocess() const { return doPrepro; }
  void doPreprocess(bool pp) { doPrepro = pp; }
  double getTimeLimit() const { return timeLimit; }
  void setTimeLimit(double tl) { timeLimit = tl; }
  bool doesReindexing() const { return reindexing; }
  void doReindexing(bool ri) { reindexing = ri; }
};

PreProcessor *preproFactory(std::string id = ""); // Empty string for default

} // namespace BiOptSat

#endif