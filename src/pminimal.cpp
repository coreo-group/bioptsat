/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "pminimal.hpp"

#include <cassert>

namespace BiOptSat {
// Constructor/Destructor
PMinimalEnumerator::PMinimalEnumerator(std::vector<clause_t> &clauses,
                                       Objective inc, Objective dec,
                                       string backend)
    : Enumerator(UNORDERED, clauses, inc, dec, backend) {
  oracleCallLog.resize(OCT_NEXT_FREE, 0);
  octNames.resize(OCT_NEXT_FREE);
  octNames[OCT_PMIN] = "P-minimal";
  octNames[OCT_ENUM] = "Enumeration";
}

PMinimalEnumerator::~PMinimalEnumerator() {
  if (incPB) {
    delete incPB;
    incPB = nullptr;
  }
  if (decPB) {
    delete decPB;
    decPB = nullptr;
  }
}

// Timer functions
void PMinimalEnumerator::stopTimer() {
  double realTime = absolute_real_time() - realStartTime;
  double cpuTime = absolute_process_time() - cpuStartTime;

  totalRealTime += realTime;
  totalCpuTime += cpuTime;

  switch (phase) {
  case PM_PHASE_MIN:
    minRealTime += realTime;
    minCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Minimization phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PM_PHASE_ENUM:
    enumRealTime += realTime;
    enumCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Enum phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PM_PHASE_NONE:
    break;
  }
  if (verbose >= 2) {
    INFO << "Total times: real time=" << totalRealTime
         << "; cpu time=" << totalCpuTime << "\n";
    INFO << "Minimization phase times: real time=" << minRealTime
         << "; cpu time=" << minCpuTime << "\n";
    INFO << "Enum phase times: real time=" << enumRealTime
         << "; cpu time=" << enumCpuTime << "\n";
  }
}

// Main Algorithm
void PMinimalEnumerator::_enumerate() {
  // Settings warnings
  if (minIncreasing != -1)
    WARN << "minIncreasing is set but has no effect for p-minimal enumerator\n";
  if (minDecreasing != -1)
    WARN << "minDecreasing is set but has no effect for p-minimal enumerator\n";

  state = SOLVING;

  if (!incPB) {
    incPB = pbEncodingFactory(oracle, BOUND_UB);
    incPB->addLits(increasing);
  }

  if (maxIncreasing != -1 &&
      static_cast<uint64_t>(maxIncreasing) < increasing.getWeightSum())
    incPB->enforceUB(maxIncreasing);

  if (!decPB) {
    decPB = pbEncodingFactory(oracle, BOUND_UB);
    decPB->addLits(decreasing);
  }

  if (maxDecreasing != -1 &&
      static_cast<uint64_t>(maxDecreasing) < decreasing.getWeightSum())
    decPB->enforceUB(maxDecreasing);

  while (true) {
    phase = PM_PHASE_MIN;
    startTimer();

    // Find starting point
    SolverState res = oracle->solve();
    ++oracleCallLog[OCT_PMIN];
    if (res == UNSAT) {
      if (verbose >= 1)
        INFO << "Terminating enumeration due to no more models\n";
      stopTimer();
      state = SOLVED;
      return;
    }
    assert(res == SAT);
    lit_t blockSwitch = 0;
    // Minimize current solution
    while (true) {
      // Save model
      auto objVals = getObjValModel(true, true, model);
      phaseModel();
      // Build assumptions
      incBound = objVals.first;
      decBound = objVals.second;
      std::vector<lit_t> assumps{};
      incPB->enforceUB(incBound, assumps);
      decPB->enforceUB(decBound, assumps);
      // Block previous dominated solutions for good
      if (blockSwitch != 0)
        oracle->addClause(-blockSwitch);
      // Assume blocked dominated solutions
      blockSwitch = oracle->getNewVar();
      clause_t blCl{blockSwitch};
      lit_t out;
      if (incBound > 0) {
        incPB->enforceUB(incBound - 1, out);
        blCl.push_back(out);
      }
      if (decBound > 0) {
        decPB->enforceUB(decBound - 1, out);
        blCl.push_back(out);
      }
      oracle->addClause(blCl);
      assumps.push_back(-blockSwitch);
      // Log Candidate
      logCandidate(incBound, decBound);
      // Query if subset solution exists
      res = oracle->solve(assumps);
      ++oracleCallLog[OCT_PMIN];

      assert(res != UNDEF);
      if (res == UNSAT) {
        stopTimer();
        phase = PM_PHASE_ENUM;
        unphaseModel(); // Unphase for enumeration
        startTimer();
        bool cont = enumSols();
        stopTimer();
        if (verbose >= 1)
          INFO << "P-minimal iteration finished; inc = " << incBound
               << "; dec = " << decBound
               << "; proc start wall time = " << proc_start_real_time()
               << "s; absolute cpu time = " << absolute_process_time() << "s\n";
        if (!cont) {
          state = BOUND_REACHED;
          return;
        }
        // Block dominated solutions for good
        oracle->addClause(-blockSwitch);
        break;
      }
    }
  }
}

// Algorithm subroutines
bool PMinimalEnumerator::enumSols() {
  if (verbose >= 2)
    INFO << "Starting solution enumeration\n";

  bool cont = true;
  bool loop = true;
  std::vector<lit_t> assumps{};
  clause_t blClause{};

  // Add already found model
  ParetoPoint<model_t> pp{
      .models = {model}, .incVal = incBound, .decVal = decBound};
  nSols++;

  // Check early termination conditions
  if (maxSolsPerPP != -1 &&
      pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
    if (verbose >= 2)
      INFO << "Terminating solution enumeration due to max solutions per "
              "pareto point reached\n";
    loop = false;
    cont = true;
  } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
    if (verbose >= 1)
      INFO << "Terminating algorithm due to max solutions reached\n";
    state = BOUND_REACHED;
    loop = false;
    cont = false;
  }
  if (loop) {
    // Block solution found previously
    blockingFunc(model, blClause);
    oracle->addClause(blClause);
    // Build assumptions
    incPB->enforceUB(incBound, assumps);
    decPB->enforceUB(decBound, assumps);
  }

  while (loop) {
    // Solve and get model
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_ENUM];
    if (ret == UNSAT) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to no more models\n";
      cont = true;
      break;
    }
    assert(ret == SAT);
#ifndef NDEBUG
    auto objVals = getObjValModel(false, false, model);
    assert(objVals.first == incBound);
    assert(objVals.second == decBound);
#else
    getObjValModel(false, false, model);
#endif
    pp.models.push_back(model);
    nSols++;
    // Check early termination conditions
    if (maxSolsPerPP != -1 &&
        pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to max solutions per "
                "pareto point reached\n";
      cont = true;
      break;
    } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
      if (verbose >= 1)
        INFO << "Terminating algorithm due to max solutions reached\n";
      cont = false;
      break;
    }
    // Block solution
    blockingFunc(model, blClause);
    oracle->addClause(blClause);
  }

  // Add new pareto point
  uint32_t i;
  for (i = 0; i < paretoFront.size(); i++)
    if (paretoFront[i].incVal > incBound)
      break;
  paretoFront.insert(paretoFront.begin() + i, pp);

  if (maxPPs != -1 && paretoFront.size() >= static_cast<uint32_t>(maxPPs)) {
    if (verbose >= 1)
      INFO << "Terminating due to maximum number of pareto points "
              "reached\n";
    cont = false;
  }

  return cont;
}

// Stats printing
void PMinimalEnumerator::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "P-Minimal Enumerator Stats\n";
  INFO << LOG_H1;

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "P-minimization phase\n";
  INFO << LOG_H3;
  INFO << "Minimization wall clock time: " << minRealTime << "s\n";
  INFO << "Minimization Cpu time: " << minCpuTime << "s\n";
  INFO << "\n";
  INFO << "Enumeration\n";
  INFO << LOG_H3;
  INFO << "Enumeration wall clock time: " << enumRealTime << "s\n";
  INFO << "Enumeration Cpu time: " << enumCpuTime << "s\n";

  INFO << "\n";
  INFO << "PB Encodings\n";
  INFO << LOG_H2;
  if (incPB) {
    INFO << "Increasing PB Encoding\n";
    INFO << LOG_H3;
    INFO << "#input lits = " << incPB->getNLits() << "\n";
    INFO << "summed weight = " << incPB->getWeightSum() << "\n";
    INFO << "upper = " << incPB->getUpper() << "\n";
    INFO << "#clauses = " << incPB->getNClauses() << "\n";
    INFO << "#vars = " << incPB->getNVars() << "\n";
    if (decPB)
      INFO << "\n";
  }
  if (decPB) {
    INFO << "Decreasing PB Encoding\n";
    INFO << LOG_H3;
    INFO << "#input lits = " << decPB->getNLits() << "\n";
    INFO << "summed weight = " << decPB->getWeightSum() << "\n";
    INFO << "upper = " << decPB->getUpper() << "\n";
    INFO << "#clauses = " << decPB->getNClauses() << "\n";
    INFO << "#vars = " << decPB->getNVars() << "\n";
  }

  Enumerator::printStats();
}
} // namespace BiOptSat