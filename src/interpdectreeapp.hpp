/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _interpdectreeapp_hpp_INCLUDED
#define _interpdectreeapp_hpp_INCLUDED

#include "decisiontreeapp.hpp"
#include "interpclassifierapp.hpp"

namespace BiOptSat {
class InterpDecTreeApp : public DecisionTreeApp, public InterpClassifierApp {
protected:
  void getSizeObj(Objective &) const;
  void getErrorObj(Objective &) const;
  void loadEncode(std::vector<clause_t> &, Objective &, Objective &);

public:
  InterpDecTreeApp() : InterpClassifierApp("Tree") {};
  virtual void printOptions() const;
  uint32_t scaleInc(uint32_t inc) const { return sizeInc ? (2 * inc - 1) : inc; }
  uint32_t scaleDec(uint32_t dec) const { return sizeInc ? dec : (2 * dec - 1); }
};
} // namespace BiOptSat

#endif