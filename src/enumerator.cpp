/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "enumerator.hpp"

#include <cassert>
#include <unordered_set>

#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"
#include "resources.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if ENABLE_BIOPTSAT
#include "bioptsat.hpp"
#endif

#if ENABLE_PMINIMAL
#include "pminimal.hpp"
#endif

#if ENABLE_HYBRID
#include "hybrid.hpp"
#endif

namespace BiOptSat {

// Options
static string cat = "2.2 - Enumerator";
static StringOption opt_enumerator(cat, "enumerator",
                                   "Select the enumerator to use", "bioptsat",
                                   StringSet({
#if ENABLE_BIOPTSAT
                                     "bioptsat",
#endif
#if ENABLE_PMINIMAL
                                         "pminimal",
#endif
#if ENABLE_HYBRID
                                         "hybrid",
#endif
                                   }));
static IntOption
    opt_max_per_pp(cat, "max-sols-per-pp",
                   "Maximum number of solutions per pareto point to "
                   "enumerate (-1 for no limit)",
                   1, IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_max_sols(
    cat, "max-sols",
    "Maximum number of solutions in total to enumerate (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption
    opt_max_pps(cat, "max-pps",
                "Maximum number of pareto points to find (-1 for no limit)", -1,
                IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_max_increasing(
    cat, "max-increasing",
    "Maximum value for the increasing objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_min_increasing(
    cat, "min-increasing",
    "Minimum value for the increasing objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_max_decreasing(
    cat, "max-decreasing",
    "Maximum value for the decreasing objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static IntOption opt_min_decreasing(
    cat, "min-decreasing",
    "Minimum value for the decreasing objective (-1 for no limit)", -1,
    IntRange(-1, std::numeric_limits<int>::max()));
static StringOption opt_phase_adjustment(
    cat, "phase-adjustment",
    "Phase adjustment of the objective variables in the solver backend", "none",
    StringSet({"none", "negative", "positive"}));
static StringOption
    opt_model_tightening(cat, "model-tightening",
                         "What process to do model tightening with", "none",
                         StringSet({"none", "flip", "learn", "both"}));
static BoolOption opt_search_trace(cat, "search-trace", "Log the search trace",
                                   false);
static BoolOption opt_sol_guided(cat, "solution-guided-search",
                                 "Do solution-guided search where applicable",
                                 false);

// Default blocking function
void defaultBlockingFunc(const model_t &model, clause_t &outClause) {
  outClause.resize(model.size() - 1);

  for (uint32_t i = 1; i < model.size(); i++)
    outClause[i - 1] = model[i] ? -i : i;
}

// Constructor/Destructor
Enumerator::Enumerator(EnumeratorType type, std::vector<clause_t> &clauses,
                       Objective inc, Objective dec, string backend)
    : type(type), oracleCallLog(OCT_NEXT_FREE, 0), increasing(inc),
      decreasing(dec), verbose(opt_verbose), maxSolsPerPP(opt_max_per_pp),
      maxSols(opt_max_sols), maxPPs(opt_max_pps),
      maxIncreasing(opt_max_increasing), minIncreasing(opt_min_increasing),
      maxDecreasing(opt_max_decreasing), minDecreasing(opt_min_decreasing),
      logSearchTrace(opt_search_trace), solGuided(opt_sol_guided) {
  if (opt_phase_adjustment == "negative")
    phaseAdjustment = PA_NEG;
  else if (opt_phase_adjustment == "positive")
    phaseAdjustment = PA_POS;
  else
    phaseAdjustment = PA_NONE;
  oracle = solverFactory(backend);

  if (opt_model_tightening == "flip")
    modelTightening = MT_FLIP;
  else if (opt_model_tightening == "learn")
    modelTightening = MT_LEARN;
  else if (opt_model_tightening == "both")
    modelTightening = MT_BOTH;
  else
    modelTightening = MT_NONE;

#ifndef NDEBUG
  uint32_t nClauses = clauses.size();
#endif
  while (clauses.size()) {
    clause_t cl = clauses.back();
    clauses.pop_back();
    addClause(cl);
    bool objClause = false;
    for (lit_t l : cl) {
      if (increasing.hasLit(l) || decreasing.hasLit(l)) {
        objClause = true;
        if (objLitOcc.find(l) != objLitOcc.end()) {
          objLitOcc[l].push_back(objClauses.size());
        } else {
          objLitOcc[l] = {objClauses.size()};
        }
      }
    }
    if (objClause) {
      for (lit_t l : cl) {
        objClauses.push_back(l);
      }
      objClauses.push_back(0);
    }
  }
  assert(nOrigClauses == nClauses);
  maxOrigVar = oracle->getNVars();

  // In case objective contains a variable that is not in the hard clauses
  for (const auto &p : increasing)
    if (var(p.first) > maxOrigVar)
      maxOrigVar = var(p.first);
  for (const auto &p : decreasing)
    if (var(p.first) > maxOrigVar)
      maxOrigVar = var(p.first);

  while (oracle->getNVars() < maxOrigVar)
    oracle->getNewVar();

  switch (phaseAdjustment) {
  case PA_NEG:
    for (const auto &p : increasing)
      oracle->phase(-p.first);
    for (const auto &p : decreasing)
      oracle->phase(-p.first);
    break;

  case PA_POS:
    for (const auto &p : increasing)
      oracle->phase(p.first);
    for (const auto &p : decreasing)
      oracle->phase(p.first);
    break;

  case PA_NONE:
    break;
  }
}

Enumerator::~Enumerator() {
  if (oracle) {
    delete oracle;
    oracle = nullptr;
  }
}

void Enumerator::enumerate() {
  INFO << "\n";
  INFO << "Starting enumeration\n";
  INFO << LOG_H1;

  enumerationStartTime = absolute_process_time();
  _enumerate();
}

// Timer functions
void Enumerator::startTimer() {
  realStartTime = absolute_real_time();
  cpuStartTime = absolute_process_time();
}

void Enumerator::stopTimer() {
  double realTime = absolute_real_time() - realStartTime;
  double cpuTime = absolute_process_time() - cpuStartTime;

  totalRealTime += realTime;
  totalCpuTime += cpuTime;
  if (verbose >= 2)
    INFO << "Total times: real time=" << totalRealTime
         << "; cpu time=" << totalCpuTime << "\n";
}

// Helper functions
std::pair<uint64_t, uint64_t> Enumerator::getObjValModel(bool tightenInc,
                                                         bool tightenDec,
                                                         model_t &outModel) {
  outModel.resize(maxOrigVar + 1);
  outModel[0] = false;
  for (var_t i = 1; i <= maxOrigVar; i++)
    outModel[i] = oracle->varModelValue(i);

  uint64_t incValue{};
  uint64_t incReduction{};
  for (const auto &p : increasing) {
    if (lval(p.first, outModel)) {
      clause_t learnedClause{};
      if (tightenInc && modelTightening && // do model tightening on this obj
          !decreasing.hasLit(-p.first) &&  // flipping won't mess up other obj
          (increasing.getWeight(-p.first) <
           increasing.getWeight(p.first)) && // flipping improves this obj
          canFlipObjLit(p.first, outModel, learnedClause) // lit can be flipped
      ) {
        if (modelTightening & MT_FLIP) {
          // flip objective literal
          outModel[var(p.first)] = !outModel[var(p.first)];
          incReduction += p.second;
        }
        if (modelTightening & MT_LEARN) {
          // learn clause that flips objective literal
          oracle->addClause(learnedClause);
          avgModelTightClsIncLen =
              ((avgModelTightClsIncLen * nModelTightClsInc) +
               learnedClause.size()) /
              (nModelTightClsInc + 1);
          ++nModelTightClsInc;
        }
      }
      if (lval(p.first, outModel)) {
        incValue += p.second;
      }
    }
  }

  uint64_t decValue{};
  uint64_t decReduction{};
  for (const auto &p : decreasing) {
    if (lval(p.first, outModel)) {
      clause_t learnedClause{};
      if (tightenDec && modelTightening && // do model tightening on this obj
          !increasing.hasLit(-p.first) &&  // flipping won't mess up other obj
          (decreasing.getWeight(-p.first) <
           decreasing.getWeight(p.first)) && // flipping improves this obj
          canFlipObjLit(p.first, outModel, learnedClause) // lit can be flipped
      ) {
        if (modelTightening & MT_FLIP) {
          // flip objective literal
          outModel[var(p.first)] = !outModel[var(p.first)];
          decReduction += p.second;
        }
        if (modelTightening & MT_LEARN) {
          // learn clause that flips objective literal
          oracle->addClause(learnedClause);
          avgModelTightClsDecLen =
              ((avgModelTightClsDecLen * nModelTightClsDec) +
               learnedClause.size()) /
              (nModelTightClsDec + 1);
          ++nModelTightClsDec;
        }
      }
      if (lval(p.first, outModel)) {
        decValue += p.second;
      }
    }
  }

  if (verbose >= 2 && modelTightening)
    INFO << "Model tightening reduced the increasing objective by "
         << incReduction << " and the decreasing objective by " << decReduction
         << "\n";

  return std::make_pair(incValue, decValue);
}

bool Enumerator::canFlipObjLit(lit_t lit, const model_t &mdl,
                               clause_t &explainingClause) const {
  if (objLitOcc.find(lit) == objLitOcc.end()) {
    // Literal does not appear in encoding
    explainingClause.clear();
    explainingClause.push_back(-lit);
    return true;
  }

  std::unordered_set<lit_t> explainingLits{};
  for (size_t clIdx : objLitOcc.find(lit)->second) {
    bool cl_sat = false;
    for (auto i = objClauses.cbegin() + clIdx; *i != 0 && i != objClauses.end();
         ++i) {
      const lit_t other = *i;
      if (other == lit)
        continue;
      if (lval(other, mdl)) {
        explainingLits.insert(-other);
        cl_sat = true;
        break;
      }
    }
    if (!cl_sat)
      return false;
  }
  explainingClause.clear();
  explainingClause.push_back(-lit);
  explainingClause.insert(explainingClause.end(), explainingLits.begin(),
                          explainingLits.end());
  return true;
}

void Enumerator::logCandidate(uint64_t inc, uint64_t dec, bool sat) const {
  if (logSearchTrace)
    INFO << "<SearchTrace>; [" << (sat ? "SAT" : "UNSAT") << "]; inc = " << inc
         << "; dec = " << dec
         << "; proc start wall time = " << proc_start_real_time()
         << "s; absolute cpu time = " << absolute_process_time() << "s\n";
}

void Enumerator::phaseModel() {
  if (solGuided) {
    for (lit_t var = 1; var <= static_cast<lit_t>(oracle->getNVars()); var++)
      oracle->phase(oracle->varModelValue(var) ? var : -var);
  }
}

void Enumerator::unphaseModel() {
  if (solGuided) {
    for (lit_t var = 1; var <= static_cast<lit_t>(oracle->getNVars()); var++)
      oracle->unphase(var);
  }
}

// Functionality
void Enumerator::addClause(clause_t cl) {
  for (lit_t l : cl)
    if (state != INIT && var(l) > maxOrigVar) {
      ERROR << "Adding a clause over variables that were not in the original "
               "clauses is not supported if not in INIT state\n";
      exit(1);
    } else if (var(l) > maxOrigVar) {
      maxOrigVar = var(l);
    }
  nOrigClauses++;
  oracle->addClause(cl);
}

// Stats and options printing
void Enumerator::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Enumerator Stats\n";
  INFO << LOG_H1;

  std::string st{};
  switch (state) {
  case INIT:
    st = "INIT";
    break;
  case SOLVING:
    st = "SOLVING";
    break;
  case SOLVED:
    st = "SOLVED";
    break;
  case BOUND_REACHED:
    st = "BOUND_REACHED";
    break;
  }

  INFO << "\n";
  INFO << "state = " << st << "\n";
  INFO << "#original clauses = " << nOrigClauses << "\n";
  INFO << "#original vars = " << maxOrigVar << "\n";
  INFO << "#solutions = " << nSols << "\n";
  INFO << "#model tight clauses inc = " << nModelTightClsInc << "\n";
  INFO << "avg model tight clause inc len = " << avgModelTightClsIncLen << "\n";
  INFO << "#model tight clauses dec = " << nModelTightClsDec << "\n";
  INFO << "avg model tight clause dec len = " << avgModelTightClsDecLen << "\n";

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Total\n";
  INFO << LOG_H3;
  INFO << "Total wall clock time: " << totalRealTime << "s\n";
  INFO << "Total Cpu time: " << totalCpuTime << "s\n";

  printOracleCallLog();

  oracle->printStats();
}

void Enumerator::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Enumerator Options\n";
  INFO << LOG_H1;

  string pa{};
  switch (phaseAdjustment) {
  case PA_NEG:
    pa = "negative";
    break;

  case PA_POS:
    pa = "positive";
    break;

  case PA_NONE:
    pa = "none";
    break;
  }

  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
  INFO << "maxSolsPerPP = " << maxSolsPerPP << "\n";
  INFO << "maxSols = " << maxSols << "\n";
  INFO << "maxPPs = " << maxPPs << "\n";
  INFO << "maxIncreasing = " << maxIncreasing << "\n";
  INFO << "minIncreasing = " << minIncreasing << "\n";
  INFO << "maxDecreasing = " << maxDecreasing << "\n";
  INFO << "minDecreasing = " << minDecreasing << "\n";
  INFO << "phaseAdjustment = " << pa << "\n";
  INFO << "modelTightening = " << (modelTightening ? "on" : "off") << "\n";
  INFO << "logSearchTrace = " << (logSearchTrace ? "on" : "off") << "\n";
}

void Enumerator::printOracleCallLog() const {
  INFO << "\n";
  INFO << "Oracle calls\n";
  INFO << LOG_H2;
  for (uint8_t oct = 0; oct < octNames.size(); oct++) {
    INFO << octNames[oct] << " = " << oracleCallLog[oct] << "\n";
  }
}

Enumerator *enumeratorFactory(std::vector<clause_t> &clauses, Objective inc,
                              Objective dec, string backend, string id) {
  if (id == "")
    id = opt_enumerator;
#if ENABLE_BIOPTSAT
  if (id == "bioptsat")
    return new BiOptSatEnumerator(clauses, inc, dec, backend);
#endif
#if ENABLE_PMINIMAL
  if (id == "pminimal")
    return new PMinimalEnumerator(clauses, inc, dec, backend);
#endif
#if ENABLE_HYBRID
  if (id == "hybrid")
    return new HybridEnumerator(clauses, inc, dec, backend);
#endif
  ERROR << "Unknown enumerator " << id << "\n";
  exit(1);
}
} // namespace BiOptSat
