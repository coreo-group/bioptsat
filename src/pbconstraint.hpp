/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _pbconstraint_hpp_INCLUDED
#define _pbconstraint_hpp_INCLUDED

#include <string>

#include "satsolver.hpp"
#include "types.hpp"

using std::string;

namespace BiOptSat {

enum PBConstrType { PBC_LE = BOUND_UB, PBC_GE = BOUND_LB, PBC_EQ = BOUND_BOTH };
enum PBConstrEncType {
  PBCE_PBC,
  PBCE_CARD,
  PBCE_UNITS,
  PBCE_CLAUSE,
  PBCE_UNSAT,
  PBCE_NONE,
  PBCE_LEN, // Needs to be last element always
};

// Class for parsing and encoding PB constraints
class PBConstraint {
protected:
  wlits_t wlits{};
  int64_t rhs{};
  PBConstrType constrType{};

public:
  PBConstraint(string); // Parse OPB constraint line
  PBConstraint(wlits_t &, int64_t, PBConstrType);
  PBConstraint(std::unordered_map<lit_t, int64_t> &, int64_t, PBConstrType);

  void addLiteral(lit_t, int64_t); // Add a literal or change it's weight
  // Add the PB constraint to a solver
  // Returns the encoding type used
  PBConstrEncType addToSolver(SatSolver *const, string cardEncType = "",
                              string pbEncType = "") const;
  inline var_t getMaxVar() const {
    var_t max{};
    for (auto const &p : wlits)
      if (var(p.first) > max)
        max = var(p.first);
    return max;
  }
};

} // namespace BiOptSat

#endif