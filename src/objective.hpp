/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _objective_hpp_INCLUDED
#define _objective_hpp_INCLUDED

#include "types.hpp"

namespace BiOptSat {
class PBEncoding;

class Objective {
  friend class PBEncoding;

protected:
  bool weighted{};
  wlits_t wLits{};
  var_t maxVar{};

public:
  Objective() {}
  Objective(const std::vector<lit_t> &);
  void addLit(lit_t, uint64_t); // Updates the lit if already in objective
  inline void addLit(lit_t l) { addLit(l, 1); }
  uint64_t removeLit(lit_t);       // Returns removed weight
  uint64_t getWeight(lit_t) const; // Get weight of literal in objective
  bool hasLit(lit_t)
      const; // Get wether a literal is in the objective (might have 0 weight)
  bool hasVar(var_t v) const { return hasLit(lit(v)) || hasLit(-lit(v)); };
  bool isWeighted() const { return weighted; }
  bool isEmpty() const { return !wLits.size(); }
  std::size_t getNLits() const { return wLits.size(); }
  var_t getMaxVar() const { return maxVar; };
  uint64_t getWeightSum() const;
  void clear() {
    wLits.clear();
    weighted = false;
  }

  using iterator = wlits_t::iterator;
  using const_iterator = wlits_t::const_iterator;

  inline iterator begin() { return wLits.begin(); }
  inline const_iterator begin() const { return wLits.cbegin(); }
  inline iterator end() { return wLits.end(); }
  inline const_iterator end() const { return wLits.cend(); }
};
} // namespace BiOptSat

#endif