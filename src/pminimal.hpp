/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _pminimal_hpp_INCLUDED
#define _pminimal_hpp_INCLUDED

#include <string>

#include "enumerator.hpp"
#include "pbencoding.hpp"

using std::string;

namespace BiOptSat {

enum PMinimalPhase { PM_PHASE_NONE, PM_PHASE_MIN, PM_PHASE_ENUM };

// Class containing the main p-minimal enumeration algorithm
class PMinimalEnumerator : public Enumerator {
protected:
  enum OracleCallType {
    OCT_PMIN = Enumerator::OCT_NEXT_FREE,
    OCT_ENUM,
    OCT_NEXT_FREE,
  };

  PBEncoding *incPB{}; // The PB encoding for the increasing objective
  PBEncoding *decPB{}; // The PB encoding for the decreasing objective

  uint64_t incBound{}; // The current bound on the increasing objective
  uint64_t decBound{}; // The current bound on the decreasing objective
  model_t model{};     // The last model

  PMinimalPhase phase{PM_PHASE_NONE};

  // Time variables and functions
  double minRealTime{};  // Wall clock time spend in the minimization phase
  double minCpuTime{};   // Cpu time spend on in the minimization phase
  double enumRealTime{}; // Wall clock time spend on enumerating solutions
  double enumCpuTime{};  // Cpu time spend on enumerating solutions

  void stopTimer(); // Stop the timers. Make sure phase is set correctly first.

  void _enumerate();

  // Algorithm subroutines
  bool enumSols();

public:
  PMinimalEnumerator(std::vector<clause_t> &clauses, Objective inc,
                     Objective dec, string backend = "");
  ~PMinimalEnumerator();

  void printStats() const;
};
} // namespace BiOptSat

#endif