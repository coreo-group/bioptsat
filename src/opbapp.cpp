/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "opbapp.hpp"

#include <cassert>
#include <fstream>
#include <limits>

#include "dummysolver.hpp"
#include "globaloptions.hpp"
#include "options.hpp"
#include "utils.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

namespace BiOptSat {
// Options
static std::string cat = "3.8 - OPB Application Options";
static StringOption opt_cardencoding(
    cat, "opb-card-encoding",
    "The cardinality encoding to use when encoding OPB instances", "",
    StringSet({
#if ENABLE_TOTALIZER
      "totalizer",
#endif
    }));
static StringOption
    opt_pbencoding(cat, "opb-pb-encoding",
                   "The PB encoding to use when encoding OPB instances", "",
                   StringSet({
#if ENABLE_GTE
                     "gte",
#endif
#if ENABLE_CARDPBSIM
                         "card-pb-sim",
#endif
                   }));

OPBApp::OPBApp()
    : cardEncType(opt_cardencoding), pbEncType(opt_pbencoding),
      incObjIdx(opt_inc_obj - 1), decObjIdx(opt_dec_obj - 1) {}

void OPBApp::loadEncode(std::vector<clause_t> &clauses, Objective &inc,
                        Objective &dec) {
  if (verbose >= 0)
    INFO << "Loading instance " << instance << "\n";

  nVars = 0;
  std::vector<PBConstraint> pbcs{};

  incName = "obj" + std::to_string(incObjIdx);
  decName = "obj" + std::to_string(decObjIdx);

  // Parse file
  string line{};
  std::fstream file{};
  file.open(instance, std::fstream::in);
  string weightStr{};
  string litStr{};
  lit_t l{};
  uint64_t w{};
  size_t objIdx{};
  size_t pos{};

  while (getline(file, line)) {
    ltrim(line); // There shouldn't be any leading whitespace, but just in case
    if (line[0] == '*')
      continue;
    if (line.substr(0, 5) == "min: ") {
      if (objIdx != incObjIdx && objIdx != decObjIdx) {
        objIdx++;
        continue;
      }
      line.erase(0, 5);
      ltrim(line);

      uint64_t offset{};

      while (line.size() && line[0] != ';') {
        // Get weight and lit token
        pos = line.find(" ");
        assert(pos != string::npos);
        weightStr = line.substr(0, pos);
        line.erase(0, pos + 1);
        ltrim(line);
        pos = line.find(" ");
        if (pos == string::npos)
          pos = line.find(";");
        assert(pos != string::npos);
        litStr = line.substr(0, pos);
        line.erase(0, pos + 1);
        ltrim(line);

        // Parse lit
        if (litStr[0] == '~') {
          assert(litStr[1] == 'x');
          l = -std::stoi(litStr.substr(2));
        } else {
          assert(litStr[0] == 'x');
          l = std::stoi(litStr.substr(1));
        }

        // Parse weight
        if (weightStr[0] == '-') {
          w = std::stoi(weightStr.substr(1));
          // Negate literal for negative weight
          l = -l;
          offset += w;
        } else if (weightStr[0] == '+') {
          w = std::stoi(weightStr.substr(1));
        } else {
          assert(weightStr[0] >= '0' && weightStr[0] <= '9');
          w = std::stoi(weightStr);
        }

        if (var(l) > nVars)
          nVars = var(l);

        if (objIdx == incObjIdx)
          inc.addLit(l, w);
        if (objIdx == decObjIdx)
          dec.addLit(l, w);
      }

      if (objIdx == incObjIdx)
        negIncOffset = offset;
      if (objIdx == decObjIdx)
        negDecOffset = offset;

      objIdx++;
    } else {
      pbcs.push_back(PBConstraint(line));
      var_t maxvar = pbcs.back().getMaxVar();
      if (maxvar > nVars)
        nVars = maxvar;
    }
  }

  DummySolver dSolver;
  // Reserve encoding variables in solver
  for (var_t v = dSolver.getNVars(); v < nVars; v = dSolver.getNewVar())
    ;

  for (size_t i = 0; i < PBCE_LEN; i++)
    pbcTranslationCounts[i] = 0;
  // Encode PB constraints
  for (const auto &pbc : pbcs) {
    PBConstrEncType pbcet = pbc.addToSolver(&dSolver, cardEncType, pbEncType);
    pbcTranslationCounts[pbcet]++;
  }

  nVars = dSolver.getNVars();
  nClauses = dSolver.getNClauses();

  clauses = dSolver.getClauses();
}

void OPBApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "OPBApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "card encoding type = " << cardEncType << "\n";
  INFO << "pb encoding type = " << pbEncType << "\n";
  INFO << "increasing objective index = " << incObjIdx << "\n";
  INFO << "decreasing objective index = " << decObjIdx << "\n";
  INFO << "increasing objective negative offset = " << negIncOffset << "\n";
  INFO << "decreasing objective negative offset = " << negDecOffset << "\n";

  Application::printOptions();
}

void OPBApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "OPBApp Stats\n";
  INFO << LOG_H1;

  INFO << "PB constraint translation summary\n";
  INFO << LOG_H2;
  INFO << "#PBC = " << pbcTranslationCounts[PBCE_PBC] << "\n";
  INFO << "#Card = " << pbcTranslationCounts[PBCE_CARD] << "\n";
  INFO << "#Units = " << pbcTranslationCounts[PBCE_UNITS] << "\n";
  INFO << "#Clause = " << pbcTranslationCounts[PBCE_CLAUSE] << "\n";
  INFO << "#UNSAT = " << pbcTranslationCounts[PBCE_UNSAT] << "\n";
  INFO << "#None = " << pbcTranslationCounts[PBCE_NONE] << "\n";

  Application::printStats();
}

} // namespace BiOptSat