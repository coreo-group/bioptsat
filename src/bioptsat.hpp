/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (OLL Optimization): Jeremias Berg - jeremias.berg@helsinki.fi
 * Based on (MSU3, OLL): Open-WBO (https://github.com/sat-group/open-wbo)
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Jeremias Berg, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _bioptsat_hpp_INCLUDED
#define _bioptsat_hpp_INCLUDED

#include <functional>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "cardencoding.hpp"
#include "enumerator.hpp"
#include "pbencoding.hpp"
#include "resources.hpp"
#include "satsolver.hpp"
#include "types.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

namespace BiOptSat {

enum BiOptSatPhase { PS_PHASE_NONE, PS_PHASE_INC, PS_PHASE_DEC, PS_PHASE_ENUM };
enum MinIncConcepts {
  CORE_GUIDED = 0b00001,
  MODEL_IMPROVING = 0b00010,
  LOWER_BOUNDING = 0b00100,
  HYBRID = 0b01000,
  SOFT_RELAX = 0b10000,
};
enum MinIncVariant {
  SAT_UNSAT = MODEL_IMPROVING,
  UNSAT_SAT = LOWER_BOUNDING,
  MSU3 = CORE_GUIDED | LOWER_BOUNDING,
  OLL = CORE_GUIDED | LOWER_BOUNDING | SOFT_RELAX,
  MSU3_SU_HYBRID = HYBRID | MSU3 | SAT_UNSAT,
#if ENABLE_OSHYBRID
  OLL_SU_HYBRID = HYBRID | OLL | SAT_UNSAT,
#endif
};
enum ConfMinVariant { CM_NONE, CM_EXACT, CM_HEURISTIC };
enum Msu3DisjointVariant { M3D_NONE, M3D_ONCE, M3D_ALL };

// Class containing the main BiOptSat enumeration algorithm
class BiOptSatEnumerator : public Enumerator {
protected:
  enum OracleCallTypes {
    OCT_SAT = Enumerator::OCT_NEXT_FREE,
    OCT_SU,
    OCT_US,
    OCT_MSU3,
    OCT_OLL,
    OCT_DEC,
    OCT_ENUM,
    OCT_CONF_MIN,
    OCT_CONF_EXH,
    OCT_NEXT_FREE,
  };

  PBEncoding *incPB{};    // The PB encoding for the increasing objective
  PBEncoding *decPB{};    // The PB encoding for the decreasing objective
  bool decPBInit = false; // Wether the decreasing PB encoding is initialized

  uint64_t incBound{};     // The current bound on the increasing objective
  uint64_t decBound{};     // The current bound on the decreasing objective
  uint64_t hardDecBound{}; // The decreasing bound that is hardened

  BiOptSatPhase phase{
      PS_PHASE_NONE}; // The current phase/subroutine of the algorithm

  // Time variables and functions
  double incRealTime{};  // Wall clock time spend on minimizing the increasing
                         // objective
  double incCpuTime{};   // Cpu time spend on the increasing objective
  double decRealTime{};  // Wall clock time spend on the decreasing objective
  double decCpuTime{};   // Cpu time spend on the decreasing objective
  double enumRealTime{}; // Wall clock time spend on enumerating solutions
  double enumCpuTime{};  // Cpu time spend on enumerating solutions

  void stopTimer(); // Stop the timers. Make sure phase is set correctly first.

  uint32_t
      nConflicts{}; // The number or conflicts found in the core guided variants
  double avgConflictLen{};    // The average length of the conflicts
  double avgRawConflictLen{}; // The average length of the conflicts before
                              // minimization

  void _enumerate();

  // Algorithm subroutines
  // See explanations in paper
  bool minIncObj();
  bool minDecObj();
  bool enumSols();

  // Implementation variants of minIncObj
  bool satUnsatIncObj(); // Model-improving (sat-unsat)
  bool unsatSatIncObj(); // Unsat-Sat lower-bounding search
  bool msu3IncObj();     // MSU3
  bool ollIncObj();      // OLL

  // OLL Subroutines
  uint64_t
  ollReformulateConfs(std::vector<std::pair<clause_t, uint64_t>>
                          &); // Reformulate delayed conflicts as cardinality
                              // constraints. Returns a lower bound increase
                              // from core exhaustion.
#if ENABLE_OSHYBRID
  void mergeOllTots(); // Transition routine for OLL sat-unsat hybrid
#endif

  // Core guided specific variables
  wlits_t
      inactiveWeight{}; // Weight of inactive literals in core guided variants.
                        // Since assumptions are built from this, it is ordered
  uint64_t inactiveWeightSum{}; // The weight sum of the inactive literals
  uint64_t decForcedWeight{}; // Weight forced from the increasing objective on
                              // the decreasing one

  uint64_t msu3Inc{}; // The amount that the MSU3 PB encoding bound is
                      // incremented each step

  // OLL specific variables
  typedef struct {
    size_t cardIdx;
    uint64_t bound;
    uint64_t weight;
  } OutLitData;
  std::vector<std::pair<CardEncoding *, uint64_t>>
      ollRefConfs{}; // Vector of reformulated conflicts and their weight
  std::unordered_map<lit_t, OutLitData> ollAssumpOutLits{}; // Lit -> OutLitData

  // Hybrid specific variables
  bool hybridSUCont{}; // Set to true if MSU3 or OLL didn't find the optimum and
                       // sat-unsat should continue
  bool hybridPhaseTwo{}; // Indicator set to true when a hybrid variant switches
                         // into the second phase
  inline bool hybridSwitchCriterion() {
    return inactiveWeightSum <=
               static_cast<uint64_t>((1 - hybridSwitchPortion) *
                                     increasing.getWeightSum()) ||
           (hybridSwitchMaxTime > 0 &&
            hybridSwitchMaxTime <=
                absolute_process_time() - enumerationStartTime);
  }

  void minimizeConflict(clause_t &); // Minimize a conflict

  // Helper functions
  void
  upperBoundInc(std::vector<lit_t> &); // Append assumptions to enforce
                                       // (O_I <= incBound) for current variant
  void upperBoundDec(
      std::vector<lit_t> &); // Append assumptions to enforce (O_D <= decBound)
  void assumeInactive(
      std::vector<lit_t> &); // Append assumptions for unrelaxed literals

  void miBoundInc(
      uint64_t,
      std::vector<lit_t> &); // Generate (O_I <= bound) for model improving
                             // variants and append to assumptions
  void msu3BoundInc(
      uint64_t,
      std::vector<lit_t> &); // Generate (O_I <= bound) for MSU3 variant and
                             // append to assumptions. Note that the bound is
                             // only over already relaxed literals.
  void ollBoundInc(
      std::vector<lit_t> &); // Generate bound on increasing objective for OLL,
                             // based on the state of the OLL specific variables
  void boundDec(uint64_t,
                std::vector<lit_t>
                    &); // Generate (O_D <= bound) and append to assumptions

  void hardenInc(uint64_t); // Harden a bound on the increasing moving
                            // objective (O_I >= bound)
  void hardenDec(
      uint64_t); // Harden a bound on the decreasing objective (O_D <= bound)

  void addToDecPB(
      lit_t); // Add a literal to the decreasing PB enc when it
              // becomes active for the increasing objective (for lazy
              // decreasing PB enc construction with core guided approaches)
  uint64_t msu3DisjointPhase(); // Disjoint core extraction phase for MSU3

  // Variable for saving model found in minimization subprocedure to not have to
  // find it again
  model_t modelCache{};

  // Options
  MinIncVariant
      minIncVariant{}; // Variant for minimizing the increasing objective
  ConfMinVariant confMinVariant{}; // Variant for minimizing conflicts
  bool confExhaustion{};           // Conflict exhaustion for OLL
  bool increasingHarden{};         // Harden bounds on the increasing objective
  bool decreasingHarden{};         // Harden bounds on the decreasing objective
  bool coreGuidedLazyDecPB{};   // Wether the decreasing PB encoding is lazily
                                // build for the core guided variants
  bool hybridImmediateSwitch{}; // Wether to switch immediately from MSU3 to
                                // sat-unsat or only in the next iteration
  bool blockDominated{}; // Block the north east rectangle from candidates
  Msu3DisjointVariant msu3DisjointVariant{}; // Enable disjoint phase for MSU3
  bool msu3GcdIncrement{};       // Wether to estimate the MSU3 increments for
                                 // weighted instances with a running GCD of the
                                 // weights
  double hybridSwitchPortion{};  // The portion of active objective literals at
                                 // which to switch to sat-unsat
  int32_t hybridSwitchMaxTime{}; // The maximum time after which to switch to
                                 // sat-unsat
  bool solGuided{}; // Whether to perform solution-guided search in sat-unsat

public:
  BiOptSatEnumerator(std::vector<clause_t> &clauses, Objective inc,
                     Objective dec, std::string backend = "");
  ~BiOptSatEnumerator();

  void printStats() const;
  void printOptions() const;

  uint32_t getNConflicts() const { return nConflicts; }
  double getAvgConflictLen() const { return avgConflictLen; }
  double getAvgRawConflictLen() const { return avgRawConflictLen; }

  // Option getter and setter (don't change options after starting to enumerate)
  MinIncVariant getMinIncVariant() const { return minIncVariant; }
  void setMinIncVariant(MinIncVariant muv) {
    minIncVariant = muv;
    inactiveWeight.clear();
    if (minIncVariant & CORE_GUIDED) {
      inactiveWeight.insert(increasing.begin(), increasing.end());
      inactiveWeightSum = increasing.getWeightSum();
    }
  }
  ConfMinVariant getConfMinVariant() const { return confMinVariant; }
  void setConfMinVariant(ConfMinVariant cmv) { confMinVariant = cmv; }
  bool hasConfExhaustion() const { return confExhaustion; }
  void setConfExhaustion(bool ce) { confExhaustion = ce; }
  bool hasIncreasingHarden() const { return increasingHarden; }
  void setIncreasingHarden(bool ih) { increasingHarden = ih; }
  bool hasDecreasingHarden() const { return decreasingHarden; }
  void setDecreasingHarden(bool dh) { decreasingHarden = dh; }
  bool hasCoreGuidedLazyDecPB() const { return coreGuidedLazyDecPB; }
  void setCoreGuidedLazyDecPB(bool cgldpb) { coreGuidedLazyDecPB = cgldpb; }
  bool hasHybridImmediateSwitch() const { return hybridImmediateSwitch; }
  void setHybridImmediateSwitch(bool his) { hybridImmediateSwitch = his; }
  bool hasBlockNorthEast() const { return blockDominated; }
  void setBlockNorthEast(bool bne) { blockDominated = bne; }
  Msu3DisjointVariant getMsu3DisjointVariant() const {
    return msu3DisjointVariant;
  }
  void setMsu3DisjointVariant(Msu3DisjointVariant m3dv) {
    msu3DisjointVariant = m3dv;
  }
  bool hasMsu3GcdIncrement() const { return msu3GcdIncrement; }
  void setMsu3GcdIncrement(bool m3gi) { msu3GcdIncrement = m3gi; }
  double getHybridSwitchPortion() const { return hybridSwitchPortion; }
  void setHybridSwitchPortion(double hsp) { hybridSwitchPortion = hsp; }
  int32_t getHybridSwitchMaxTime() const { return hybridSwitchMaxTime; }
  void setHybridSwitchMaxTime(int32_t hsmt) { hybridSwitchMaxTime = hsmt; }
};

} // namespace BiOptSat

#endif