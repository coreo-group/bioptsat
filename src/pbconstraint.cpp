/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "pbconstraint.hpp"

#include <cassert>
#include <limits>

#include "cardencoding.hpp"
#include "pbencoding.hpp"
#include "utils.hpp"

namespace BiOptSat {
PBConstraint::PBConstraint(wlits_t &wlits, int64_t rhs, PBConstrType constrType)
    : wlits(wlits), rhs(rhs), constrType(constrType) {}

PBConstraint::PBConstraint(std::unordered_map<lit_t, int64_t> &iwlits,
                           int64_t rhs, PBConstrType constrType)
    : rhs(rhs), constrType(constrType) {
  for (auto const &p : iwlits) {
    if (p.second >= 0)
      wlits[p.first] = p.second;
    else {
      // Negate literal for negative coefficients
      wlits[-p.first] = -p.second;
      rhs -= p.second;
    }
  }
}

// Parse an OPB file according to
// https://www.cril.univ-artois.fr/PB12/format.pdf
// Negated literals are allowed, even though they shouldn't appear in linear
// instances; '<=' is also allowed; positive integers without '+' are allowed
PBConstraint::PBConstraint(string opbStr) {
  ltrim(opbStr);
  assert(opbStr.size() > 0 && (opbStr[0] == '+' || opbStr[0] == '-' ||
                               (opbStr[0] >= '0' && opbStr[0] <= '9')));

  // Parse LHS
  size_t pos;
  string weightStr{};
  string litStr{};
  lit_t l{};
  uint64_t w{};
  while (opbStr[0] != '>' && opbStr[0] != '=' && opbStr[0] != '<') {
    // Get weight and lit token
    pos = opbStr.find(" ");
    assert(pos != string::npos);
    weightStr = opbStr.substr(0, pos);
    opbStr.erase(0, pos + 1);
    ltrim(opbStr);
    pos = opbStr.find(" ");
    assert(pos != string::npos);
    litStr = opbStr.substr(0, pos);
    opbStr.erase(0, pos + 1);
    ltrim(opbStr);

    // Parse lit
    if (litStr[0] == '~') {
      assert(litStr[1] == 'x');
      l = -std::stoi(litStr.substr(2));
    } else {
      assert(litStr[0] == 'x');
      l = std::stoi(litStr.substr(1));
    }

    // Parse weight
    if (weightStr[0] == '-') {
      w = std::stoi(weightStr.substr(1));
      // Negate literal for negative weight
      l = -l;
      rhs += w;
    } else if (weightStr[0] == '+') {
      w = std::stoi(weightStr.substr(1));
    } else {
      assert(weightStr[0] >= '0' && weightStr[0] <= '9');
      w = std::stoi(weightStr);
    }

    wlits[l] = w;
  }

  // Parse constraint type
  if (opbStr[0] == '>') {
    assert(opbStr[1] == '=');
    constrType = PBC_GE;
    opbStr.erase(0, 2);
  } else if (opbStr[0] == '=') {
    constrType = PBC_EQ;
    opbStr.erase(0, 1);
  } else {
    assert(opbStr.substr(0, 2) == "<=");
    constrType = PBC_LE;
    opbStr.erase(0, 2);
  }
  ltrim(opbStr);

  // Parse RHS
  pos = opbStr.find(";");
  assert(pos != string::npos);
  string rhsStr = opbStr.substr(0, pos);
  rtrim(rhsStr);
  if (rhsStr[0] == '-')
    rhs -= std::stoi(rhsStr.substr(1));
  else if (rhsStr[0] == '+')
    rhs += std::stoi(rhsStr.substr(1));
  else {
    assert(rhsStr[0] >= '0' && rhsStr[0] <= '9');
    rhs += std::stoi(rhsStr);
  }
}

void PBConstraint::addLiteral(lit_t l, int64_t w) {
  if (w == 0)
    wlits.erase(l);
  else if (w < 0) {
    wlits[-l] = -w;
    rhs -= w;
  } else
    wlits[l] = w;
}

PBConstrEncType PBConstraint::addToSolver(SatSolver *const solver,
                                          string cardEncType,
                                          string pbEncType) const {
  assert(wlits.size() > 0);
  if (rhs < 0) {
    if (constrType & PBC_LE) {
      // Constraint cannot be satisfied
      lit_t l = wlits.cbegin()->first;
      solver->addClause(l);
      solver->addClause(-l);
      return PBCE_UNSAT;
    }
    // Constraint always satisfied
    return PBCE_NONE;
  }

  // Get stats of weights
  uint64_t div = wlits.cbegin()->second;
  uint64_t sum{};
  uint64_t min = std::numeric_limits<uint64_t>::max();
  uint64_t max{};
  for (auto const &p : wlits) {
    div = gcd(div, p.second);
    sum += p.second;
    if (p.second < min)
      min = p.second;
    if (p.second > max)
      max = p.second;
  }

  if (static_cast<uint64_t>(rhs) > sum) {
    if (constrType & PBC_GE) {
      // Constraint cannot be satisfied
      lit_t l = wlits.cbegin()->first;
      solver->addClause(l);
      solver->addClause(-l);
      return PBCE_UNSAT;
    }
    // Constraint always satisfied
    return PBCE_NONE;
  }

  if (static_cast<uint64_t>(rhs) < min) {
    if (constrType == PBC_GE) {
      // Constraint is clause
      clause_t cl{};
      for (auto const &p : wlits)
        cl.push_back(p.first);
      solver->addClause(cl);
      return PBCE_CLAUSE;
    }
    if (constrType == PBC_LE || (constrType == PBC_EQ && rhs == 0)) {
      // Constraint forces all literals
      for (auto const &p : wlits)
        solver->addClause(-p.first);
      return PBCE_UNITS;
    }
    // Constraint cannot be satisfied
    lit_t l = wlits.cbegin()->first;
    solver->addClause(l);
    solver->addClause(-l);
    return PBCE_UNSAT;
  }

  if (static_cast<uint64_t>(rhs) == sum) {
    if (constrType & PBC_GE) {
      // Constraint forces all literals
      for (auto const &p : wlits)
        solver->addClause(p.first);
      return PBCE_UNITS;
    }
  }

  if (static_cast<uint64_t>(rhs) == min) {
    if (constrType & PBC_LE) {
      // Force literals with w > min
      for (auto const &p : wlits)
        if (p.second > min)
          solver->addClause(-p.first);
      // At most one of all others (pairwise encoding)
      for (auto const &p1 : wlits)
        for (auto const &p2 : wlits)
          if (p1.first > p2.first && p1.second == min && p2.second == min)
            solver->addClause(-p1.first, -p2.first);
    }
    if (constrType & PBC_GE) {
      // Clause over literals
      clause_t cl{};
      for (auto const &p : wlits)
        if (p.second == min || !(constrType & PBC_LE))
          cl.push_back(p.first);
      solver->addClause(cl);
    }
    if (constrType == PBC_GE)
      return PBCE_CLAUSE;
    return PBCE_CARD;
  }

  if (min == max) {
    if (rhs % min && constrType == PBC_EQ) {
      // Constraint cannot be satisfied
      lit_t l = wlits.cbegin()->first;
      solver->addClause(l);
      solver->addClause(-l);
      return PBCE_UNSAT;
    }

    std::vector<lit_t> lits{};
    for (auto const &p : wlits)
      lits.push_back(p.first);

    CardEncoding *cardEnc =
        cardEncodingFactory(solver, (BoundType)constrType, cardEncType);
    cardEnc->addLits(lits);
    if (constrType & PBC_LE)
      cardEnc->enforceUB(rhs / min);
    if (constrType & PBC_GE)
      cardEnc->enforceLB(rhs / min - (rhs % min ? 0 : 1));
    delete cardEnc;

    return PBCE_CARD;
  }

  if (rhs % div && constrType == PBC_EQ) {
    // Constraint cannot be satisfied
    lit_t l = wlits.cbegin()->first;
    solver->addClause(l);
    solver->addClause(-l);
    return PBCE_UNSAT;
  }

  wlits_t divWlits{};
  for (auto const &p : wlits)
    divWlits[p.first] = p.second / div;

  if ((pbEncType == "gte" || pbEncType == "") && constrType == PBC_EQ) {
    // The generalized totalizer encoding cannot directly encode both bounds so
    // two encodings need to be formed
    PBEncoding *pbLbEnc = pbEncodingFactory(solver, BOUND_LB, pbEncType);
    pbLbEnc->addLits(divWlits);
    pbLbEnc->enforceLB(rhs / div - (rhs % div ? 0 : 1));
    delete pbLbEnc;

    PBEncoding *pbUbEnc = pbEncodingFactory(solver, BOUND_UB, pbEncType);
    pbUbEnc->addLits(divWlits);
    pbUbEnc->enforceUB(rhs / div);

    return PBCE_PBC;
  }

  PBEncoding *pbEnc =
      pbEncodingFactory(solver, (BoundType)constrType, pbEncType);
  pbEnc->addLits(divWlits);
  if (constrType & PBC_LE)
    pbEnc->enforceUB(rhs / div);
  if (constrType & PBC_GE)
    pbEnc->enforceLB(rhs / div - (rhs % div ? 0 : 1));
  delete pbEnc;

  return PBCE_PBC;
}

} // namespace BiOptSat