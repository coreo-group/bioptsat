/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author: Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "encodings.hpp"

namespace BiOptSat {
void exactlyOne(uint32_t &nVars, std::vector<clause_t> &outClauses,
                const std::vector<lit_t> &lits) {
  if (lits.size() == 0) {
    outClauses.push_back({});
    return;
  }
  if (lits.size() == 1) {
    outClauses.push_back({lits[0]});
    return;
  }
  if (lits.size() == 2) {
    outClauses.push_back({lits[0], lits[1]});
    outClauses.push_back({-lits[0], -lits[1]});
    return;
  }
  std::vector<lit_t> counterVars(lits.size());
  for (uint32_t i = 0; i < lits.size(); i++)
    counterVars[i] = ++nVars;
  outClauses.push_back({-lits[0], counterVars[0]});
  outClauses.push_back({lits[0], -counterVars[0]});
  for (uint32_t i = 1; i < lits.size(); i++) {
    outClauses.push_back({-counterVars[i - 1], counterVars[i]});
    outClauses.push_back({-lits[i], counterVars[i]});
    outClauses.push_back({-lits[i], -counterVars[i - 1]});
    outClauses.push_back({lits[i], -counterVars[i], counterVars[i - 1]});
  }
  outClauses.push_back({counterVars[lits.size() - 1]});
}

void condExactlyOne(uint32_t &nVars, std::vector<clause_t> &outClauses,
                    lit_t cond, const std::vector<lit_t> &lits) {
  if (lits.size() == 0) {
    outClauses.push_back({-cond});
    return;
  }
  if (lits.size() == 1) {
    outClauses.push_back({-cond, lits[0]});
    return;
  }
  if (lits.size() == 2) {
    outClauses.push_back({-cond, lits[0], lits[1]});
    outClauses.push_back({-cond, -lits[0], -lits[1]});
    return;
  }
  std::vector<lit_t> counterVars(lits.size());
  for (uint32_t i = 0; i < lits.size(); i++)
    counterVars[i] = ++nVars;
  outClauses.push_back({-cond, -lits[0], counterVars[0]});
  outClauses.push_back({-cond, lits[0], -counterVars[0]});
  for (uint32_t i = 1; i < lits.size(); i++) {
    outClauses.push_back({-cond, -counterVars[i - 1], counterVars[i]});
    outClauses.push_back({-cond, -lits[i], counterVars[i]});
    outClauses.push_back({-cond, -lits[i], -counterVars[i - 1]});
    outClauses.push_back({-cond, lits[i], -counterVars[i], counterVars[i - 1]});
  }
  outClauses.push_back({-cond, counterVars[lits.size() - 1]});
  for (uint32_t i = 0; i < lits.size(); i++)
    outClauses.push_back({cond, -counterVars[i]});
}

lit_t tseitinAnd(uint32_t &nVars, std::vector<clause_t> &outClauses, lit_t l1,
                 lit_t l2) {
  lit_t tseitin = ++nVars;
  outClauses.push_back({-tseitin, l1});
  outClauses.push_back({-tseitin, l2});
  outClauses.push_back({tseitin, -l1, -l2});
  return tseitin;
}
} // namespace BiOptSat