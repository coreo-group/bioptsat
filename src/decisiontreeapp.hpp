/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _dectreeapp_hpp_INCLUDED
#define _dectreeapp_hpp_INCLUDED

#include <cassert>
#include <cstdint>
#include <unordered_map>
#include <vector>

#include "application.hpp"
#include "hash.hpp"

using std::make_pair;
using std::pair;
using std::unordered_map;
using std::vector;

namespace BiOptSat {
// Abstract base class for applications based on decision trees
// The encoding used is from Narodytska et al. IJCAI'18 / Hu et al. IJCAI'20
class DecisionTreeApp : virtual public Application {
protected:
  bool encoded{};       // Wether or not the instance is already encoded
  uint32_t maxNodes{};  // The maximum number of nodes in the DT
  uint32_t nSamples{};  // Number of samples in the data
  uint32_t nFeatures{}; // Number of features in the data
  bool hasNegated{};    // Wether negated features are used

  // Helpers for indexing
  vector<uint32_t> LR(uint32_t i) const;
  vector<uint32_t> RR(uint32_t i) const;
  inline uint32_t evenNodes(uint32_t i) const {
    assert(i % 2 == 0);
    return i / 2;
  }
  var_t
      isRightChild(uint32_t,
                   uint32_t) const; // r vars are not used but are substituted
                                    // to l vars with the help of this function
  var_t isParent(uint32_t,
                 uint32_t) const; // p vars are not used but are substituted to
                                  // l or r vars with the help of this function
  inline var_t nodeActive(
      uint32_t i) { // Returns var that describes if node with index i is active
    return nodeUsed[evenNodes(i % 2 ? i + 1 : i)];
  }

  // Variable sets
  // Variables from Narodytska et al. IJCAI'18
  // Feature value 0 will go to the right, value 1 to the left child
  vector<var_t> isLeaf{};                                             // v vars
  unordered_map<pair<uint32_t, uint32_t>, var_t> isLeftChild{};       // l vars
  unordered_map<pair<uint32_t, uint32_t>, var_t> featureAssignNode{}; // a vars
  unordered_map<pair<uint32_t, uint32_t>, var_t> featureDiscNode{};   // u vars
  unordered_map<pair<uint32_t, uint32_t>, var_t> featureDiscPos{}; // d^0 vars
  unordered_map<pair<uint32_t, uint32_t>, var_t> featureDiscNeg{}; // d^1 vars
  vector<var_t> nodeClassTrue{};                                   // c vars
  // Variables from Hu et al. IJCAI'20
  vector<var_t> sampleClassifiedCorrectly{}; // b vars
  vector<var_t> nodeUsed{};                  // m vars
  // Helper for separating into positive and negative samples
  std::vector<bool> posSample{};

  void baseEncoding(std::vector<clause_t> &); // Encode the instance to clauses
  void formatSolution(const model_t &, vector<string> &) const;

public:
  DecisionTreeApp(uint32_t maxNodes = 0); // maxNodes = 0 for CLI param
  void blockSolution(const model_t &, clause_t &) const;
  virtual void printStats() const;
  virtual void printOptions() const;

  // Option getter and setter (don't change options after encoding)
  uint32_t getMaxNodes() const { return maxNodes; }
  void setMaxNodes(uint32_t mn) {
    assert(!encoded);
    maxNodes = mn;
  }
};
} // namespace BiOptSat

#endif