/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Based on (MSU3, OLL): Open-WBO (https://github.com/sat-group/open-wbo)
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _totalizer_hpp_INCLUDED
#define _totalizer_hpp_INCLUDED

#include <cmath>
#include <vector>

#include "cardencoding.hpp"
#include "satsolver.hpp"
#include "types.hpp"

using std::vector;

namespace BiOptSat {

class TotMerger; // Friend class defined in totmerger.hpp

// Implementation of the totalizer encoding for incremental use
class Totalizer : public CardEncoding {
  friend class TotMerger;

private:
  vector<lit_t> outLits{}; // The set of output literals

  vector<lit_t> tmpBuffer{}; // Temporary buffer for building the totalizer

  vector<vector<lit_t>> treeLeft{};  // Left hand sides of the adders
  vector<vector<lit_t>> treeRight{}; // Right hand sides of the adders
  vector<vector<lit_t>> treeOut{};   // Outputs of the adders
  vector<uint32_t> treeUpper{};      // Maximum encoded value of the adders

  uint64_t depth{};

  void toCnf(vector<lit_t> &); // Encode variables in tmpBuffer to outputs
  void adder(vector<lit_t> &, vector<lit_t> &, vector<lit_t> &); // Encode adder
  void build(vector<lit_t> &,
             uint32_t);       // Build totalizer over inLits up to upper
  void join(Totalizer &);     // Join with a second totalizer
  void updateUpper(uint32_t); // Update the maximum encoded value

  void _enforceUB(uint64_t, std::vector<lit_t> &);
  void _enforceLB(uint64_t, std::vector<lit_t> &);

  uint64_t minNeededVars(uint64_t) const;

public:
  using CardEncoding::CardEncoding;

  virtual void printStats(bool header = false) const;

  // Stats getter
  uint64_t getDepth() const { return depth; }
  double getBalanceMeasure() const {
    return static_cast<double>(getNVars()) /
           static_cast<double>(minNeededVars(inLits.size()));
  }
};

} // namespace BiOptSat

#endif