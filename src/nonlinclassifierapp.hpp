/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _nonlinclassifierapp_hpp_INCLUDED
#define _nonlinclassifierapp_hpp_INCLUDED

#include "application.hpp"

namespace BiOptSat {
// Abstract base class for learning classifiers optimal according to non-linear
// performance metrics
class NonLinClassifierApp : virtual public Application {
protected:
  const string clfName = "Classifier";

  // Options
  bool fpInc{};

  void setupObjectives(Objective &, Objective &); // Configure objectives
  virtual void getFalsePositiveObj(
      Objective &) const = 0; // Get the false positive objective to use
  virtual void getFalseNegativeObj(
      Objective &) const = 0; // Get the false negative objective to use
  void setObjectiveNames();

public:
  NonLinClassifierApp(string);
  void printOptions() const;

  // Option getter and setter (don't change options after loading instance)
  bool hasFpUp() const { return fpInc; }
  void setFpUp(bool fu) {
    fpInc = fu;
    setObjectiveNames();
  }
};
} // namespace BiOptSat

#endif