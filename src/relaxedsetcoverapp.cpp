/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "relaxedsetcoverapp.hpp"

#include "options.hpp"

namespace BiOptSat {
// Options
static string cat = "3.6 - Relaxed Set Cover";
static StringOption opt_increasing_obj(
    cat, "relaxed-set-cover-increasing",
    "The increasing objective for the relaxed set-cover problem", "elements",
    StringSet({"elements", "relaxation"}));

RelaxedSetCoverApp::RelaxedSetCoverApp() {
  if (opt_increasing_obj == "relaxation") {
    relaxationInc = true;
    incName = "Relaxation";
    decName = "Element weight";
  } else {
    relaxationInc = false;
    incName = "Element weight";
    decName = "Relaxation";
  }
}

void RelaxedSetCoverApp::loadEncode(std::vector<clause_t> &clauses,
                                    Objective &inc, Objective &dec) {
  baseEncoding(clauses, inc, dec);
  if (!dec.isEmpty())
    WARN << "Set-cover instance has two objectives, second one will be "
            "ignored\n";
  dec.clear();
  if (relaxationInc) {
    dec = inc;
    inc.clear();
  }
  for (clause_t &cl : clauses) {
    cl.push_back(++nVars);
    if (relaxationInc)
      inc.addLit(nVars);
    else
      dec.addLit(nVars);
  }
}

void RelaxedSetCoverApp::setupEnumerator() {
  // Omitting trivial empty cover
  if (relaxationInc) {
    if (enumerator->getMaxIncreasing() == -1)
      enumerator->setMaxIncreasing(nClauses - 1);
    if (enumerator->getMinDecreasing() == -1)
      enumerator->setMinDecreasing(1);
  } else if (!relaxationInc && enumerator->getMaxDecreasing() == -1)
    enumerator->setMaxDecreasing(nClauses - 1);

  // Set blocking strategy
  if (enumerator->getEnumType() == ORDERED) {
    if (relaxationInc)
      blockingStrategy = BLOCK_POS;
    else
      blockingStrategy = BLOCK_NEG;
  } else
    blockingStrategy = BLOCK_BOTH;
}

ParetoFront<model_t> RelaxedSetCoverApp::getParetoFront() const {
  // Add trivial empty cover
  model_t emptyMdl(nVars, false);
  ParetoFront<model_t> pf = enumerator->getParetoFront();
  reconstructPf(pf);
  if (relaxationInc && (pf.size() == 0 || pf.back().decVal != 0)) {
    ParetoPoint<model_t> pp{
        .models = {emptyMdl}, .incVal = nClauses, .decVal = 0};
    pf.push_back(pp);
  } else if (!relaxationInc && (pf.size() == 0 || pf.front().incVal != 0)) {
    ParetoPoint<model_t> pp{
        .models = {emptyMdl}, .incVal = 0, .decVal = nClauses};
    pf.insert(pf.begin(), pp);
  }
  return pf;
}
} // namespace BiOptSat