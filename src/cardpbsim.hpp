/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _cardpbsim_hpp_INCLUDED
#define _cardpbsim_hpp_INCLUDED

#include "cardencoding.hpp"
#include "pbencoding.hpp"

namespace BiOptSat {
// Simulating a PB encoding with a cardinality encoding
class CardPBSim : public PBEncoding {
protected:
  CardEncoding *cardEnc{}; // Underlying cardinality encoding

  void _enforceUB(uint64_t, std::vector<lit_t> &);
  void _enforceLB(uint64_t, std::vector<lit_t> &);

public:
  using PBEncoding::PBEncoding;
  ~CardPBSim() {
    if (cardEnc)
      delete cardEnc;
  }

  inline uint64_t nextHigherPossible(uint64_t b) {
    // Cardinality constraints have no knowledge of reachable values
    return ++b;
  }
  inline uint64_t nextLowerPossible(uint64_t b) {
    // Cardinality constraints have no knowledge of reachable values
    return --b;
  }

  virtual void printStats(bool header = false) const;

  virtual inline uint32_t getNClauses() const {
    return (cardEnc ? cardEnc->getNClauses() : 0) + nClauses;
  }
  virtual inline uint32_t getNVars() const {
    return (cardEnc ? cardEnc->getNVars() : 0) + nVars;
  }
};
} // namespace BiOptSat

#endif