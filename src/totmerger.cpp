/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "totmerger.hpp"

#include <map>
#include <set>

#include "options.hpp"

namespace BiOptSat {
// Options
static std::string cat = "5 - Encodings";
static BoolOption opt_reserve(cat, "tot-merger-reserve-vars",
                              "Reverse variables in the solver in advance "
                              "rather than when they are defined",
                              true);
static BoolOption opt_cone_of_influence(
    cat, "tot-merger-cone-of-influence",
    "Use the cone of influence optimization in the GTE encoding", true);

TotMerger::TotMerger(SatSolver *const solver, const BoundType boundType)
    : PBEncoding(solver, boundType), reserveVars(opt_reserve),
      coi(opt_cone_of_influence) {
  // For now the GTE only works for upper bounds
  if (boundType != BOUND_UB) {
    ERROR << "The generalized totalizer encoding (used for merging totalizers) "
             "is only implemented for upper bounding\n";
    exit(1);
  }
}

TotMerger::Node *
TotMerger::mergeTotalizers(std::vector<TotData>::const_iterator start,
                           size_t n) {
  assert(n > 0);
  // Recursion anchor: build tot leaf
  if (n == 1)
    return new Node(this->solver, start->tot, start->weightMultiplier,
                    start->weightOffset);

  // Equally split inputs
  std::vector<TotData>::const_iterator split = std::next(start, n / 2);
  Node *left = mergeTotalizers(start, n / 2);
  Node *right = mergeTotalizers(split, n / 2 + (n % 2 ? 1 : 0));

  return new Node(this->solver, left, right);
}

TotMerger::Node *TotMerger::buildGteTree(wlits_t::const_iterator start,
                                         size_t n) {
  assert(n > 0);
  // Recursion anchor: build leaf
  if (n == 1)
    return new Node(this->solver, start->second, start->first);

  // Equally split inputs
  wlits_t::const_iterator split = std::next(start, n / 2);
  Node *left = buildGteTree(start, n / 2);
  Node *right = buildGteTree(split, n / 2 + (n % 2 ? 1 : 0));

  return new Node(this->solver, left, right);
}

void TotMerger::addTotsToTree(std::vector<TotData> &tots) {
  if (tots.empty())
    return;

  Node *newNode = mergeTotalizers(tots.begin(), tots.size());
  if (reserveVars)
    nVars += newNode->reserveVars();
  if (!root) {
    root = newNode;
  } else {
    root = new Node(this->solver, root, newNode);
    if (reserveVars)
      nVars += root->reserveVars(false);
  }
  nTots += tots.size();
  for (const TotData &totData : tots) {
    totWeightSum += totData.tot->getNLits() * totData.weightMultiplier -
                    totData.weightOffset;
    if (totData.weightMultiplier > maxTotMultiplier)
      maxTotMultiplier = totData.weightMultiplier;
  }
  tots.clear();
}

void TotMerger::addLitsToTree(wlits_t &extension) {
  if (extension.empty())
    return;

  Node *gteNode = buildGteTree(extension.begin(), extension.size());
  assert(gteNode->nodeType == Node::GTE);
  if (reserveVars)
    nVars += gteNode->reserveVars();
  if (!root) {
    root = gteNode;
  } else {
    root = new Node(this->solver, root, gteNode);
    if (reserveVars)
      nVars += root->reserveVars(false);
  }
}

void TotMerger::addTotalizer(Totalizer *const tot,
                             const uint64_t weightMultiplier,
                             const uint64_t weightOffset) {
  assert(tot->boundType & BOUND_UB);
  TotData totData{.tot = tot,
                  .weightMultiplier = weightMultiplier,
                  .weightOffset = weightOffset};
  totsToInclude.push_back(totData);
}

void TotMerger::_enforceUB(uint64_t b, std::vector<lit_t> &assumps) {
  assert(boundType == BOUND_UB);
  // Add all totalizers to the tree
  // This could be done more lazily but probably wouldn't give much advantage
  addTotsToTree(totsToInclude);
  uint64_t maxWeight =
      maxTotMultiplier; // Maximum weight of a literal that can be set

  // Find weighted literals to add to the tree
  // Also assume literals with weight > b to false
  wlits_t litExt{};
  for (const auto &p : wLits) {
    if (p.second <= b) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        it->second += wAdd;
      }
      // Find the maximum active leaf weight
      if (wAdd > maxWeight)
        maxWeight = wAdd;
      if (p.second - wAdd > maxWeight)
        maxWeight = p.second - wAdd;

      if (wAdd)
        litExt[p.first] = wAdd;
    } else
      assumps.push_back(-p.first);
  }

  if (maxWeight == 0) {
    assert(litExt.empty());
    // No input literals can be set, do not need to encode anything
    return;
  }

  addLitsToTree(litExt);

  if (!root)
    return;

  // Encode up to b + maxWeight.
  // If encoded that far, it is guaranteed that one of the output lits with
  // weight >= b will be set.
  std::pair<uint32_t, uint32_t> added;
  if (coi) {
    added = root->encode(b + 1, b + maxWeight);
  } else {
    added = root->encode(1, b + maxWeight);
  }
  nVars += added.first;
  nClauses += added.second;

  // No output lit in the range b+1 -- b+maxWeight can be set
  for (auto it = root->weights.upper_bound(b);
       it != root->weights.end() && it->first <= b + maxWeight; ++it)
    assumps.push_back(-it->second);
}

void TotMerger::_enforceLB(uint64_t b, std::vector<lit_t> &assumps) {
  ERROR << "The generalized totalizer encoding (used for merging totalizers) "
           "is only implemented for upper bounding\n";
  exit(1);
}

uint64_t TotMerger::nextHigherPossible(uint64_t b) {
  assert(boundType == BOUND_UB);
  // Add all totalizers to the tree
  // This could be done more lazily but probably wouldn't give much advantage
  addTotsToTree(totsToInclude);

  // Add all vars with weight <=b that are not encoded yet but part of the GTE
  // to the tree
  wlits_t extension{};
  uint64_t minNotEncoded = std::numeric_limits<uint64_t>::max();
  for (const auto &p : wLits) {
    if (p.second <= b) {
      auto it = encodedWLits.find(p.first);
      uint64_t wAdd{};
      if (it == encodedWLits.end()) {
        wAdd = p.second;
        encodedWLits[p.first] = wAdd;
      } else {
        wAdd = p.second - it->second;
        it->second += wAdd;
      }

      if (wAdd)
        extension[p.first] = wAdd;
    } else {
      if (p.second < minNotEncoded)
        minNotEncoded = p.second;
    }
  }
  addLitsToTree(extension);

  if (!root)
    return minNotEncoded;

  const auto it = root->weights.upper_bound(b);

  if (it == root->weights.end())
    return minNotEncoded;

  return minNotEncoded > it->first ? it->first : minNotEncoded;
}

uint64_t TotMerger::nextLowerPossible(uint64_t b) {
  assert(boundType == BOUND_UB);
  // Add all totalizers to the tree
  // This could be done more lazily but probably wouldn't give much advantage
  addTotsToTree(totsToInclude);

  // Add all vars to the tree
  wlits_t extension{};
  for (const auto &p : wLits) {
    auto it = encodedWLits.find(p.first);
    uint64_t wAdd{};
    if (it == encodedWLits.end()) {
      wAdd = p.second;
      encodedWLits[p.first] = wAdd;
    } else {
      wAdd = p.second - it->second;
      it->second += wAdd;
    }

    if (wAdd)
      extension[p.first] = wAdd;
  }
  addLitsToTree(extension);

  assert(root);
  auto it = root->weights.lower_bound(b);
  if (it == root->weights.begin())
    return 0;
  return (--it)->first;
}

void TotMerger::printStats(bool header) const {
  if (header) {
    INFO << "\n";
    INFO << "\n";
    INFO << "GeneralizedTotalizer Stats\n";
    INFO << LOG_H1;
    INFO << "\n";
  }
  INFO << "depth = " << getDepth() << "\n";
  INFO << "nTots = " << nTots << "\n";
  PBEncoding::printStats(false);
}

TotMerger::Node::~Node() {
  if (tot)
    delete tot;
}

std::pair<uint32_t, uint32_t> TotMerger::Node::encode(uint64_t _lower, uint64_t _upper,
                                                      bool recurse) {
  if (nodeType == TOT) {
    // Extend totazlier
    uint32_t oldClauses = tot->getNClauses();
    tot->updateUpper(
        (_upper + totWeightOffset) / totWeightMultiplier +
        ((_upper + totWeightOffset) % totWeightMultiplier ? 1 : 0));
    upper = std::max(_upper,
                     tot->getNLits() * totWeightMultiplier - totWeightOffset);
    return std::make_pair(0, tot->getNClauses() - oldClauses);
  }

  return GeneralizedTotalizer::Node::encode(_lower, _upper, recurse);
}
} // namespace BiOptSat