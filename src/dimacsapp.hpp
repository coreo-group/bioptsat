/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _dimacsapp_hpp_INCLUDED
#define _dimacsapp_hpp_INCLUDED

#include "application.hpp"

#include <string>

using std::string;
namespace BiOptSat {

// An application for using MCNF DIMACS files as input
// The files are expected to be in the following format:
// Lines starting with 'h' are considered clauses.
// Lines starting with 'o[idx]' are considered to be part of the objective with
// index 'idx' (indices start at 1, e.g. 'o1 <weight> <lit> 0').
class DimacsApp : public Application {
protected:
  bool oldBicnf{};

  size_t incObjIdx{};
  size_t decObjIdx{};

  void loadEncode(std::vector<clause_t> &, Objective &, Objective &);

public:
  DimacsApp();

  virtual void printOptions() const;

  // Option getter and setter
  size_t getIncObjIdx() const { return incObjIdx; }
  void setIncObjIdx(size_t ioi) { incObjIdx = ioi; }
  size_t getDecObjIdx() const { return decObjIdx; }
  void setDecObjIdx(size_t doi) { decObjIdx = doi; }
  bool parsesOldBicnf() const { return oldBicnf; }
  void parseOldBicnf(bool ob) { oldBicnf = ob; }
};

} // namespace BiOptSat

#endif
