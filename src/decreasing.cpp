/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "logging.hpp"

namespace BiOptSat {
bool BiOptSatEnumerator::minDecObj() {
  if (verbose >= 2)
    INFO << "Starting minimization of decreasing objective\n";
  std::vector<lit_t> assumps{};
  bool first = true;
  while (decBound >
         (minDecreasing == -1 ? 0 : static_cast<uint64_t>(minDecreasing))) {
    if (verbose >= 3)
      INFO << "minDecObj iteration, bound=" << decBound << "\n";
    upperBoundInc(assumps);
    boundDec(decBound - 1, assumps);
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_DEC];
    if (ret == UNSAT) {
      if (first)
        hardenDec(decBound);
      break;
    }
    first = false;
    assert(ret == SAT);
    auto objVals = getObjValModel(false, true, modelCache);
    assert(objVals.second < decBound);
    phaseModel();
    decBound = objVals.second;
    hardenDec(decBound);
    assumps.clear();
    logCandidate(incBound, decBound);
  }
  if (verbose >= 2)
    INFO << "Found current minimum for decreasing objective: " << decBound
         << "\n";
  // Unphase model since unclear what the next subroutine will be
  unphaseModel();
  return true;
}

void BiOptSatEnumerator::addToDecPB(lit_t l) {
  if (!coreGuidedLazyDecPB)
    // Not lazily building PB encoding
    return;

  if (!(minIncVariant & CORE_GUIDED))
    WARN << "Called addToDecPB from variant that is not core guided\n";

  if (decPB) {
    // If PB encoding exists, add literal
    wlits_t lits{};
    if (decreasing.hasLit(l)) {
      lits[l] = decreasing.getWeight(l);
    }

    // See note in boundDec below, lazily adding negated literals is not sound
    // with hardening

    // else if (decreasing.hasLit(-l)) {
    //   assert(static_cast<uint64_t>(decPB->getOffset()) >=
    //          decreasing.getWeight(-l));
    //   decPB->setOffset(decPB->getOffset() - decreasing.getWeight(-l));
    //   lits[-l] = decreasing.getWeight(-l);
    // }

    if (!lits.empty()) {
      decPB->addLits(lits);

      assert(
          decreasingHarden); // Feature required with the current implementation
      if (hardDecBound < decPB->getWeightSum() + decPB->getOffset())
        decPB->enforceUB(hardDecBound);
    }
  }
}

void BiOptSatEnumerator::boundDec(uint64_t bound,
                                  std::vector<lit_t> &outAssumps) {
  if (bound >= decreasing.getWeightSum())
    return;

  if (!decPB) {
    decPB = pbEncodingFactory(oracle, BOUND_UB);
    if (coreGuidedLazyDecPB && (minIncVariant & CORE_GUIDED)) {
      wlits_t decNotInc{};
      int64_t weightOffset{};
      for (const auto &p : decreasing)
        // Note: Theoretically literals that appear _negated_ in the increasing
        // objective can be lazily added as well. However, _this is unsound
        // together with hardening_. (Assume out of literals {a,b,c}, c is not
        // yet in the totalizer because -c is forced by the other objective,
        // then enforcing a bound {a,b,c}<=2 would actually enforce a bound
        // {a,b}<=1 since c forces a weight of 1, but this bound cannot soundly
        // be hardened.) Since the currently implementation of the lazy
        // decreasing totalizer _requires_ hardening, negated literals are not
        // lazily added.

        // if (inactiveWeight.find(-p.first) != inactiveWeight.end())
        //   // Negated literal inactive in increasing objective -> forces
        //   // weight
        //   weightOffset += p.second;
        // else
        if (inactiveWeight.find(p.first) == inactiveWeight.end())
          decNotInc[p.first] = p.second;
      assert(decNotInc.size() > 0);
      decPB->addLits(decNotInc);
      decPB->setOffset(weightOffset);
    } else
      decPB->addLits(decreasing);
    decPBInit = true;
  }

  decPB->enforceUB(bound - decForcedWeight, outAssumps);
}
} // namespace BiOptSat