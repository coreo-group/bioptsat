/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "syntheticadvantageapp.hpp"

#include "dummysolver.hpp"
#include "totalizer.hpp"

namespace BiOptSat {
void SyntheticAdvantageApp::loadInstance(string inst) {
  instance = inst;

  size_t pos = inst.find("-");
  N = std::stoi(inst.substr(0, pos));
  inst.erase(0, pos + 1);
  K = std::stoi(inst);

  if (verbose >= 0)
    INFO << "Generating instance with N=" << N << " and K=" << K << "\n";

  DummySolver solver;

  std::vector<lit_t> allLits{};
  Objective increasing{};
  Objective decreasing{};

  for (uint32_t i = 0; i < K; i++) {
    lit_t l = solver.getNewVar();
    increasing.push_back(l);
    allLits.push_back(l);
  }

  for (uint32_t i = 0; i < N; i++) {
    lit_t l = solver.getNewVar();
    decreasing.push_back(l);
    allLits.push_back(l);
  }

  Totalizer tot(&solver, CARD_LB);
  tot.build(allLits, K);
  solver.addClause({tot.getOutLit(K - 1)});

  std::vector<clause_t> clauses = solver.getClauses();
  nClauses = clauses.size();
  nVars = solver.getNVars();

  finalizeLoading(clauses, increasing, decreasing);
}

void SyntheticAdvantageApp::formatSolution(
    const model_t &model, std::vector<string> &outLines) const {
  outLines.clear();
  string out{};
  for (uint32_t i = 1; i < N + K; i++)
    if (model[i])
      out += " " + std::to_string(i);
  outLines.push_back(out);
}
} // namespace BiOptSat