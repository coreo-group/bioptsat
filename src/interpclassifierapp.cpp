/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "interpclassifierapp.hpp"

namespace BiOptSat {
// Options
static std::string cat = "3.4 - Interpretable Classifiers";
static StringOption opt_increasing_obj(cat, "interp-clf-increasing",
                                    "The increasing objective for learning "
                                    "interpretable classifiers",
                                    "size", StringSet({"size", "error"}));

InterpClassifierApp::InterpClassifierApp(string name) : clfName(name) {
  if (opt_increasing_obj == "size")
    sizeInc = true;
  else
    sizeInc = false;
  setObjectiveNames();
}

void InterpClassifierApp::setupObjectives(Objective &inc, Objective &dec) {
  if (sizeInc) {
    getSizeObj(inc);
    getErrorObj(dec);
  } else {
    getSizeObj(dec);
    getErrorObj(inc);
  }
}

void InterpClassifierApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "InterpClassifierApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "increasing objective = " << (sizeInc ? "size" : "error") << "\n";
}

void InterpClassifierApp::setObjectiveNames() {
  incName = "Classification error";
  decName = clfName + " size";
  if (sizeInc) {
    incName = clfName + " size";
    decName = "Classification error";
  }
}
} // namespace BiOptSat