/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "maxpre.hpp"

#include <algorithm>
#include <unordered_map>

#include "options.hpp"

namespace BiOptSat {
// Options
static std::string cat = "4.1 - MaxPre";
static StringOption opt_techniques(cat, "maxpre-techniques",
                                   "The techniques string to use in MaxPre",
                                   "[[uvsrgc]VRTG]");
static StringOption
    opt_prerun(cat, "maxpre-prerun",
               "The type of prerun to do. None, fixing all variables, "
               "fixing only objective variables.",
               "none", StringSet({"none", "all", "obj"}));
static StringOption
    opt_preruntech(cat, "maxpre-prerun-techniques",
                   "The techniques string to use for a pre run only used for "
                   "determining fixed variable assignments in MaxPre.",
                   "[[buvsrgc]VRTG]");
static IntOption opt_loglevel(cat, "maxpre-loglevel",
                              "The loglevel for MaxPre preprocessing", 0,
                              IntRange(0, 2));
static BoolOption
    opt_reindexing(cat, "maxpre-reindexing",
                   "Whether to do reindexing of variables in MaxPre or not",
                   false);

MaxPre::MaxPre()
    : PreProcessor(), techniques(opt_techniques),
      prerunTechniques(opt_preruntech), logLevel(opt_loglevel),
      maxpreReindexing(opt_reindexing) {
  if (opt_prerun == "all")
    preRun = PR_ALL;
  else if (opt_prerun == "obj")
    preRun = PR_OBJ;
  else
    preRun = PR_NONE;
}

MaxPre::~MaxPre() {
  if (backend)
    delete backend;
  if (prerunBackend)
    delete prerunBackend;
}

void MaxPre::_preprocess() {
  std::vector<std::vector<int>> maxsatClauses = *clauses;
  std::vector<std::pair<uint64_t, uint64_t>> maxsatWeights(
      clauses->size(), std::make_pair(0, 0));

  std::unordered_map<lit_t, std::pair<uint64_t, uint64_t>> objWeights{};
  for (auto &p : *obj1)
    objWeights[p.first] = std::make_pair(p.second, 0);
  for (auto &p : *obj2) {
    if (objWeights.find(p.first) != objWeights.end()) {
      objWeights[p.first] = std::make_pair(objWeights[p.first].first, p.second);
    } else {
      objWeights[p.first] = std::make_pair(0, p.second);
    }
  }

  uint64_t top{};
  maxsatClauses.reserve(clauses->size() + objWeights.size());
  maxsatWeights.reserve(clauses->size() + objWeights.size());
  for (auto &pair : objWeights) {
    top += pair.second.first + pair.second.second;
    maxsatClauses.push_back({-pair.first});
    maxsatWeights.push_back(pair.second);
  }
  top += 1;
  for (uint32_t i = 0; i < clauses->size(); i++)
    maxsatWeights[i] = std::make_pair(top, top);

  if (preRun & PR_DO) {
    fixingPreRun(maxsatClauses, maxsatWeights, top);

    maxsatClauses.reserve(clauses->size() + objWeights.size() +
                          fixedLits.size());
    maxsatWeights.reserve(clauses->size() + objWeights.size() +
                          fixedLits.size());

    for (auto l : fixedLits) {
      maxsatClauses.push_back({l});
      maxsatWeights.push_back(std::make_pair(top, top));
    }
  }

  if (backend)
    delete backend;
  backend = new maxPreprocessor::PreprocessorInterface(
      maxsatClauses, maxsatWeights, top, !maxpreReindexing);

  double maxPreTL = timeLimit;
  if (maxPreTL < 0)
    maxPreTL = 1e9;
  backend->preprocess(techniques, logLevel, maxPreTL);
}

void MaxPre::fixingPreRun(
    const vector<vector<int>> &maxsatClauses,
    const vector<std::pair<uint64_t, uint64_t>> &maxsatWeights, uint64_t top) {
  if (!(preRun & PR_DO))
    return;

  vector<vector<int>> copyMaxsatClauses{};
  vector<std::pair<uint64_t, uint64_t>> copyMaxsatWeights{};
  copyMaxsatClauses.reserve(maxsatClauses.size());
  copyMaxsatWeights.reserve(maxsatClauses.size());
  for (size_t i = 0; i < maxsatClauses.size(); ++i) {
    vector<int> copyClause{};
    for (int l : maxsatClauses[i])
      copyClause.push_back(l);
    copyMaxsatClauses.push_back(copyClause);
    copyMaxsatWeights.push_back(maxsatWeights[i]);
  }

  if (prerunBackend)
    delete prerunBackend;
  prerunBackend = new maxPreprocessor::PreprocessorInterface(
      copyMaxsatClauses, copyMaxsatWeights, top, true);

  double maxPreTL = timeLimit;
  if (maxPreTL < 0)
    maxPreTL = 1e9;
  prerunBackend->preprocess(prerunTechniques, logLevel, maxPreTL);

  vector<lit_t> backendFixed = prerunBackend->getFixed();

  if (preRun == PR_OBJ) {
    for (lit_t l : backendFixed) {
      if (obj1->hasVar(var(l)) || obj2->hasVar(var(l)))
        fixedLits.push_back(l);
    }
  } else {
    fixedLits = backendFixed;
  }
}

void MaxPre::_reconstruct(model_t &model) {
  std::vector<int> trueLits{};
  trueLits.reserve(model.size() - 1);
  for (size_t v = 1; v < model.size(); v++)
    trueLits.push_back(model[v] ? v : -v);

  assert(backend);
  trueLits = backend->reconstruct(trueLits);

  model.resize(nOrigVars + 1);
  for (int l : trueLits)
    model[l > 0 ? l : -l] = l > 0;
}

void MaxPre::getPreProInstance(std::vector<clause_t> &outClauses,
                               Objective &outObj1, uint64_t &_weightOffset1,
                               Objective &outObj2, uint64_t &_weightOffset2) {
  assert(backend != nullptr);

  std::vector<std::vector<int>> preproClauses{};
  std::vector<std::pair<uint64_t, uint64_t>> preproWeights{};
  std::vector<int> preproLabels{};
  backend->getInstance(preproClauses, preproWeights, preproLabels);

  outClauses.clear();
  outObj1.clear();
  outObj2.clear();
  nPreproVars = 0;
  for (uint32_t i = 0; i < preproClauses.size(); i++) {
    if (preproWeights[i].first >= backend->getTopWeight()) {
      assert(preproWeights[i].second >= backend->getTopWeight());
      // Transfer hard clause
      outClauses.push_back(preproClauses[i]);
      for (lit_t l : preproClauses[i])
        if (var(l) > nPreproVars)
          nPreproVars = var(l);
    } else {
      assert(preproClauses[i].size() == 1);
      if (var(preproClauses[i][0]) > nPreproVars)
        nPreproVars = var(preproClauses[i][0]);
      outObj1.addLit(-preproClauses[i][0], preproWeights[i].first);
      outObj2.addLit(-preproClauses[i][0], preproWeights[i].second);
    }
  }
  nPreproClauses = outClauses.size();

  std::vector<uint64_t> rmWeight = backend->getRemovedWeight();
  if (rmWeight.size() > 0) {
    weightOffset1 = rmWeight[0];
    _weightOffset1 = weightOffset1;
    if (rmWeight.size() > 1) {
      weightOffset2 = rmWeight[1];
      _weightOffset2 = weightOffset2;
    }
  }
}

void MaxPre::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "MaxPre Stats\n";
  INFO << LOG_H1;
  INFO << "#fixedVars = " << fixedLits.size() << "\n";

  if (backend) {
    INFO << "\n";
    INFO << "Max pre reported stats\n";
    INFO << LOG_H2;
    backend->printPreprocessorStats(std::cout);
    INFO << "\n";
    INFO << "Max pre log\n";
    INFO << LOG_H2;
    backend->printTechniqueLog(std::cout);
  }

  if (prerunBackend) {
    INFO << "\n";
    INFO << "Max pre prerun reported stats\n";
    INFO << LOG_H2;
    prerunBackend->printPreprocessorStats(std::cout);
    INFO << "\n";
    INFO << "Max pre prerun log\n";
    INFO << LOG_H2;
    prerunBackend->printTechniqueLog(std::cout);
  }

  PreProcessor::printStats();
}

void MaxPre::printOptions() const {
  string prerun{};
  switch (preRun) {
  case PR_NONE:
    prerun = "none";
    break;
  case PR_ALL:
    prerun = "all";
    break;
  case PR_OBJ:
    prerun = "obj";
    break;
  }
  INFO << "\n";
  INFO << "\n";
  INFO << "MaxPre Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "techniques = " << techniques << "\n";
  INFO << "preRun = " << prerun << "\n";
  INFO << "prerunTechniques = " << prerunTechniques << "\n";
  INFO << "logLevel = " << logLevel << "\n";
  INFO << "maxpreReindexing = " << (maxpreReindexing ? "on" : "off") << "\n";

  PreProcessor::printOptions();
}
} // namespace BiOptSat