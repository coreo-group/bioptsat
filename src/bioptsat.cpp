/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"

namespace BiOptSat {

// Options
static string cat = "2.2.1 - BiOptSat";
static StringOption opt_mininc_variant(
    cat, "mininc-variant", "The variant for the minIncObj subroutine",
    "sat-unsat", StringSet({
      "sat-unsat", "unsat-sat", "msu3", "oll", "msu3-su-hybrid",
#if ENABLE_OSHYBRID
          "oll-su-hybrid"
#endif
    }));
static StringOption
    opt_conf_minimization(cat, "conf-minimization",
                          "The conflict minimization approach", "heuristic",
                          StringSet({"none", "exact", "heuristic"}));
static BoolOption
    opt_conf_exhaustion(cat, "conf-exhaustion",
                        "Turn conflict exhaustion for OLL on or off", true);
static BoolOption opt_increasing_harden(
    cat, "harden-increasing",
    "Lower (as well as upper) bound the increasing objective "
    "in the PB encoding and harden the bounds to unit clauses",
    false);
static BoolOption opt_decreasing_harden(
    cat, "harden-decreasing",
    "Harden the bounds on the decreasing objective to unit clauses", true);
static BoolOption
    opt_core_guided_lazy_dec_tot(cat, "core-guided-lazy-dec-tot",
                                 "Wether the decreasing PB encoding is lazily "
                                 "built for the core guided variants",
                                 true);
static BoolOption
    opt_hybrid_immediate_switch(cat, "hybrid-immediate-switch",
                                "Wether the hybrid should switch from "
                                "core-guided to sat-unsat immediately or "
                                "only in the next iteration",
                                true);
static BoolOption opt_block_dominated(cat, "block-dominated",
                                      "Block solutions dominated by candidates",
                                      false);
static StringOption
    opt_msu3_disjoint_variant(cat, "msu3-disjoint-variant",
                              "Variant of disjoint phase for MSU3", "none",
                              StringSet({"none", "once", "all"}));
static BoolOption
    opt_msu3_gcd_increment(cat, "msu3-gcd-increment",
                           "Estimate the MSU3 increment with a running GCD",
                           true);
static DoubleOption
    opt_hybrid_switch_portion(cat, "hybrid-switch-portion",
                              "The portion of objective literals that need to "
                              "be active for the hybrid to switch to sat-unsat",
                              0.7, DoubleRange(0.0, false, 1.0, true));
static IntOption opt_hybrid_switch_max_time(
    cat, "hybrid-switch-max-time",
    "The maximum CPU time after which to switch to sat-unsat (-1 for no limit)",
    -1, IntRange(-1, std::numeric_limits<int32_t>::max()));

// Constructor/Destructor
BiOptSatEnumerator::BiOptSatEnumerator(std::vector<clause_t> &clauses,
                                       Objective inc, Objective dec,
                                       std::string backend)
    : Enumerator(ORDERED, clauses, inc, dec, backend),
      confExhaustion(opt_conf_exhaustion),
      increasingHarden(opt_increasing_harden),
      decreasingHarden(opt_decreasing_harden),
      coreGuidedLazyDecPB(opt_core_guided_lazy_dec_tot),
      hybridImmediateSwitch(opt_hybrid_immediate_switch),
      blockDominated(opt_block_dominated),
      msu3GcdIncrement(opt_msu3_gcd_increment),
      hybridSwitchPortion(opt_hybrid_switch_portion),
      hybridSwitchMaxTime(opt_hybrid_switch_max_time) {
  if (coreGuidedLazyDecPB && !decreasingHarden) {
    WARN << "Lazily building decreasing totalizer requires hardening of the "
            "decreasing objective. Turning this feature on.\n";
    decreasingHarden = true;
  }

  if (opt_mininc_variant == "unsat-sat")
    minIncVariant = UNSAT_SAT;
  else if (opt_mininc_variant == "msu3")
    minIncVariant = MSU3;
  else if (opt_mininc_variant == "oll")
    minIncVariant = OLL;
  else if (opt_mininc_variant == "msu3-su-hybrid")
    minIncVariant = MSU3_SU_HYBRID;
#if ENABLE_OSHYBRID
  else if (opt_mininc_variant == "oll-su-hybrid")
    minIncVariant = OLL_SU_HYBRID;
#endif
  else
    minIncVariant = SAT_UNSAT;

  if (opt_conf_minimization == "none")
    confMinVariant = CM_NONE;
  else if (opt_conf_minimization == "exact")
    confMinVariant = CM_EXACT;
  else
    confMinVariant = CM_HEURISTIC;

  if (opt_msu3_disjoint_variant == "none")
    msu3DisjointVariant = M3D_NONE;
  else if (opt_msu3_disjoint_variant == "once")
    msu3DisjointVariant = M3D_ONCE;
  else
    msu3DisjointVariant = M3D_ALL;

  if (minIncVariant & CORE_GUIDED) {
    inactiveWeight.insert(increasing.begin(), increasing.end());
    inactiveWeightSum = increasing.getWeightSum();
  }

  hardDecBound = decreasing.getWeightSum();

  oracleCallLog.resize(OCT_NEXT_FREE, 0);
  octNames.resize(OCT_NEXT_FREE);
  octNames[OCT_SAT] = "Satisfiable";
  octNames[OCT_SU] = "Sat-unsat";
  octNames[OCT_US] = "Unsat-sat";
  octNames[OCT_MSU3] = "MSU3";
  octNames[OCT_OLL] = "OLL";
  octNames[OCT_DEC] = "Decreasing";
  octNames[OCT_ENUM] = "Enumeration";
  octNames[OCT_CONF_MIN] = "Conflict minimization";
  octNames[OCT_CONF_EXH] = "Conflict exhaustion";
}

BiOptSatEnumerator::~BiOptSatEnumerator() {
  if (incPB) {
    delete incPB;
    incPB = nullptr;
  }
  if (decPB) {
    delete decPB;
    decPB = nullptr;
  }
  for (auto p : ollRefConfs) {
    assert(p.first);
    delete p.first;
  }
}

// Timer functions
void BiOptSatEnumerator::stopTimer() {
  double realTime = absolute_real_time() - realStartTime;
  double cpuTime = absolute_process_time() - cpuStartTime;

  totalRealTime += realTime;
  totalCpuTime += cpuTime;

  switch (phase) {
  case PS_PHASE_INC:
    incRealTime += realTime;
    incCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Increasing phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PS_PHASE_DEC:
    decRealTime += realTime;
    decCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Decreasing phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PS_PHASE_ENUM:
    enumRealTime += realTime;
    enumCpuTime += cpuTime;
    if (verbose >= 2) {
      INFO << "Enum phase time: real time=" << realTime
           << "; cpu time=" << cpuTime << "\n";
    }
    break;

  case PS_PHASE_NONE:
    break;
  }
  if (verbose >= 2) {
    INFO << "Total times: real time=" << totalRealTime
         << "; cpu time=" << totalCpuTime << "\n";
    INFO << "Increasing phase times: real time=" << incRealTime
         << "; cpu time=" << incCpuTime << "\n";
    INFO << "Decreasing phase times: real time=" << decRealTime
         << "; cpu time=" << decCpuTime << "\n";
    INFO << "Enum phase times: real time=" << enumRealTime
         << "; cpu time=" << enumCpuTime << "\n";
  }
}

// Main Algorithm
void BiOptSatEnumerator::_enumerate() {
  // Settings warnings
  if (minIncVariant == OLL && increasingHarden)
    WARN << "Hardening the increasing objective is not available for the OLL "
            "variant\n";
  if (minIncreasing != -1)
    WARN << "minIncreasing is set but has no effect for BiOptSat enumerator\n";

  state = SOLVING;
  std::vector<lit_t> assumps{};
  // Set no initial bound for decreasing obj
  if (maxDecreasing != -1) {
    decBound = maxDecreasing;
    upperBoundDec(assumps);
    hardenDec(decBound);
  } else
    // Set no initial bound for decreasing obj
    decBound = decreasing.getWeightSum();
  // Check that problem is SAT
  SolverState res = oracle->solve(assumps);
  ++oracleCallLog[OCT_SAT];
  if (res != SAT) {
    WARN << "Problem not satisfiable; terminating\n";
    state = SOLVED;
    return;
  }

  bool ret{};
  while (true) {
    // Clear cache just in case
    modelCache.clear();
    // Increasing phase
    phase = PS_PHASE_INC;
    startTimer();
    ret = minIncObj();
    stopTimer();
    if (!ret) {
      state = SOLVED;
      if (verbose >= 2)
        INFO << "Terminating pareto front enumeration due to no more "
                "solutions\n";
      break;
    }
    if (maxIncreasing != -1 &&
        incBound > static_cast<uint64_t>(maxIncreasing)) {
      state = BOUND_REACHED;
      if (verbose >= 2)
        INFO << "Terminating pareto front enumeration due to maximum "
                "increasing value reached\n";
      break;
    }
    // Decreasing phase
    phase = PS_PHASE_DEC;
    startTimer();
    ret = minDecObj();
    stopTimer();
    if (minDecreasing != -1 &&
        decBound < static_cast<uint64_t>(minDecreasing)) {
      state = BOUND_REACHED;
      if (verbose >= 2)
        INFO
            << "Terminating pareto front enumeration due to minimum decreasing "
               "value reached\n";
      break;
    }
    // Enumeration phase
    phase = PS_PHASE_ENUM;
    startTimer();
    ret = enumSols();
    stopTimer();
    if (verbose >= 1)
      INFO << "BiOptSat iteration finished; inc = " << incBound
           << "; dec = " << decBound
           << "; proc start wall time = " << proc_start_real_time()
           << "s; absolute cpu time = " << absolute_process_time() << "s\n";
    if (!ret) {
      // Maximum number of solutions enumerated
      state = BOUND_REACHED;
      break;
    }
    // Termination condition, no lower D or higher I value possible
    if (decBound <=
            (minDecreasing == -1 ? 0 : static_cast<uint64_t>(minDecreasing)) ||
        incBound >= (maxIncreasing == -1
                         ? increasing.getWeightSum()
                         : static_cast<uint64_t>(maxIncreasing))) {
      // No more solutions to enumerate
      state = SOLVED;
      break;
    }
    // Check if solution with D < b_D exists
    decBound--;
    assert(decPB);
    hardenDec(decBound);
  }
}

// Algorithm subroutines
bool BiOptSatEnumerator::minIncObj() {
  if (verbose >= 2)
    INFO << "Starting minimization of increasing objective\n";
  bool ret{};
  switch (minIncVariant) {
  case SAT_UNSAT:
    ret = satUnsatIncObj();
    break;
  case UNSAT_SAT:
    ret = unsatSatIncObj();
    break;
  case MSU3:
    ret = msu3IncObj();
    break;
  case OLL:
    ret = ollIncObj();
    break;
  case MSU3_SU_HYBRID:
    if (!hybridPhaseTwo) {
      ret = msu3IncObj();
      if (hybridSwitchCriterion()) {
        hybridPhaseTwo = true;
        if (verbose >= 1)
          INFO << "Switching from MSU3 to Sat-Unsat; inc = " << incBound
               << "; inactiveWeightSum = " << inactiveWeightSum
               << "; proc start wall time = " << proc_start_real_time()
               << "s; absolute cpu time = " << absolute_process_time() << "s\n";
        if (!inactiveWeight.empty()) {
          // Finish building PB encoding
          for (const auto &p : inactiveWeight)
            addToDecPB(p.first);
          incPB->addLits(inactiveWeight);
          inactiveWeight.clear();
          inactiveWeightSum = 0;
        }
        if (hybridSUCont)
          ret = satUnsatIncObj();
      }
    } else {
      ret = satUnsatIncObj();
    }
    break;
#if ENABLE_OSHYBRID
  case OLL_SU_HYBRID:
    if (!hybridPhaseTwo) {
      ret = ollIncObj();
      if (hybridSwitchCriterion()) {
        hybridPhaseTwo = true;
        if (verbose >= 1)
          INFO << "Switching from OLL to Sat-Unsat; inc = " << incBound
               << "; inactiveWeightSum = " << inactiveWeightSum
               << "; proc start wall time = " << proc_start_real_time()
               << "s; absolute cpu time = " << absolute_process_time() << "s\n";
        mergeOllTots();
        if (hybridSUCont)
          ret = satUnsatIncObj();
      }
    } else {
      ret = satUnsatIncObj();
    }
    break;
#endif
  }
  if (verbose >= 2)
    INFO << "Found current minimum for increasing objective: " << incBound
         << "\n";
  return ret;
}

bool BiOptSatEnumerator::enumSols() {
  if (verbose >= 2)
    INFO << "Starting solution enumeration\n";
  std::vector<lit_t> assumps{};
  upperBoundInc(assumps);
  if (!decreasingHarden)
    upperBoundDec(assumps);
  model_t model{};
  clause_t blClause{};
  paretoFront.push_back((ParetoPoint<model_t>){
      .models = {}, .incVal = incBound, .decVal = decBound});
  ParetoPoint<model_t> &pp = paretoFront.back();
  bool cont = true;
  bool loop = true;

  // Save solution found in previously
  if (modelCache.size()) {
    pp.models.push_back(modelCache);
    nSols++;
  }
  // Check early termination conditions
  if (maxSolsPerPP != -1 &&
      pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
    if (verbose >= 2)
      INFO << "Terminating solution enumeration due to max solutions per "
              "pareto point reached\n";
    loop = false;
    cont = true;
  } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
    if (verbose >= 1)
      INFO << "Terminating algorithm due to max solutions reached\n";
    state = BOUND_REACHED;
    loop = false;
    cont = false;
  }
  if (loop && modelCache.size()) {
    // Block solution found previously
    blockingFunc(modelCache, blClause);
    oracle->addClause(blClause);
  }

  while (loop) {
    // Solve and get model
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_ENUM];
    if (ret == UNSAT) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to no more models\n";
      cont = true;
      break;
    }
    assert(ret == SAT);
#ifndef NDEBUG
    auto objVals = getObjValModel(false, false, model);
    assert(objVals.first == incBound);
    assert(objVals.second == decBound);
#else
    getObjValModel(false, false, model);
#endif
    pp.models.push_back(model);
    nSols++;
    // Check early termination conditions
    if (maxSolsPerPP != -1 &&
        pp.models.size() >= static_cast<uint32_t>(maxSolsPerPP)) {
      if (verbose >= 2)
        INFO << "Terminating solution enumeration due to max solutions per "
                "pareto point reached\n";
      cont = true;
      break;
    } else if (maxSols != -1 && nSols >= static_cast<uint32_t>(maxSols)) {
      if (verbose >= 1)
        INFO << "Terminating algorithm due to max solutions reached\n";
      state = BOUND_REACHED;
      cont = false;
      break;
    }
    // Block solution
    blockingFunc(model, blClause);
    oracle->addClause(blClause);
  }
  if (verbose >= 2)
    INFO << "Solution enumeration enumerated " << pp.models.size()
         << " solutions. Total solutions: " << nSols << "\n";
  if (maxPPs != -1 && paretoFront.size() >= static_cast<uint32_t>(maxPPs)) {
    if (verbose >= 1)
      INFO << "Terminating algorithm due to max pareto points reached\n";
    state = BOUND_REACHED;
    cont = false;
  }
  return cont;
}

void BiOptSatEnumerator::minimizeConflict(clause_t &conf) {
  if (confMinVariant == CM_NONE)
    return;

  clause_t origConf = conf;
  std::vector<lit_t> assumps{};

  if (confMinVariant == CM_EXACT) {
    // Exact minimization
    conf.clear();
    for (uint32_t i = 0; i < origConf.size(); i++) {
      assumps.clear();
      for (uint32_t j = i + 1; j < origConf.size(); j++)
        assumps.push_back(-origConf[j]);
      for (uint32_t j = 0; j < conf.size(); j++)
        assumps.push_back(-conf[j]);
      SolverState ret = oracle->solve(assumps);
      ++oracleCallLog[OCT_CONF_MIN];
      if (ret == UNSAT) // Literal not necessary
        continue;
      conf.push_back(origConf[i]);
    }
  } else if (confMinVariant == CM_HEURISTIC) {
    // Heuristic minimization
    for (lit_t l : origConf)
      assumps.push_back(-l);
#ifndef NDEBUG
    SolverState ret = oracle->solve(assumps);
#else
    oracle->solve(assumps);
#endif
    ++oracleCallLog[OCT_CONF_MIN];
    assert(ret == UNSAT);
    conf.clear();
    oracle->getConflict(conf);
    assert(conf.size() <= origConf.size());
    if (conf.size() < origConf.size())
      minimizeConflict(conf);
  }

  if (verbose >= 3)
    INFO << "Core minimization reduced core from " << origConf.size() << " to "
         << conf.size() << " literals\n";
}

// Helper functions
void BiOptSatEnumerator::upperBoundInc(std::vector<lit_t> &outAssumps) {
  switch (minIncVariant) {
  case SAT_UNSAT:
  case UNSAT_SAT:
    return miBoundInc(incBound, outAssumps);
  case MSU3:
    return msu3BoundInc(incBound, outAssumps);
  case OLL:
    return ollBoundInc(outAssumps);
  case MSU3_SU_HYBRID:
    if (hybridPhaseTwo)
      return miBoundInc(incBound, outAssumps);
    else
      return msu3BoundInc(incBound, outAssumps);
#if ENABLE_OSHYBRID
  case OLL_SU_HYBRID:
    if (hybridPhaseTwo)
      return miBoundInc(incBound, outAssumps);
    else
      return ollBoundInc(outAssumps);
#endif
  }
}

void BiOptSatEnumerator::upperBoundDec(std::vector<lit_t> &outAssumps) {
  boundDec(decBound, outAssumps);
}

void BiOptSatEnumerator::assumeInactive(std::vector<lit_t> &outAssumps) {
  if (!(minIncVariant & CORE_GUIDED))
    return;

  outAssumps.reserve(outAssumps.size() + inactiveWeight.size() + 1);
  for (const auto &p : inactiveWeight)
    outAssumps.push_back(-p.first);
}

void BiOptSatEnumerator::miBoundInc(uint64_t bound,
                                    std::vector<lit_t> &outAssumps) {
  if (!(minIncVariant & MODEL_IMPROVING || minIncVariant == UNSAT_SAT))
    WARN << "Calling miBoundInc but not in model improving mode\n";

  if (bound >= increasing.getWeightSum())
    return;

  if (!incPB) {
    incPB = pbEncodingFactory(oracle, increasingHarden ? BOUND_BOTH : BOUND_UB);
    incPB->addLits(increasing);
  }

  incPB->enforceUB(bound, outAssumps);
}

void BiOptSatEnumerator::hardenInc(uint64_t bound) {
  if (increasingHarden && incPB)
    incPB->enforceLB(bound - 1);
}

void BiOptSatEnumerator::hardenDec(uint64_t bound) {
  if (decreasingHarden && decPB && bound < decreasing.getWeightSum()) {
    decPB->enforceUB(bound);
    hardDecBound = bound;
  }
}

// Stats printing
void BiOptSatEnumerator::printStats() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "BiOptSat Enumerator Stats\n";
  INFO << LOG_H1;

  INFO << "\n";
  INFO << "#conflicts = " << nConflicts << "\n";
  INFO << "avg conflict len = " << avgConflictLen << "\n";
  INFO << "avg raw conflict len = " << avgRawConflictLen << "\n";
  INFO << "msu3Inc = " << msu3Inc << "\n";

  INFO << "\n";
  INFO << "PB Encodings\n";
  INFO << LOG_H2;
  if (incPB) {
    INFO << "Increasing PB Encoding\n";
    INFO << LOG_H3;
    incPB->printStats(false);
    if (decPB)
      INFO << "\n";
  }
  if (decPB) {
    INFO << "Decreasing PB Encoding\n";
    INFO << LOG_H3;
    decPB->printStats(false);
  }

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Increasing Objective\n";
  INFO << LOG_H3;
  INFO << "Increasing wall clock time: " << incRealTime << "s\n";
  INFO << "Increasing Cpu time: " << incCpuTime << "s\n";
  INFO << "\n";
  INFO << "Decreasing Objective\n";
  INFO << LOG_H3;
  INFO << "Decreasing wall clock time: " << decRealTime << "s\n";
  INFO << "Decreasing Cpu time: " << decCpuTime << "s\n";
  INFO << "\n";
  INFO << "Enumeration\n";
  INFO << LOG_H3;
  INFO << "Enumeration wall clock time: " << enumRealTime << "s\n";
  INFO << "Enumeration Cpu time: " << enumCpuTime << "s\n";

  Enumerator::printStats();
}

// Options printing
void BiOptSatEnumerator::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "BiOptSat Enumerator Options\n";
  INFO << LOG_H1;

  std::string minInc{};
  switch (minIncVariant) {
  case SAT_UNSAT:
    minInc = "SAT_UNSAT";
    break;
  case UNSAT_SAT:
    minInc = "UNSAT_SAT";
    break;
  case MSU3:
    minInc = "MSU3";
    break;
  case OLL:
    minInc = "OLL";
    break;
  case MSU3_SU_HYBRID:
    minInc = "MSU3_SU_HYBRID";
    break;
#if ENABLE_OSHYBRID
  case OLL_SU_HYBRID:
    minInc = "OLL_SU_HYBRID";
    break;
#endif
  }

  std::string minConf{};
  switch (confMinVariant) {
  case CM_NONE:
    minConf = "NONE";
    break;
  case CM_EXACT:
    minConf = "EXACT";
    break;
  case CM_HEURISTIC:
    minConf = "HEURISTIC";
    break;
  }

  std::string disjVariant{};
  switch (msu3DisjointVariant) {
  case M3D_NONE:
    disjVariant = "NONE";
    break;
  case M3D_ONCE:
    disjVariant = "ONCE";
    break;
  case M3D_ALL:
    disjVariant = "ALL";
    break;
  }

  INFO << "\n";
  INFO << "minIncVariant = " << minInc << "\n";
  INFO << "confMinVariant = " << minConf << "\n";
  INFO << "confExhaustion = " << (confExhaustion ? "on" : "off") << "\n";
  INFO << "increasingHarden = " << (increasingHarden ? "on" : "off") << "\n";
  INFO << "decreasingHarden = " << (decreasingHarden ? "on" : "off") << "\n";
  INFO << "coreGuidedLazyDecPB = " << (coreGuidedLazyDecPB ? "on" : "off")
       << "\n";
  INFO << "hybridImmediateSwitch = " << (hybridImmediateSwitch ? "on" : "off")
       << "\n";
  INFO << "blockDominated = " << (blockDominated ? "on" : "off") << "\n";
  INFO << "msu3DisjointVariant = " << disjVariant << "\n";
  INFO << "msu3GcdIncrement = " << (msu3GcdIncrement ? "on" : "off") << "\n";
  INFO << "hybridSwitchPortion = " << hybridSwitchPortion << "\n";
  INFO << "hybridSwitchMaxTime = " << hybridSwitchMaxTime << "\n";

  Enumerator::printOptions();
}

} // namespace BiOptSat
