/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "decisionruleapp.hpp"

#include "dataset.hpp"

namespace BiOptSat {
// Options
static std::string cat = "3.2 - Decision Rules";
static StringOption opt_rule_type(cat, "rule-type", "The type of rule to learn",
                                  "cnf", StringSet({"cnf", "dnf"}));
static BoolOption
    opt_symmetry_breaking(cat, "dec-rule-symmetry-breaking",
                          "Wether to add symmetry breaking clauses or not",
                          true);
static IntOption
    opt_n_clauses(cat, "dec-rule-clauses",
                  "The number of clauses in the learned decision rule", 2,
                  IntRange(1, 20));

DecisionRuleApp::DecisionRuleApp(uint32_t _nRuleClauses, RuleType type)
    : nRuleClauses(_nRuleClauses), symmetryBreaking(opt_symmetry_breaking) {
  setRuleType(type);
  if (nRuleClauses == 0)
    nRuleClauses = opt_n_clauses;
}

void DecisionRuleApp::formatSolution(const model_t &model,
                                     std::vector<string> &outLines) const {
  string litSep = "|";
  string clSep = "&";
  if (dnf) {
    litSep = "&";
    clSep = "|";
  }
  for (uint32_t i = 0; i < nRuleClauses; i++) {
    std::string buf = "(";
    for (uint32_t j = 0; j < nFeatures; j++)
      if (model[clauseContainsFeature[clauseFeature(i, j)]])
        buf += " " +
               ((hasNegated && j > nFeatures / 2)
                    ? ("-" + std::to_string(j - nFeatures / 2))
                    : std::to_string(j)) +
               " " + litSep;
    if (buf.length() > 1)
      buf.pop_back();
    else
      buf += " ";
    buf += ")";
    if (i < nRuleClauses - 1)
      buf += " " + clSep;
    outLines.push_back(buf);
  }
}

void DecisionRuleApp::blockSolution(const model_t &model,
                                    clause_t &outClause) const {
  outClause.clear();
  for (uint32_t i = 0; i < nRuleClauses; i++)
    for (uint32_t j = 0; j < nFeatures; j++) {
      var_t var = clauseContainsFeature[clauseFeature(i, j)];
      if (model[var] && (blockingStrategy & BLOCK_POS))
        outClause.push_back(-var);
      else if (!model[var] && (blockingStrategy & BLOCK_NEG))
        outClause.push_back(var);
    }
}

void DecisionRuleApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "DecisionRuleApp Stats\n";
  INFO << LOG_H1;

  INFO << "Learning " << (dnf ? "DNF" : "CNF") << "-rule\n";
  INFO << "#rule clauses = " << nRuleClauses << "\n";
  INFO << "#samples = " << nSamples << "\n";
  INFO << "#features = " << nFeatures << "\n";

  Application::printStats();
}

void DecisionRuleApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "DecisionRuleApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "#rule clauses = " << nRuleClauses << "\n";
  INFO << "symmetryBreaking = " << (symmetryBreaking ? "on" : "off") << "\n";
  INFO << "rule type = " << (dnf ? "DNF" : "CNF") << "\n";
  Application::printOptions();
}

void DecisionRuleApp::setRuleType(RuleType type) {
  assert(!encoded);
  switch (type) {
  case DEFAULT_RULE:
    if (opt_rule_type == "cnf")
      dnf = false;
    else
      dnf = true;
    break;
  case CNF_RULE:
    dnf = false;
    break;
  case DNF_RULE:
    dnf = true;
    break;
  }
}

void DecisionRuleApp::baseEncoding(std::vector<clause_t> &outClauses) {
  if (verbose >= 0)
    INFO << "Encoding instance " << instance << " for " << (dnf ? "DNF" : "CNF")
         << "-learning with " << nRuleClauses << " clauses\n";

  outClauses.clear();

  // Load data
  DataSet data{instance};
  nSamples = data.nSamples();
  nFeatures = data.nFeatures();
  hasNegated = data.hasNegatedColumns();

  // Create variables
  nVars = 0;

  clauseContainsFeature.resize(nRuleClauses * nFeatures);
  for (uint32_t i = 0; i < nRuleClauses; i++)
    for (uint32_t j = 0; j < nFeatures; j++)
      clauseContainsFeature[clauseFeature(i, j)] = ++nVars;

  sampleNotMatchClause.resize(nSamples * nRuleClauses);
  for (uint32_t i = 0; i < nSamples; i++)
    for (uint32_t j = 0; j < nRuleClauses; j++)
      sampleNotMatchClause[sampleClause(i, j)] = ++nVars;

  sampleNoisy.resize(nSamples);
  for (uint32_t i = 0; i < nSamples; i++)
    sampleNoisy[i] = ++nVars;

  if (symmetryBreaking) {
    clausesEqualTill.resize((nRuleClauses - 1) * nFeatures);
    for (uint32_t i = 0; i < nRuleClauses - 1; i++)
      for (uint32_t j = 0; j < nFeatures; j++)
        clausesEqualTill[clauseFeature(i, j)] = ++nVars;
  }

  posSample.resize(nSamples);
  nPosSamples = data.getNPositive();

  // Encoding
  // MLIC
  for (uint32_t i = 0; i < nSamples; i++) {
    // Encode constraint for every sample
    DataSample sample = data.getSample(i);
    posSample[i] = sample.getClass();
    if ((sample.getClass() && !dnf) || (!sample.getClass() && dnf))
      // Positive sample and CNF or negative sample and DNF
      // -eta -> (X v B)
      for (uint32_t l = 0; l < nRuleClauses; l++) {
        clause_t clause = {lit(sampleNoisy[i])};
        for (uint32_t j = 0; j < nFeatures; j++)
          if (sample.getFeature(j))
            clause.push_back(clauseContainsFeature[clauseFeature(l, j)]);
        outClauses.push_back(clause);
      }
    else {
      // Negative sample and CNF or positive sample and DNF
      // -eta -> V z
      // z -> -(X v B)
      clause_t clause = {lit(sampleNoisy[i])};
      for (uint32_t l = 0; l < nRuleClauses; l++) {
        clause.push_back(sampleNotMatchClause[sampleClause(i, l)]);
        for (uint32_t j = 0; j < nFeatures; j++)
          if (sample.getFeature(j))
            outClauses.push_back(
                {-lit(sampleNotMatchClause[sampleClause(i, l)]),
                 -lit(clauseContainsFeature[clauseFeature(l, j)])});
      }
      outClauses.push_back(clause);
    }
  }
  if (symmetryBreaking) {
    // Symmetry breaking
    for (uint i = 0; i < nRuleClauses - 1; i++)
      for (uint32_t j = 0; j < nFeatures; j++) {
        lit_t b_i1 = clauseContainsFeature[clauseFeature(i, j)];
        lit_t b_i2 = clauseContainsFeature[clauseFeature(i + 1, j)];
        lit_t e_j1 = clausesEqualTill[clauseFeature(i, j)];

        if (j == 0) {
          // (b_i^j <-> b_i-1^j) <-> e_i^j
          outClauses.push_back({b_i1, -b_i2, -e_j1});
          outClauses.push_back({-b_i1, b_i2, -e_j1});
          outClauses.push_back({-b_i1, -b_i2, e_j1});
          outClauses.push_back({b_i1, b_i2, e_j1});
          // ~e_i^j -> (b_i^j & ~b_i+1^j)
          // Order enforcing clauses
          outClauses.push_back({e_j1, b_i1});
          outClauses.push_back({e_j1, -b_i2});
          continue;
        }

        lit_t e_j0 = clausesEqualTill[clauseFeature(i, j - 1)];

        // e_i^j-1 & (b_i^j <-> b_i-1^j) <-> e_i^j
        outClauses.push_back({-e_j1, e_j0});
        outClauses.push_back({b_i1, -b_i2, -e_j1});
        outClauses.push_back({-b_i1, b_i2, -e_j1});
        outClauses.push_back({-b_i1, -b_i2, -e_j0, e_j1});
        outClauses.push_back({b_i1, b_i2, -e_j0, e_j1});
        // (e_i^j-1 & ~e_i^j) -> b_i^j & ~b_i+1^j
        // Order enforcing clauses
        outClauses.push_back({-e_j0, e_j1, b_i1});
        outClauses.push_back({-e_j0, e_j1, -b_i2});
      }
  }

  nClauses = outClauses.size();

  encoded = true;
}
} // namespace BiOptSat