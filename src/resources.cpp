/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "resources.hpp"

#ifdef __WIN32

#ifndef __WIN32_WINNT
#define __WIN32_WINNT 0x0600
#endif

#include <psapi.h>
#include <windows.h>

#else

#include <sys/resource.h>
#include <sys/time.h>

#endif

namespace BiOptSat {

// Time functions from CaDiCaL
// (https://github.com/arminbiere/cadical/blob/808dfac53a37a9beb3f28817d1904b22448d4c2e/src/resources.cpp)

#ifdef __WIN32

double absolute_real_time() {
  FILETIME f;
  GetSystemTimeAsFileTime(&f);
  ULARGE_INTEGER t;
  t.LowPart = f.dwLowDateTime;
  t.HighPart = f.dwHighDateTime;
  double res = (__int64)t.QuadPart;
  res *= 1e-7;
  return res;
}

double absolute_process_time() {
  double res = 0;
  FILETIME fc, fe, fu, fs;
  if (GetProcessTimes(GetCurrentProcess(), &fc, &fe, &fu, &fs)) {
    ULARGE_INTEGER u, s;
    u.LowPart = fu.dwLowDateTime;
    u.HighPart = fu.dwHighDateTime;
    s.LowPart = fs.dwLowDateTime;
    s.HighPart = fs.dwHighDateTime;
    res = (__int64)u.QuadPart + (__int64)s.QuadPart;
    res *= 1e-7;
  }
  return res;
}

#else

double absolute_real_time() {
  struct timeval tv;
  if (gettimeofday(&tv, 0))
    return 0;
  return 1e-6 * tv.tv_usec + tv.tv_sec;
}

// We use 'getrusage' for 'process_time' and 'maximum_resident_set_size'
// which is pretty standard on Unix but probably not available on Windows
// etc.  For different variants of Unix not all fields are meaningful.

double absolute_process_time() {
  double res;
  struct rusage u;
  if (getrusage(RUSAGE_SELF, &u))
    return 0;
  res = u.ru_utime.tv_sec + 1e-6 * u.ru_utime.tv_usec;  // user time
  res += u.ru_stime.tv_sec + 1e-6 * u.ru_stime.tv_usec; // + system time
  return res;
}

#endif

const double proc_start_absolute_real_time = absolute_real_time();

double proc_start_real_time() {
  return absolute_real_time() - proc_start_absolute_real_time;
}

}