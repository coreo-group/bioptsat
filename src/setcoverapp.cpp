/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "setcoverapp.hpp"

#include <cassert>
#include <fstream>

#include "logging.hpp"
#include "utils.hpp"

namespace BiOptSat {
SetCoverApp::SetCoverApp() {
  incName = "Element weight 1";
  decName = "Element weight 2";
}
void SetCoverApp::baseEncoding(std::vector<clause_t> &outClauses,
                               Objective &inc, Objective &dec) {
  if (verbose >= 0)
    INFO << "Loading instance " << instance << "\n";

  // Parse file
  string line{};
  std::vector<string> toks{};
  std::fstream file{};
  file.open(instance, std::fstream::in);

  // First line
  getline(file, line);
  size_t pos{};
  toks.clear();
  while ((pos = line.find(" ")) != string::npos) {
    toks.push_back(line.substr(0, pos));
    line.erase(0, pos + 1);
  }
  if (line != "")
    toks.push_back(line);
  assert(toks.size() == 2);
  nElements = std::stoi(toks[0]);
  nVars = nElements;
  nClauses = std::stoi(toks[1]);

  // Second line
  getline(file, line);
  if (line == "2")
    biobjective = true;
  else {
    assert(line == "1");
    biobjective = false;
  }

  // Inc objective
  getline(file, line);
  toks.clear();
  while ((pos = line.find(" ")) != string::npos) {
    toks.push_back(line.substr(0, pos));
    line.erase(0, pos + 1);
  }
  if (line != "")
    toks.push_back(line);
  assert(toks.size() == nVars);
  std::vector<uint32_t> weights(nVars, 0);
  incGCD = 0;
  for (uint32_t i = 0; i < nVars; i++) {
    uint32_t w = std::stoi(toks[i]);
    weights[i] = w;
    incGCD = gcd(incGCD, w);
  }
  for (uint32_t i = 0; i < nVars; i++) {
    uint32_t w = weights[i] / incGCD;
    inc.addLit(i + 1, w);
  }

  // Dec objective
  if (biobjective) {
    getline(file, line);
    toks.clear();
    while ((pos = line.find(" ")) != string::npos) {
      toks.push_back(line.substr(0, pos));
      line.erase(0, pos + 1);
    }
    if (line != "")
      toks.push_back(line);
    assert(toks.size() == nVars);
    decGCD = 0;
    for (uint32_t i = 0; i < nVars; i++) {
      uint32_t w = std::stoi(toks[i]);
      weights[i] = w;
      decGCD = gcd(decGCD, w);
    }
    for (uint32_t i = 0; i < nVars; i++) {
      uint32_t w = weights[i] / decGCD;
      dec.addLit(i + 1, w);
    }
  }

  // Sets
  while (getline(file, line)) {
#ifndef NDEBUG
    uint32_t len = std::stoi(line);
#endif
    getline(file, line);
    toks.clear();
    while ((pos = line.find(" ")) != string::npos) {
      toks.push_back(line.substr(0, pos));
      line.erase(0, pos + 1);
    }
    if (line != "")
      toks.push_back(line);
    assert(toks.size() == len);
    clause_t cl{};
    for (string tok : toks)
      cl.push_back(std::stoi(tok));
    outClauses.push_back(cl);
  }
  assert(outClauses.size() == nClauses);
}

void SetCoverApp::formatSolution(const model_t &model,
                                 std::vector<string> &outLines) const {
  outLines.clear();
  string out{};
  for (uint32_t i = 1; i <= nElements; i++)
    if (model[i])
      out += std::to_string(i) + " ";
  if (out.size() > 0)
    out.pop_back();
  outLines.push_back(out);
}

void SetCoverApp::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "Set Cover App Stats\n";
  INFO << LOG_H1;

  INFO << "nElements = " << nElements << "\n";
  INFO << "incGCD = " << incGCD << "\n";
  INFO << "decGCD = " << decGCD << "\n";

  Application::printStats();
}

void SetCoverApp::blockSolution(const model_t &model,
                                clause_t &outClause) const {
  outClause.clear();
  for (uint32_t i = 1; i <= nElements; i++) {
    if (model[i] && (blockingStrategy & BLOCK_POS))
      outClause.push_back(-i);
    else if (!model[i] && (blockingStrategy & BLOCK_NEG))
      outClause.push_back(i);
  }
}
} // namespace BiOptSat