/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "application.hpp"

#include <cassert>
#include <fstream>

#include "logging.hpp"

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if ENABLE_DIMACS
#include "dimacsapp.hpp"
#endif
#if ENABLE_OPB
#include "opbapp.hpp"
#endif
#if ENABLE_PACKUP
#include "packupapp.hpp"
#endif
#if ENABLE_INTERPDECRULES
#include "interpdecruleapp.hpp"
#endif
#if ENABLE_INTERPDECTREES
#include "interpdectreeapp.hpp"
#endif
#if ENABLE_NONLINDECRULES
#include "nonlindecruleapp.hpp"
#endif
#if ENABLE_NONLINDECTREES
#include "nonlindectreeapp.hpp"
#endif
#if ENABLE_BIOBJSETCOVER
#include "biobjsetcoverapp.hpp"
#endif
#if ENABLE_RELAXEDSETCOVER
#include "relaxedsetcoverapp.hpp"
#endif
#if ENABLE_SYNTHETICADVANTAGE
#include "syntheticadvantageapp.hpp"
#endif

namespace BiOptSat {
// Options
static string cat = "3 - Application";
static StringOption opt_application(cat, "application",
                                    "Select the application for the algorithm",
                                    "dimacs", StringSet({
#if ENABLE_DIMACS
                                      "dimacs",
#endif
#if ENABLE_OPB
                                          "opb",
#endif
#if ENABLE_PACKUP
                                          "packup",
#endif
#if ENABLE_INTERPDECRULES
                                          "interp-dec-rules",
#endif
#if ENABLE_INTERPDECTREES
                                          "interp-dec-trees",
#endif
#if ENABLE_NONLINDECRULES
                                          "non-lin-dec-rules",
#endif
#if ENABLE_NONLINDECTREES
                                          "non-lin-dec-trees",
#endif
#if ENABLE_BIOBJSETCOVER
                                          "biobj-set-cover",
#endif
#if ENABLE_RELAXEDSETCOVER
                                          "relaxed-set-cover",
#endif
#if ENABLE_SYNTHETICADVANTAGE
                                          "synthetic-advantage",
#endif
                                    }));
static BoolOption opt_print_stats(cat, "app-print-stats",
                                  "Wether or not to print application stats",
                                  true);
static BoolOption
    opt_dump_dimacs(cat, "dump-dimacs",
                    "Dump the encoded instance into a DIMACS file", false);
static BoolOption opt_dump_pf(cat, "dump-pareto-front",
                              "Dump found pareto front to a .sol file", false);
static BoolOption
    opt_domain_specific_blocking(cat, "domain-specific-blocking",
                                 "Turn on/off domain specific blocking clauses",
                                 true);
static BoolOption
    opt_swap_objectives(cat, "swap-objectives",
                        "Swap increasing and decreasing objectives", false);

Application::Application()
    : verbose(opt_verbose), doPrintStats(opt_print_stats),
      outputPath(opt_output_path), doDumpDimacs(opt_dump_dimacs),
      doDumpParetoFront(opt_dump_pf),
      doDomainSpecificBlocking(opt_domain_specific_blocking),
      swapObjectives(opt_swap_objectives) {}

void Application::solve() {
  if (!loaded) {
    WARN << "Calling solve on application without having an instance loaded\n";
    return;
  }
  INFO << "\n";
  INFO << LOG_H1;
  INFO << "\n";
  if (verbose >= 0)
    INFO << "Solving instance " << instance << " with " << nClauses
         << " clauses and " << nVars << " variables\n";

  printOptions();

  enumerator->enumerate();

  solved = true;

  dumpParetoFront();
  printParetoFront();
}

void Application::preprocess(std::vector<clause_t> &clauses, Objective &inc,
                             Objective &dec) {
  if (!prepro)
    prepro = preproFactory();
  prepro->setInstance(clauses, inc, dec);
  prepro->preprocess();
  prepro->getInstance(clauses, inc, incOffset, dec, decOffset);
}

void Application::reconstruct(model_t &model) const {
  if (prepro)
    prepro->reconstruct(model);
}

void Application::reconstructPf(ParetoFront<model_t> &pf) const {
  for (ParetoPoint<model_t> &pp : pf) {
    pp.incVal += incOffset;
    pp.decVal += decOffset;
    for (model_t &mdl : pp.models)
      reconstruct(mdl);
  }
}

void Application::loadInstance(string path) {
  instance = path;

  double realStartTime = absolute_real_time();
  double cpuStartTime = absolute_process_time();

  std::vector<clause_t> clauses{};
  Objective inc{};
  Objective dec{};
  loadEncode(clauses, inc, dec);

  if (swapObjectives) {
    // Swap objectives
    std::swap(inc, dec);
    std::swap(incName, decName);
  }

  nClauses = clauses.size();

  encodingRealTime += absolute_real_time() - realStartTime;
  encodingCpuTime += absolute_process_time() - cpuStartTime;
  if (verbose >= 2)
    INFO << "Loading/encoding times: real time=" << encodingRealTime
         << "; cpu time=" << encodingCpuTime << "\n";

  preprocess(clauses, inc, dec);

  incWeighted = inc.isWeighted();
  decWeighted = dec.isWeighted();
  incWeightSum = inc.getWeightSum();
  decWeightSum = dec.getWeightSum();

  dumpDimacs(clauses, inc, dec);

  solved = false;
  if (enumerator) {
    delete enumerator;
    enumerator = nullptr;
  }

  // Create enumerator
  enumerator = enumeratorFactory(clauses, inc, dec);
  enumerator->setBlockFunction([&](const model_t &mdl, clause_t &outCl) {
    this->blockSolutionWrapper(mdl, outCl);
  });

  setupEnumerator();

  loaded = true;
}

void Application::formatSolution(const model_t &model,
                                 std::vector<string> &outLines) const {
  string buf = "";
  for (uint32_t i = 1; i < model.size(); i++)
    buf += (model[i] ? "1" : "0");
  outLines.push_back(buf);
}

void Application::printSolution(const model_t &model) const {
  std::vector<string> lines{};
  formatSolution(model, lines);
  for (string s : lines)
    SOL << s << "\n";
}

void Application::blockSolution(const model_t &model, clause_t &clause) const {
  clause.clear();
  clause.reserve(model.size() - 1);

  for (uint32_t i = 1; i < model.size(); i++)
    clause.push_back(model[i] ? -i : i);
}

void Application::printParetoFront() const {
  if (!solved)
    WARN << "Calling printParetoFront but instance is not solved\n";

  assert(enumerator);
  ParetoFront<model_t> pf = getParetoFront();

  INFO << "\n";
  INFO << "\n";
  INFO << "Found Pareto Front\n";
  INFO << LOG_H1;
  for (ParetoPoint<model_t> pp : pf) {
    INFO << "Pareto point: " << incName << " = " << scaleInc(pp.incVal) << "; "
         << decName << " = " << scaleDec(pp.decVal)
         << "; #solutions = " << pp.models.size() << "\n";
    INFO << LOG_H2;
    for (model_t mdl : pp.models) {
      printSolution(mdl);
      INFO << LOG_H3;
    }
  }
}

void Application::printStats() const {
  if (!doPrintStats)
    return;

  INFO << "\n";
  INFO << "\n";
  INFO << "Application Stats\n";
  INFO << LOG_H1;

  INFO << "Instance: " << instance << "\n";
  INFO << "#clauses = " << nClauses << "\n";
  INFO << "#vars = " << nVars << "\n";
  INFO << "inc weighted = " << (incWeighted ? "yes" : "no") << "\n";
  INFO << "dec weighted = " << (decWeighted ? "yes" : "no") << "\n";
  INFO << "inc weight sum = " << incWeightSum << "\n";
  INFO << "dec weight sum = " << decWeightSum << "\n";

  INFO << "\n";
  INFO << "Times\n";
  INFO << LOG_H2;
  INFO << "Loading/Encoding\n";
  INFO << LOG_H3;
  INFO << "Loading/encoding wall clock time: " << encodingRealTime << "s\n";
  INFO << "Loading/encoding Cpu time: " << encodingCpuTime << "s\n";

  if (enumerator)
    enumerator->printStats();

  if (prepro)
    prepro->printStats();
}

void Application::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "Application Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "verbose = " << verbose << "\n";
  INFO << "dump dimacs = " << (doDumpDimacs ? "yes" : "no") << "\n";
  INFO << "output path = " << outputPath << "\n";

  if (enumerator)
    enumerator->printOptions();

  if (prepro)
    prepro->printOptions();
}

void Application::dumpDimacs(const std::vector<clause_t> &clauses,
                             const Objective &inc, const Objective &dec) const {
  if (!doDumpDimacs)
    return;

  if (verbose >= 1)
    INFO << "Dumping encoded problem to " << outputPath << ".mcnf\n";

  std::ofstream file{outputPath + ".mcnf"};
  file << "c Bi-Objective MCNF Dimacs file generated by BiOptSat\n";
  file << "c Original instance file: " << instance << "\n";
  file << "c Lines containing clauses start with h\n";
  file << "c Soft clauses start either with o1 or o2, depending on which "
          "objective they belong to\n";
  file << "c All soft clauses are unit\n";

  for (clause_t cl : clauses) {
    file << "h ";
    for (lit_t l : cl)
      file << l << " ";
    file << "0\n";
  }

  for (const auto &p : inc)
    file << "o1 " << p.second << " " << -p.first << " 0\n";

  for (const auto &p : dec)
    file << "o2 " << p.second << " " << -p.first << " 0\n";

  file.close();
}

void Application::dumpParetoFront() const {
  if (!doDumpParetoFront)
    return;

  if (!solved)
    WARN << "Calling dumpParetoFront but instance is not solved\n";

  std::ofstream file{outputPath + ".sol"};

  assert(enumerator);
  ParetoFront<model_t> pf = getParetoFront();

  for (ParetoPoint<model_t> pp : pf) {
    file << "c Pareto point: " << incName << " = " << scaleInc(pp.incVal)
         << "; " << decName << " = " << scaleDec(pp.decVal)
         << "; #solutions = " << pp.models.size() << "\n";
    file << "c " << LOG_H2;
    for (model_t mdl : pp.models) {
      std::vector<string> lines{};
      formatSolution(mdl, lines);
      for (string s : lines)
        file << "s " << s << "\n";
      file << "c " << LOG_H3;
    }
  }
}

ParetoFront<model_t> Application::getParetoFront() const {
  assert(enumerator);
  ParetoFront<model_t> pf = enumerator->getParetoFront();
  reconstructPf(pf);
  return pf;
}

Application *appFactory(string id) {
  if (id == "")
    id = opt_application;
#if ENABLE_DIMACS
  if (id == "dimacs")
    return new DimacsApp();
#endif
#if ENABLE_OPB
  if (id == "opb")
    return new OPBApp();
#endif
#if ENABLE_PACKUP
  if (id == "packup")
    return new PackUpApp();
#endif
#if ENABLE_INTERPDECRULES
  if (id == "interp-dec-rules")
    return new InterpDecRuleApp();
#endif
#if ENABLE_INTERPDECTREES
  if (id == "interp-dec-trees")
    return new InterpDecTreeApp();
#endif
#if ENABLE_NONLINDECRULES
  if (id == "non-lin-dec-rules")
    return new NonLinDecRuleApp();
#endif
#if ENABLE_NONLINDECTREES
  if (id == "non-lin-dec-trees")
    return new NonLinDecTreeApp();
#endif
#if ENABLE_BIOBJSETCOVER
  if (id == "biobj-set-cover")
    return new BiobjSetCoverApp();
#endif
#if ENABLE_RELAXEDSETCOVER
  if (id == "relaxed-set-cover")
    return new RelaxedSetCoverApp();
#endif
#if ENABLE_SYNTHETICADVANTAGE
  if (id == "synthetic-advantage")
    return new SyntheticAdvantageApp();
#endif
  ERROR << "Unknown application " << id << "\n";
  exit(1);
}
} // namespace BiOptSat
