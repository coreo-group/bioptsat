/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author: Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _encodings_hpp_INCLUDED
#define _encodings_hpp_INCLUDED

#include <vector>

#include "types.hpp"

// Functions for generating helpful encodings

// All functions take as the first two parameters:
// nVars: reference to a variable counter that is incremented when new vars are
// added
// outClauses: reference to vector of clauses where encoding is appended

namespace BiOptSat {
// Exactly one of the literals is true
void exactlyOne(uint32_t &, std::vector<clause_t> &,
                const std::vector<lit_t> &);
// If the condition literal is true, exactly one of the literals is true
void condExactlyOne(uint32_t &, std::vector<clause_t> &, lit_t,
                    const std::vector<lit_t> &);
// Tseitin and: the returned literal is true iff the two inputs are true
lit_t tseitinAnd(uint32_t &, std::vector<clause_t> &, lit_t, lit_t);
} // namespace BiOptSat

#endif