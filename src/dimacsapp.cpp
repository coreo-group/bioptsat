/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "dimacsapp.hpp"

#include <cassert>
#include <fstream>
#include <string>
#include <vector>

#include "globaloptions.hpp"
#include "logging.hpp"
#include "options.hpp"

namespace BiOptSat {
static std::string cat = "3.10 - Dimacs Application Options";
static BoolOption
    opt_bicnf(cat, "dimacs-old-fmt",
              "Parse the old bicnf format rather than the new mcnf", false);

DimacsApp::DimacsApp()
    : oldBicnf(opt_bicnf), incObjIdx(opt_inc_obj), decObjIdx(opt_dec_obj) {}

void DimacsApp::loadEncode(std::vector<clause_t> &clauses, Objective &inc,
                           Objective &dec) {
  if (verbose >= 0)
    INFO << "Loading instance " << instance << "\n";

  std::pair<std::vector<std::pair<uint64_t, clause_t>>,
            std::vector<std::pair<uint64_t, clause_t>>>
      softClauses{};

  nVars = 0;

  // Parse file
  std::string line{};
  std::vector<std::string> toks{};
  std::fstream file;
  file.open(instance, std::fstream::in);
  std::string incObjTok = "o" + std::to_string(incObjIdx);
  std::string decObjTok = "o" + std::to_string(decObjIdx);

  while (getline(file, line)) {
    if (line[0] == 'c')
      continue;
    size_t pos = 0;
    toks.clear();
    while ((pos = line.find(" ")) != std::string::npos) {
      toks.push_back(line.substr(0, pos));
      line.erase(0, pos + 1);
    }
    toks.push_back(line);
    assert(toks.size() >= 1);
    if (toks[0] == "h") {
      // Parse clause
      assert(toks.back() == "0");
      clause_t cl{};
      lit_t l{};
      for (uint32_t i = 1; i < toks.size() - 1; i++) {
        l = std::stoi(toks[i]);
        if (var(l) > nVars)
          nVars = var(l);
        cl.push_back(l);
      }
      clauses.push_back(cl);
      continue;
    }
    if (oldBicnf) {
      if (toks[0] == "1") {
        // Parse increasing objective soft _lit_
        assert(toks.size() == 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        lit_t l = std::stoi(toks[2]);
        if (var(l) > nVars)
          nVars = var(l);
        inc.addLit(l, w);
        continue;
      }
      if (toks[0] == "2") {
        // Parse decreasing objective soft _lit_
        assert(toks.size() == 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        lit_t l = std::stoi(toks[2]);
        if (var(l) > nVars)
          nVars = var(l);
        dec.addLit(l, w);
        continue;
      }
    } else {
      if (toks[0] == incObjTok) {
        // Parse increasing objective soft _clause_
        assert(toks.size() >= 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        if (toks.size() == 4) {
          // Soft clause is unit, do not need to add blocking lit
          lit_t l = -std::stoi(toks[2]);
          if (var(l) > nVars)
            nVars = var(l);
          inc.addLit(l, w);
          continue;
        }
        // True soft _clause_
        clause_t cl{};
        for (size_t i = 2; i < toks.size() - 1; ++i) {
          lit_t l = std::stoi(toks[i]);
          cl.push_back(l);
          if (var(l) > nVars)
            nVars = var(l);
        }
        softClauses.first.emplace_back(std::make_pair(w, cl));
        continue;
      }
      if (toks[0] == decObjTok) {
        // Parse increasing objective soft _clause_
        assert(toks.size() >= 4);
        assert(toks.back() == "0");
        uint64_t w = std::stoi(toks[1]);
        if (toks.size() == 4) {
          // Soft clause is unit, do not need to add blocking lit
          lit_t l = -std::stoi(toks[2]);
          if (var(l) > nVars)
            nVars = var(l);
          dec.addLit(l, w);
          continue;
        }
        // True soft _clause_
        clause_t cl{};
        for (size_t i = 2; i < toks.size() - 1; ++i) {
          lit_t l = std::stoi(toks[i]);
          cl.push_back(l);
          if (var(l) > nVars)
            nVars = var(l);
        }
        softClauses.second.emplace_back(std::make_pair(w, cl));
        continue;
      }
      if (toks[0][0] == 'o') {
        // Ignore other objectives in the instance
        continue;
      }
    }
    ERROR << "Encountered unexpected line\n";
    exit(1);
  }

  // Add blocking literals to soft clauses
  for (std::pair<uint64_t, clause_t> wcl : softClauses.first) {
    wcl.second.push_back(++nVars);
    clauses.push_back(wcl.second);
    inc.addLit(nVars, wcl.first);
  }
  for (std::pair<uint64_t, clause_t> wcl : softClauses.second) {
    wcl.second.push_back(++nVars);
    clauses.push_back(wcl.second);
    dec.addLit(nVars, wcl.first);
  }
}

void DimacsApp::printOptions() const {
  INFO << "\n";
  INFO << "\n";
  INFO << "DimacsApp Options\n";
  INFO << LOG_H1;
  INFO << "\n";
  INFO << "parse old bicnf format = " << (oldBicnf ? "on" : "off") << "\n";
  INFO << "increasing objective index = " << incObjIdx << "\n";
  INFO << "decreasing objective index = " << decObjIdx << "\n";

  Application::printOptions();
}

} // namespace BiOptSat
