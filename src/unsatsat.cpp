/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "logging.hpp"

namespace BiOptSat {
bool BiOptSatEnumerator::unsatSatIncObj() {
  std::vector<lit_t> assumps{};
  if (!decreasingHarden)
    upperBoundDec(assumps);
  // Check that there are more solutions
  SolverState ret = oracle->solve(assumps);
  ++oracleCallLog[OCT_US];
  if (ret == UNSAT)
    // End of enumeration
    return false;
  assert(ret == SAT);
  uint64_t bound = incBound;
  std::pair<uint64_t, uint64_t> objVals;
  while (bound < increasing.getWeightSum()) {
    if (verbose >= 3)
      INFO << "minIncObj (unsat-sat) iteration, bound=" << bound << "\n";
    assumps.clear();
    if (!decreasingHarden)
      upperBoundDec(assumps);
    miBoundInc(bound, assumps);
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_US];
    if (ret == SAT) {
      // Cache model
      objVals = getObjValModel(false, true, modelCache);
      assert(bound == objVals.first);
      break;
    }
    assert(ret == UNSAT);
    bound = incPB->nextHigherPossible(bound);
    hardenInc(bound);
  }
  if (bound < increasing.getWeightSum()) {
    decBound = objVals.second;
    hardenDec(decBound);
  }
  logCandidate(bound, decBound);
  incBound = bound;
  return true;
}
}