/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _application_hpp_INCLUDED
#define _application_hpp_INCLUDED

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "enumerator.hpp"
#include "globaloptions.hpp"
#include "preprocessor.hpp"
#include "types.hpp"

using std::string;

namespace BiOptSat {
enum BlockingStrategy { // Bit map for blocking
  BLOCK_POS = 1,
  BLOCK_NEG = 2,
  BLOCK_BOTH = 3
};

// Abstract base class for applications to BiOptSat
class Application {
protected:
  Enumerator *enumerator{}; // The enumerator algorithm used
  uint32_t nClauses{};      // The number of encoded clauses
  uint32_t nVars{};         // The number of variables in the encoding
  uint64_t incWeightSum{}; // The sum of all weights of the increasing objective
  uint64_t decWeightSum{}; // The sum of all weight of the decreasing objective
  bool incWeighted{};      // Whether the increasing objective is weighted
  bool decWeighted{};      // Whether the decreasing objective is weighted
  string incName =
      "Increasing objective"; // The name of the increasing objective. Can be
                              // initialized differently by inheriting class.
  string decName =
      "Decreasing objective"; // The name of the decreasing objective. Can be
                              // initialized differently by inheriting class.
  uint64_t incOffset{};       // Offset for the increasing objective (e.g., from
                              // preprocessing)
  uint64_t decOffset{};       // Offset for the decreasing objective (e.g., from
                              // preprocessing)

  // Time variables and functions
  double encodingRealTime{}; // The total wall clock time spent loading and
                             // encoding
  double encodingCpuTime{};  // The total cpu time spent loading and encoding

  bool loaded{};        // Wether a instance is loaded and encoded
  bool solved{};        // Wether the instance is solved
  string instance = ""; // A descriptor of the instance, typically a file path

  PreProcessor *prepro{}; // The preprocessor to use

  void preprocess(std::vector<clause_t> &, Objective &,
                  Objective &); // Execute preprocessing on the loaded instance
  void reconstruct(
      model_t &) const; // Reconstruct a model for the preprocessed instance
  void reconstructPf(ParetoFront<model_t> &)
      const; // Reconstruct a pareto front for the preprocessed instance
  virtual void formatSolution(const model_t &,
                              std::vector<string> &)
      const; // Format a given solution to lines as strings.
             // Used for printing to stdout and files.
  virtual void
  loadEncode(std::vector<clause_t> &, Objective &,
             Objective &) = 0; // To be implemented by concrete application.
                               // Encode the instance, populate the
                               // objectives and the passed clause vector.
                               // Should set nVars, nClauses is optional.
  virtual void setupEnumerator() {
  } // Optionally setup steps for the enumerator after loading.

  // Options
  uint32_t verbose{};  // The verbosity level of output
  bool doPrintStats{}; // Wether or not to print statistics
  string outputPath{}; // The output path to dump to if applicable
  bool doDumpDimacs{}; // Wether or not to dump the encoded instance to a file
  bool doDumpParetoFront{}; // Wether or not to dump the found pareto front to a
                            // file
  bool doDomainSpecificBlocking{}; // Wether to use domain specific blocking of
                                   // the default blocking clause over all
                                   // variables
  bool swapObjectives{}; // Whether to swap the objectives

public:
  Application();
  virtual ~Application() {
    if (enumerator) {
      delete enumerator;
      enumerator = nullptr;
    }
  }
  void loadInstance(string);                 // Load an instance
  void solve();                              // Solve the instance
  void printSolution(const model_t &) const; // Print a solution
  inline void blockSolutionWrapper(const model_t &model,
                                   clause_t &blClause) const {
    // Takes care of preprocessing mappings before and after computing a
    // blocking clause
    model_t rModel = model;
    if (prepro)
      prepro->reconstruct(rModel);

    if (doDomainSpecificBlocking)
      blockSolution(rModel, blClause);
    else
      Application::blockSolution(rModel, blClause);

    if (prepro)
      prepro->reindexClause(blClause);
  }
  virtual void blockSolution(const model_t &,
                             clause_t &) const; // Block a solution
  virtual void printParetoFront() const;        // Print the found pareto front
  virtual uint32_t scaleInc(uint32_t inc)
      const { // Function applied to the increasing objective before printing
    return inc;
  }
  virtual uint32_t scaleDec(uint32_t dec)
      const { // Function applied to the decreasing objective before printing
    return dec;
  }
  virtual void
  printStats() const; // Print stats of the application and the enumerator
  virtual void
  printOptions() const; // Print options of the application and the enumerator
  inline void interrupt() {
    if (enumerator)
      enumerator->interrupt();
  }
  virtual ParetoFront<model_t> getParetoFront() const;
  void dumpDimacs(const std::vector<clause_t> &, const Objective &,
                  const Objective &) const;
  void dumpParetoFront() const; // Dump the found pareto front to .sol a file

  // Stats getter
  uint32_t getNClauses() const { return nClauses; }

  // Options getter and setter
  uint32_t getVerbose() const { return verbose; }
  void setVerbose(uint32_t verb) { verbose = verb; }
  bool hasPrintStats() const { return doPrintStats; }
  void setPrintStats(bool ps) { doPrintStats = ps; }
  string getOutputPath() const { return outputPath; }
  void setOutputPath(string s) { outputPath = s; }
  bool hasDumpDimacs() const { return doDumpDimacs; }
  void setDumpDimacs(bool dd) { doDumpDimacs = dd; }
  bool hasDumpParetoFront() const { return doDumpParetoFront; }
  void setDumpParetoFront(bool dpf) { doDumpParetoFront = dpf; }
  bool hasDomainSpecificBlocking() const { return doDomainSpecificBlocking; }
  void setDomainSpecificBlocking(bool dsb) { doDomainSpecificBlocking = dsb; }
};

// Application factory to generate fitting application class from string
Application *appFactory(string id = ""); // Empty string for default

} // namespace BiOptSat

#endif