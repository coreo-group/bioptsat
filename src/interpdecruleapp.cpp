/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "interpdecruleapp.hpp"

namespace BiOptSat {
InterpDecRuleApp::InterpDecRuleApp() : InterpClassifierApp("Rule") {}

void InterpDecRuleApp::getSizeObj(Objective &outObj) const {
  for (uint32_t i = 0; i < nRuleClauses; i++)
    for (uint32_t j = 0; j < nFeatures; j++)
      outObj.addLit(clauseContainsFeature[clauseFeature(i, j)]);
}

void InterpDecRuleApp::getErrorObj(Objective &outObj) const {
  for (uint32_t i = 0; i < nSamples; i++)
    outObj.addLit(sampleNoisy[i]);
}

void InterpDecRuleApp::loadEncode(std::vector<clause_t> &clauses, Objective &inc,
                              Objective &dec) {
  baseEncoding(clauses);
  setupObjectives(inc, dec);
}

void InterpDecRuleApp::setupEnumerator() {
  // Omitting trivial constant negative classifier
  if (sizeInc && enumerator->getMaxDecreasing() == -1)
    enumerator->setMaxDecreasing(nPosSamples - 1);
  else if (!sizeInc) {
    if (enumerator->getMaxIncreasing() == -1)
      enumerator->setMaxIncreasing(nPosSamples - 1);
    if (enumerator->getMinDecreasing() == -1)
      enumerator->setMinDecreasing(nRuleClauses);
  }

  // Set blocking strategy
  if (enumerator->getEnumType() == ORDERED) {
    if (sizeInc)
      blockingStrategy = BLOCK_NEG;
    else
      blockingStrategy = BLOCK_POS;
  } else
    blockingStrategy = BLOCK_BOTH;
}

ParetoFront<model_t> InterpDecRuleApp::getParetoFront() const {
  // Add trivial constant negative classifier, if not already there
  model_t emptyMdl(nVars, false);
  ParetoFront<model_t> pf = enumerator->getParetoFront();
  reconstructPf(pf);
  if (sizeInc && (pf.size() == 0 || pf.front().incVal != 0)) {
    ParetoPoint<model_t> pp{
        .models = {emptyMdl}, .incVal = 0, .decVal = nPosSamples};
    pf.insert(pf.begin(), pp);
  } else if (!sizeInc && (pf.size() == 0 || pf.back().decVal != 0)) {
    ParetoPoint<model_t> pp{
        .models = {emptyMdl}, .incVal = nPosSamples, .decVal = 0};
    pf.push_back(pp);
  }
  return pf;
}

void InterpDecRuleApp::printOptions() const {
  InterpClassifierApp::printOptions();
  DecisionRuleApp::printOptions();
}
} // namespace BiOptSat