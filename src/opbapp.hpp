/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _opbapp_hpp_INCLUDED
#define _opbapp_hpp_INCLUDED

#include "application.hpp"
#include "pbconstraint.hpp"
#include "types.hpp"

using std::string;

namespace BiOptSat {
// An application for using multi-objective OPB files as input
class OPBApp : public Application {
protected:
  string cardEncType{};
  string pbEncType{};

  size_t incObjIdx{};
  size_t decObjIdx{};

  uint64_t negIncOffset{};
  uint64_t negDecOffset{};

  void loadEncode(std::vector<clause_t> &, Objective &, Objective &);

  // Stats
  uint64_t pbcTranslationCounts[PBCE_LEN];

public:
  OPBApp();

  virtual void printOptions() const;
  virtual void printStats() const;

  // Option getter and setter
  string getCardEncType() const { return cardEncType; }
  void setCardEncType(string cet) { cardEncType = cet; }
  string getPBEncType() const { return pbEncType; }
  void setPBEncType(string pet) { pbEncType = pet; }
  size_t getIncObjIdx() const { return incObjIdx; }
  void setIncObjIdx(size_t ioi) { incObjIdx = ioi; }
  size_t getDecObjIdx() const { return decObjIdx; }
  void setDecObjIdx(size_t doi) { decObjIdx = doi; }
};

} // namespace BiOptSat

#endif