/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Author (Encoding): Andreas Niskanen - andreas.niskanen@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 * Copyright © 2022 Andreas Niskanen, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _decisionruleapp_hpp_INCLUDED
#define _decisionruleapp_hpp_INCLUDED

#include <cassert>
#include <cstdint>
#include <vector>

#include "application.hpp"

namespace BiOptSat {
enum RuleType { // Rule type selection
  CNF_RULE,
  DNF_RULE,
  DEFAULT_RULE
};

// Abstract base class for applications based on decision rules
// The encoding used is the MLIC encoding
class DecisionRuleApp : virtual public Application {
protected:
  bool encoded{};          // Wether or not the instance is already encoded
  bool dnf{};              // Wether to learn DNFs or CNFs
  uint32_t nRuleClauses{}; // Number of clauses in the learned rule
  uint32_t nSamples{};     // Number of samples in the data
  uint32_t nFeatures{};    // Number of features in the data
  bool symmetryBreaking{}; // Wether to add symmetry breaking or not
  bool hasNegated{};       // Wether negated features are used

  // Indexing functions for the variables sets with two indices
  // The individual indices start at 0
  inline uint32_t clauseFeature(uint32_t c, uint32_t f) const {
    return c * nFeatures + f;
  }
  inline uint32_t sampleClause(uint32_t s, uint32_t c) const {
    return s * nRuleClauses + c;
  }

  // Variable sets
  std::vector<var_t> clauseContainsFeature{}; // b variables of the MLIC paper
  std::vector<var_t>
      sampleNotMatchClause{};       // Tseitin variables (z) from the MLIC paper
  std::vector<var_t> sampleNoisy{}; // The eta variables from the MLIC paper
  std::vector<var_t> clausesEqualTill{}; // The e variables from our paper for
                                         // symmetry breaking
  // Helper for separating into positive and negative samples
  std::vector<bool> posSample{};
  uint32_t nPosSamples{};

  void baseEncoding(std::vector<clause_t> &); // Encode the instance to clauses
  void formatSolution(const model_t &, std::vector<string> &) const;

  BlockingStrategy blockingStrategy{
      BLOCK_BOTH}; // Wether to block all, only positive or only negative rule
                   // defining literals

public:
  DecisionRuleApp(
      uint32_t nRuleClauses = 0,
      RuleType type = DEFAULT_RULE); // nRuleClauses = 0 for CLI param
  void blockSolution(const model_t &, clause_t &) const;
  virtual void printStats() const;
  virtual void printOptions() const;

  // Option getter and setter (don't change options after encoding)
  uint32_t getNRuleClauses() const { return nRuleClauses; }
  void setNRuleClauses(uint32_t nrc) {
    assert(!encoded);
    nRuleClauses = nrc;
  }
  bool hasSymmetryBreaking() const { return symmetryBreaking; }
  void setSymmetryBreaking(bool sb) { symmetryBreaking = sb; }
  bool encodesDnf() const { return dnf; }
  void setRuleType(RuleType);
  bool usesNegated() const { return hasNegated; }
};
} // namespace BiOptSat

#endif