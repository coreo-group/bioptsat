/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "bioptsat.hpp"

#include <cassert>

#include "logging.hpp"

namespace BiOptSat {
bool BiOptSatEnumerator::satUnsatIncObj() {
  std::vector<lit_t> assumps{};
  if (!decreasingHarden)
    upperBoundDec(assumps);
  // Overestimate increasing objective
  SolverState ret = oracle->solve(assumps);
  ++oracleCallLog[OCT_SU];
  if (ret == UNSAT)
    // End of enumeration
    return false;
  assert(ret == SAT);
  // Cache model
  auto objVals = getObjValModel(false, false, modelCache);
  uint64_t oldBound{};
  uint64_t oldDec{};
  while (objVals.first > incBound) {
    if (verbose >= 3)
      INFO << "minIncObj (sat-unsat) iteration, bound=" << objVals.first
           << "\n";
    assumps.clear();
    if (!decreasingHarden)
      upperBoundDec(assumps);
    miBoundInc(objVals.first - 1, assumps);
    SolverState ret = oracle->solve(assumps);
    ++oracleCallLog[OCT_SU];
    if (ret == UNSAT)
      break;
    assert(ret == SAT);
    oldBound = objVals.first;
    // Cache model and get tight objective values
    objVals = getObjValModel(true, true, modelCache);
    assert(objVals.first < oldBound);
    // Phase model for solution-guided search
    phaseModel();
    // Keep track of decVal to update decBound
    oldDec = objVals.second;
    logCandidate(objVals.first, objVals.second);
    // Block dominated of previous candidate
    if (blockDominated && incPB && oldBound < increasing.getWeightSum() &&
        decPB && oldDec < decreasing.getWeightSum()) {
      clause_t cl{};
      lit_t out;
      incPB->enforceUB(oldBound, out);
      cl.push_back(out);
      decPB->enforceUB(oldDec, out);
      cl.push_back(out);
      oracle->addClause(cl);
    }
  }
  hardenInc(objVals.first);
  decBound = objVals.second;
  hardenDec(decBound);
  incBound = objVals.first;
  return true;
}
} // namespace BiOptSat