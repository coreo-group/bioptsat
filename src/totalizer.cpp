/*
 * Author: Christoph Jabs - christoph.jabs@helsinki.fi
 * Based on (MSU3, OLL): Open-WBO (https://github.com/sat-group/open-wbo)
 *
 * Copyright © 2022 Christoph Jabs, University of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "totalizer.hpp"

#include <cassert>

#include "globaloptions.hpp"
#include "logging.hpp"

namespace BiOptSat {
void Totalizer::build(vector<lit_t> &_inLits, uint32_t _upper) {
  if (verbose >= 3)
    INFO << "Building totalizer over " << inLits.size() << " literals\n";

  inLits = _inLits;

  depth = ceil(log2(inLits.size()));

  outLits.clear();

  upper = _upper;
  if (upper > inLits.size())
    upper = inLits.size();

  // Corner case (single literal)
  if (inLits.size() == 1) {
    outLits.push_back(inLits[0]);
    treeUpper.push_back(upper);
    treeOut.push_back(outLits);
    treeLeft.push_back({});
    treeRight.push_back({});
    return;
  }

  // Create out literals
  for (uint32_t i = 0; i < inLits.size(); i++) {
    outLits.push_back(solver->getNewVar());
    nVars++;
  }

  tmpBuffer = inLits;

  toCnf(outLits);
  assert(tmpBuffer.size() == 0);

  if (verbose >= 3)
    INFO << "Totalizer stats: #clauses=" << nClauses << "; #vars=" << nVars
         << "\n";
}

void Totalizer::toCnf(vector<lit_t> &lits) {
  vector<lit_t> left{};
  vector<lit_t> right{};

  assert(lits.size() > 1);
  uint32_t split = floor(lits.size() / 2);

  for (uint32_t i = 0; i < lits.size(); i++) {
    if (i < split) {
      // left branch
      if (split == 1) {
        assert(tmpBuffer.size() > 0);
        left.push_back(tmpBuffer.back());
        tmpBuffer.pop_back();
      } else {
        left.push_back(solver->getNewVar());
        nVars++;
      }
    } else {
      // right branch
      if (lits.size() - split == 1) {
        assert(tmpBuffer.size() > 0);
        right.push_back(tmpBuffer.back());
        tmpBuffer.pop_back();
      } else {
        right.push_back(solver->getNewVar());
        nVars++;
      }
    }
  }

  if (left.size() > 1)
    toCnf(left);
  if (right.size() > 1)
    toCnf(right);
  adder(left, right, lits);
}

void Totalizer::adder(vector<lit_t> &left, vector<lit_t> &right,
                      vector<lit_t> &output) {
  assert(output.size() == left.size() + right.size());

  // Save tree for iterative extension
  treeLeft.push_back(left);
  treeRight.push_back(right);
  treeOut.push_back(output);
  treeUpper.push_back(upper);

  // Encode adder
  for (uint32_t i = 0; i <= left.size(); i++) {
    for (uint32_t j = 0; j <= right.size(); j++) {
      if (i + j > upper + 1)
        continue;

      if (boundType == BOUND_UB || boundType == BOUND_BOTH) {
        if (i == 0 && j != 0)
          solver->addClause(-right[j - 1], output[i + j - 1]);
        else if (j == 0 && i != 0)
          solver->addClause(-left[i - 1], output[i + j - 1]);
        else if (i != 0 && j != 0)
          solver->addClause(-left[i - 1], -right[j - 1], output[i + j - 1]);
        if (i != 0 || j != 0)
          nClauses++;
      }

      if (boundType == BOUND_LB || boundType == BOUND_BOTH) {
        if (i == left.size() && j != right.size())
          solver->addClause(right[j], -output[i + j]);
        else if (j == right.size() && i != left.size())
          solver->addClause(left[i], -output[i + j]);
        else if (i != left.size() && j != right.size())
          solver->addClause(left[i], right[j], -output[i + j]);
        if (i != left.size() || j != right.size())
          nClauses++;
      }
    }
  }
}

void Totalizer::join(Totalizer &tot) {
  uint32_t leftIdx = treeUpper.size() - 1;
  uint joinedSize = treeUpper.size() + tot.treeUpper.size();
  treeLeft.reserve(joinedSize);
  treeRight.reserve(joinedSize);
  treeOut.reserve(joinedSize);
  treeUpper.reserve(joinedSize);
  treeLeft.insert(treeLeft.end(), tot.treeLeft.begin(), tot.treeLeft.end());
  treeRight.insert(treeRight.end(), tot.treeRight.begin(), tot.treeRight.end());
  treeOut.insert(treeOut.end(), tot.treeOut.begin(), tot.treeOut.end());
  treeUpper.insert(treeUpper.end(), tot.treeUpper.begin(), tot.treeUpper.end());

  if (tot.depth > depth)
    depth = tot.depth + 1;
  else
    depth += 1;

  nClauses += tot.nClauses;

  vector<lit_t> left = treeOut[leftIdx];
  vector<lit_t> right = treeOut[joinedSize - 1];

  inLits.insert(inLits.end(), tot.inLits.begin(), tot.inLits.end());

  outLits.clear();
  for (uint32_t i = 0; i < left.size() + right.size(); i++) {
    outLits.push_back(solver->getNewVar());
    nVars++;
  }
  adder(left, right, outLits);
}

void Totalizer::updateUpper(uint32_t _upper) {
  if (_upper <= upper)
    return;
  upper = _upper;
  if (upper > outLits.size())
    upper = outLits.size();

  if (verbose >= 3)
    INFO << "Extending totalizer to upper bound of " << upper << "\n";

  for (uint32_t z = 0; z < treeUpper.size(); z++) {
    // Encode additional adder clauses
    for (uint32_t i = 0; i <= treeLeft[z].size(); i++) {
      for (uint32_t j = 0; j <= treeRight[z].size(); j++) {
        if (i + j > upper + 1 || i + j <= treeUpper[z] + 1)
          continue;

        if (boundType == BOUND_UB || boundType == BOUND_BOTH) {
          if (i == 0 && j != 0)
            solver->addClause(-treeRight[z][j - 1], treeOut[z][i + j - 1]);
          else if (j == 0 && i != 0)
            solver->addClause(-treeLeft[z][i - 1], treeOut[z][i + j - 1]);
          else if (i != 0 && j != 0)
            solver->addClause(-treeLeft[z][i - 1], -treeRight[z][j - 1],
                              treeOut[z][i + j - 1]);
          if (i != 0 || j != 0)
            nClauses++;
        }

        if (boundType == BOUND_LB || boundType == BOUND_BOTH) {
          if (i == treeLeft[z].size() && j != treeRight[z].size())
            solver->addClause(treeRight[z][j], -treeOut[z][i + j]);
          else if (j == treeRight[z].size() && i != treeLeft[z].size())
            solver->addClause(treeLeft[z][i], -treeOut[z][i + j]);
          else if (i != treeLeft[z].size() && j != treeRight[z].size())
            solver->addClause(treeLeft[z][i], treeRight[z][j],
                              -treeOut[z][i + j]);
          if (i != treeLeft[z].size() || j != treeRight[z].size())
            nClauses++;
        }
      }
    }
    treeUpper[z] = upper;
  }
}

void Totalizer::_enforceUB(uint64_t b, std::vector<lit_t> &assumps) {
  if (inLits.size() == 0) {
    // Build totalizer in the first place
    build(extension, b);
  } else if (extension.size()) {
    // Build new tot from additional lits
    Totalizer tot2(solver, boundType);
    tot2.build(extension, b);
    join(tot2);
  }
  updateUpper(b);

  extension.clear();

  assumps.push_back(-outLits[b]);
}

void Totalizer::_enforceLB(uint64_t b, std::vector<lit_t> &assumps) {
  if (inLits.size() == 0) {
    // Build totalizer in the first place
    build(extension, b);
  } else if (extension.size()) {
    // Build new tot from additional lits
    Totalizer tot2(solver, boundType);
    tot2.build(extension, b);
    join(tot2);
  }
  updateUpper(b);

  extension.clear();

  assumps.push_back(outLits[b]);
}

void Totalizer::printStats(bool header) const {
  if (header) {
    INFO << "\n";
    INFO << "\n";
    INFO << "Totalizer Stats\n";
    INFO << LOG_H1;
    INFO << "\n";
  }
  INFO << "#input lits = " << getNLits() << "\n";
  INFO << "upper = " << getUpper() << "\n";
  INFO << "#clauses = " << getNClauses() << "\n";
  INFO << "#vars = " << getNVars() << "\n";
  INFO << "depth = " << getDepth() << "\n";
  INFO << "balance measure = " << getBalanceMeasure() << "\n";
}

uint64_t Totalizer::minNeededVars(uint64_t nIn) const {
  if (nIn == 1)
    return 0;
  uint64_t left = nIn / 2;
  uint64_t right = nIn / 2 + (nIn % 2 ? 1 : 0);
  return minNeededVars(left) + minNeededVars(right) + nIn;
}
} // namespace BiOptSat